# Continous integration

[![pipeline status](https://gitlab.inria.fr/tlaine/test-kombo-server/badges/master/pipeline.svg)](https://gitlab.inria.fr/tlaine/test-kombo-server/commits/master)
[![Quality Gate](https://sonarqube.inria.fr/sonarqube/api/badges/gate?key=thibaultlaine:test-kombo-server)](https://sonarqube.inria.fr/sonarqube/dashboard/index/thibaultlaine:test-kombo-server)
[![Coverage](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=thibaultlaine:test-kombo-server&metric=coverage)](https://sonarqube.inria.fr/sonarqube/dashboard/index/thibaultlaine:test-kombo-server)

[![Bugs](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=thibaultlaine:test-kombo-server&metric=bugs)](https://sonarqube.inria.fr/sonarqube/dashboard/index/thibaultlaine:test-kombo-server)
[![Vulnerabilities](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=thibaultlaine:test-kombo-server&metric=vulnerabilities)](https://sonarqube.inria.fr/sonarqube/dashboard/index/thibaultlaine:test-kombo-server)
[![Code smells](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=thibaultlaine:test-kombo-server&metric=code_smells)](https://sonarqube.inria.fr/sonarqube/dashboard/index/thibaultlaine:test-kombo-server)

[![Line of code](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=thibaultlaine:test-kombo-server&metric=ncloc)](https://sonarqube.inria.fr/sonarqube/dashboard/index/thibaultlaine:test-kombo-server)
[![Comment ratio](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=thibaultlaine:test-kombo-server&metric=comment_lines_density)](https://sonarqube.inria.fr/sonarqube/dashboard/index/thibaultlaine:test-kombo-server)

# Links
- Sonarqube : https://sonarqube.inria.fr/sonarqube/dashboard?id=thibaultlaine%3Atest-kombo-server
- Socumentation : https://tlaine.gitlabpages.inria.fr/test-kombo-server/

# Informations

test-kombo-server