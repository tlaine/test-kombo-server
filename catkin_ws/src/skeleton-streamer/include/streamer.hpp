#ifndef STREAMER_HPP_INCLUDED
#define STREAMER_HPP_INCLUDED


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>

#include <iostream>
#include <string>
#include <sstream>



class Streamer
{
    public:
        /// Default
        Streamer(const int& port = 5013);
        ~Streamer();
        //

        /// Communication functions
        bool sendMessageTo(const uint8_t* msg, const unsigned short& msgSize, const char* destination, const char* port);
        //

        /// Set / get functions
        bool getIpFromHostname(const std::string& hostname, std::string* ip, const int& lastState = 0) const;
        unsigned long getErrorsCount() const;
        //

    protected:
        /// Attributes
        int udpsocket;
        unsigned int timestamp;
        unsigned long errors;
        //


        /// Protected functions
        int resolveDestination(const char* destination, int family, const char* port, sockaddr_storage* pAddr);

        //
};

#endif // STREAMER_HPP_INCLUDED
