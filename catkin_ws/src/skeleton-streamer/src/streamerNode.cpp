#include "ros/ros.h"
#include "streamer.hpp"

#include <fstream>
#include <streambuf>
#include <string>
#include <sstream>

#include <jansson.h>

#define LOOP_RATE 30
#define DISPERSION 0.01


std::string getNoisy(const std::string& s);

namespace std
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}


// node process
int main(int argc, char **argv)
{
    ros::init(argc, argv, "skeletonStreamerNode");
    ros::NodeHandle n;
    ros::Rate loop_rate(LOOP_RATE);
    
    Streamer skeletonStreamer;
    std::ifstream opt("/home/thibault/Documents/GitHub/test-kombo-server/catkin_ws/src/skeleton-streamer/optitrack.json");
    std::string optitrackFile((std::istreambuf_iterator<char>(opt)), std::istreambuf_iterator<char>());
    std::ifstream knt("/home/thibault/Documents/GitHub/test-kombo-server/catkin_ws/src/skeleton-streamer/kinect.json");
    std::string kinectFile((std::istreambuf_iterator<char>(knt)), std::istreambuf_iterator<char>());
    
    while (ros::ok())
    {
        std::string msg1 = getNoisy(optitrackFile);
        std::string msg2 = getNoisy(kinectFile);
        skeletonStreamer.sendMessageTo((const uint8_t*)msg1.c_str(), msg1.size(), "127.0.0.1", "49197"); // optitrack
        skeletonStreamer.sendMessageTo((const uint8_t*)msg2.c_str(), msg2.size(), "127.0.0.1", "49195"); // kinect
        
        //std::cout<<msg2<<std::endl;
        
        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}


std::string getNoisy(const std::string& s)
{
    std::string noisy = "{\n";
    json_error_t error;
	json_t* root = json_loads(s.c_str(), 0, &error);
	
    const char* skeletonName;
	json_t* value_message_content;
	json_object_foreach(root, skeletonName, value_message_content)
	{
	    noisy += "\t\"" + std::string(skeletonName) + "\":\n\t{\n";
	    
	    const char* key_joint_name;
		json_t* value_joint_properties;
		json_object_foreach(value_message_content, key_joint_name, value_joint_properties)
		{
		    if(strcmp(key_joint_name, "idSensor") == 0)
		    {
		        noisy += "\t\t\"idSensor\":" + std::to_string(json_integer_value(value_joint_properties)) + ",\n";
		    }
		    else
		    {
		        noisy += "\t\t\"" + std::string(key_joint_name) + "\":{ ";
		        const char* key_property_name;
			    json_t* value_property;
			    json_object_foreach(value_joint_properties, key_property_name, value_property)
			    {
			        if(strcmp(key_property_name, "position") == 0)
			        {
			            double x = json_real_value(json_object_get(value_property, "x")) + DISPERSION*(0.01*((rand()%200)-100));
					    double y = json_real_value(json_object_get(value_property, "y")) + DISPERSION*(0.01*((rand()%200)-100));
					    double z = json_real_value(json_object_get(value_property, "z")) + DISPERSION*(0.01*((rand()%200)-100));
					
			            noisy += "\"position\":{\"x\":" + std::to_string(x) + ", \"y\":" + std::to_string(y) + ", \"z\":" + std::to_string(z) + "}, ";
			        }
			        else
			        {
			            noisy += "\"" + std::string(key_property_name) + "\":" + std::to_string(json_integer_value(value_property)) + ", ";
			        }
			    }
			    noisy.erase(noisy.size()-2,1); // delete end comma				
		        noisy += " },\n";   		    
		    }

		}
	    noisy.erase(noisy.size()-2,1); // delete end comma
	    noisy += "\t},\n";
	}
	noisy.erase(noisy.size()-2,1); // delete end comma
	noisy += "}";
    return noisy;
}

