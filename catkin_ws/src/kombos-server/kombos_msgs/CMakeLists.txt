##########
## Init ##
##########

# Package configuration
cmake_minimum_required(VERSION 2.8.3)
project(kombos_msgs)

# Flags
set(CMAKE_CXX_FLAGS "-std=c++11 ${CMAKE_CXX_FLAGS}")

# Internal packages
find_package(catkin REQUIRED COMPONENTS
	roscpp
	rospy
	std_msgs
	visualization_msgs
	sensor_msgs
	message_generation
)

###########
## Build ##
###########

# Add files
add_message_files(
	FILES
	rulaArray.msg
	indice.msg
	indiceSon.msg
	sound.msg
	motion.msg
	control_server_cmd.msg
	mesureKinect.msg
	mesureKalman.msg
	kalmanState.msg
	position_msg.msg
	orientation_msg.msg
	quaternion_msg.msg
	segments.msg
	segment_nb.msg
	khi2.msg
	PostureClass.msg
	matrix.msg
)

# Build files
generate_messages(
	DEPENDENCIES
	std_msgs
	visualization_msgs
	sensor_msgs
	geometry_msgs
)

############
## Config ##
############

# Catkin packages
catkin_package(
	CATKIN_DEPENDS message_runtime std_msgs
)

# Catkin include
include_directories(
	${catkin_INCLUDE_DIRS}
)
