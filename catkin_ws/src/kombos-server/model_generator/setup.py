#!/usr/bin/env python
from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup
d = generate_distutils_setup()
d['packages'] = ['model_generator',
                 'model_generator.unity_bridge',
                 'model_generator.posture_classification',
                 'model_generator.models',
                 'model_generator.tools']
d['package_dir'] = {'': 'src'}
setup(**d)
