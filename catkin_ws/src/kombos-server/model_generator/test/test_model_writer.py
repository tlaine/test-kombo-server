#!/usr/bin/env python
from model_generator.model_writer import ModelWriter
from model_generator.model_writer import URDFModelWriter
from model_generator.anthropometric_scaling import AnthropometricScaling
import unittest
from os.path import join, exists
from os import makedirs
from shutil import rmtree


class TestModelWriter(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestModelWriter, self).__init__(*args, **kwargs)
        self.output_dir = join("/tmp", "test_model_writer")

    def setUp(self):
        makedirs(self.output_dir)

    def tearDown(self):
        rmtree(self.output_dir)

    @unittest.expectedFailure
    def test_factory_fail(self):
        ModelWriter.factory("NotImplemented")

    def test_factory_success(self):
        writer = ModelWriter.factory("URDF")
        self.assertTrue(isinstance(writer, URDFModelWriter))

    def test_write_urdf_model(self):
        writer = ModelWriter.factory("URDF")
        # get a model from anthropometric scaling
        ant_scale = AnthropometricScaling()
        lenghts = ant_scale.scale("male", 180)
        # set the output folder
        writer.output_dir = self.output_dir
        filename = "test"
        self.assertFalse(exists(join(writer.output_dir, filename + ".urdf")))
        writer.write_model(lenghts, filename)
        # check that output file exist
        self.assertTrue(exists(join(writer.output_dir, filename + ".urdf")))


if __name__ == '__main__':
    unittest.main()