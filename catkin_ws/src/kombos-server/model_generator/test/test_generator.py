#!/usr/bin/env python
from model_generator.generator import Generator
import unittest
from os.path import exists, join
from os import makedirs
from shutil import rmtree


class TestGenerator(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestGenerator, self).__init__(*args, **kwargs)
        self.output_dir = join("/tmp", "test_model_generator")
        self.gen = Generator()
        self.gen.writer.output_dir = self.output_dir

    def setUp(self):
        makedirs(self.output_dir)

    def tearDown(self):
        rmtree(self.output_dir)

    def test_generate_model(self):
        filename = "test_model"
        gender = "male"
        self.assertFalse(exists(join(self.output_dir, filename + ".urdf")))
        # write the model
        model = self.gen.generate_model(gender, 180, filename)
        self.assertTrue(exists(join(self.output_dir, filename + ".urdf")))

    def test_generate_random_height(self):
        gender = "male"
        height = self.gen.generate_random_height(gender)
        self.assertTrue(height > self.gen.bounds_values["5"][gender])
        self.assertTrue(height < self.gen.bounds_values["95"][gender])

    def test_generate_random_model(self):
        filename = "test_random_model"
        gender = "male"
        self.assertFalse(exists(join(self.output_dir, filename + ".urdf")))
        # write the model
        model = self.gen.generate_random_model(gender, filename)
        self.assertTrue(exists(join(self.output_dir, filename + ".urdf")))


if __name__ == '__main__':
    unittest.main()