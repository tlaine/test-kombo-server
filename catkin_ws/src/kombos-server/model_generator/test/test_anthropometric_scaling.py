#!/usr/bin/env python
from model_generator.anthropometric_scaling import AnthropometricScaling
import rospy
import unittest


class TestAnthropometricScaling(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestAnthropometricScaling, self).__init__(*args, **kwargs)
        self.ant_scale = AnthropometricScaling("nasa")

    def test_closest_percentile(self):
        # test closest percentile given nasa anthropometric references
        self.assertEqual(self.ant_scale.closest_percentile("female", 145), "5")
        self.assertEqual(self.ant_scale.closest_percentile("female", 149), "5")
        self.assertEqual(self.ant_scale.closest_percentile("female", 155), "50")
        self.assertEqual(self.ant_scale.closest_percentile("female", 163), "95")
        self.assertEqual(self.ant_scale.closest_percentile("female", 170), "95")

    @unittest.expectedFailure
    def test_scaling_switch_failure(self):
        self.assertTrue(isinstance(self.ant_scale.scale("female", 165, "not_implemented"),dict))

    def test_scaling_switch(self):
        self.assertTrue(isinstance(self.ant_scale.scale("female", 165),dict))

    def test_uniform_scaling(self):
        test_height = 162.5
        scaled_legnths = self.ant_scale.scale("female", test_height)
        self.assertAlmostEqual(scaled_legnths["stature"], test_height)
        self.assertAlmostEqual(scaled_legnths["shin"], (42.7 * test_height / 165.1))


if __name__ == '__main__':
    rospy.init_node("test_anthropometric_scaling")
    unittest.main()