#!/usr/bin/env python
import json
from os.path import join
import rospy
import rospkg


class AnthropometricScaling(object):
    """Definition of the AnthropometricScaling class

    This class is used to scale a model from anthropometric references
    """
    def __init__(self, anthropometric_ref="nasa"):
        # initialize config folder
        r = rospkg.RosPack()
        self.conf_dir = join(r.get_path("model_generator"), "config", "anthropometrics")
        # read the anthropometric measurement from database/config file
        self._ant_ref_data = {}
        self.read_anthropometry(anthropometric_ref)

    def read_anthropometry(self, filename):
        """Read the antrhopometric dataset and set the reference values.

        Keyword arguments:
        filename: Filename of the desired antrhopometric dataset

        Set arguments:
        ant_ref_data -- Dict containing all anthropometric references
        """
        ant_file = join(self.conf_dir, filename + ".json")
        with open(ant_file) as datafile:
            self._ant_ref_data = json.load(datafile)

    def closest_percentile(self, gender, height):
        """Perform a simple distance checking and select the closest percentile

        Keyword arguments:
        gender -- Desired gender of the scaled model
        height -- Desired height of the scaled model

        Return:
        close_perc -- The closest percentile of the references ("5", "50" or "95")
        """

        min_dist = 1000
        close_perc = "0"
        for key, value in self._ant_ref_data["stature"].iteritems():
            dist = abs(height - value[gender])
            if dist < min_dist:
                min_dist = dist
                close_perc = key
        return close_perc

    def scale(self, gender, height, method="uniform"):
        """Scale the model using a specified method

        Keyword arguments:
        gender -- Desired gender of the scaled model
        height -- Desired height of the scaled model
        method -- Desired method to use for the scaling ("uniform")

        Return:
        scaled_lengths -- Dict of the scaled model segment lengths

        Exceptions:

        """
        close_perc = self.closest_percentile(gender, height)
        if method == "uniform":
            return self._uniform_scaling(gender, height, close_perc)
        else:
            return 0

    def _uniform_scaling(self, gender, height, close_perc):
        """Scale the model using the uniform method
        Scaling factor is calculated using the formula:
        s = height / ref_height

        Keyword arguments:
        gender -- Desired gender of the scaled model
        height -- Desired height of the scaled model
        close_perc -- Closest percentile of the reference model

        Return:
        scaled_lengths -- Dict of the scaled model segment lengths
        """
        scaled_lengths = {}
        # calculate the scaling factor based on the height of the reference
        ref_height = self._ant_ref_data["stature"][close_perc][gender]
        scale_factor = height / ref_height

        for key, value in self._ant_ref_data.iteritems():
            # only scale body segments not stature
            if key != "stature":
                # multiply the reference height with the scale factor
                ref_lenght = value[close_perc][gender]
                scaled_lengths[key] = ref_lenght * scale_factor
        # add the stature value and the gender
        scaled_lengths["stature"] = height
        scaled_lengths["gender"] = gender
        return scaled_lengths

    def get_reference_heights(self):
        """Extract the reference heights from the anthropometric file
        for both genders

        Return arguments:
        heights -- Dictionnay of heights at 5th, 50th and 90th percentile
        for both genders
        """
        return self._ant_ref_data["stature"]

