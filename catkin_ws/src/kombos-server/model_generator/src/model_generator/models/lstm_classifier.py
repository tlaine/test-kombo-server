#!/usr/bin/env python
import torch
import torch.nn as nn
import torch.optim as optim
from model_generator.models.classifier import Classifier
# from model_generator.models.bn_lstm import LSTM
# from model_generator.models.bn_lstm import LSTMCell
# from model_generator.models.bn_lstm import BNLSTMCell


class LSTMClassifier(Classifier):
    """Class representing an LSTM based classifier"""
    def __init__(self, input_size, hidden_size, output_size, num_layers=1, normalize=False, chunk_size=10, epochs=2, save_folder=None, use_gpu=False):
        super(LSTMClassifier, self).__init__("lstm", epochs, save_folder, use_gpu)
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.num_layers = num_layers
        self.chunk_size = chunk_size
        self.lstm = nn.LSTM(input_size, hidden_size, num_layers=num_layers)
        self.hidden2label = nn.Linear(hidden_size, output_size)

        self.criterion = nn.CrossEntropyLoss()
        self.optimizer = optim.SGD(self.parameters(), lr=0.1, momentum=0.9)
        self.max_top = 3 if output_size > 3 else 1

    def _init_hidden(self, data):
        """
        Initialize the hidden layer of the LSTM network

        Keywoard arguments:
        data -- Input data that serves the hidden layer creation (keep the same dtype and shape)

        Return arguments:
        (h0, c0) -- Tuple representing the hidden layer of the network
        """
        h0 = data.new_zeros(self.num_layers, data.size(0), self.hidden_size, dtype=torch.float)
        c0 = data.new_zeros(self.num_layers, data.size(0), self.hidden_size, dtype=torch.float)
        return (h0, c0)

    def forward(self, input):
        """
        Apply the forward method of the neural network to the input data

        Keywoard arguments:
        input -- The single vector of features in entry of the network

        Return arguments:
        output -- Value returned by the last layer of the network
        """
        input.transpose_(0, 1)
        _, self.hidden = self.lstm(input, self.hidden)
        output = self.hidden2label(self.hidden[0][-1])
        return output

    def predict(self, inputs):
        """
        Predict the label given input variable using a forward pass and get the
        largest index

        Keywoard arguments:
        inputs -- The set of features to predict

        Return:
        predicted -- The set of predicted categories
        """
        # initialize hiddent state
        with torch.no_grad():
            self.hidden = self._init_hidden(inputs)
            # predict
            output = self.forward(inputs)
            # extract prediction by taking the max of the predicted vector
            predicted_labels = []
            predicted_probs = []
            for i in range(len(inputs[0])):
                top_prob, top_label = output[i].topk(self.max_top)
                predicted_labels.append([int(x) for x in top_label])
                predicted_probs.append([float(x) for x in top_prob])
        return predicted_labels, predicted_probs

    def fit(self, train_set, test_set=None):
        """
        Fit the network to the train set in input

        Keywoard arguments:
        train_set -- Tuple containing the features to train on
        and their corresponding labels
        test_set -- Optional, tuple containing the features to evaluate
        the learned model on and their corresponding labels
        """
        self.cumulative_loss = []
        self.cumulative_eval = []
        self.cumulative_iter = []
        iteration = 0
        try:
            for e in range(self.epochs):
                try:
                    for batch_idx, (data, target) in enumerate(train_set):
                        self.train(True)
                        data, target = data.to(self.device), target.to(self.device)
                        # zero the parameter gradients
                        self.hidden = self._init_hidden(data)
                        # truncated backprogation
                        input_parts = torch.split(data, self.chunk_size, dim=1)
                        for input_part in input_parts:
                            self.optimizer.zero_grad()
                            self.hidden[0].detach()
                            self.hidden[1].detach()
                            output = self.forward(input_part)
                            loss = self.criterion(output, target)
                            loss.backward(retain_graph=True)
                            loss = loss.item()
                            self.optimizer.step()
                            # self.scheduler.step(loss)

                        if iteration % self.print_every == 0:
                            self.train(False)
                            self._score(iteration, loss, test_set)
                        iteration += 1
                except OSError:
                    pass
        except KeyboardInterrupt:
            print("Learning process interrupted")
        # save results to file
        self._save_results()
        self._save_best_model()
