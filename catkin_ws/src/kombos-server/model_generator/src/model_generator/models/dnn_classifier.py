from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from model_generator.models.classifier import Classifier
import torch


class DNNClassifier(Classifier):
    def __init__(self, input_size, hidden_size, output_size, epochs=2, save_folder=None):
        """
        In the constructor we instantiate two nn.Linear modules and assign them as
        member variables.
        """
        super(DNNClassifier, self).__init__("dnn", epochs, save_folder)

        self.input_layer = nn.Linear(input_size, hidden_size)
        self.hidden_layer = nn.Linear(hidden_size, hidden_size)
        self.output = nn.Linear(hidden_size, output_size)
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.output_size = output_size

        self.criterion = nn.CrossEntropyLoss()
        self.optimizer = optim.Adam(self.parameters(), lr=0.005)

        self.print_every = 100

    def forward(self, input):
        """
        Apply the forward method of the neural network to the input data

        Keywoard arguments:
        input -- The single vector of features in entry of the network

        Return arguments:
        output -- Value returned by the last layer of the network
        """
        input = input.view(len(input), len(input[0]) * len(input[0][0]))
        h1 = F.relu(self.input_layer(input))
        h2 = F.relu(self.hidden_layer(h1))
        x = self.output(h2)
        return x

    def predict(self, inputs):
        """
        Predict the label given input variable using a forward pass and get the
        largest index

        Keywoard arguments:
        inputs -- The set of features to predict

        Return:
        predicted -- The set of predicted categories
        """
        inputs = Variable(inputs, volatile=True)
        output = self.forward(inputs)
        predicted = torch.max(output.data, 1)[1]
        return [predicted], [1]

    def fit(self, train_set, test_set=None):
        """
        Fit the network to the train set in input

        Keywoard arguments:
        train_set -- Tuple containing the features to train on
        and their corresponding labels
        test_set -- Optional, tuple containing the features to evaluate
        the learned model on and their corresponding labels
        """
        self.cumulative_loss = []
        self.cumulative_eval = []
        self.cumulative_iter = []
        iteration = 0
        try:
            for e in range(self.epochs):
                try:
                    for batch_idx, (data, target) in enumerate(train_set):
                        data, target = Variable(data), Variable(target)
                        self.optimizer.zero_grad()
                        output = self.forward(data)
                        target = target.view(target.numel())
                        loss = self.criterion(output, target)
                        loss.backward()
                        loss = loss.data[0]
                        self.optimizer.step()
                        if iteration % self.print_every == 0:
                            self._score(iteration, loss, test_set)
                        iteration += 1
                except OSError:
                    pass
        except KeyboardInterrupt:
            print("Learning process interrupted")
        # save results to file
        self._save_results()
        self._save_best_model()
