import torch
import torch.nn
import abc
from os.path import join
from os.path import isdir
from os.path import exists
from os import makedirs
from os import rename
import json
from numpy import argmax
from numpy import argmin


class Network(torch.nn.Module):
    """Abstract Class to represent a generic classifier"""
    def __init__(self, network_type, epochs=2, save_folder=None, use_gpu=False):
        super(Network, self).__init__()
        __metaclass__ = abc.ABCMeta
        self.network_type = network_type
        self.epochs = epochs
        self.print_every = 100
        self.save_folder = save_folder
        self.input_size = -1
        self.hidden_size = -1
        self.output_size = -1
        self.device = torch.device("cuda" if use_gpu else "cpu")
        self.to(self.device)

        self.cumulative_loss = []
        self.cumulative_eval = []
        self.cumulative_iter = []

    @abc.abstractmethod
    def forward(self, input):
        """
        Apply the forward method of the neural network to the input data

        Keywoard arguments:
        input -- The single vector of features in entry of the network

        Return arguments:
        output -- Value returned by the last layer of the network
        """
        raise NotImplementedError()
        return 0

    @abc.abstractmethod
    def fit(self, train_set, test_set=None):
        """
        Fit the network to the train set in input

        Keywoard arguments:
        train_set -- Tuple containing the features to train on
        and their corresponding labels
        test_set -- Optional, tuple containing the features to evaluate
        the learned model on and their corresponding labels
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def evaluate(self, test_set, full_eval=False):
        """
        Evaluate the network on the inputs and their corresponding targets

        Keywoard arguments:
        test_set -- Tuple containing the features to evaluate
        the learned model on and their corresponding labels
        full_eval -- If True, calculate the success rate over the full
        test dataset. Otherwise, only calculate it on the first batch

        Return:
        success_rate -- Pourcentage of correct predictions
        """
        return NotImplementedError()

    def _save_results(self):
        """
        Save the learning curve in a json file
        """
        if self.save_folder is not None:
            if not isdir(self.save_folder):
                makedirs(self.save_folder)
            filename = (self.network_type + "_" +
                        str(self.epochs) + "_epochs.json")
            save_data = {}
            save_data["loss"] = self.cumulative_loss
            save_data["evaluation"] = self.cumulative_eval
            save_data["iterations"] = self.cumulative_iter
            with open(join(self.save_folder, filename), 'w') as savefile:
                json.dump(save_data, savefile)

    def _temporary_save(self, iteration):
        tmp_save_folder = join("/tmp", self.network_type)
        if not isdir(tmp_save_folder):
            makedirs(tmp_save_folder)
        self.save(tmp_save_folder, str(iteration))

    def _save_best_model(self):
        if self.save_folder is not None:
            if not isdir(self.save_folder):
                makedirs(self.save_folder)
            result_file = join(self.save_folder, self.network_type + "_" + str(self.epochs) + "_epochs.json")
            if exists(result_file):
                with open(result_file) as datafile:
                    result_data = json.load(datafile)
                if result_data["evaluation"]:
                    best_index = argmax(result_data["evaluation"])
                else:
                    best_index = argmin(result_data["loss"])
                # copy the model file
                best_model = join("/tmp", self.network_type, str(result_data["iterations"][best_index]) + ".pt")
                new_model_dest = join(self.save_folder, self.network_type + "_" + str(self.epochs) + "_epochs.pt")
                rename(best_model, new_model_dest)
                # copy the parameter file
                best_param = join("/tmp", self.network_type, str(result_data["iterations"][best_index]) + "_parameters.json")
                new_param_dest = join(self.save_folder, self.network_type + "_" + str(self.epochs) + "_epochs_parameters.json")
                rename(best_param, new_param_dest)

    def save(self, folder, filename):
        """
        Save the learned model in a file and the training parameters in a json
        """
        torch.save(self.state_dict(), join(folder, filename + ".pt"))
        parameters = {}
        parameters["input_size"] = self.input_size
        parameters["hidden_size"] = self.hidden_size
        parameters["output_size"] = self.output_size
        with open(join(folder, filename + "_parameters.json"), 'w') as outfile:
            json.dump(parameters, outfile)

    def load(self, filepath):
        if not exists(filepath):
            print("File {0:s} does not exist".format(filepath))
        self.load_state_dict(torch.load(filepath))

    def _score(self, iteration, loss, test_set=None):
        """
        Score the learned model and print learning progress

        Keywoard arguments:
        iteration -- Current iteration
        loss -- Cumulative loss since last scoring
        test_set -- Optional, tuple containing the evaluation set
        """
        print('%d %.4f' % (iteration, loss))
        self.cumulative_iter.append(iteration)
        self.cumulative_loss.append(float(loss))
        if test_set is not None:
            evaluation = self.evaluate(test_set)
            print('success rate: %f' % (evaluation))
            self.cumulative_eval.append(float(evaluation))
        self._temporary_save(iteration)
