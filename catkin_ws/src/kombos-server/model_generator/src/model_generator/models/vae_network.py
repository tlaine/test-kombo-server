import torch
from torch.autograd import Variable
import numpy as np
import torch.nn.functional as F
import torch.optim as optim
from torch import nn
from mssa_learning.models.network import Network


class Normal(object):
    def __init__(self, mu, sigma, log_sigma, v=None, r=None):
        self.mu = mu
        self.sigma = sigma  # either stdev diagonal itself, or stdev diagonal from decomposition
        self.logsigma = log_sigma
        dim = mu.get_shape()
        if v is None:
            v = torch.FloatTensor(*dim)
        if r is None:
            r = torch.FloatTensor(*dim)
        self.v = v
        self.r = r


class Encoder(Network):
    def __init__(self, input_size, hidden_size, output_size):
        super(Encoder, self).__init__("encoder")
        self.linear1 = torch.nn.Linear(input_size, hidden_size)
        self.linear2 = torch.nn.Linear(hidden_size, output_size)

    def forward(self, input):
        """
        Apply the forward method of the neural network to the input data

        Keywoard arguments:
        input -- The single vector of features in entry of the network

        Return arguments:
        output -- Value returned by the last layer of the network
        """
        x = F.relu(self.linear1(input))
        return F.relu(self.linear2(x))

    def fit(self, train_set, test_set=None):
        """
        Fit the network to the train set in input

        Keywoard arguments:
        train_set -- Tuple containing the features to train on
        and their corresponding labels
        test_set -- Optional, tuple containing the features to evaluate
        the learned model on and their corresponding labels
        """
        raise NotImplementedError()


class Decoder(Network):
    def __init__(self, input_size, hidden_size, output_size):
        super(Decoder, self).__init__("decoder")
        self.linear1 = torch.nn.Linear(input_size, hidden_size)
        self.linear2 = torch.nn.Linear(hidden_size, output_size)

    def forward(self, input):
        """
        Apply the forward method of the neural network to the input data

        Keywoard arguments:
        input -- The single vector of features in entry of the network

        Return arguments:
        output -- Value returned by the last layer of the network
        """
        x = F.relu(self.linear1(input))
        return F.relu(self.linear2(x))

    def fit(self, train_set, test_set=None):
        """
        Fit the network to the train set in input

        Keywoard arguments:
        train_set -- Tuple containing the features to train on
        and their corresponding labels
        test_set -- Optional, tuple containing the features to evaluate
        the learned model on and their corresponding labels
        """
        raise NotImplementedError()


class VAE(Network):
    def __init__(self, input_size, hidden_size, latent_size, epochs=2, save_folder=None):
        super(VAE, self).__init__("vae", epochs, save_folder)
        self.encoder = Encoder(input_size, hidden_size, hidden_size)
        self.decoder = Decoder(latent_size, hidden_size, input_size)
        self._enc_mu = torch.nn.Linear(hidden_size, latent_size)
        self._enc_log_sigma = torch.nn.Linear(hidden_size, latent_size)
        self.criterion = nn.MSELoss()
        self.optimizer = optim.Adam(self.parameters(), lr=0.001)
        self.z_mean = 0
        self.z_sigma = 1
        self.print_every = 500

    def _latent_loss(self):
        mean_sq = self.z_mean * self.z_mean
        stddev_sq = self.z_sigma * self.z_sigma
        return 0.5 * torch.sum(mean_sq + stddev_sq - torch.log(stddev_sq) - 1)

    def _sample_latent(self, h_enc):
        """
        Return the latent normal sample z ~ N(mu, sigma^2)
        """
        mu = self._enc_mu(h_enc)
        log_sigma = self._enc_log_sigma(h_enc)
        sigma = torch.exp(log_sigma)
        std_z = torch.from_numpy(np.random.normal(0, 1, size=sigma.size())).float()
        self.z_mean = mu
        self.z_sigma = sigma
        return mu + sigma * Variable(std_z, requires_grad=False)  # Reparameterization trick

    def forward(self, input):
        """
        Apply the forward method of the neural network to the input data

        Keywoard arguments:
        input -- The single vector of features in entry of the network

        Return arguments:
        output -- Value returned by the last layer of the network
        """
        input = input.view(len(input), len(input[0][0]))
        h_enc = self.encoder(input)
        z = self._sample_latent(h_enc)
        return self.decoder(z)

    def evaluate(self, test_set, full_eval=False):
        """
        Evaluate the network on the inputs and their corresponding targets

        Keywoard arguments:
        test_set -- Tuple containing the features to evaluate
        the learned model on and their corresponding labels
        full_eval -- If True, calculate the success rate over the full
        test dataset. Otherwise, only calculate it on the first batch

        Return:
        success_rate -- Pourcentage of correct predictions
        """
        nb_elem = 0
        avg_dist = 0
        if full_eval:
            for (data, _) in test_set:
                data = Variable(data, volatile=True)
                predicted = self.forward(data)
                data = data.view(len(data), len(data[0][0]))
                dist = torch.pow(predicted - data, 2).sum(1)
                avg_dist += dist.sum(0)
                nb_elem += len(data)
        else:
            (data, _) = next(iter(test_set))
            data = Variable(data, volatile=True)
            predicted = self.forward(data)
            data = data.view(len(data), len(data[0][0]))
            dist = torch.pow(predicted - data, 2).sum(1)
            avg_dist += dist.sum(0)
            nb_elem += len(data)
        success_rate = avg_dist / float(nb_elem)
        return float(success_rate.data.numpy())

    def fit(self, train_set, test_set=None):
        """
        Fit the network to the train set in input

        Keywoard arguments:
        train_set -- Tuple containing the features to train on
        and their corresponding labels
        test_set -- Optional, tuple containing the features to evaluate
        the learned model on and their corresponding labels
        """
        self.cumulative_loss = []
        self.cumulative_eval = []
        iteration = 0
        try:
            for epoch in range(self.epochs):
                try:
                    for batch_idx, (data, _) in enumerate(train_set):
                        data = Variable(data)
                        self.optimizer.zero_grad()
                        output = self.forward(data)
                        data = data.view(len(data), len(data[0][0]))
                        loss = self._latent_loss()
                        loss = self.criterion(output, data) + loss
                        loss.backward()
                        loss = loss.data[0]
                        self.optimizer.step()
                        if iteration % self.print_every == 0:
                            self._score(iteration, loss, test_set)
                        iteration += 1
                except OSError:
                    pass
        except KeyboardInterrupt:
            print("Learning process interrupted")
        # save results to file
        self._save_results()
        self._save_best_model()
