import abc
from sklearn.metrics import confusion_matrix
from model_generator.models.network import Network
from progressbar import ProgressBar
import numpy as np
import torch


class Classifier(Network):
    """Abstract Class to represent a generic classifier"""
    def __init__(self, network_type, epochs=2, save_folder=None, use_gpu=False):
        super(Classifier, self).__init__(network_type, epochs, save_folder, use_gpu)
        __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def forward(self, input):
        """
        Apply the forward method of the neural network to the input data

        Keywoard arguments:
        input -- The single vector of features in entry of the network

        Return arguments:
        output -- Value returned by the last layer of the network
        """
        raise NotImplementedError()
        return 0

    @abc.abstractmethod
    def predict(self, inputs):
        """
        Predict the label given input variable using a forward pass and get the
        largest index

        Keywoard arguments:
        inputs -- The set of features to predict

        Return:
        predicted -- The set of predicted categories
        """
        raise NotImplementedError()
        return 0

    @abc.abstractmethod
    def fit(self, train_set, test_set=None):
        """
        Fit the network to the train set in input

        Keywoard arguments:
        train_set -- Tuple containing the features to train on
        and their corresponding labels
        test_set -- Optional, tuple containing the features to evaluate
        the learned model on and their corresponding labels
        """
        raise NotImplementedError()

    def evaluate(self, test_set, full_eval=False):
        """
        Evaluate the network on the inputs and their corresponding targets

        Keywoard arguments:
        test_set -- Tuple containing the features to evaluate
        the learned model on and their corresponding labels
        full_eval -- If True, calculate the success rate over the full
        test dataset. Otherwise, only calculate it on the first batch

        Return:
        success_rate -- Pourcentage of correct predictions
        """
        accuracy = []
        with torch.no_grad():
            if full_eval:
                for (data, target) in test_set:
                    data, target = data.to(self.device), target.to(self.device)
                    predicted = np.array(self.predict(data)[0])
                    compared = [int(predicted[:, 0][i] == target.numpy()[i]) for i in range(len(target))]
                    accuracy.append(np.mean(compared))
            else:
                (data, target) = next(iter(test_set))
                data, target = data.to(self.device), target.to(self.device)
                predicted = np.array(self.predict(data)[0])
                compared = [int(predicted[:, 0][i] == target.numpy()[i]) for i in range(len(target))]
                accuracy.append(np.mean(compared))
            success_rate = np.mean(accuracy)
            return success_rate

    def calculate_confusion_matrix(self, test_set):
        y_pred = []
        y_test = []
        bar = ProgressBar()
        for (data, target) in bar(test_set):
            predicted = self.predict(data)
            for i, p in enumerate(predicted):
                y_pred.append(p[0])
                y_test.append(int(target[i]))
        return confusion_matrix(y_test, y_pred)
