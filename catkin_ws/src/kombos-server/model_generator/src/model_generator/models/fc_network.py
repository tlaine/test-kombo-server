import torch
import torch.optim as optim
from torch import nn
from mssa_learning.models.network import Network


class FCNetwork(Network):
    def __init__(self, input_size, hidden_size, output_size, num_layers=1, epochs=2, save_folder=None):
        super(FCNetwork, self).__init__("fc_network", epochs, save_folder)

        self.input_layer = torch.nn.Linear(input_size, hidden_size)
        self.output_layer = torch.nn.Linear(hidden_size, output_size)
        self.hidden_layers = [torch.nn.Linear(hidden_size, hidden_size) for _ in range(num_layers)]

        self.criterion = nn.MSELoss()
        self.optimizer = optim.Adam(self.parameters(), lr=0.001)
        self.print_every = 500

    def forward(self, input):
        """
        Apply the forward method of the neural network to the input data

        Keywoard arguments:
        input -- The single vector of features in entry of the network

        Return arguments:
        output -- Value returned by the last layer of the network
        """
        x = self.input_layer(input)
        for l in self.hidden_layers:
            x = l(x)
        return self.output_layer(x)

    def evaluate(self, test_set, full_eval=False):
        """
        Evaluate the network on the inputs and their corresponding targets

        Keywoard arguments:
        test_set -- Tuple containing the features to evaluate
        the learned model on and their corresponding labels
        full_eval -- If True, calculate the success rate over the full
        test dataset. Otherwise, only calculate it on the first batch

        Return:
        success_rate -- Pourcentage of correct predictions
        """
        return None

    def fit(self, train_set, test_set=None):
        """
        Fit the network to the train set in input

        Keywoard arguments:
        train_set -- Tuple containing the features to train on
        and their corresponding labels
        test_set -- Optional, tuple containing the features to evaluate
        the learned model on and their corresponding labels
        """
        self.cumulative_loss = []
        self.cumulative_eval = []
        iteration = 0
        try:
            for epoch in range(self.epochs):
                try:
                    for batch_idx, (data, target) in enumerate(train_set):
                        self.train(True)
                        data, target = data.to(self.device), target.to(self.device)

                        self.optimizer.zero_grad()
                        output = self.forward(data)
                        loss = self.criterion(output, target)
                        loss.backward()
                        loss = loss.item()
                        self.optimizer.step()

                        if iteration % self.print_every == 0:
                            self._score(iteration, loss, test_set)
                        iteration += 1
                except OSError:
                    pass
        except KeyboardInterrupt:
            print("Learning process interrupted")
        # save results to file
        self._save_results()
        self._save_best_model()
