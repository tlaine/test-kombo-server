import torch.nn as nn
import torch.optim as optim
from model_generator.models.classifier import Classifier
import torch


class DNNCorrespondance(Classifier):
    def __init__(self, input_size, hidden_size, output_size, window_size=30, num_layers=1, chunk_size=10, epochs=2, save_folder=None, type_net=3):
        """
        In the constructor we instantiate two nn.Linear modules and assign them as
        member variables.
        """
        super(DNNCorrespondance, self).__init__("dnn_corr", epochs, save_folder)
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.num_layers = num_layers
        self.type_net = type_net

        self.chunk_size = chunk_size
        self.use_gpu = False
        self.seq_len = window_size

        if self.type_net == 3:
            self.lstm_cam_e = nn.LSTM(input_size, hidden_size, num_layers=num_layers, batch_first=True)
            self.lstm_cam_w = nn.LSTM(input_size, hidden_size, num_layers=num_layers, batch_first=True)
            self.lstm_imu = nn.LSTM(input_size, hidden_size, num_layers=num_layers, batch_first=True)
            self.out2hidden = nn.Linear(hidden_size*3, hidden_size)
        elif self.type_net == 2:
            self.lstm_cam_w = nn.LSTM(input_size*2, hidden_size, num_layers=num_layers, batch_first=True)
            self.lstm_imu = nn.LSTM(input_size, hidden_size, num_layers=num_layers, batch_first=True)
            self.out2hidden = nn.Linear(hidden_size*2, hidden_size)
        else:
            self.lstm_imu = nn.LSTM(input_size*3, hidden_size, num_layers=num_layers, batch_first=True)

        self.hidden2label = nn.Linear(hidden_size, output_size)

        self.learning_rate = 0.005
        self.criterion = nn.CrossEntropyLoss()
        self.optimizer = optim.Adam(self.parameters(), lr=self.learning_rate)

    def _init_hidden(self, data):
        """
        Initialize the hidden layer of the LSTM network

        Keywoard arguments:
        data -- Input data that serves the hidden layer creation (keep the same dtype and shape)

        Return arguments:
        (h0, c0) -- Tuple representing the hidden layer of the network
        """
        h0 = data.new_zeros(self.num_layers, data.size(0), self.hidden_size, dtype=torch.float)
        c0 = data.new_zeros(self.num_layers, data.size(0), self.hidden_size, dtype=torch.float)
        return (h0, c0)

    def forward(self, inputs):
        """
        Apply the forward method of the neural network to the input data

        Keywoard arguments:
        input -- The single vector of features in entry of the network

        Return arguments:
        output -- Value returned by the last layer of the network
        """
        if self.type_net == 3:
            _, self.hidden_cam_e = self.lstm_cam_e(inputs[:,:,-3:], self.hidden_cam_e)
            _, self.hidden_cam_w = self.lstm_cam_w(inputs[:,:,3:6], self.hidden_cam_w)
            _, self.hidden_imu = self.lstm_imu(inputs[:,:,:3], self.hidden_imu)
            hidden_group = torch.cat((self.hidden_cam_e[0][-1], self.hidden_cam_w[0][-1], self.hidden_imu[0][-1]), 1)
            h = self.out2hidden(hidden_group)
            output = self.hidden2label(h)
        elif self.type_net == 2:
            _, self.hidden_cam_w = self.lstm_cam_w(inputs[:,:,-6:], self.hidden_cam_w)
            _, self.hidden_imu = self.lstm_imu(inputs[:,:,:3], self.hidden_imu)
            hidden_group = torch.cat((self.hidden_cam_w[0][-1], self.hidden_imu[0][-1]), 1)
            h = self.out2hidden(hidden_group)
            output = self.hidden2label(h)
        else:
            _, self.hidden_imu = self.lstm_imu(inputs, self.hidden_imu)
            output = self.hidden2label(self.hidden_imu[0][-1])

        return output

    def predict(self, inputs):
        """
        Predict the label given input variable using a forward pass and get the
        largest index

        Keywoard arguments:
        inputs -- The set of features to predict

        Return:
        predicted -- The set of predicted categories
        """
        # initialize hiddent state
        with torch.no_grad():
            self.hidden_cam_e = self._init_hidden(inputs)
            self.hidden_cam_w = self._init_hidden(inputs)
            self.hidden_imu = self._init_hidden(inputs)
            # predict
            output = self.forward(inputs)
            # extract prediction by taking the max of the predicted vector
            predicted = torch.max(output.data, 1)[1]
        return [predicted], [-1]

    def fit(self, train_set, test_set=None):
        """
        Fit the network to the train set in input

        Keywoard arguments:
        train_set -- Tuple containing the features to train on
        and their corresponding labels
        test_set -- Optional, tuple containing the features to evaluate
        the learned model on and their corresponding labels
        """
        self.cumulative_loss = []
        self.cumulative_eval = []
        self.cumulative_iter = []
        iteration = 0
        try:
            for e in range(self.epochs):
                try:
                    for batch_idx, (data, target) in enumerate(train_set):
                        self.train(True)
                        data, target = data.to(self.device), target.to(self.device)
                        # zero the parameter gradients
                        self.hidden_cam_e = self._init_hidden(data)
                        self.hidden_cam_w = self._init_hidden(data)
                        self.hidden_imu = self._init_hidden(data)
                        # truncated backprogation
                        input_parts = torch.split(data, self.chunk_size, dim=1)
                        target = target.view(target.numel())
                        for input_part in input_parts:
                            self.optimizer.zero_grad()
                            self.hidden_cam_e[0].detach()
                            self.hidden_cam_e[1].detach()
                            self.hidden_cam_w[0].detach()
                            self.hidden_cam_w[1].detach()
                            self.hidden_imu[0].detach()
                            self.hidden_imu[1].detach()
                            output = self.forward(input_part)
                            loss = self.criterion(output, target)
                            loss.backward(retain_graph=True)
                            loss = loss.item()
                            self.optimizer.step()
                        if iteration % self.print_every == 0:
                            self.train(False)
                            self._score(iteration, loss, test_set)
                        iteration += 1
                except OSError:
                    pass
        except KeyboardInterrupt:
            print("Learning process interrupted")
        self._save_results()
        self._save_best_model()
