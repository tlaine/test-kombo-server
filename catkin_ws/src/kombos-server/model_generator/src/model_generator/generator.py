#!/usr/bin/env python
import rospy
from model_generator.anthropometric_scaling import AnthropometricScaling
from model_generator.model_writer import ModelWriter
from numpy.random import uniform


class Generator(object):
    """Class to generate a model from anthropometric scaling"""
    def __init__(self, anthropometric_file='nasa', scaling_method="uniform", writer="URDF"):
        # initialize the antropometric scaler
        self.scaling_method = scaling_method
        self.ant_scale = AnthropometricScaling(anthropometric_file)
        self.writer = ModelWriter.factory(writer)
        self.bounds_values = self.ant_scale.get_reference_heights()

    def set_output_directory(self, output_dir):
        self.writer.output_dir = output_dir

    def generate_model(self, gender, height, filename=None):
        """Generate a model given a specified gender

        Keyword arguments:
        gender -- The gender of the model
        height -- The height of the model
        filename -- Name of the saved file. If None the
        model will not be written

        Return arguments:
        model -- The generated model
        """
        # get the lengths of the model scaled from anthropometry
        scaled_heights = self.ant_scale.scale(gender, height, self.scaling_method)
        # generate the model
        model = self.writer.write_model(scaled_heights, filename)
        return model

    def generate_random_height(self, gender):
        """Generate a random height based on the gender

        Keyword arguments:
        gender -- The gender of the model. This impact the range of
        possible heights for the random calculation

        Return arguments:
        random_height -- The calculated random height
        """
        min_value = self.bounds_values["5"][gender]
        max_value = self.bounds_values["95"][gender]
        return uniform(min_value, max_value)

    def generate_random_model(self, gender, filename=None):
        """Generate a random model given a specified gender

        Keyword arguments:
        gender -- The gender of the random model
        filename -- Name of the saved file. If None the
        model will not be written

        Return arguments:
        model -- The generated model
        """
        # start by getting a random height
        rand_height = self.generate_random_height(gender)
        # generate the model from height and gender
        model = self.generate_model(gender, rand_height, filename)
        return model
