#!/usr/bin/env python
import rospkg
import rospy
import json
import sys

from collections import deque
from model_generator.models.dnn_classifier import DNNClassifier
from torchvision.transforms import Compose
from model_generator.dataloader.transformations import *
from os.path import join, exists
from kombos_msgs.msg import mesureKalman
from sensor_msgs.msg import Imu
from threading import RLock

class Classifier(object):
    """Class to wrap the classifier. Include functions to train the model and predict incomming postures as well as csv files."""

    def __init__(self, model_name, bufferlen=20):
        r = rospkg.RosPack()
        conf_dir = join(r.get_path("model_generator"), "models", "dnn")
        model_dir = join(r.get_path("model_generator"), "models", "dnn")

        self.model_path = join(model_dir, model_name + ".pt")
        self.parameters = join(conf_dir, model_name + "_parameters.json")
        self.position_buffer = deque(maxlen=bufferlen)
        self.speed_buffer = deque(maxlen=bufferlen)
        self.accel_buffer = deque(maxlen=bufferlen)
        self.lock = RLock()
        self.sub = rospy.Subscriber("/PointCloudTF", mesureKalman, self.handle_kalman_msgs)
        self.subimu = rospy.Subscriber("/imu", Imu, self.handle_imu_msgs)

        if exists(self.parameters):
            with(open(self.parameters)) as datafile:
                training_parameters = json.load(datafile)
            input_size = training_parameters["input_size"]
            hidden_size = training_parameters["hidden_size"]
            output_size = training_parameters["output_size"]
        else:
            print("The classifier parameters are not set, please add the correct file.")
            sys.exit(1)

        self.model = DNNClassifier(input_size, hidden_size, output_size)
        if exists(self.model_path):
            self.model.load(self.model_path)
        self.transforms = Compose([ToTensor()])

        self.publisher = rospy.Publisher("/PointCloudTFClassified", mesureKalman, queue_size=10)

    def handle_kalman_msgs(self, msg):
        poses_values = []
        names = ["WristRight", "ElbowRight"]
        for x in msg.data.mesures:
            if x.name in names:
                poses_values += [x.position.x, x.position.y, x.position.z]
        with self.lock:
            if len(poses_values) == 3 * len(names):
                if self.position_buffer:
                    if not np.array_equal(self.position_buffer[-1], poses_values):
                        speed_values = np.array(poses_values) - np.array(self.position_buffer[-1])
                        if self.speed_buffer:
                            accel_values = np.array(speed_values) - np.array(self.speed_buffer[-1])
                            self.accel_buffer.append(accel_values)
                        self.speed_buffer.append(speed_values)
                        self.position_buffer.append(poses_values)
                else:
                    self.position_buffer.append(poses_values)

    def handle_imu_msgs(self, msg):
        imu_data = [msg.orientation.x, msg.orientation.y, msg.orientation.z,
                    msg.angular_velocity.x, msg.angular_velocity.y, msg.angular_velocity.z,
                    msg.linear_acceleration.x, msg.linear_acceleration.y, msg.linear_acceleration.z]
        with self.lock:
            if len(self.accel_buffer) == self.accel_buffer.maxlen:
                self.classify_imu(imu_data)
                rospy.loginfo("Imu Received")

    def classify_imu(self, imu_data):
        data = [imu_data[-3:] + self.accel_buffer[-1][:3].tolist()]
        input = self.transforms((data, -1))[0]
        predicted_labels, predicted_probs = self.model.predict(input)
        print(predicted_labels)

    def run(self):
        """Start the node and listen to incomming messages."""
        if not rospy.is_shutdown():
            rospy.loginfo("Classifier ready to receive postures")
            rospy.spin()
