#!/usr/bin/env python
import numpy as np
import torch


class Normalize(object):
    def __init__(self, scalers):
        self.scalers = scalers

    def __call__(self, sample):
        scaled_sample = np.zeros(len(sample[0]), len(sample[0][0]))
        for d in range(len(sample[0][0])):
            scaled_sample[:, d] = self.scalers[d].transform(sample[0][:, d])
        return scaled_sample, sample[1]


class ToTensor(object):
    """docstring for ToTensor"""
    def __call__(self, sample):
        data_tensor = torch.tensor(sample[0], dtype=torch.float)
        label_tensor = torch.tensor(sample[1], dtype=torch.long)
        return data_tensor, label_tensor


class NormalizeAcceleration(object):
    def __init__(self, maxdata, maxtarget):
        self.maxdata = maxdata
        self.maxtarget = maxtarget

    """docstring for ToTensor"""
    def __call__(self, sample):
        data = abs(sample[0] / self.maxdata)
        target = abs(sample[1] / self.maxtarget)
        return data, target
