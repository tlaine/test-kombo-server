from torch.utils.data.sampler import SubsetRandomSampler
from torch.utils.data import DataLoader
import numpy as np


class Loader(object):
    def __init__(self, batch_size, use_gpu, num_workers=1):
        self.batch_size = batch_size
        self.use_gpu = use_gpu
        self.num_workers = num_workers
        self.indices = []
        self.dataset = None

    def calculate_minimum_samples(self):
        samples_per_class = np.zeros(self.nb_labels)
        for l in self.dataset.indexed_labels:
            samples_per_class[l] += 1
        return min(samples_per_class)

    def extract_training_base(self, ratio=0.9):
        idx = self.indices
        np.random.shuffle(idx)
        nb_samples = int(ratio * self.calculate_minimum_samples())
        samples_per_class = np.zeros(self.nb_labels)
        max_samples = np.ones(self.nb_labels) * nb_samples
        sub_targets = self.dataset.indexed_labels

        i = 0
        train_idx = []
        test_idx = []
        while not np.equal(samples_per_class, max_samples).all() and i < len(sub_targets):
            if samples_per_class[sub_targets[idx[i]]] < nb_samples:
                train_idx.append(idx[i])
                samples_per_class[sub_targets[idx[i]]] += 1
            else:
                test_idx.append(idx[i])
            i += 1

        for j in range(i, len(sub_targets)):
            test_idx.append(idx[j])

        train_sampler = SubsetRandomSampler(train_idx)
        test_sampler = SubsetRandomSampler(test_idx)

        train_data = DataLoader(self.dataset,
                                sampler=train_sampler,
                                drop_last=True,
                                batch_size=self.batch_size,
                                num_workers=self.num_workers,
                                pin_memory=self.use_gpu)
        test_data = DataLoader(self.dataset,
                               sampler=test_sampler,
                               drop_last=True,
                               batch_size=self.batch_size,
                               num_workers=self.num_workers,
                               pin_memory=self.use_gpu)

        return train_data, test_data
