#!/usr/bin/env python
import h5py
import sys
import numpy as np
from torch.utils.data.dataset import Dataset
from os.path import exists
from model_generator.dataloader.loader import Loader


class H5IMUDataset(Dataset):
    def __init__(self, h5_filepath, key, window_size=30, transform=None):
        if not exists(h5_filepath):
            print("Incorrect file path, file {0:s} does not exists".format(h5_filepath))
            sys.exit(1)
        self.h5_filepath = h5_filepath
        self.transform = transform
        self.key = key
        self.window_size = window_size
        self.indexed_list = []
        self.indexed_labels = []
        self.index_data(window_size)

    @staticmethod
    def check_validity(data):
        valid = (len(data) > 0 and np.isfinite(data).all())
        return valid

    def index_data(self, window_size):
        h5_data = h5py.File(self.h5_filepath)
        data = h5_data[self.key]
        valid = self.check_validity(data)
        if valid:
            for i in range(len(data) - window_size):
                self.indexed_list.append([i, i + window_size])
                window_label = data[i:i + window_size, -1]
                if sum(window_label) > (window_size / 2):
                    label = 1
                else:
                    label = 0
                self.indexed_labels.append(label)
        h5_data.close()

    def __getitem__(self, index):
        h5_data = h5py.File(self.h5_filepath)
        indexes = self.indexed_list[index]
        # extract data from h5
        data = h5_data[self.key][indexes[0]:indexes[1],:-1]
        target = self.indexed_labels[index]
        sample = (data, target)
        if self.transform:
            sample = self.transform(sample)
        h5_data.close()
        return sample

    def __len__(self):
        length_data = len(self.indexed_labels)
        return length_data


class H5IMULoader(Loader):
    def __init__(self, h5_filepath, key, window_size=30, batch_size=200, use_gpu=False, transform=None):
        super(H5IMULoader, self).__init__(batch_size, use_gpu, num_workers=1)
        self.dataset = H5IMUDataset(h5_filepath, key, window_size, transform=transform)
        self.indices = list(range(len(self.dataset)))
        self.window_size = window_size
        self.nb_labels = 2

    # def calculate_mean_and_std(self):
    #     bar = progressbar.ProgressBar()
    #     mean = np.zeros(self.dataset.nb_components)
    #     sigma = np.zeros(self.dataset.nb_components)
    #     for (data, _) in bar(self.dataset):
    #         mean += np.sum(data.numpy(), axis=0)
    #         sigma += np.sum(np.power(data.numpy(), 2), axis=0)
    #     mean /= (len(self.dataset) * self.window_size)
    #     sigma /= (len(self.dataset) * self.window_size)
    #     sigma -= np.power(mean, 2)
    #     sigma = np.sqrt(sigma)
    #     return mean, sigma
