#!/usr/bin/env python
import h5py
from torch.utils.data.dataset import Dataset
from os.path import exists
import sys
from model_generator.dataloader.loader import Loader
import numpy as np
import progressbar


class H5Dataset(Dataset):
    def __init__(self, h5_filepath, key, frequency=30, time=1, indexes=None, nb_labels=None, transform=None):
        if not exists(h5_filepath):
            print("Incorrect file path, file {0:s} does not exists".format(h5_filepath))
            sys.exit(1)
        self.fps = 30
        self.h5_filepath = h5_filepath
        self.key = key
        self.indexed_list = []
        self.indexed_labels = []
        self.window_size = self.fps * time
        self.time = time
        self.frequency = frequency
        self.indexes = indexes
        self.nb_labels = nb_labels
        self.transform = transform
        if self.indexes is None:
            self.indexes = np.s_[:]
        self.index_data()

    @staticmethod
    def check_validity(data):
        valid = (len(data) > 0 and np.isfinite(data).all())
        return valid

    def include_data(self, label):
        res = not(self.nb_labels and label >= self.nb_labels)
        return res

    def index_data(self):
        h5_data = h5py.File(self.h5_filepath, 'r')
        for key in h5_data:
            for rec in h5_data[key]:
                data = h5_data[key][rec]['body0'][self.key]
                valid = self.check_validity(data)
                if valid:
                    label = int(key[-3:]) - 1
                    if self.include_data(label):
                        for i in range(len(data) - self.window_size):
                            self.indexed_list.append([key, rec, i, i + self.window_size])
                            self.indexed_labels.append(label)
        self.nb_labels = max(self.indexed_labels) + 1
        self.nb_components = len(data[0][self.indexes])
        h5_data.close()

    def __getitem__(self, index):
        h5_data = h5py.File(self.h5_filepath, 'r')
        indexes = self.indexed_list[index]
        # extract data from h5
        step = int(self.fps / self.frequency)
        temp = h5_data[indexes[0]][indexes[1]]['body0'][self.key]
        data = np.array([temp[i][self.indexes] for i in range(indexes[2], indexes[3], step)])
        label = self.indexed_labels[index]
        sample = (data, label)
        if self.transform:
            sample = self.transform(sample)
        h5_data.close()
        return sample

    def __len__(self):
        return len(self.indexed_list)


class H5Loader(Loader):
    def __init__(self, h5_filepath, key, frequency=30, time=1, batch_size=200, use_gpu=False, indexes=None, nb_labels=None, transform=None):
        super(H5Loader, self).__init__(batch_size, use_gpu, num_workers=1)
        self.dataset = H5Dataset(h5_filepath, key, frequency, time, indexes=indexes, nb_labels=nb_labels, transform=transform)
        self.indices = list(range(len(self.dataset)))
        self.nb_labels = self.dataset.nb_labels
        self.nb_components = self.dataset.nb_components
        self.time = time
        self.frequency = frequency

    def calculate_mean_and_std(self):
        window_size = self.frequency * self.time
        bar = progressbar.ProgressBar()
        mean = np.zeros(self.dataset.nb_components)
        sigma = np.zeros(self.dataset.nb_components)
        for (data, _) in bar(self.dataset):
            mean += np.sum(data.numpy(), axis=0)
            sigma += np.sum(np.power(data.numpy(), 2), axis=0)
        mean /= (len(self.dataset) * window_size)
        sigma /= (len(self.dataset) * window_size)
        sigma -= np.power(mean, 2)
        sigma = np.sqrt(sigma)
        return mean, sigma
