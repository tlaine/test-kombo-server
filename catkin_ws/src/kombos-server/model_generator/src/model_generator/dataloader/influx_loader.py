import numpy as np

from influxdb import InfluxDBClient
from torch.utils.data.sampler import SubsetRandomSampler
from torch.utils.data import DataLoader
from torch.utils.data.dataset import Dataset


class InfluxDataset(Dataset):
    TABLE_DATA = 'skeleton_ai'
    TABLE_TARGET = 'skeleton_ai'

    def __init__(self, tag_key, host='localhost', port=8086, user='kombo_aio', password='KomboAioNetKarakuri', database='kombo_v1', transform=None):
        self.connect_db(host, port, user, password, database)
        self.index_data()

    def connect_db(self, host, port, user, password, database):
        self.influx_client = InfluxDBClient(host,
                                            port,
                                            user,
                                            password,
                                            database)

    def index_data(self):
        query = "SELECT * FROM \"%s\" WHERE ai = '%s' ORDER BY time ASC" % (self.TABLE_DATA, self.tag_key)
        res = self.influx_client.query(query).get_points()
        self.indexes = []
        previous_timestamp = 0
        # loop over data to generate index
        for x in res:
            current_timestamp = x.get('time')
            if current_timestamp != previous_timestamp:
                self.indexes.push(x.get('time'))
            previous_timestamp = current_timestamp

    # def index_data(self):
    #     # get all data for the specified group
    #     query = "SELECT * FROM \"%s\" WHERE ai = '%s' ORDER BY time ASC" % (self.TABLE_DATA, self.tag_key)
    #     res = self.influx_client.query(query).get_points()
    #     self.indexes = []
    #     previous_timestamp = 0
    #     # loop over data to generate index
    #     for x in res:
    #         current_timestamp = x.get('time')
    #         if current_timestamp != previous_timestamp:
    #             self.indexes.push(x.get('time'))
    #         previous_timestamp = current_timestamp

    # def index_target(self):
    #     # get all data for the specified group
    #     query = "SELECT * FROM \"%s\" WHERE ai = '%s' ORDER BY time ASC" % (self.TABLE_TARGET, self.tag_key)
    #     res = self.influx_client.query(query).get_points()
    #     self.indexes = []
    #     previous_timestamp = 0
    #     # loop over data to generate index
    #     for x in res:
    #         current_timestamp = x.get('time')
    #         if current_timestamp != previous_timestamp:
    #             self.indexes.push(x.get('time'))
    #         previous_timestamp = current_timestamp

    def __getitem__(self, index):
        query = "SELECT * FROM \"%s\" WHERE ai = '%s' AND time = '%s' ORDER BY name ASC" % (self.TABLE, self.tag_key, self.indexes[index])
        res = self.influx_client.query(query).get_points()
        data = []
        for x in res:
            data.push(x.get('xo', 0))
            data.push(x.get('yo', 0))
            data.push(x.get('zo', 0))
            data.push(x.get('xn', 0))
            data.push(x.get('yn', 0))
            data.push(x.get('zn', 0))

        sample = (data, data)
        if self.transform:
            sample = self.transform(sample)
        return sample

    def __len__(self):
        return len(self.indexes)


class InfluxLoader(object):
    def __init__(self, tag_key, batch_size, use_gpu, num_workers=1):
        self.batch_size = batch_size
        self.use_gpu = use_gpu
        self.num_workers = num_workers
        self.dataset = InfluxDataset(tag_key)

    def extract_training_base(self, ratio=0.9):
        train_idx = np.arange(ratio * len(self.dataset))
        test_idx = np.arange((1 - ratio) * len(self.dataset))

        np.random.shuffle(train_idx)
        np.random.shuffle(test_idx)

        train_sampler = SubsetRandomSampler(train_idx)
        test_sampler = SubsetRandomSampler(test_idx)

        train_data = DataLoader(self.dataset,
                                sampler=train_sampler,
                                drop_last=True,
                                batch_size=self.batch_size,
                                num_workers=self.num_workers,
                                pin_memory=self.use_gpu)
        test_data = DataLoader(self.dataset,
                               sampler=test_sampler,
                               drop_last=True,
                               batch_size=self.batch_size,
                               num_workers=self.num_workers,
                               pin_memory=self.use_gpu)

        return train_data, test_data
