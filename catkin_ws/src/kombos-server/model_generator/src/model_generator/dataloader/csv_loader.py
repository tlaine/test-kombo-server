#!/usr/bin/env python
import pandas as pd
from torch.utils.data.dataset import Dataset
from os.path import exists
import sys
from model_generator.dataloader.loader import Loader
from model_generator.tools.model import get_theta_names


class CSVDataset(Dataset):
    def __init__(self, csv_filepath, transform=None):
        if not exists(csv_filepath):
            print("Incorrect file path, file {0:s} does not exists".format(csv_filepath))
            sys.exit(1)
        self.csv_data = pd.read_csv(csv_filepath)
        self.nb_labels = 2
        self.transform = transform
        self.thetas = [x + ".theta" for x in get_theta_names()]
        self.indexed_labels = [int(x == "bad") for x in self.csv_data["tag"]]
        self.nb_components = len(self.thetas)

    def __getitem__(self, index):
        data = []
        for name in self.thetas:
            value = self.csv_data[name][index]
            value = float(value) if value != "unset" else 0
            data.append(value)
        label = self.indexed_labels[index]
        sample = (data, label)

        if self.transform:
            sample = self.transform(sample)
        return sample

    def __len__(self):
        return len(self.csv_data)


class CSVLoader(Loader):
    def __init__(self, csv_filepath, batch_size=200, use_gpu=False, transform=None):
        super(CSVLoader, self).__init__(batch_size, use_gpu)
        self.dataset = CSVDataset(csv_filepath, transform=transform)
        self.indices = list(range(len(self.dataset)))
        self.max_target = self.dataset.nb_labels - 1
        self.nb_components = self.dataset.nb_components
        self.nb_labels = self.dataset.nb_labels
