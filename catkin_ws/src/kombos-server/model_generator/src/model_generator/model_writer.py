#!/usr/bin/env python
from __future__ import generators
import abc
import rospkg
import os
from os.path import join, exists, isdir
import xacro
from os import makedirs


class ModelWriter(object):
    """Factory class to create a model writer"""
    def factory(type):
        """Factory function to instantiate the writer based on its type

        Keyword arguments:
        type -- Base type of the writer to instantiate

        Return arguments:
        writer -- Instance of the selected writer object
        """
        if type == "URDF": return URDFModelWriter()
        assert 0, "Bad writer creation: " + type
    __metaclass__ = abc.ABCMeta
    factory = staticmethod(factory)

    @abc.abstractmethod
    def __init__(self):
        r = rospkg.RosPack()
        self.pkg_path = r.get_path("model_generator")
        self.output_dir = None

    @abc.abstractmethod
    def write_model(self, lengths_dict, filename=None):
        """Write a model from a dict of lengths

        Keyword arguments:
        lengths_dict -- The dict containing the segment lengths
        filename -- Name of the saved file. If None, the model will not be written

        Return arguments:
        model -- The generated model
        """

class URDFModelWriter(ModelWriter):
    """URDF model writer.
    Generate a URDF model from the dict of lengths.
    """
    def __init__(self):
        super(URDFModelWriter, self).__init__()
        self.xacro_dir = join(self.pkg_path, "config", "xacro")
        self.output_dir = join(self.pkg_path, "models", "urdf")
        if not exists(self.output_dir):
            makedirs(self.output_dir)


    def write_model(self, lengths_dict, filename=None):
        """Write a model from a dict of lengths

        Keyword arguments:
        lengths_dict -- The dict containing the segment lengths
        filename -- Name of the saved file. If None, URDF will not be written

        Return arguments:
        model -- The generated URDF
        """
        # map the given lengths_dict with the expected dict for xacro
        human_lengths = {}
        # head + neck
        human_lengths["head_radius"] = lengths_dict["head"] / 100
        human_lengths["neck_length"] = lengths_dict["neck"] / 100
        human_lengths["neck_radius"] = 0.04
        # arms
        human_lengths["upper_arm_length"] = lengths_dict["upper_arm"] / 100
        human_lengths["forearm_length"] = lengths_dict["forearm"] / 100
        human_lengths["hand_length"] = lengths_dict["hand_length"] / 100
        human_lengths["shoulder_offset_height"] = -0.05
        human_lengths["shoulder_offset_width"] = lengths_dict["shoulder_width"] / 100
        # trunk
        human_lengths["torso_length"] = 0.0
        human_lengths["torso_radius"] = 0.0
        human_lengths["spine_up_length"] = lengths_dict["torso"] / 100
        human_lengths["spine_up_radius"] = 0.15
        human_lengths["spine_down_length"] = lengths_dict["waist"] / 100
        human_lengths["spine_down_radius"] = 0.1
        # legs
        human_lengths["hip_offset_height"] = 0.0
        human_lengths["hip_offset_width"] = lengths_dict["hip_width"] / 100 - 0.05
        human_lengths["thigh_length"] = lengths_dict["thigh"] / 100
        human_lengths["thigh_radius"] = 0.05
        human_lengths["shin_length"] = lengths_dict["shin"] / 100
        human_lengths["foot_length"] = 0.1
        # substitute the length in xacro
        str_lengths = {k: str(v) for k, v in human_lengths.iteritems()}
        # lauch xacro converion
        xacro_skeleton = join(self.xacro_dir, "human_skeleton.urdf.xacro")
        assert exists(xacro_skeleton)
        model = xacro.process_file(xacro_skeleton, mappings=str_lengths)
        # rearrange the xml for visibility
        model = model.toprettyxml(indent='  ')
        # write the URDF if filename is specified
        if filename is not None:
            assert isdir(self.output_dir)
            with open(join(self.output_dir, filename + ".urdf"), 'w') as f:
                f.write(model)
        # return the URDF
        return model