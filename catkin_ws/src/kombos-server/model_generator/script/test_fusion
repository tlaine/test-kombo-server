#!/usr/bin/env python
import rospy
from kombos_msgs.msg import mesureKalman
import numpy as np
from collections import deque
import matplotlib.pyplot as plt
import tf


class TestFusion(object):
    def __init__(self, maxlen=100):
        self.maxlen = maxlen
        self.lookup_matrices = ["tf_1", "tf_2", "tf_3"]
        self.camera_timestamps = {}
        self.queues = {}
        for c in ['camera1', 'camera2', 'camera3']:
            self.queues[c] = deque(maxlen=maxlen)
            self.camera_timestamps[c] = deque(maxlen=maxlen)
        rospy.Subscriber("/PointCloudFusion", mesureKalman, self.handle_point_cloud_msgs)
        self.tfb = tf.TransformBroadcaster()

    @staticmethod
    def matrix_to_numpy(m):
        np_mat = np.array(m).reshape((4, 4))
        return np_mat

    def handle_point_cloud_msgs(self, msg):
        msg_time = rospy.get_rostime()
        for m in msg.matrices:
            if m.name in self.lookup_matrices:
                mat = self.matrix_to_numpy(m.M)
                mat = np.linalg.inv(mat)
                name = "camera" + m.name[-1]
                self.queues[name].append(mat)
                self.camera_timestamps[name].append(msg_time.to_sec())

        if self.queues["camera1"]:
            T = np.linalg.inv(self.queues["camera1"][-1])
            quat = tf.transformations.quaternion_from_matrix(T)
            vect = T[:-1, -1]
            self.tfb.sendTransform(vect, quat, rospy.Time.now(), "/camera1", "/human_base")

    def calculate_calibration_matrix(self, index1, index2):
        def centeroid(arr):
            length = arr.shape[0]
            sum_x = np.sum(arr[:, 0])
            sum_y = np.sum(arr[:, 1])
            sum_z = np.sum(arr[:, 2])
            return np.array([[sum_x / length], [sum_y / length], [sum_z / length]])

        if (len(self.queues["camera" + str(index1)]) == self.maxlen and
           len(self.queues["camera" + str(index2)]) == self.maxlen):
            associative_indexes = []
            ind2 = 0
            next_ind2 = 0
            for ind1 in range(len(self.camera_timestamps["camera" + str(index1)])):
                ind2 = next_ind2
                while (ind2 < len(self.camera_timestamps["camera" + str(index2)])
                      and abs(self.camera_timestamps["camera" + str(index1)][ind1] - self.camera_timestamps["camera" + str(index2)][ind2]) > 0.02):
                    ind2 += 1
                if ind2 < len(self.camera_timestamps["camera" + str(index2)]):
                    associative_indexes.append([ind1, ind2])
                    next_ind2 = ind2 + 1
            inda = associative_indexes[0][1]
            indb = associative_indexes[0][0]
            A = self.queues["camera" + str(index2)][inda][:-1, :-1]
            Pa = self.queues["camera" + str(index2)][inda][:-1, -1]
            B = self.queues["camera" + str(index1)][indb][:-1, :-1]
            Pb = self.queues["camera" + str(index1)][indb][:-1, -1]
            for i in range(1, len(associative_indexes)):
                inda = associative_indexes[i][1]
                indb = associative_indexes[i][0]
                A = np.vstack((A, self.queues["camera" + str(index2)][inda][:-1, :-1]))
                Pa = np.vstack((Pa, self.queues["camera" + str(index2)][inda][:-1, -1]))
                B = np.vstack((B, self.queues["camera" + str(index1)][indb][:-1, :-1]))
                Pb = np.vstack((Pa, self.queues["camera" + str(index1)][indb][:-1, -1]))
            M = np.dot(B.T, A)
            u, s, vh = np.linalg.svd(M, full_matrices=True)
            R = np.dot(u, vh)
            cPa = centeroid(Pa)
            cPb = centeroid(Pb)
            P = -np.dot(R, cPa) + cPb
            T = np.hstack((R, P))
            T = np.vstack((T, [0, 0, 0, 1]))

            print T

    def save_results(self):
        t = np.arange(len(self.queues['x'][0]))
        # Two subplots, the axes array is 1-d
        f, axarr = plt.subplots(3, sharex=True)
        for i, c in enumerate(['x', 'y', 'z']):
            for j in range(3):
                mean = np.mean(self.queues[c][j])
                axarr[j].plot(t, self.queues[c][j] - mean)
        axarr[0].set_title('Sharing X axis')
        plt.show()

    def run(self):
        rate = rospy.Rate(1)
        while not rospy.is_shutdown():
            # self.calculate_calibration_matrix(1, 2)
            print "----------------"
            self.calculate_calibration_matrix(1, 3)
            # print "################"
            rate.sleep()


if __name__ == '__main__':
    rospy.init_node("test_fusion")
    test = TestFusion()
    test.run()
