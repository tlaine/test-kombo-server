from os.path import join
from model_generator.models.lstm_classifier import LSTMClassifier
from model_generator.dataloader.h5_loader import H5Loader
from model_generator.dataloader.transformations import *
from model_generator.tools.plot import *
import json
from torchvision.transforms import Compose
import matplotlib.pyplot as plt


def lstm_calculation(window_size, batch_size, hidden_size, epochs=20, num_layers=1):
	#nb_components = None
	#nb_labels = None
	#label_list = None
	#data_folder = join("..", "data", "dataset_converted" , "h5")
	data_folder = join("..", "data")
	dataset = "classification"
	key = "thetas"
	save_folder = "../models/lstm"
	transform = Compose([ToTensor()])
	loader = H5Loader(join(data_folder, dataset + ".h5"), key=key, window_size=window_size, batch_size=batch_size, transform=transform)
	train_data, test_data = loader.extract_training_base(ratio=0.4)
	D_in = loader.nb_components  # Dimension of the feature vector at each time step
	D_out = loader.nb_labels  # number of classes to learn
	model = LSTMClassifier(D_in, hidden_size, D_out, window_size, num_layers=num_layers, epochs=epochs, save_folder=save_folder)
	model.fit(train_data, test_data)
	result = model.evaluate(test_data, True)
	return result


def plot_ratio():
	plt.figure(num=None, figsize=(10, 8), dpi=80, facecolor='w', edgecolor='r')
	plot_data_distribution(train_data, loader.nb_labels, title="Train data distribution")
	plt.show()


def plot_matrix():
	cnf_matrix = model.calculate_confusion_matrix(test_data)
	plt.figure(num=None, figsize=(10, 8), dpi=80, facecolor='w', edgecolor='k')
	plot_confusion_matrix(cnf_matrix, title='Normalized confusion matrix')
	plt.show()


if __name__ == '__main__':
	window_size = 30 # test entre 10 et 40 : 5 en 5 - 7
	batch_size = 200 # test entre 150 et 250 : 10 en 10 - 11
	hidden_size = 128 # test entre 2^5 et 2^9 - 5
	epochs = 10
	num_layers = 1
	file = open("myOutFile.txt", "w")

	for i in range(20,45,5):
		window_size = i
		for j in range(180,230,10):
			batch_size = j
			for k in range(6,9):
				hidden_size = pow(2,k)
				print('Parameters are : w_s = %d, b_s = %d, h_s = %d' % (window_size, batch_size, hidden_size))
				for it in range(1,11):
					accuracy = lstm_calculation(window_size, batch_size, hidden_size, epochs, num_layers)
					print('Result iteration %d is : %.7f' % (it, accuracy))
					file.write('%.5f, ' % (accuracy))
				file.write('; \n')
