#!/usr/bin/env bash

stage_welcome()
{
  prompt_str $1 $2 "This program will install the kombos project on your computer. Press any key to continue, or Ctrl^C to exit"
  sudo updatedb
}

stage_prerequisite()
{
  print_stage $1 $2 "Installing required packages..."
  sudo apt install nano curl build-essential cmake git python python-pip
}

stage_ros_install()
{
  print_stage $1 $2 "Installing ROS..."
  sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
  sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116
  sudo apt update
  sudo apt install ros-kinetic-desktop-full
}

stage_ros_configure()
{
  print_stage $1 $2 "Configuring ROS..."
  sudo rosdep init
  rosdep update
  source /opt/ros/kinetic/setup.bash
  sudo apt update
}

stage_ros_scripts()
{
  print_stage $1 $2 "Installing ROS add-ons..."
  sudo apt install python-rosinstall python-rosinstall-generator python-wstool python-catkin-pkg python-catkin-tools ros-kinetic-catkin
}

stage_ros_workspace()
{
  print_stage $1 $2 "Creating ROS workspace..."
  mkdir $ROS_WORKSPACE
  mkdir $ROS_WORKSPACE/src
  sudo chown -R $USER $ROS_WORKSPACE
  echo "Created at $ROS_WORKSPACE."
}

stage_jansson()
{
  print_stage $1 $2 "Installing Jansson..."
  cd /tmp
  curl -f http://www.digip.org/jansson/releases/$JANSSON_TARGET.tar.bz2 -o $JANSSON_TARGET.tar.bz2
  bunzip2 -c $JANSSON_TARGET.tar.bz2 | tar xf - && cd $JANSSON_TARGET
  ./configure
  sudo make && sudo make install
  cd -
}

stage_kombos()
{
  print_stage $1 $2 "Installing Kombos..."
  source $ROS_OPT
  cd $ROS_WORKSPACE/src
  git clone git@gitlab.inria.fr:auctus/$PROJECT_NAME.git
  cd ..

  CONFIG_BUILD=$(prompt_str "Do you want to build the project now (Y/N) ?")
  if [[ $CONFIG_BUILD == "y" || $CONFIG_BUILD == "Y" ]]; then
    catkin build
  else
    echo "You can build it later by using the 'catkin build' command in this directory."
  fi
}

stage_kombos_addons()
{
  print_stage $1 $2 "Installing Kombos add-ons..."
  sudo pip install -r $ROS_WORKSPACE/src/$PROJECT_NAME/model_generator/requirements.txt
}

stage_kombos_enable()
{
  print_stage $1 $2 "Configuring Kombos..."
  echo "Enabling ROS and Kombos sourcing in .bashrc"
  touch $BASHRC_PATH_NEW
	while read -r line
	do
		if [[ $line != $BASHRC_MARKER ]] && [[ $line != "source"*"setup.bash" ]] ; then
			echo -e $line >> $BASHRC_PATH_NEW
		fi
	done < $BASHRC_PATH
  echo -e "\n$BASHRC_MARKER\n$BASHRC_COMMAND\n$BASHRC_COMMAND_LOCAL" >> $BASHRC_PATH_NEW

	mv $BASHRC_PATH_NEW $BASHRC_PATH
  chmod a+rw $BASHRC_PATH
  source $BASHRC_PATH
  cd $FOLDER_START
}

stage_createap()
{
  print_stage $1 $2 "Creating hotspot..."
  CONFIG_CREATEAP=$(prompt_str "Do you want to install create_ap (free software available at https://github.com/oblique/create_ap) and setup the hostpot (Y/N) ?")
  if [[ $CONFIG_CREATEAP == "y" || $CONFIG_CREATEAP == "Y" ]] ; then
    sudo apt-get install util-linux iw iptables iproute2 dnsmasq procps hostapd haveged
    cd /tmp
    git clone https://github.com/oblique/create_ap
    cd create_ap
    sudo make install
    cd $FOLDER_START
    sudo systemctl daemon-reload
    sudo systemctl stop create_ap
    CONFIG_CREATEAP_SSID=$(prompt_str "Hotspot name (SSID)")
    CONFIG_CREATEAP_PASS=$(prompt_str_pass "Hotspot password")
    echo ""
    CONFIG_CREATEAP_INTERFACE=$(prompt_str "Wireless interface to use")
    sudo echo "CHANNEL=default
GATEWAY=192.168.8.1
WPA_VERSION=2
ETC_HOSTS=0
DHCP_DNS=gateway
NO_DNS=0
NO_DNSMASQ=0
HIDDEN=0
MAC_FILTER=0
MAC_FILTER_ACCEPT=/etc/hostapd/hostapd.accept
ISOLATE_CLIENTS=0
SHARE_METHOD=none
IEEE80211N=0
IEEE80211AC=0
HT_CAPAB=[HT40+]
VHT_CAPAB=
DRIVER=nl80211
NO_VIRT=0
COUNTRY=
FREQ_BAND=2.4
NEW_MACADDR=
DAEMONIZE=0
NO_HAVEGED=0
WIFI_IFACE=$CONFIG_CREATEAP_INTERFACE
SSID=$CONFIG_CREATEAP_SSID
PASSPHRASE=$CONFIG_CREATEAP_PASS
USE_PSK=0" > $CREATE_AP_CONF_PATH
    echo "Configuration copied to $CREATE_AP_CONF_PATH. You can edit it whenever you want."
    sudo systemctl start create_ap
    sudo systemctl enable create_ap
  else
    echo "You can configurate the hostpot later, but won't be able to run live capture until then."
  fi
}

print_bold()
{
    echo -e "$(tput bold)$1$(tput sgr0)"
}

print_stage()
{
  print_bold "\n[$1/$2] $3"
}

prompt_str()
{
    read -e -p "$(tput setaf 3)$1:$(tput sgr0) " str
    echo $str
}

prompt_str_pass()
{
    read -s -e -p "$(tput setaf 3)$1:$(tput sgr0) " str
    echo $str
}

echo "###########################"
echo "# KOMBOS SERVER INSTALLER #"
echo "###########################"
echo ""

STAGE_COUNT=9

PROJECT_NAME="kombos-server"
FOLDER_START=$(pwd)
ROS_OPT="/opt/ros/kinetic/setup.bash"
ROS_WORKSPACE=$HOME"/ROS"
BASHRC_PATH=$HOME"/.bashrc"
BASHRC_PATH_NEW=$BASHRC_PATH".tmp"
BASHRC_MARKER="# Kombos auto-generated commands"
BASHRC_COMMAND="source $ROS_OPT"
BASHRC_COMMAND_LOCAL="source $ROS_WORKSPACE/devel/setup.bash"
JANSSON_TARGET="jansson-2.11"
CONFIG_BUILD="n"
CONFIG_CREATEAP="n"
CREATE_AP_CONF_PATH="/etc/create_ap.conf"

stage_welcome
stage_prerequisite 1 $STAGE_COUNT
stage_ros_install 2 $STAGE_COUNT
stage_ros_configure 3 $STAGE_COUNT
stage_ros_scripts 4 $STAGE_COUNT
stage_ros_workspace 5 $STAGE_COUNT
stage_jansson 6 $STAGE_COUNT
stage_kombos 7 $STAGE_COUNT
stage_kombos_enable 8 $STAGE_COUNT
stage_createap 9 $STAGE_COUNT

echo -e "\nInstallation finished !\n"
