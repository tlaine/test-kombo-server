# Installation process

As of july 2018, the Kombos project works on Ubuntu 16.04 and is made to work with a server streaming Kinect V2 depth map to JSON.


## 1. Installation

Simply download the [installer](https://gitlab.inria.fr/auctus/kombos-server/raw/master/installer.sh) and execute it :

```bash
./installer.sh
```

It will create a new catkin workspace at ~/ROS.

If you already have a catkin workspace, skip the optional build part and move the kombos-server repository to wherever you want.

## 2. Compilation and use

Navigate to the ~/ROS folder and build the project with catkin :


```bash
catkin build
```

Launch the program with the following command :

```bash
roslaunch prog_kinect kombos.launch
```

Et voilà !

## 3. Troubleshooting

Empty section, please (don't) expand it !
