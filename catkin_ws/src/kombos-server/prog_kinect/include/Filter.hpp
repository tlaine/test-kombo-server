#ifndef FILTER_HPP
#define FILTER_HPP

#include <stdexcept>

namespace prog_kinect
{
	template<typename T>
	class Filter
	{
		public:
			virtual T filter(T x) = 0;
			template<typename...A> void except(const char* message, const A... args)
			{
				char* buffer;
				sprintf(buffer, message, args...);
				throw std::invalid_argument(buffer);
			}
	};
}

#endif // FILTER_HPP
