#ifndef UTILS_H
#define UTILS_H

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <cmath>
#include <vector>
#include <iostream>
#include <fstream>
#include <ros/ros.h>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <kombos_msgs/matrix.h>
#include <kombos_msgs/position_msg.h>

namespace prog_kinect
{
	class Utils
	{
		public:
			static float rad2deg(const float& radian);
			static float deg2rad(const float& degree);

			static Eigen::VectorXf vector_join(const Eigen::VectorXf& vector_a, const Eigen::VectorXf& vector_b);
			static Eigen::VectorXf vector_sub(const Eigen::VectorXf& vector, const int& start, const int& end);
			static Eigen::MatrixXf vector2matrix(const std::vector<float>& v, const int& size);
			static kombos_msgs::matrix matrix2msg(const std::string& name, const Eigen::MatrixXf& matrix);
			static Eigen::MatrixXf msg2matrix(const kombos_msgs::matrix& msg);

			static Eigen::Matrix4f passageMatrixRotX(const float& deg);
			static Eigen::Matrix4f passageMatrixRotY(const float& deg);
			static Eigen::Matrix4f passageMatrixRotZ(const float& deg);
			static Eigen::Matrix4f passageMatrixTrans(const float& x, const float& y, const float& z);
			static Eigen::Matrix4f passageInverse(const Eigen::MatrixXf& T);

			static kombos_msgs::position_msg build_message_position(const std::string& name, const float& x, const float& y, const float& z, const bool& fiabilite);

			static float distance(float da, float db = 0.0, float dc = 0.0, float dd = 0.0);
			static float length_calculation(std::map<std::string, Eigen::Vector3f> joints_positions, const std::string& proximal, const std::string& distal);

			static Eigen::MatrixXf transform(int i, float Pbx, float Pby, float Pbz, float th01, float th02, float th03, Eigen::VectorXf th, Eigen::VectorXf l);
			static Eigen::MatrixXf transform_inv(int i, float Pbx, float Pby, float Pbz, float th01, float th02, float th03, Eigen::VectorXf th, Eigen::VectorXf l);
			static Eigen::MatrixXf transform_base(float Pbx, float Pby, float Pbz, float Phrx, float Phry, float Phrz, float Phlx, float Phly, float Phlz, float Pmx, float Pmy, float Pmz, int points_number);
			static Eigen::MatrixXf transform_base_inv(float Pbx, float Pby, float Pbz, float Phrx, float Phry, float Phrz, float Phlx, float Phly, float Phlz, float Pmx, float Pmy, float Pmz);
			static Eigen::VectorXf transform_origin(Eigen::MatrixXf T);
			static Eigen::Vector3f theta_l(int conf, int i_l, int i_th1, int i_th2, Eigen::VectorXf solution, Eigen::VectorXf& l, Eigen::VectorXf& th);

			static Eigen::MatrixXf jacobian(int states_count, int measures_count, bool has_nobottom_speed, Eigen::VectorXf th);
			static Eigen::VectorXf jacobian_function(int measures_count, bool has_nobottom_speed, Eigen::VectorXf th);
		};
}

#endif // UTILS_H
