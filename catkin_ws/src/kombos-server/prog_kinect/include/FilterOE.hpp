#ifndef FILTEROE_H
#define FILTEROE_H

/* -*- coding: utf-8 -*-
*
* OneEuroFilter.cc -
*
* Original author: Nicolas Roussel (nicolas.roussel@inria.fr)
*
*/

#include <cmath>
#include <ctime>
#include <chrono>
#include <vector>
#include "Filter.hpp"
#include "FilterLowpass.hpp"

static const float DEFAULT_FREQUENCY = 30;
static const float DEFAULT_CM = 1;
static const float DEFAULT_CD = 1;
static const char* MESSAGE_INVALID_FREQUENCY = "[FILTEROE] Invalid value %f for frequency (should be strictly positive), defaulting to %f";
static const char* MESSAGE_INVALID_CM = "[FILTEROE] Invalid value %f for minimum cutoff (should be strictly positive), defaulting to %f";
static const char* MESSAGE_INVALID_CD = "[FILTEROE] Invalid value %f for derivative cutoff (should be strictly positive), defaulting to %f";

namespace prog_kinect
{
	class FilterOE : public Filter<std::vector<std::vector<double>>>
	{
	    private:
	        double frequency;
	        double cutoff_min;
	        double cutoff_derivative;
	        double beta;

	        FilterLowpass* filter_x;
	        FilterLowpass* filter_dx;

	        std::chrono::time_point<std::chrono::system_clock> time_last;

	    public:
	        FilterOE(double f, double b=0.0, double cm=1.0, double cd=1.0);
	        ~FilterOE();
	        std::vector<std::vector<double>> filter(std::vector<std::vector<double>> x);
	        double get_alpha(double cutoff);
	        void set_frequency(double f);
	        void set_cutoff_min(double cm);
	        void set_cutoff_derivative(double cd);
	        void set_beta(double b);
	};
}

#endif // FILTEROE
