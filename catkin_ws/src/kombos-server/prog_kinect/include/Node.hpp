#ifndef NODE_HPP
#define NODE_HPP

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <vector>
#include <numeric>
#include <algorithm>
#include <chrono>
#include <boost/algorithm/string.hpp>
#include <ros/ros.h>
#include "../include/Network.hpp"
#include <kombos_msgs/control_server_cmd.h>
#include "../include/json2model.h"

static const char* PUBLISHER_MESSAGE_INVALID = "[%s/PUBLISHER_SEND] COULD NOT FIND TOPIC %s";

namespace prog_kinect
{
	class Node
	{
		protected:
			std::string title;
			int loop_rate;
			std::vector<std::string> args;
			ros::NodeHandle* nodeHandle;
			std::map<std::string, ros::Subscriber> subscribers;
			std::map<std::string, ros::Publisher> publishers;

			// Model Data
			bool model_basducorps;
			bool model_vitesse;
			std::map<std::string, int> joints_indices;
			std::map<int, std::string> indices_joints;
			std::vector<std::string> states_names;

			int joints_count;
			int states_count;
			int measures_count;

			std::map<std::string, std::pair<std::string,std::string>> limb_extremity;
			std::map<std::string, float> limb_coef_proximal;
			std::map<std::string, float> limb_percent_weight;
			std::map<std::string, float> limb_density;

			virtual void on_start() = 0;
			virtual void on_update() = 0;
			virtual void on_end() = 0;

		public:
			Node(const std::string& name, const int& rate, int argc, char** argv);
			~Node();
			int run();
			bool has_arg(const char* arg);

			template<class T> T* get_param(const char* arg)
			{
				T value;
				return nodeHandle->getParam(arg, value) ? &value : nullptr;
			}

			template<class T> void set_param(const char* arg, T value)
			{
				nodeHandle->setParam(arg, value);
			}

			template<class T, typename C> void subscriber_add(const std::string& name, void(C::*method)(const typename T::ConstPtr&), const int& size = 10)
			{
				ros::Subscriber subscriber = nodeHandle->subscribe(name.c_str(), size, method, (C*)this);
				subscribers.insert(std::pair<std::string, ros::Subscriber>(name.c_str(), subscriber));
			}

			template<class T> void publisher_add(const std::string& name, const int& size = 10)
			{
				ros::Publisher publisher = nodeHandle->advertise<T>(name.c_str(), size);
				publishers.insert(std::pair<std::string, ros::Publisher>(name.c_str(), publisher));
			}

			template<class T> void publisher_send(const std::string& name, const T &message)
			{
				ros::Publisher* publisher = NULL;
				std::map<std::string, ros::Publisher>::iterator iterator = publishers.find(name.c_str());
				if(iterator != publishers.end())
				{
					publisher = &(iterator->second);
					publisher->publish(message);
				}
				else
				{
					std::string name_node_str = std::string(title);
					std::string name_topic_str = std::string(name.c_str());
					boost::to_upper(name_node_str);
					boost::to_upper(name_topic_str);
					ROS_WARN(PUBLISHER_MESSAGE_INVALID, name_node_str.c_str(), name_topic_str.c_str());
				}
			}

	};
}

#endif // NODE_HPP
