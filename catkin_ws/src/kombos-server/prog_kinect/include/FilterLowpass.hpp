#ifndef FILTER_LOWPASS_H
#define FILTER_LOWPASS_H

#include <cstdio>
#include <stdexcept>
#include "Filter.hpp"

static const double DEFAULT_ALPHA = 0.5;
static const char* MESSAGE_INVALID_ALPHA = "[FILTERLOWPASS] Invalid value %f for alpha (should be between 0 excluded and 1 excluded), defaulting to %f";

namespace prog_kinect
{
	class FilterLowpass : public Filter<double>
	{
	    private:
	        double value_raw;
	        double value_filtered;
	        double alpha;
	        bool is_initialized;

	    public:
	        FilterLowpass(double x, double a);
	        double filter(double x);
	        double filter_alpha(double x, double a);
	        bool get_is_initialized();
	        double get_last();
	        void set_alpha(double a);
	};
}

#endif // FILTER_LOWPASS_H
