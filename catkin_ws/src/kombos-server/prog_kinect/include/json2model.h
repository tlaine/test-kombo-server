#ifndef JSON2MODEL_H
#define JSON2MODEL_H

#include <typeinfo>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <list>
#include <fcntl.h>
#include <jansson.h>
#include <ros/ros.h>
#include <Eigen/Geometry>
#include <Eigen/Core>
#include <Eigen/LU>
#include <Eigen/Sparse>
#include "JsonReader.hpp"

static const char* PATH_JSON_PACKAGE = "prog_kinect";
static const char* PATH_JSON_FILENAME = "/config/model_parameters.json";

namespace prog_kinect
{
	class json2model
	{
		protected:
			bool basDuCorps;
			bool vitesse;
			bool kinect2;

			int nbJointsHaut;
			int nbJointsBas;
			int nbStatesHaut;
			int nbStatesBas;
			int nbStatesAngle;
			int nbStatesLength;
			int nbStatesBase;
			int nbStatesAngleVel;
			int nbStatesBaseVel;

			std::vector<std::string> jointsOrdered;
			std::map<std::string, int> jointsIdx;
			std::map<int, std::string> idxJoints;
			std::map<std::string,std::string> jointConf;
			std::map<std::string,std::string> jointInvert;

			std::vector<std::string> statesOrdered;
			std::map<std::string,std::string> statesType;
			std::map<std::string,std::string> statesConf;
			std::map<std::string, float> statesLimitMin;
			std::map<std::string, float> statesLimitMax;

			std::vector<std::string> limbOrdered;
			std::map<std::string,std::string> limbLength;
			std::map<std::string,std::string> limbProximal;
			std::map<std::string,std::string> limbDistal;
			std::map<std::string, float> limbCoefProximal;
			std::map<std::string, float> limbPercentWeight;
			std::map<std::string, float> limbDensity;
			std::map<std::string, float> limbCircumference;

			std::map<std::string,std::vector<std::string>> jointsToStates;

		public:
				EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
				json2model(bool _default = false);
				bool GetBasDuCorps() const;
				bool GetVitesse() const;
				bool GetKinect2() const;

				Eigen::Vector2i GetSize() const;
				int GetNbLength() const;
				int GetNbAngle() const;

				std::vector<std::string> GetJoints() const;
				std::map<std::string,int> GetJointIdx() const;
				std::map<int, std::string> GetIdxJoint() const;
				std::map<std::string, std::string> GetInvertJoints() const;

				std::vector<std::string> GetAllStates() const;
				std::vector<std::string> GetStates() const;
				std::map<std::string,std::string> GetStatesType() const;
				std::vector<std::string> GetLength() const;
				std::vector<std::string> GetAngle() const;
				std::map<std::string, float> GetAngleLimitMinMax(bool min) const;
				std::map<std::string, float> GetLengthLimitMinMax(bool min) const;

				std::map<std::string, std::vector<std::string>> GetJointsToStates() const;

				std::vector<std::string> GetLimb() const;
				std::map<std::string,std::string> GetLimbLength() const;
				std::map<std::string,std::string> GetLimbProximal() const;
				std::map<std::string,std::string> GetLimbDistal() const;
				std::map<std::string, float> GetLimbCoefProximal() const;
				std::map<std::string, float> GetLimbPercentWeight() const;
				std::map<std::string, float> GetLimbDensity() const;
				std::map<std::string, float> GetLimbCircumference() const;
				std::map<std::string, std::pair<std::string,std::string>> GetLimbExtremity() const;

	};
}

#endif
