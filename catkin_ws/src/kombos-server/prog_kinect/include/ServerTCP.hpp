#ifndef SERVERTCP_HPP
#define SERVERTCP_HPP

#include <cstring>
#include <netinet/ip.h>
#include <unistd.h>
#include <fcntl.h>
#include <ros/ros.h>

static const int DATA_SIZE_TCP = 8294400;
static const char* MESSAGE_LISTENING_SUCCESS_TCP = "[SERVER/SOCKET] LISTENING AT %d:%d, SETTING TO NON-BLOCK MODE";
static const char* MESSAGE_LISTENING_FAILURE_TCP = "[SERVER/SOCKET] COULD NOT LISTEN AT %d:%d";

namespace prog_kinect
{
    class ServerTCP
    {
        private:
            int connexion;
            bool is_listening;

        public:
            ServerTCP();
            ~ServerTCP();
            void init(const int& port = 9002);
            std::string retrieve();
    };
}

#endif // SERVER_HPP
