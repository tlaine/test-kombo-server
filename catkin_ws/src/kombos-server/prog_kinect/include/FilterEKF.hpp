#ifndef FILTEREKF_H
#define FILTEREKF_H

#include <typeinfo>
#include <ctime>
#include <cmath>
#include <vector>
#include <list>
#include <Eigen/Core>
#include <Eigen/LU>
#include <Eigen/Sparse>
#include <Eigen/Geometry>
#include <ros/ros.h>
#include "Utils.hpp"
#include "json2model.h"
#include "Filter.hpp"

static const int JOINT_LIMIT_MIN = -1;
static const int JOINT_LIMIT_MAX = 1;
static const int STATE_COUNT_NOBOTTOM_NOSPEEDS = 33;
static const int STATE_COUNT_BOTTOM_NOSPEEDS = 46;
static const int STATE_COUNT_NOBOTTOM_SPEEDS = 59;
static const int STATE_COUNT_BOTTOM_SPEEDS = 80;
static const char* MESSAGE_START = "[FILTEREKF] LOADING CONSTRAINTS : %d STATES FOUND (%d MEASURES, %d LENGTHS) FOR %d CONSTRAINTS";

namespace prog_kinect
{
	class FilterEKF : public Filter<Eigen::VectorXf>
	{
		private:
			bool has_bottom = false;
			bool has_speeds = false;

			Eigen::MatrixXf joints_limits;
			Eigen::VectorXf states;
			Eigen::VectorXf states_length_index;
			std::vector<std::string> states_names;
			Eigen::VectorXf states_limits;
			std::vector<std::string> states_constraints;
			int states_count;
			Eigen::VectorXf measures;
			int measures_count;
			std::vector<std::string> angles;
			int angles_count;

			Eigen::SparseMatrix<float> F; // Jacobian of process model n*n
			Eigen::SparseMatrix<float> Ft; // Jacobian of process model transpose

			Eigen::SparseMatrix<float> H; // Jacobian of measurement model m*n
			Eigen::SparseMatrix<float> Ht; // Jacobian of measure model transpose

			Eigen::SparseMatrix<float> P; // Prediction error covariance n*n
			Eigen::SparseMatrix<float> Q; // Process noise covariance n*n
			Eigen::SparseMatrix<float> R; // Measurement error covariance m*m

		public:
			EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
			FilterEKF(const json2model& model);
			~FilterEKF();
			Eigen::VectorXf get_states();
			Eigen::VectorXf get_measures();
			void set_r(const Eigen::MatrixXf& r);
			void config_f(const Eigen::MatrixXf& f);
			void config_h(std::map<std::string,Eigen::MatrixXf> _RJ = {});

			//TODO GARDER FILTER SANS CETTE FONCTION
			Eigen::VectorXf filter(Eigen::VectorXf x) { return Eigen::VectorXf::Zero(0); }
			void reset(const Eigen::VectorXf& value, const Eigen::MatrixXf& r, const Eigen::MatrixXf& q, const Eigen::MatrixXf& f);
			void update(const Eigen::VectorXf& y, std::map<std::string,Eigen::MatrixXf> _RJ);
			void predict();
			void constraints();
			Eigen::MatrixXf inverse(const Eigen::MatrixXf& m);
	};
}

#endif // FILTEREKF_H
