#ifndef NETWORK_HPP
#define NETWORK_HPP

//GENERAL
static const char* MESSAGE_TRUE = "TRUE";
static const char* MESSAGE_FALSE = "FALSE";
static const float LENGTH_FOR_OUTLIER = 1;

// ROS NODE
static const char* ROS_NODE_JSON2CLOUD_NAME = "/Json2Cloud";
static const short ROS_NODE_JSON2CLOUD_RATE = 100;
static const char* ROS_NODE_CLOUD2CLOUDTF_NAME = "/Cloud2CloudTF";
static const short ROS_NODE_CLOUD2CLOUDTF_RATE = 50;
static const char* ROS_NODE_CLOUDFUSION_NAME = "/CloudFusion";
static const short ROS_NODE_CLOUDFUSION_RATE = 60;
static const char* ROS_NODE_IK_NAME = "/IK";
static const short ROS_NODE_IK_RATE = 40;
static const char* ROS_NODE_FK_NAME = "/FK";
static const short ROS_NODE_FK_RATE = 40;
static const char* ROS_NODE_KALMAN_NAME = "/test_kalman";
static const short ROS_NODE_KALMAN_RATE = 0;
static const char* ROS_NODE_KALMAN2KALMANOE_NAME = "/Cloud2CloudOE";
static const short ROS_NODE_KALMAN2KALMANOE_RATE = 40;
static const char* ROS_NODE_GET_RULA_NAME = "/GetRULA";
static const short ROS_NODE_GET_RULA_RATE = 0;
static const char* ROS_NODE_GET_REBA_NAME = "/GetREBA";
static const short ROS_NODE_GET_REBA_RATE = 0;
static const char* ROS_NODE_GET_MOTION_NAME = "/GetMotion";
static const short ROS_NODE_GET_MOTION_RATE = 60;
static const char* ROS_NODE_BADPOSTURE_NAME = "/BadPosture";
static const short ROS_NODE_BADPOSTURE_RATE = 0;
static const char* ROS_NODE_JOINT_NAME_COM = "/CentreOfMass";
static const short ROS_NODE_COM_RATE = 0;
static const char* ROS_NODE_CLOUD2TRANSFORM_NAME = "/Cloud2Transform";
static const short ROS_NODE_CLOUD2TRANSFORM_RATE = 20;
static const char* ROS_NODE_GET_INDICE_NAME = "/NodeGetIndice";
static const short ROS_NODE_GET_INDICE_RATE = 60;
static const char* ROS_NODE_GET_ANTHROPO_NAME = "/NodeGetAnthropo";
static const short ROS_NODE_GET_ANTHROPO_RATE = 0;
static const char* ROS_NODE_GET_BALANCE_NAME = "/NodeGetBalance";
static const short ROS_NODE_GET_BALANCE_RATE = 0;
static const char* ROS_NODE_GET_HEIGHT_NAME = "/NodeGetHeight";
static const short ROS_NODE_GET_HEIGHT_RATE = 0;
static const char* ROS_NODE_RVIZ_NAME = "/Rviz";
static const short ROS_NODE_RVIZ_RATE = 0;

// ROS MESSAGE
static const char* ROS_TOPIC_RECORD = "/record/cmd";

static const char* ROS_TOPIC_PC = "/PointCloud";
static const char* ROS_TOPIC_PC_TF = "/PointCloudTF";
static const char* ROS_TOPIC_PC_FUSION_TF = "/PointCloudFusionTF";
static const char* ROS_TOPIC_PC_FUSION_TF_CORRECTED = "/PointCloudFusionTF_corrected";
static const char* ROS_TOPIC_PC_KALMAN_TF = "/PointCloudKalmanTF";
static const char* ROS_TOPIC_PC_CORRECTED_TF = "/PointCloudCorrectedTF";
static const char* ROS_TOPIC_PC_CORRECTED = "/PointCloudCorrected";
static const char* ROS_TOPIC_PC_CORRECTED_OE = "/PointCloudCorrectedOE";
static const char* ROS_TOPIC_PC_CORRECTED_OE_FILTERED = "/PointCloudCorrectedOEFiltered";
static const char* ROS_TOPIC_PC_CLASSIFIED = "/PointCloudClassified";
static const char* ROS_TOPIC_PC_CLASSIFIED_COM = "/PointCloudClassified_COM";

static const char* ROS_TOPIC_KALMAN_INIT = "/kalmanInit";
static const char* ROS_TOPIC_KALMAN_CLOUD = "/kalmanCloud";
static const char* ROS_TOPIC_KALMAN_CORRECTED = "/kalmanCorrected";

static const char* ROS_TOPIC_INDICE_RULA = "/IndiceRULA";
static const char* ROS_TOPIC_INDICE_REBA = "/IndiceREBA";
static const char* ROS_TOPIC_INDICE_MOTION = "/IndiceMotion";
static const char* ROS_TOPIC_SEGMENTS = "/NbSegment";
static const char* ROS_TOPIC_INDICE_SOUND = "/sound";
static const char* ROS_TOPIC_INDICE = "/indice";
static const char* ROS_TOPIC_LIMBRADIUS = "/limbRadius";
static const char* ROS_TOPIC_IND_PROPERTIES = "/IndividualProperties";
static const char* ROS_TOPIC_ANTHROPO = "/CharacAnthropo";
static const char* ROS_TOPIC_ANTHROPO_COMPLETED = "/CharacAnthropoComplete";
static const char* ROS_TOPIC_ANTHROPO_FINAL = "/CharacAnthropofinal";
static const char* ROS_TOPIC_BALANCE = "/Balance";

static const char* ROS_TOPIC_O = "/Opti";
static const char* ROS_TOPIC_OC = "/OptiCloud";
static const char* ROS_TOPIC_OC_TF = "/OptiCloud_TF";
static const char* ROS_TOPIC_OC_THETA_TF = "/OptiCloud_Theta_TF";
static const char* ROS_TOPIC_PC_TF_OPTI_CORRECTED = "/PointCloud_TF_corrected";
static const char* ROS_TOPIC_PC_TF_OPTI_FORWARD_CORRECTED = "/PointCloud_TF_corrected_Forward";

static const char* ROS_TOPIC_PC_TF_1_MARKERS = "/PointCloudTF/1/Markers";
static const char* ROS_TOPIC_PC_TF_2_MARKERS = "/PointCloudTF/2/Markers";
static const char* ROS_TOPIC_PC_TF_3_MARKERS = "/PointCloudTF/3/Markers";
static const char* ROS_TOPIC_PC_1_MARKERS = "/PointCloud/1/Markers";
static const char* ROS_TOPIC_PC_2_MARKERS = "/PointCloud/2/Markers";
static const char* ROS_TOPIC_PC_3_MARKERS = "/PointCloud/3/Markers";
static const char* ROS_TOPIC_PC_FUSION_TF_MARKERS = "/PointCloudFusionTF/Markers";
static const char* ROS_TOPIC_PC_CORRECTED_MARKERS = "/PointCloudCorrected/Markers";
static const char* ROS_TOPIC_PC_CORRECTED_OE_MARKERS = "/PointCloudCorrectedOE/Markers";
static const char* ROS_TOPIC_PC_CLASSIFIED_MARKERS = "/PointCloudClassified/Markers";
static const char* ROS_TOPIC_PC_OPTI_TF_MARKERS = "/OptiCloud_Theta_TF/Markers";
static const char* ROS_TOPIC_PC_TF_OPTI_CORRECTED_MARKERS = "/PointCloud_TF_corrected/Markers";
static const char* ROS_TOPIC_BALANCE_MARKER = "/Balance/Marker";
static const char* ROS_TOPIC_COM_MARKERS = "/COM/Marker";

// JSON2CLOUD
static const char* ARG_MOCK = "mock";
static const char* REDOX_HOST = "172.17.0.1";
static const int REDOX_PORT = 6379;
static const char* SERVER_COMMAND_START = "start";
static const char* SERVER_NAME_NORMAL = "Normal";
static const char* SERVER_NAME_MOCK = "Mock";
static const char* SERVER_NAME_OPTITRACK = "OPTITRACK";
static const int SERVER_PORT_NORMAL = 49195;
static const int SERVER_PORT_MOCK = 49196;
static const int SERVER_PORT_OPTITRACK = 49197;
static const int SERVER_TIMEOUT_SECONDS = 1;
static const char* SERVER_MESSAGE_START = "[JSON2CLOUD/SERVER] STARTING %s SERVER AT %d";
static const char* SERVER_MESSAGE_NODATA = "[JSON2CLOUD/SERVER] RECEIVING NO DATA AFTER %d SECONDS :(";
static const char* REDOX_KEY_DATA = "DATA_FROM_KINECT";
static const char* REDOX_KEY_ERRORS = "COUNT_NO_DATA_FROM_KINECT";
static const char* REDOX_KEY_SKELETON = "skeleton";
static const char* REDOX_KEY_SPEED = "speed_key";
static const char* JSON_KEY_POSITION = "position";
static const char* JSON_KEY_ORIENTATION = "orientation";
static const char* JSON_KEY_SPEED = "speed";
static const char* JSON_KEY_TRACKINGSTATE = "trackingState";
static const char* JSON_KEY_BODYTRACKINGID = "bodyTrackingId";
static const char* JSON_KEY_SOUND = "sound";
static const char* JSON_KEY_LIMBRADIUS = "limb_radius";
static const char* JSON_KEY_INDIVIDUAL = "individual_property";
static const char* JSON_KEY_GENDER = "gender";
static const char* JSON_KEY_EMOTIONS = "emotions";
static const char* JSON_KEY_AGE = "age";
static const int JSON_VALUE_TRACKINGSTATE_NUMBER = 25;
static const float JSON_VALUE_TRACKINGSTATE_TOLERANCE = 0.75F;
static const int JSON_VALUE_SOUND_MIN = 0;
static const int JSON_VALUE_SOUND_MAX = 150;
static const char* TYPE_OPTI = "optitrack";

// CLOUD2CLOUDTF
static const char* JOINT_NAME_SPINEBASE = "SpineBase";
static const char* JOINT_NAME_SPINESHOULDER = "SpineShoulder";
static const char* JOINT_NAME_SPINEMID = "SpineMid";
static const char* JOINT_NAME_NECK = "Neck";
static const char* JOINT_NAME_HEAD = "Head";
static const char* JOINT_NAME_HIP_RIGHT = "HipRight";
static const char* JOINT_NAME_HIP_LEFT = "HipLeft";
static const char* JOINT_NAME_SHOULDER_RIGHT = "ShoulderRight";
static const char* JOINT_NAME_SHOULDER_LEFT = "ShoulderLeft";
static const char* JOINT_NAME_ELBOW_RIGHT = "ElbowRight";
static const char* JOINT_NAME_ELBOW_LEFT = "ElbowLeft";
static const char* JOINT_NAME_WRIST_RIGHT = "WristRight";
static const char* JOINT_NAME_WRIST_LEFT = "WristLeft";
static const char* JOINT_NAME_HAND_RIGHT = "HandRight";
static const char* JOINT_NAME_HAND_LEFT = "HandLeft";
static const char* JOINT_NAME_HANDTIP_RIGHT = "HandTipRight";
static const char* JOINT_NAME_HANDTIP_LEFT = "HandTipLeft";
static const char* JOINT_NAME_THUMB_RIGHT = "ThumbRight";
static const char* JOINT_NAME_THUMB_LEFT = "ThumbLeft";
static const char* JOINT_NAME_KNEE_RIGHT = "KneeRight";
static const char* JOINT_NAME_KNEE_LEFT = "KneeLeft";
static const char* JOINT_NAME_ANKLE_RIGHT = "AnkleRight";
static const char* JOINT_NAME_ANKLE_LEFT = "AnkleLeft";
static const char* JOINT_NAME_FOOT_RIGHT = "FootRight";
static const char* JOINT_NAME_FOOT_LEFT = "FootLeft";
static const char* MATRIX_NAME_R = "r_";
static const char* MATRIX_NAME_TF = "tf_";
static const char* MATRIX_NAME_TF1 = "tf_1";
static const char* ARG_CALIBRATION = "/calibration_1W2";
static const double MAX_DELTAT = 0.4;

// IKFK
static const int DEFAULT_NUMBER_JOINTS = 18;
static const int DEFAULT_NUMBER_STATES = 12 + 28 + 6;
static const int DEFAULT_NUMBER_LENGTHS = 12;
static const int JOINT_ID_SPINEBASE = 0;
static const int JOINT_ID_SPINEMID = 1;
static const int JOINT_ID_SPINESHOULDER = 2;
static const int JOINT_ID_HEAD = 3;
static const int JOINT_ID_SHOULDERRIGHT = 4;
static const int JOINT_ID_ELBOWRIGHT = 5;
static const int JOINT_ID_WRISTRIGHT = 6;
static const int JOINT_ID_HANDRIGHT = 7;
static const int JOINT_ID_SHOULDERLEFT = 8;
static const int JOINT_ID_ELBOWLEFT = 9;
static const int JOINT_ID_WRISTLEFT = 10;
static const int JOINT_ID_HANDLEFT = 11;
static const int JOINT_ID_HIPRIGHT = 12;
static const int JOINT_ID_KNEERIGHT = 13;
static const int JOINT_ID_ANKLERIGHT = 14;
static const int JOINT_ID_HIPLEFT = 15;
static const int JOINT_ID_KNEELEFT = 16;
static const int JOINT_ID_ANKLELEFT = 17;
static const char* MODEL_MESSAGE_START = "[IKFK] LOADING MODEL : %d STATES FOUND (%d JOINTS, %d LENGTHS)";
static const char* MODEL_MESSAGE_LENGTH_DELTA = "[IKFK] LENGTH DELTA OF %f ON JOINT %s";

// KALMAN
static const char* MODEL_MESSAGE_KALMAN_START = "[KALMAN] LOADING MODEL : %d STATES FOUND, %d MEASURES FOUND (HAS_BOTTOM IS %s, HAS_SPEED IS %s)";
static const char* MODEL_MESSAGE_KALMAN_INIT = "[KALMAN] INIT WITH : R = %f, RV = %f, QL = %f, QO = %f, QOV = %f, DT = %f";
static const char* MODEL_MESSAGE_KALMAN_LOOP = "[KALMAN] LOOPING ON %d JOINTS...";
static const double DEFAULT_NUMBER_POINTS = 12;
static const double DEFAULT_VALUE_QL = 0;
static const double DEFAULT_VALUE_QO = 1E-2;
static const double DEFAULT_VALUE_QOV = 1;
static const double DEFAULT_VALUE_R = 1E-3;
static const double DEFAULT_VALUE_RV = 10;
static const double DEFAULT_VALUE_DT = 0.04;

// CLOUDFUSION
static const double TRUST_GOOD = 1.0;
static const double TRUST_MEDIUM = 0.5;
static const double TRUST_BAD = 0.1;

// ANTHROPO & CENTEROFMASS
static const char* JOINT_NAME_COM = "CoM";
static const char* NAME_ANTHROPO_DATA = "anthropo";
static const char* NAME_BALANCE_DATA = "balance";
static const float MAX_HUMAN_SIZE = 2.30;
static const float MIN_HUMAN_SIZE = 1.10;
static const float MAX_HUMAN_WEIGHT = 150;
static const float MIN_HUMAN_WEIGHT = 40;

static const int X_AXIS = 0;
static const int Y_AXIS = 1;
static const int Z_AXIS = 2;

// INDICE RULA
static const char* REBA_MESSAGE_INTERVALS_INCONSISTENT = "[INDICEREBA] INCONSISTENTS NUMBER OF SCORES (%d SCORES FOR %d INTERVALS)";
static const double REBA_THRESHOLD_SHOULDER_ABDUCTION = 30.0;
static const double REBA_THRESHOLD_SHOULDER_CROSSING = 70.0;
static const double REBA_THRESHOLD_WRIST_ROTATION = 5.0;
static const double REBA_THRESHOLD_NECK_BENT = 5.0;
static const double REBA_THRESHOLD_TRUNK_BENT = 5.0;
static const double REBA_TABLE_A[5][3][4] =
{
	{
		{ 1, 2, 3, 4 },
		{ 1, 2, 3, 4 },
		{ 3, 3.5, 5, 6 }
	},
	{
		{ 2, 3, 4, 5 },
		{ 3, 4, 5, 6 },
		{ 4, 5, 6, 7 }
	},
	{
		{ 2, 4, 5, 6 },
		{ 4, 5, 6, 7 },
		{ 5, 6, 7, 8 }
	},
	{
		{ 3, 5, 6, 7 },
		{ 5, 6, 7, 8 },
		{ 6, 7, 8, 9 }
	},
	{
		{ 4, 6, 7, 8 },
		{ 6, 7, 8, 9 },
		{ 7, 8, 9, 9.5 }
	}
};
static const double REBA_TABLE_B[6][3][3] =
{
	{
		{ 1, 2, 2.5 },
		{ 1, 2, 3 },
		{ 2, 3, 3.5 }
	},
	{
		{ 1, 2, 3 },
		{ 2, 3, 4 },
		{ 3, 3.5, 5 }
	},
	{
		{ 3, 4, 5 },
		{ 4, 5, 5.5 },
		{ 5, 5.5, 6 }
	},
	{
		{ 4, 5, 5.5 },
		{ 5, 6, 7 },
		{ 6, 7, 8 }
	},
	{
		{ 6, 7, 8 },
		{ 7, 8, 8.5 },
		{ 7.5, 8.5, 9 }
	},
	{
		{ 7, 8, 8.5 },
		{ 8, 9, 9.5 },
		{ 8.5, 9, 10 }
	}
};

static const char* RULA_MESSAGE_INTERVALS_INCONSISTENT = "[INDICERULA] INCONSISTENTS NUMBER OF SCORES (%d SCORES FOR %d INTERVALS)";
static const double RULA_THRESHOLD_SHOULDER_ABDUCTION = 30.0;
static const double RULA_THRESHOLD_SHOULDER_CROSSING = 70.0;
static const double RULA_THRESHOLD_WRIST_ROTATION = 5.0;
static const double RULA_THRESHOLD_NECK_BENT = 5.0;
static const double RULA_THRESHOLD_TRUNK_BENT = 5.0;
static const double RULA_TABLE_A[6][3][4] =
{
	{
		{ 1, 2, 2.5, 3 },
		{ 2, 2.5, 3, 3.5 },
		{ 2, 3, 3.5, 4 }
	},
	{
		{ 2, 3, 3.5, 4 },
		{ 3, 3.33, 3.66, 4 },
		{ 3, 4, 4.5, 5 }
	},
	{
		{ 3, 4, 4.5, 5 },
		{ 3, 4, 4.5, 5 },
		{ 4, 4.33, 4.66, 5 }
	},
	{
		{ 4, 4.33, 4.66, 5 },
		{ 4, 4.33, 4, 5 },
		{ 4, 4.5, 5, 6 }
	},
	{
		{ 5, 5.33, 5.66, 6 },
		{ 5, 6.33, 6.66, 7 },
		{ 6, 6.5, 7, 7.5 }
	},
	{
		{ 7, 7.33, 7.66, 8 },
		{ 8, 8.33, 8.66, 9 },
		{ 9, 9.25, 9.5, 9.75 }
	}
};
static const double RULA_TABLE_B[6][6] =
{
	{ 1, 2, 3, 5, 6, 7 },
	{ 2, 2.75, 4, 5, 6, 7 },
	{ 3, 3.5, 4, 5, 6, 7 },
	{ 5, 5.33, 5.66, 7, 7.5, 8 },
	{ 7, 7.33, 7.66, 8, 8.33, 8.66 },
	{ 8, 8.25, 8.5, 8.75, 9, 9.5 },
};
static const double RULA_TABLE_C[9][9] =
{
	{ 1, 2, 3, 3.5, 4, 5, 5.25, 5.5, 5.75 },
	{ 2, 2.5, 3, 4, 4.5, 5, 5.25, 5.5, 5.75 },
	{ 3, 3.33, 3.66, 4, 4.5, 5, 6, 6.33, 6.66 },
	{ 3, 3.33, 3.66, 4, 5, 6, 6.25, 6.5, 6.75 },
	{ 4, 4.33, 4.66, 5, 6, 7, 7.25, 7.5, 7.75 },
	{ 4, 4.5, 5, 6, 6.5, 7, 7.25, 7.5, 7.75 },
	{ 5, 5.5, 6, 6.5, 7, 7.2, 7.4, 7.6, 7.8 },
	{ 5, 5.5, 6, 7, 7.16, 7.32, 7.48, 7.64, 7.8 },
	{ 5, 5.5, 6, 7, 7.16, 7.32, 7.48, 7.64, 7.8 }
};

// INDICE MOTION
static const double MOTION_DISTANCE_TRESHOLD = 0.024;

#endif // NETWORK_HPP
