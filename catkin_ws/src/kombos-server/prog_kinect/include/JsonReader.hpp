#ifndef JSONREADER_H
#define JSONREADER_H

#include <cstring>
#include <sstream>
#include <jansson.h>
#include <ros/ros.h>
#include <ros/package.h>

static const char* JSON_MESSAGE_INVALID = "[JSONREADER] %s : ERROR ON LINE %d COLUMN %d : %s";
static const char* JSON_MESSAGE_NOTOBJECT = "[JSONREADER] %s : JSON IS VALID BUT NOT AN OBJECT";
static const char* JSON_MESSAGE_NOTCHILD = "[JSONREADER] COULD NOT FIND CHILD %s of %s";

namespace prog_kinect
{
	class JsonReader
	{
		private:
			std::string name;
			json_t* content;
			std::vector<JsonReader*> elements;

		public:
			JsonReader(const std::string& key, json_t* root);
			~JsonReader();
			std::string get_name();
			int get_int();
			double get_double();
			bool get_bool();
			std::string get_string();
			int at_key(const std::string& key);
			JsonReader* at(const std::string& key);
			void foreach(const std::function<void(const std::string& name, JsonReader*)>& callback);

			static void read(const std::string& content, const std::function<void(JsonReader*)>& callback);
			static void read_at(const std::string& path, const std::function<void(JsonReader*)>& callback);
			static void parse(json_t* root, json_error_t error, const std::function<void(JsonReader*)>& callback);
		};
}

#endif // JSONREADER_H
