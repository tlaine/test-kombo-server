#ifndef SERVER_HPP
#define SERVER_HPP

#include <cstring>
#include <netinet/ip.h>
#include <unistd.h>
#include <fcntl.h>
#include <ros/ros.h>

static const int DATA_SIZE = 8294400;
static const char* MESSAGE_LISTENING_SUCCESS = "[SERVER/SOCKET] LISTENING AT %d:%d, SETTING TO NON-BLOCK MODE";
static const char* MESSAGE_LISTENING_FAILURE = "[SERVER/SOCKET] COULD NOT LISTEN AT %d:%d";

namespace prog_kinect
{
    class Server
    {
        private:
            int connexion;
            int id_numii;
            bool is_listening;
            sockaddr_in socket_server;

        public:
            Server();
            ~Server();
            void init(const int& port = 9000);
            std::string retrieve();
            bool sendData(std::string data);
    };
}

#endif // SERVER_HPP
