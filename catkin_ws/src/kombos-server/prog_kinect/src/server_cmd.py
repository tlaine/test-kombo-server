#!/usr/bin/env python
import socket
import threading
import rospy
import redis
import datetime
import json
import copy
from time import sleep
captors = []


def to_json(myjson):
    json_object = False
    try:
        json_object = json.loads(myjson)
    except ValueError:
        return False
    except Exception:
        return False
    return json_object


def serialize(mylist):
    tmp_list = copy.deepcopy(mylist)
    for i, captor in enumerate(tmp_list):
        tmp_list[i]["is_up_since"] = str(tmp_list[i]["is_up_since"])
    return tmp_list


def update_is_up(ip):
    global captors
    iterator = 0
    tmp_captor = {}
    for captor in captors:
        if captor["ip"] == ip:
            tmp_captor = captor
            break
        else:
            iterator += 1
    if tmp_captor == {}:
        return False
    captors[iterator]["is_up_since"] = datetime.datetime.now()


def update_status(ip, redis_client):
    global captors
    serialized_list = []
    for i, captor in enumerate(captors):
        n_captor = captor.copy()
        if captor["ip"] == ip:
	    if datetime.datetime.now() <= captor["is_up_since"] + datetime.timedelta(seconds=4):
                captors[i]["status"] = True
                n_captor["status"] = True
            else:
                captors[i]["status"] = False
                n_captor["status"] = False
        n_captor["is_up_since"] = str(n_captor["is_up_since"])
        serialized_list.append(n_captor)
    redis_client.set("CAPTORS_LIST", json.dumps(serialized_list))


class ClientThread(threading.Thread):
    def __init__(self, ip, port, clientsocket, id_numii):

        threading.Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.daemon = True
        self.clientsocket = clientsocket
        self.redis = redis.StrictRedis(host='localhost', port=6379)
        self.id_numii = id_numii
        print "Nouveau thread pour %s %s" % (self.ip, self.port, )

    def run(self):
        try:
            while True:
                cmd = self.clientsocket.recv(2048)
                update_status(self.ip, self.redis)
                # Skeleton Finder send data in JSON format periodically, we proced it
                cmd_json = to_json(cmd)
                if cmd_json and isinstance(cmd_json, dict):
                    if 'battery_level' in cmd_json:
                        self.redis.set("BATTERY_LEVEL", cmd_json['battery_level'])
                # Command received FROM skeleton finder.
                elif cmd == "get_id_captor":
                    print(self.id_numii)
                    self.clientsocket.send(str(self.id_numii).encode())
                else:
                    if cmd == "i_m_up":
                        update_is_up(self.ip)
                    if cmd and not cmd == "{\"battery_level\": 1}i_m_up":
                        self.redis.set('speed_key', cmd)
                sleep(0.02)
            print("Client deconnecte...")
        except Exception as e:
            print('Error occured, clientThread stopped now !')
            print(e)


class Server(threading.Thread):

    def __init__(self, ip, port):

        threading.Thread.__init__(self)
        self.daemon = True
        self.id_numii = 0
        self.start_socket(ip, port)

    def start_socket(self, ip, port):
        self.ip = ip
        self.port = port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((ip, port))

    def wait_connection(self):
        global captors
        iterator = 0
        captor_exists = False
        self.socket.listen(10)
        (clientsocket, (ip, port)) = self.socket.accept()
        print(captors)
        for captor in captors:
            if captor["ip"] == ip:
                captor_exists = True
                self.id_numii = captor["id"]
                captors[iterator]["is_up_since"] = datetime.datetime.now()
            else:
                iterator += 1
        if not captor_exists:
            self.id_numii = self.id_numii + 1
            tmp_json = {"ip": ip, "id": self.id_numii, "status": 1, "is_up_since": datetime.datetime.now()}
            print(tmp_json)
            captors.append(tmp_json)
            captor_exists = False
        newthread = ClientThread(ip, port, clientsocket, self.id_numii)
        newthread.start()

    def run(self):
        while True:
            self.wait_connection()
            # if number_1_exists:
            #     self.id_numii = self.id_numii + 1
            # else:
            #     number_1_exists = True
            #     self.id_numii = 1
            # data = self.clientsocket.recv(2048)


if __name__ == '__main__':
    print('=== Start ===')
    redis_client = redis.StrictRedis(host='localhost', port=6379)
    redis_client.set("CAPTORS_CONNECTED", '{}')
    try:
        rospy.init_node("server_cmd", anonymous=False, log_level=rospy.DEBUG)
        RATE = rospy.Rate(10)
        while not rospy.is_shutdown():
            print 'start serv'
            serv = Server('0.0.0.0', 14721)
            print 'start thread'
            serv.start()
            while not rospy.is_shutdown():
                RATE.sleep()
            print('serv end. Closing')
    except rospy.ROSInterruptException:
        pass
