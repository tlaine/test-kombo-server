#!/usr/bin/env python
# pylint: disable=ungrouped-imports

import time
import json
import rospy

from utils.red import redis
from utils.watcher import WATCHER
from kombos_msgs.msg import rulaArray

ROS_NODE_COUNTERS = "counters"
ROS_TOPIC_INDICE_RULA = "/IndiceRULA"
ROS_TOPIC_INDICE_REBA = "/IndiceREBA"
REDIS_KEY_SEGMENTS = "segments"
REDIS_KEY_SEGMENTS_REBA = "segments_reba"
REDIS_KEY_SEGMENT_PREV = "prev"
REDIS_KEY_SEGMENT_COUNT = "count"
REDIS_KEY_SEGMENT_BADAT = "bad_at"
REDIS_KEY_SEGMENT_START = "start_time"
REDIS_KEY_SEGMENT_CUMUL = "cumul_time"
MESSAGE_START = "[COUNTERS] STARTING COUNTERS"

MIN_DURATION = 0.2


def on_rula(data):
    #segments = json.loads(redis.get(REDIS_KEY_SEGMENTS))
    segments = json.loads(redis.get(REDIS_KEY_SEGMENTS_REBA))
    for i in range(len(data.name)):
        key = data.name[i]
        value = data.indice[i]
        segment = segments.get(key, None)
        if segment:
            prev = int(segment[REDIS_KEY_SEGMENT_PREV])
            count = int(segment[REDIS_KEY_SEGMENT_COUNT])
            bad_at = int(segment[REDIS_KEY_SEGMENT_BADAT])
            start = float(segment[REDIS_KEY_SEGMENT_START])
            cumul = float(segment[REDIS_KEY_SEGMENT_CUMUL])

            if prev < bad_at and value >= bad_at:
                start = time.time()
            elif prev >= bad_at and value < bad_at:
                duration = time.time() - start
                if MIN_DURATION < duration:
                    cumul += duration  # resultat en s
                    count += 1

            segment[REDIS_KEY_SEGMENT_PREV] = value
            segment[REDIS_KEY_SEGMENT_COUNT] = count
            segment[REDIS_KEY_SEGMENT_START] = start
            segment[REDIS_KEY_SEGMENT_CUMUL] = round(cumul, 4)
            segments[key] = segment

    redis.set(REDIS_KEY_SEGMENTS_REBA, json.dumps(segments))


if __name__ == '__main__':
    try:
        rospy.init_node(ROS_NODE_COUNTERS, anonymous=False, log_level=rospy.DEBUG)
        # rospy.Subscriber(ROS_TOPIC_INDICE_RULA, rulaArray, on_rula)
        rospy.Subscriber(ROS_TOPIC_INDICE_REBA, rulaArray, on_rula)
        WATCHER.watch_log()

        rospy.loginfo(MESSAGE_START)

        rospy.spin()

    except rospy.ROSInterruptException:
        pass
