#!/usr/bin/env python
import rospy
import math
import rospkg
from utils.watcher import WATCHER
from kombos_msgs.msg import mesureKinect, kalmanState
from collections import deque
from threading import RLock
from os.path import join
import json
from copy import deepcopy


class JointValuesDerivation(object):
    def __init__(self):
        self.joint_memory = deque(maxlen=5)
        self.lookup_joints = []
        self.lookup_vel = []
        self.derivation_publisher = rospy.Publisher("/PointCloudKalmanDerivated", mesureKinect, queue_size=10)
        self.point_cloud_kalman_listener = rospy.Subscriber("/PointCloudKalman", mesureKinect, self.handle_point_cloud_kalman_msgs)
        self.kalman_state_listener = rospy.Subscriber("/kalmanCloud", kalmanState, self.handle_kalman_state_msgs)
        self.lock = RLock()
        self.init_lookup_tables()
        self.previous_state = None

    def init_lookup_tables(self):
        r = rospkg.RosPack()
        with open(join(r.get_path("humain"), 'config', 'model_parameters.json')) as datafile:
            data = json.load(datafile)
        for k, v in data['States'].iteritems():
            if v['type'] == "angle":
                self.lookup_joints.append(k)
            elif v['type'] == "angleVel":
                self.lookup_vel.append(k)

    @staticmethod
    def constraint_angles(ang):
        sign = lambda a: (a > 0) - (a < 0)
        return sign(ang) * (abs(ang) % math.pi)

    def calculate_derivations(self, js):
        res = {}
        res["names"] = []
        res["positions"] = []
        res["velocities"] = []
        res["accelerations"] = []
        res["timestamp"] = js["timestamp"]
        velocity = len(self.joint_memory) > 0
        acceleration = len(self.joint_memory) > 1
        for i, n in enumerate(self.lookup_joints):
            idx = js["name"].index(n)
            k = self.constraint_angles(js["value"][idx])
            res["names"].append(n)
            res["positions"].append(k)
            if velocity:
                # if self.lookup_vel[i] in js["name"]:
                #     idx = js["name"].index(self.lookup_vel[i])
                #     k = self.constraint_angles(js["value"][idx])
                #     res["names"].append(n)
                #     res["velocities"].append(k)
                #     if acceleration:
                #         js1 = self.joint_memory[-1]
                #         k1 = js1["velocities"][i]
                #         dt1 = (js["timestamp"] - js1["timestamp"]).to_sec()
                #         res["accelerations"].append((k - k1) / dt1)
                # else:
                js1 = self.joint_memory[-1]
                k1 = js1["positions"][i]
                dt1 = (js["timestamp"] - js1["timestamp"]).to_sec()
                res["velocities"].append((k - k1) / dt1)
                if acceleration:
                    js2 = self.joint_memory[-2]
                    k2 = js2["positions"][i]
                    dt2 = (js1["timestamp"] - js2["timestamp"]).to_sec()
                    res["accelerations"].append((k - 2 * k1 + k2) / (dt1 * dt2))
        return res

    def publish_derivation(self, deriv, msg):
        msg.kalman_state.name = []
        msg.kalman_state.value = []
        for i, n in enumerate(deriv["names"]):
            msg.kalman_state.name.append(n)
            msg.kalman_state.value.append(deriv["positions"][i])
            if len(self.joint_memory) > 0:
                msg.kalman_state.name.append('dot_' + n)
                msg.kalman_state.value.append(deriv["velocities"][i])
            if len(self.joint_memory) > 1:
                msg.kalman_state.name.append('ddot_' + n)
                msg.kalman_state.value.append(deriv["accelerations"][i])
        self.derivation_publisher.publish(msg)

    def handle_kalman_state_msgs(self, msg):
        with self.lock:
            self.previous_state = msg

    def handle_point_cloud_kalman_msgs(self, msg):
        # HUGE HACK: Replace state with the one published from ik_fk
        if self.previous_state is None:
            return
        with self.lock:
            msg.kalman_state = deepcopy(self.previous_state)

        js = {}
        js["name"] = msg.kalman_state.name
        js["value"] = msg.kalman_state.value
        js["timestamp"] = rospy.Time.now()
        # calculte the derivate
        deriv = self.calculate_derivations(js)
        # publish it
        self.publish_derivation(deriv, msg)
        self.joint_memory.append(deriv)

    def run(self):
        rospy.spin()


if __name__ == '__main__':
    rospy.init_node("joint_value_derivations")
    WATCHER.watch_log()
    jvd = JointValuesDerivation()
    jvd.run()
