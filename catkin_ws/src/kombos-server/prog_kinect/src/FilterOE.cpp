#include "../include/FilterOE.hpp"

using namespace std;
using namespace std::chrono;

namespace prog_kinect
{
	FilterOE::FilterOE(double f, double b, double cm, double cd)
	{
	    set_frequency(f);
	    set_cutoff_min(cm);
	    set_cutoff_derivative(cd);
	    set_beta(b);

	    filter_x = new FilterLowpass(0.0, get_alpha(cm));
	    filter_dx = new FilterLowpass(0.0, get_alpha(cd));

	    time_last = system_clock::now();
	}

	FilterOE::~FilterOE()
	{
	    delete filter_x;
	    delete filter_dx;
	}

	vector<vector<double>> FilterOE::filter(vector<vector<double>> x)
	{
	    vector<vector<double>> result;

		time_point<system_clock> time_now = system_clock::now();
		set_frequency(duration<double>(time_now - time_last).count());
	    time_last = time_now;

	    for(auto& it_joint : x)
	    {
	        vector<double> result_joint;
	        for(auto& it_coordinate : it_joint)
	        {
	            double dx = filter_x->get_is_initialized() ? (it_coordinate - filter_x->get_last()) * frequency : 0.0;
	            double edx = filter_dx->filter_alpha(dx, get_alpha(cutoff_derivative));
	            double cutoff = cutoff_min + beta * fabs(edx);

	            result_joint.push_back(filter_x->filter_alpha(it_coordinate, get_alpha(cutoff)));
	        }

	        result.push_back(result_joint);
	    }

	    return result;
	}

	double FilterOE::get_alpha(double cutoff)
	{
	    double te = 1.0 / frequency;
	    double tau = 1.0 / (2 * M_PI * cutoff);
	    return 1.0 / (1.0 + tau / te);
	}

	void FilterOE::set_frequency(double f)
	{
	    if (f <= 0)
	    {
	        frequency = DEFAULT_FREQUENCY;
	        except(MESSAGE_INVALID_FREQUENCY, f, frequency);
	    }
	    else
	    {
	        frequency = f;
	    }
	}

	void FilterOE::set_cutoff_min(double cm)
	{
	    if (cm <= 0)
	    {
	        cutoff_min = DEFAULT_CM;
	        except(MESSAGE_INVALID_CM, cm, cutoff_min);
	    }
	    else
	    {
	        cutoff_min = cm;
	    }
	}

	void FilterOE::set_cutoff_derivative(double cd)
	{
	    if (cd <= 0)
	    {
	        cutoff_derivative = DEFAULT_CD;
	        except(MESSAGE_INVALID_CD, cd, cutoff_derivative);
	    }
	    else
	    {
	        cutoff_derivative = cd;
	    }
	}

	void FilterOE::set_beta(double b)
	{
	    beta = b;
	}
}
