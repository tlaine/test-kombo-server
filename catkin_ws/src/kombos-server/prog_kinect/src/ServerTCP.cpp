#include "../include/ServerTCP.hpp"

using namespace std;

namespace prog_kinect
{
	ServerTCP::ServerTCP()
	{
		connexion = 0;
		is_listening = false;
	}

	ServerTCP::~ServerTCP()
	{
		if(connexion)
		{
			close(connexion);
		}
	}

	void ServerTCP::init(const int& port)
	{
		connexion = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

		sockaddr_in socket_server;
		socket_server.sin_family = AF_INET;
		socket_server.sin_addr.s_addr = INADDR_ANY;
		socket_server.sin_port = htons(port);
		bzero(&(socket_server.sin_zero), 8);

		if(bind(connexion, (sockaddr*)&socket_server, sizeof(socket_server)) == 0)
		{
			fcntl(connexion, F_SETFL, fcntl(connexion, F_GETFL) | O_NONBLOCK);
			is_listening = true;
			ROS_INFO(MESSAGE_LISTENING_SUCCESS_TCP, socket_server.sin_addr.s_addr, port);
		}
		else
		{
			ROS_WARN(MESSAGE_LISTENING_FAILURE_TCP, socket_server.sin_addr.s_addr, port);
		}
	}

	string ServerTCP::retrieve()
	{
		/*
		Cette fonction est appellée à chaque tour de boucle. Elle vérifie qu'un paquet UDP arrive via réseau (socket), auquel cas elle le traite.
		Si aucun paquet n'est disponible, la fonction "recvfrom", qui s'occupe de regarder le buffer du socket, n'est pas bloquante. Il est donc possible d'effectuer
		d'autres opérations dans la boucle principale du main.
		*/
		char data[DATA_SIZE_TCP];
		int data_count = recvfrom(connexion, data, DATA_SIZE_TCP, 0, NULL, NULL);
		if(data_count < DATA_SIZE_TCP)
		{
			data[data_count] = '\0';
		}

		return string(data);
	}
}
