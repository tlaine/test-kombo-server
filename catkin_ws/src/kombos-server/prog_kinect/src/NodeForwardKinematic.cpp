#include <Eigen/Geometry>
#include <kombos_msgs/mesureKinect.h>
#include <kombos_msgs/mesureKalman.h>
#include <kombos_msgs/kalmanState.h>
#include "../include/Network.hpp"
#include "../include/Utils.hpp"
#include "../include/json2model.h"
#include "../include/Node.hpp"

using namespace std;
using namespace kombos_msgs;
using namespace Eigen;

namespace prog_kinect
{
	class NodeForwardKinematic : public Node
	{
		private:
			map<string, int> msg_position_order;
			map<int, int> states_to_th;
			map<int, int> states_to_l;

			/* --------------------------------------
			** Msg corrected from angle to position
			*/

			void kinematic_opti(const mesureKalman::ConstPtr& message)
			{
				mesureKalman message_corrected = *message;
				message_corrected.data = kinematic_forward(message->data);

				publisher_send(ROS_TOPIC_PC_TF_OPTI_FORWARD_CORRECTED, message_corrected);
			}

			int sign(const float& x)
			{
				return (x < 0) ? -1 : 1;
			}

			mesureKinect kinematic_forward(const mesureKinect& message)
			{
				int message_mesures_count = message.mesures.size();
				map<string, float> states_msg;
				for(int i = 0; i < message_mesures_count; i++)
				{
					msg_position_order[message.mesures[i].name] = i;
				}

				for(int i = 0; i < message.kalman_state.name.size(); i++)
				{
					states_msg[message.kalman_state.name[i]] = message.kalman_state.value[i];
				}

				mesureKinect mesure;
				// copy the non-modified elements
				mesure.kalman_state = message.kalman_state;
				mesure.vitesses = message.vitesses;

				// init pos measure with old data
				//mesure.mesures.resize(message.mesures.size());
				mesure.mesures = message.mesures;

				VectorXf l = VectorXf::Zero((joints_count == 18) ? 21 : joints_count);
				VectorXf th = VectorXf::Zero((joints_count - 1) * 2);
				float Pbx = states_msg["SpineBaseX"];
				float Pby = states_msg["SpineBaseY"];
				float Pbz = states_msg["SpineBaseZ"];
				float th01 = 0; //message.kalman_state.value[3];
				float th02 = 0; //message.kalman_state.value[4];
				float th03 = 0; //message.kalman_state.value[5];

				int j = 0;
				for(vector<string>::iterator it = states_names.begin(); it != states_names.end(); ++it)
				{
					if(states_to_th.count(j) > 0)
					{
						th[states_to_th[j]] = states_msg[*it];
					}

					if(states_to_l.count(j) > 0)
					{
						l[states_to_l[j]] = states_msg[*it];
					}
					j++;
				}

				for(int i = 0; i < joints_count; i++)
				{
					VectorXf P;
					P = Utils::transform_origin(Utils::transform(i, Pbx, Pby, Pbz, th01, th02, th03, th, l));
					mesure.mesures[msg_position_order[indices_joints[i]]].name = indices_joints[i];
					mesure.mesures[msg_position_order[indices_joints[i]]].position.x = P(0);
					mesure.mesures[msg_position_order[indices_joints[i]]].position.y = P(1);
					mesure.mesures[msg_position_order[indices_joints[i]]].position.z = P(2);
					mesure.mesures[msg_position_order[indices_joints[i]]].fiabilite = 1;
				}
				return mesure;
			}

		public:
			NodeForwardKinematic(int argc, char** argv):
			Node(ROS_NODE_FK_NAME, ROS_NODE_FK_RATE, argc, argv)
			{
				subscriber_add<mesureKalman>("/PointCloud_TF_corrected", &NodeForwardKinematic::kinematic_opti);
				//subscriber_add<mesureKalman>(ROS_TOPIC_PC_CORRECTED_TF, &NodeForwardKinematic::kinematic_opti);

				publisher_add<mesureKalman>(ROS_TOPIC_PC_TF_OPTI_FORWARD_CORRECTED);
			}

			void on_start()
			{
				states_to_th =
				{
					{6, 0},
					{7, 1},
					{8, 2},
					{9, 3},
					{10, 4},
					{11, 5},
					{12, 6},
					{13, 7},
					{14, 8},
					{15, 9},
					{16, 10},
					{17, 11},
					{18, 12},
					{19, 13},
					{20, 14},
					{21, 15},
					{22, 16},
					{23, 17},
					{24, 18},
					{25, 19},
					{33, 20},
					{34, 21},
					{35, 22},
					{36, 23},
					{37, 24},
					{38, 25},
					{39, 26},
					{40, 27}
				};
				states_to_l =
				{
					{26, 0},
					{27, 1},
					{28, 2},
					{29, 3},
					{30, 4},
					{31, 5},
					{32, 6},
					{41, 11},
					{42, 12},
					{43, 13},
					{44, 14},
					{45, 15}
				};
			}

			void on_update()
			{

			}

			void on_end()
			{

			}
		};
}

int main(int argc, char** argv)
{
	return (prog_kinect::NodeForwardKinematic(argc, argv)).run();
}
