"""numiicharacterization Script

Usage:
	numiicharacterization.py <tag>

Options:
	-h --help
"""

import numpy as np
import dateutil.parser
from influxdb import InfluxDBClient
from docopt import docopt


class Characterization(object):
	SKELETON_OPTI_ANGLE_TABLE = 'skeleton_opti_angle'
	SKELETON_TF_ANGLE_TABLE = 'skeleton_tf_angle'
	SKELETON_OPTI_TABLE = 'skeleton_opti'
	SKELETON_TF_TABLE = 'skeleton_tf'

	def __init__(self, tag_key, transform=None):
		#self, tag_key, host='localhost', port=8086, user='ilfautkombos', password='pZWH7U59LU9wLsnTgMSDSN8J5gvb6AzZRVe8XGJH', database='kombo_v4', transform=None):
		self.tag_key = tag_key
		self.data_angle_name = {}
		self.opti_data = []
		self.tf_data = []
		self.opti_angle_data = []
		self.tf_angle_data = []

		self.connect_db()

		self.generate_skeleton_tf()
		self.generate_skeleton_opti()
		#self.sync_data = self.synchronize(self.opti_data, self.tf_data)
		#print(self.opti_angle_data)
		self.sync_data_angle = self.synchronize(self.opti_angle_data, self.tf_angle_data)
		#print(self.sync_data_angle)
		#print('Number of data after synchronization : ' + str(len(self.sync_data)))
		print('Number of data_angle after synchronization : ' + str(len(self.sync_data_angle)))
		self.display_nrmse()

	def connect_db(self, host='localhost', port=8086, user='kombo_aio', password='KomboAioNetKarakuri', database='kombo_v1'):
		self.influx_client = InfluxDBClient(host, port, user, password, database)

	def _prepare_query(self, TABLE):
		return "SELECT * FROM \"%s\" WHERE ai = '%s' ORDER BY time ASC" % (TABLE, self.tag_key)

	def generate_skeleton_opti(self):
		query = self._prepare_query(self.SKELETON_OPTI_TABLE)
		self.opti_data = self._fetch_data_position(query)
		query = self._prepare_query(self.SKELETON_OPTI_ANGLE_TABLE)
		self.opti_angle_data = self._fetch_data_angle(query)
		print(len(self.opti_angle_data))

	def generate_skeleton_tf(self):
		query = self._prepare_query(self.SKELETON_TF_TABLE)
		self.tf_data = self._fetch_data_position(query)
		query = self._prepare_query(self.SKELETON_TF_ANGLE_TABLE)
		self.tf_angle_data = self._fetch_data_angle(query)
		print(len(self.tf_angle_data))

	def _fetch_data_angle(self, query):
		list_data = []
		res = self.influx_client.query(query).get_points()
		previous_timestamp = 0
		# loop over data to generate index
		print("=> Start data")
		data = {}
		data_angle_name_full = False
		for x in res:
			current_timestamp = x.get('time')
			if current_timestamp != previous_timestamp and previous_timestamp != 0:
				if not data_angle_name_full:
					self.data_angle_name = [angle_name for angle_name in sorted(data)]
					data_angle_name_full = True

				data_ordered = [data[_] for _ in sorted(data)]
				list_data.append({'timestamp': previous_timestamp, 'data': data_ordered})
				data = {}
			data[x.get('name_angle')] = x.get('theta_radian')
			#print(x.get('name'))
			previous_timestamp = current_timestamp
		print("=> Generating data angle done!")
		i = 0
		for x in sorted(data):
			i += 1
		return list_data

	def _fetch_data_position(self, query):
		list_data = []
		res = self.influx_client.query(query).get_points()
		previous_timestamp = 0
		# loop over data to generate index
		print("=> Start data")
		data = {}
		for x in res:
			current_timestamp = x.get('time')
			if current_timestamp != previous_timestamp and previous_timestamp != 0:
				data_ordered = [data[_] for _ in sorted(data)]
				list_data.append({'timestamp': previous_timestamp, 'data': data_ordered})
				data = {}
			data[x.get('name_position')] = {'position_x': x.get('x'), 'position_y': x.get('y'), 'position_z': x.get('z')}
			previous_timestamp = current_timestamp
		print("=> Generating data position done!")
		return list_data

	def synchronize(self, data_opti, data_numii):
		result_sync = []
		current_opti_index = 1
		for t in data_numii:
			value_found = False
			t_time = dateutil.parser.parse(t['timestamp'])
			data_opti_time = dateutil.parser.parse(data_opti[current_opti_index]['timestamp'])
			while t_time > data_opti_time:
				current_opti_index += 1
				data_opti_time = dateutil.parser.parse(data_opti[current_opti_index]['timestamp'])
				value_found = True
			if value_found:
				#print(abs((t_time - data_opti_time).total_seconds()))
				if abs((t_time - data_opti_time).total_seconds()) < abs((t_time - dateutil.parser.parse(data_opti[current_opti_index - 1]['timestamp'])).total_seconds()):
					#print('_______')
					#print(current_opti_index)
					#print(data_opti[current_opti_index]['data'])
					result_sync.append({'numii':t['data'], 'opti':data_opti[current_opti_index]['data'], 'timestamp':t['timestamp']})
				else:
					result_sync.append({'numii':t['data'], 'opti':data_opti[current_opti_index - 1]['data'], 'timestamp':t['timestamp']})

		return result_sync

	def display_nrmse(self):
		if len(self.sync_data_angle) == 0:
			print('--- NRMSE can not be calculated : synchronization data_angle are empty')
			return
		print('numii : ' + str(len(self.sync_data_angle[0]['numii'])))
		print('opti : ' + str(len(self.sync_data_angle[0]['opti'])))
		print('name : ' + str(len(self.data_angle_name)))
		for i in range(len(self.sync_data_angle[0]['numii'])):
			print(str(i) + ' --- NRMSE : ' + str(self.data_angle_name[i]))
			nrmse_res = self.nrmse(np.array([x['numii'][i] for x in self.sync_data_angle]), np.array([x['opti'][i] for x in self.sync_data_angle]))
			print('     = ' + str(nrmse_res*100) + '%')
		"""if len(self.sync_data) == 0:
			print('--- NRMSE can not be calculated : synchronization data are empy')
			exit()"""

	def nrmse(self, predicted_data, observed_data):
		if(len(predicted_data) != len(observed_data)):
			print('Datasets do not have same size -> can not calculate NRMSE')
			exit()
		#n = len(predicted_data)
		rmse = np.sqrt(((predicted_data - observed_data) ** 2).mean())
		maxmin = max(observed_data) - min(observed_data)
		if maxmin == 0:
			print('Value of observed data not good ')
			return -0.0
		else:
			return rmse / maxmin


if __name__ == '__main__':
	arguments = docopt(__doc__, version='0.1')
	tag = arguments.get('<tag>', None)
	if not tag:
		print("---> Missing arguments")
		exit()

	print('=== START ===')
	db_connect = Characterization(tag)
	print('=== DONE ===')
