#include <kombos_msgs/mesureKinect.h>
#include <kombos_msgs/rulaArray.h>
#include "../include/Network.hpp"
#include "../include/Utils.hpp"
#include "../include/Node.hpp"

using namespace std;
using namespace kombos_msgs;
using namespace Eigen;

namespace prog_kinect
{
	class NodeGetRULA : public Node
	{
		private:
			map<string, double> states;
			map<string, double> offsets;

			void get_indice(const mesureKinect::ConstPtr& message)
			{
				kalmanState state = message->kalman_state;
				for(int i = 0; i < state.name.size(); ++i)
				{
					states[state.name[i]] = state.value[i];
				}

				map<string, Vector3f> positions;
				for(int i = 0; i < message->mesures.size(); ++i)
				{
					position_msg position = message->mesures[i];
					positions[position.name] = Vector3f(position.position.x, position.position.y, position.position.z);
				}
				float diff_height_feet = positions[JOINT_NAME_FOOT_RIGHT][1] - positions[JOINT_NAME_FOOT_LEFT][1];
				bool right_foot = true, left_foot = true;

				if(diff_height_feet < -0.05)
				{
					left_foot = false; // RIGHT FOOT ON FLOOR
				}
				else if(diff_height_feet > 0.05)
				{
					right_foot = false; // LEFT FOOT ON FLOOR
				}

				int arm_upper_right = get_indice_arm_upper(get_angle("repauleE"), get_angle("repauleE") > RULA_THRESHOLD_SHOULDER_ABDUCTION, false);
				int arm_lower_right = get_indice_arm_lower(get_angle("rcoudeFE"), get_angle("repaulePE") > RULA_THRESHOLD_SHOULDER_CROSSING);
				int arm_wrist_right = get_indice_arm_wrist(get_angle("rpoignetFE"), get_angle("rpoignetE") > RULA_THRESHOLD_WRIST_ROTATION);
				int arm_wrist_twist_right = 1;
				float arm_right = RULA_TABLE_A[(int)(arm_upper_right - 1)][(int)(arm_lower_right - 1)][(int)(arm_wrist_right - 1)] + arm_wrist_twist_right;

				int arm_upper_left = get_indice_arm_upper(get_angle("lepauleE"), get_angle("lepauleE") > RULA_THRESHOLD_SHOULDER_ABDUCTION, false);
				int arm_lower_left = get_indice_arm_lower(get_angle("lcoudeFE"), get_angle("lepaulePE") > RULA_THRESHOLD_SHOULDER_CROSSING);
				int arm_wrist_left = get_indice_arm_wrist(get_angle("lpoignetFE"), get_angle("lpoignetE") > RULA_THRESHOLD_WRIST_ROTATION);
				int arm_wrist_twist_left = 0;
				float arm_left = RULA_TABLE_A[(int)(arm_upper_left - 1)][(int)(arm_lower_left - 1)][(int)(arm_wrist_left - 1)] + arm_wrist_twist_left;

				int neck_angle = -get_angle("teteFE", false);
				int neck = get_indice_neck(neck_angle, get_angle("teteIL"), (neck_angle < 0));
				int trunk = get_indice_trunk(get_angle("thoraxFE"), get_angle("thoraxIL") > RULA_THRESHOLD_TRUNK_BENT);
				int legs;
				if(left_foot && right_foot) legs = 1;
				else legs = 2;

				float mid = RULA_TABLE_B[(int)(neck - 1)][(int)(trunk - 1)] + legs;
				float right = arm_right + mid;
				float left = arm_left + mid;

				rulaArray message_indices;
				message_indices.name =
				{
					"Head",
					"Spine",
					"ShoulderRight",
					"ElbowRight",
					"WristRight",
					"ShoulderLeft",
					"ElbowLeft",
					"WristLeft",
					"RulaRight",
					"RulaLeft",
          			"Rula"
				};
				message_indices.indice =
				{
					(float)neck,
					(float)trunk,
					(float)arm_upper_right,
					(float)arm_lower_right,
					(float)arm_wrist_right,
					(float)arm_upper_left,
					(float)arm_lower_left,
					(float)arm_wrist_left,
					(float)right,
					(float)left,
          			(float)max(right, left)
				};

				publisher_send(ROS_TOPIC_INDICE_RULA, message_indices);
			}

			double get_angle(const string& state, const bool& absolute = true)
			{
				double angle = Utils::rad2deg(states[state]) + offsets[state];
				return absolute ? abs(angle) : angle;
			}

			int get_indice_interval(const double& angle, const vector<double> bounds, const vector<double> scores)
			{
				double score = 1;

				if(scores.size() == bounds.size() - 1)
				{
					for(int i = 0; i < bounds.size() - 1; ++i)
					{
						double bound_min = bounds[i];
						double bound_max = bounds[i + 1];
						if(angle >= bound_min && angle < bound_max)
						{
							score = scores[i];
						}
					}
				}
				else
				{
					ROS_ERROR(RULA_MESSAGE_INTERVALS_INCONSISTENT, scores.size(), bounds.size() - 1);
				}

				return score;
			}

			int get_indice_arm_upper(const double& angle_shoulder_flex, const bool& is_abducted, const bool& is_leaned)
			{
				int score = get_indice_interval(angle_shoulder_flex, {0.0, 20.0, 45.0, 90.0, 180.0}, {1, 2, 3, 4});
				score += is_abducted ? 1 : 0;
				score += is_leaned ? 1 : 0;

				return score;
			}

			int get_indice_arm_lower(const double& angle_elbow_flex, const bool& is_crossing)
			{
				int score = get_indice_interval(angle_elbow_flex, {0.0, 50.0, 110.0, 180.0}, {2, 1, 2});
				score += is_crossing ? 1 : 0;

				return score;
			}

			int get_indice_arm_wrist(const double& angle_wrist_flex, const bool& is_bent)
			{
				int score = get_indice_interval(angle_wrist_flex, {0.0, 6.0, 15.0, 90.0}, {1, 2, 3});
				score += is_bent ? 1 : 0;

				return score;
			}

			int get_indice_neck(const double& angle_neck_flex, const double& angle_neck_bent, const bool& is_backwards)
			{
				int score = !is_backwards ? get_indice_interval(angle_neck_flex, {0.0, 10.0, 20.0, 90.0}, {1, 2, 3}) : 4;
				score += get_indice_interval(angle_neck_bent, {0.0, 6.0, 20.0}, {0, 1});

				return score;
			}

			int get_indice_trunk(const double& angle_trunk_flex, const bool& is_bent)
			{
				int score = get_indice_interval(angle_trunk_flex, {0.0, 4.0, 10.0, 20.0, 60.0}, {1, 2, 3, 4});
				score += is_bent ? 1 : 0;

				return score;
			}

		public:
			NodeGetRULA(int argc, char** argv):
			Node(ROS_NODE_GET_RULA_NAME, ROS_NODE_GET_RULA_RATE, argc, argv)
			{
				subscriber_add<mesureKinect>(ROS_TOPIC_PC_CLASSIFIED, &NodeGetRULA::get_indice, 0);

				publisher_add<rulaArray>(ROS_TOPIC_INDICE_RULA, 0);
			}

			void on_start()
			{
				offsets =
				{
					{ "lepauleE", -30},
					{ "repauleE", 30},
					{ "teteFE", -10},
					{ "teteIL", -2},
					{ "thoraxIL", 1}
				};
			}

			void on_update()
			{

			}

			void on_end()
			{

			}
	};
}

int main(int argc, char** argv)
{
	return (prog_kinect::NodeGetRULA(argc, argv)).run();
}
