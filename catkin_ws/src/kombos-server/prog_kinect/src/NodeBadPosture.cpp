/* EN GRANDE PARTIE OBSOLETE
SI ON LE REUTILISE IL FAUT PENSER A VERIFIER
LES MEMBRES DE LA CLASSE MERE AVEC JSON2CLOUD
*/


#include <Eigen/Core>
#include <Eigen/Geometry>
#include <kombos_msgs/mesureKalman.h>
#include "../include/Network.hpp"
#include "../include/Utils.hpp"
#include "../include/json2model.h"
#include "../include/Node.hpp"

using namespace std;
using namespace ros;
using namespace Eigen;
using namespace kombos_msgs;

namespace prog_kinect
{
	class NodeBadPosture : public Node
	{
		private:
			map<string, Vector3f> joints_positions;
			map<string, Vector3f> joints_speeds;
			map<string, bool> joints_fiabilites;
			map<string, float> states;
			/*vector<vector<string>> joints_positions_triplets_members;
			vector<vector<string>> joints_positions_triplets_bases;*/
			vector<string> length_list;
			map<string, vector<float>> length_buffer;
			map<string, float> length_mean;
			map<string, float> length_std;
			map<string, float> angle_lim_max;
			map<string, float> angle_lim_min;
			map<string, float> length_lim_max;
			map<string, float> length_lim_min;
			map<string, pair<string,string>> length_extremity;
			map<string, string> length_correspondance;
			int buffer_size = 100;


			void posture_filter(const mesureKalman::ConstPtr& msg)
			{
				string kinect_name = msg->data.typeCapteur;

				for (int i = 0; i < msg->data.mesures.size(); ++i)
				{
					position_msg position = msg->data.mesures[i];
					joints_positions[position.name] = Vector3f(position.position.x, position.position.y, position.position.z);
					joints_fiabilites[position.name] = position.fiabilite;

					if(msg->data.vitesses.size() > 0)
					{
						position_msg speed = msg->data.vitesses[i];
						joints_speeds[speed.name] = Vector3f(speed.position.x, speed.position.y, speed.position.z);
					}
				}
				if(length_buffer["humerus"].size() > buffer_size-10) length_check();
			}

			void length_buffering(const mesureKalman::ConstPtr& msg)
			{
				string kinect_name = msg->data.typeCapteur;
				kalmanState state_msg = msg->data.kalman_state;

				for (int i = 0; i < state_msg.value.size(); ++i)
				{
					states[state_msg.name[i]] = state_msg.value[i];
				}

				float sum, sq_sum;
				for(vector<string>::iterator it = length_list.begin(); it != length_list.end(); ++it)
				{
					if(length_buffer[*it].size() > buffer_size) length_buffer[*it].erase(length_buffer[*it].begin());
					length_buffer[*it].push_back(states[*it]);
					// MEAN
					sum = accumulate(length_buffer[*it].begin(), length_buffer[*it].end(), 0.0);
					length_mean[*it] = sum / length_buffer[*it].size();
					// STD
					sq_sum = inner_product(length_buffer[*it].begin(), length_buffer[*it].end(), length_buffer[*it].begin(), 0.0);
					length_std[*it] = sq_sum / length_buffer[*it].size() - (length_mean[*it] * length_mean[*it]);

					//ROS_INFO_STREAM("LONGUEUR " << *it << ": \t mean = " << length_mean[*it] << "& std = " << length_std[*it]);
				}
				length_mean["pelvis"] = Vector3f(length_mean["pelvisA"], length_mean["pelvisL"], length_mean["pelvisML"]).norm();
			}

			float length_check()
			{

				map<string,float> l;
			  for(map<string, pair<string,string>> ::iterator it = length_extremity.begin(); it != length_extremity.end(); it++)
			  {
			  	l[it->first] = sqrt(pow(joints_positions[it->second.first][0] - joints_positions[it->second.second][0],2) + pow(joints_positions[it->second.first][1] - joints_positions[it->second.second][1],2) + pow(joints_positions[it->second.first][2] - joints_positions[it->second.second][2],2));
			  }

			  for(map<string,string>::iterator it = length_correspondance.begin(); it != length_correspondance.end(); it++)
			  {
			  	float delta = (30 * length_mean[it->second])/100;
			  	if(l[it->first] < length_mean[it->second] - delta || l[it->first] > length_mean[it->second] + delta)
			  	{
			  		ROS_INFO_STREAM("LONGUEUR : " << it->first << " = " << l[it->first] << " // min = "<< length_mean[it->second] - delta << " & max = " << length_mean[it->second] + delta );
			  	}
			  }

			}

		public:
			EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
			NodeBadPosture(int argc, char** argv):
			Node(ROS_NODE_BADPOSTURE_NAME, ROS_NODE_BADPOSTURE_RATE, argc, argv)
			{
				subscriber_add<mesureKalman>(ROS_TOPIC_PC_KALMAN_TF, &NodeBadPosture::length_buffering);
				subscriber_add<mesureKalman>(ROS_TOPIC_PC_FUSION_TF, &NodeBadPosture::posture_filter);

				//publisher_add<mesureKalman>(ROS_TOPIC_PC_CORRECTED_OE_FILTERED);
			}

			void on_start()
			{
				json2model model = json2model();
				length_list = model.GetLength();
				length_lim_min = model.GetLengthLimitMinMax(true);
				length_lim_max = model.GetLengthLimitMinMax(false);
				angle_lim_min = model.GetAngleLimitMinMax(true);
				angle_lim_max = model.GetAngleLimitMinMax(false);

				/*joints_positions_triplets_members = {
					{JOINT_NAME_SHOULDER_RIGHT, JOINT_NAME_ELBOW_RIGHT, JOINT_NAME_HAND_RIGHT},
					{JOINT_NAME_SHOULDER_LEFT, JOINT_NAME_ELBOW_LEFT, JOINT_NAME_HAND_LEFT},
					{JOINT_NAME_HIP_RIGHT, JOINT_NAME_KNEE_RIGHT, JOINT_NAME_ANKLE_RIGHT},
					{JOINT_NAME_HIP_LEFT, JOINT_NAME_KNEE_LEFT, JOINT_NAME_ANKLE_LEFT}
				}

				joints_positions_triplets_bases = {
					{JOINT_NAME_SHOULDER_RIGHT, JOINT_NAME_SPINESHOULDER, JOINT_NAME_SHOULDER_LEFT},
					{JOINT_NAME_SPINESHOULDER, JOINT_NAME_SPINEMID, JOINT_NAME_SPINEBASE},
					{JOINT_NAME_HEAD, JOINT_NAME_NECK, JOINT_NAME_SPINESHOULDER},
					{JOINT_NAME_HIP_RIGHT, JOINT_NAME_SPINEBASE, JOINT_NAME_HIP_LEFT}
				}*/
				length_extremity = {
					{"rhumerus", {JOINT_NAME_ELBOW_RIGHT, JOINT_NAME_SHOULDER_RIGHT}},
					{"lhumerus", {JOINT_NAME_ELBOW_LEFT, JOINT_NAME_SHOULDER_LEFT}},
					{"rradius", {JOINT_NAME_WRIST_RIGHT, JOINT_NAME_ELBOW_RIGHT}},
					{"lradius", {JOINT_NAME_WRIST_LEFT, JOINT_NAME_ELBOW_LEFT}},
					{"rmain", {JOINT_NAME_HAND_RIGHT, JOINT_NAME_WRIST_RIGHT}},
					{"lmain", {JOINT_NAME_HAND_LEFT, JOINT_NAME_WRIST_LEFT}},
					{"rclavicule", {JOINT_NAME_SHOULDER_RIGHT, JOINT_NAME_SPINESHOULDER}},
					{"lclavicule", {JOINT_NAME_SHOULDER_LEFT, JOINT_NAME_SPINESHOULDER}},
					{"thorax", {JOINT_NAME_SPINESHOULDER, JOINT_NAME_SPINEMID}},
					{"abdomen", {JOINT_NAME_SPINEMID, JOINT_NAME_SPINEBASE}},
					{"tete", {JOINT_NAME_HEAD, JOINT_NAME_SPINESHOULDER}},
					{"rpelvis", {JOINT_NAME_HIP_RIGHT, JOINT_NAME_SPINEBASE}},
					{"lpelvis", {JOINT_NAME_HIP_LEFT, JOINT_NAME_SPINEBASE}},
					{"rfemur", {JOINT_NAME_KNEE_RIGHT, JOINT_NAME_HIP_RIGHT}},
					{"lfemur", {JOINT_NAME_KNEE_LEFT, JOINT_NAME_HIP_LEFT}},
					{"rtibia", {JOINT_NAME_ANKLE_RIGHT, JOINT_NAME_KNEE_RIGHT}},
					{"ltibia", {JOINT_NAME_ANKLE_LEFT, JOINT_NAME_KNEE_LEFT}},
				};

				length_correspondance = {
					{"rhumerus", "humerus"},
					{"lhumerus", "humerus"},
					{"rradius", "radius"},
					{"lradius", "radius"},
					{"rmain", "main"},
					{"lmain", "main"},
					{"rclavicule", "clavicule"},
					{"lclavicule", "clavicule"},
					{"thorax", "thorax"},
					{"abdomen", "abdomen"},
					{"tete", "tete"},
					{"rpelvis", "pelvis"},
					{"lpelvis", "pelvis"},
					{"rfemur", "femur"},
					{"lfemur", "femur"},
					{"rtibia", "tibia"},
					{"ltibia", "tibia"}
				};
				/*
				// creation nom longueur
				lengthName["rhumerus"] = "humerus";
				lengthName["lhumerus"] = "humerus";
				lengthName["rradius"] = "radius";
				lengthName["lradius"] = "radius";
				lengthName["rmain"] = "main";
				lengthName["lmain"] = "main";
				lengthName["rclavicule"] = "clavicule";
				lengthName["lclavicule"] = "clavicule";
				lengthName["thorax"] = "thorax";
				lengthName["abdomen"] = "abdomen";
				lengthName["rpelvis"] = "pelvis";
				lengthName["lpelvis"] = "pelvis";
				lengthName["rfemur"] = "femur";
				lengthName["lfemur"] = "femur";
				lengthName["rtibia"] = "tibia";
				lengthName["ltibia"] = "tibia";
				lengthName["tete"] = "tete";
				lengthName["bebetete"] = "bebetete";*/
			}

			void on_update()
			{

			}

			void on_end()
			{

			}
		};
}

int main(int argc, char** argv)
{
	return (prog_kinect::NodeBadPosture(argc, argv)).run();
}

/* EN GRANDE PARTIE OBSOLETE
SI ON LE REUTILISE IL FAUT PENSER A VERIFIER
LES MEMBRES DE LA CLASSE MERE AVEC JSON2CLOUD
*/
