#include <Eigen/Geometry>
#include <kombos_msgs/mesureKinect.h>
#include <kombos_msgs/mesureKalman.h>
#include <kombos_msgs/anthropo.h>
#include "../include/Network.hpp"
#include "../include/Utils.hpp"
#include "../include/Node.hpp"

using namespace std;
using namespace std::chrono;
using namespace kombos_msgs;
using namespace Eigen;

namespace prog_kinect
{
	class NodeGetHeight : public Node
	{
		private:
			const int min_size_warn = 200;
			const int max_size_list = 150;

			vector<float> size_list;
			vector<string> size_segments;
			map<string, float> proportion_limb;
			map<string, string> name_correlation;

			int cpt_ind_properties = 0;
			int cpt_male = 0;
			int cpt_neutral = 0;
			int cpt_angry = 0;
			int cpt_happy = 0;
			int cpt_surprise = 0;
			float average_age = 0;

			float individual_size = 0;
			int cpt_outlier_size = 0;
			bool outlier_activated = false;

			void get_indice(const mesureKinect::ConstPtr& message)
			{
				int n = message->kalman_state.name.size();
				map<string, float> states;
				for(int i = 0; i < n; i++)
				{
					states[message->kalman_state.name[i]] = message->kalman_state.value[i];
				}

				float mean_size = size_calculation(states);

        // Moyenne cumulee avec des valeurs 'coherentes'
        if(mean_size < MAX_HUMAN_SIZE && mean_size > MIN_HUMAN_SIZE)
        {
	        if(individual_size == 0) {
	        	size_list.push_back(mean_size);
	        	individual_size = mean_size;
	        }
	        else {
	        	if(size_consistency(mean_size, 5/100.0)){
		        	if(size_list.size() > max_size_list) size_list.erase(size_list.begin());
		        	size_list.push_back(mean_size);
		        	individual_size = accumulate( size_list.begin(), size_list.end(), 0.0)/size_list.size();
		        }
	        }
        }

        anthropo message_anth;
        message_anth.size = individual_size;
        if(individual_size != 0) publisher_send(ROS_TOPIC_ANTHROPO, message_anth);
			}

			float size_calculation(map<string, float> length_map)
			{
				float mean_size = 0;
				int cpt = 0;

				// Methode proportion
				for(map<string, float>::iterator it = proportion_limb.begin(); it != proportion_limb.end(); it++)
				{
					mean_size += length_map[it->first] / it->second;
					cpt++;
				}

				// Methode size with length height
				float size_tmp = 0;
        for (vector<string>::iterator it = size_segments.begin(); it != size_segments.end(); it++ )
        {
        	size_tmp += length_map[*it];
        }
        size_tmp += 0.25;
        mean_size += size_tmp;
        mean_size /= (cpt + 1);

        return mean_size;
			}

			bool size_consistency(float size_estimate, float factor)
			{
				if(size_list.size() > max_size_list - 10)
				{
					float diff = abs(individual_size - size_estimate);
					float threshold = individual_size * factor;

					if(diff < threshold) {
						return true;
					}
					else {
						return false;
					}
				}
				else return true;
			}

			void warning_new_people(const mesureKalman::ConstPtr& message)
			{
				map<string, Vector3f> joints_positions;
				map<string, float> limb_length;

				bool is_valid = true;
				int cpt_fiabilite = 0;
				for (int i = 0; i < message->data.mesures.size(); ++i)
				{
					position_msg position = message->data.mesures[i];
					if(is_valid && !isnan(position.position.x) && !isnan(position.position.y) && !isnan(position.position.z))
					{
						joints_positions[position.name] = Vector3f(position.position.x, position.position.y, position.position.z);
						cpt_fiabilite += position.fiabilite;
					}
					else is_valid = false;
				}

				if(cpt_fiabilite > 15)
				{
					float length = 0;
					for(map<string, string>::iterator it = name_correlation.begin(); it != name_correlation.end(); it++)
					{
						limb_length[it->first] = Utils::length_calculation(joints_positions, limb_extremity[it->second].first, limb_extremity[it->second].second);
					}

					float mean_size = size_calculation(limb_length);

					if(mean_size < MAX_HUMAN_SIZE && mean_size > MIN_HUMAN_SIZE)
	        {
	        	if(size_consistency(mean_size, 10/100.0)){
		        	if(outlier_activated) {
		        		ROS_WARN_STREAM(" >>>>>>>>>>> " << cpt_outlier_size << "<<<<<<<<<<<<");
		        		outlier_activated = false;
		        		cpt_outlier_size = 0;
		        	}
		        }
		        else {
		        	if(!outlier_activated){
		        		outlier_activated = true;
		        		cpt_outlier_size++;
		        	}
		        	else{
		        		cpt_outlier_size++;
		        	}
		        }
		      }

	        if(cpt_outlier_size > min_size_warn)
	        {
	        	ROS_ERROR_STREAM("Maybe not the same person in front of camera");
	        	stringstream ss;
						ss << "{\"user\":" << false << "}";
						string str = ss.str();
						_redox.set("Warning_user", str);
	        }
	      }
			}

			void initialize_size()
			{
				size_list.clear();
			}

		public:
			EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
			NodeGetHeight(int argc, char** argv):
			Node(ROS_NODE_GET_HEIGHT_NAME, ROS_NODE_GET_HEIGHT_RATE, argc, argv)
			{
				subscriber_add<control_server_cmd>(ROS_TOPIC_RECORD, &NodeGetHeight::server_toggle);
				subscriber_add<mesureKinect>(ROS_TOPIC_PC_CLASSIFIED, &NodeGetHeight::get_indice, 0);
				subscriber_add<mesureKalman>(ROS_TOPIC_PC_FUSION_TF, &NodeGetHeight::warning_new_people);

				publisher_add<anthropo>(ROS_TOPIC_ANTHROPO, 0);
			}

			void on_start()
			{
				redox_init(REDOX_HOST, REDOX_PORT);
			  size_segments =
				{
					"abdomen",
					"thorax",
					"tete",
					"pelvisA",
					"femur",
					"tibia"
				};

				proportion_limb = {
					{"humerus", 0.136},
					{"radius", 0.146},
					{"tibia", 0.246}
				};

				name_correlation =
				{
					{"abdomen","Abdomen"},
					{"thorax","Thorax"},
					{"tete","HeadNeck"},
					{"pelvisA","Pelvis"},
					{"femur","ThighLeft"},
					{"tibia","LegLeft"},
					{"humerus","UpperArmRight"},
					{"radius","ForeArmRight"}
				};
			}

			void on_update()
			{

			}

			void on_end()
			{
				_redox.disconnect();
			}

			void action_on_toggle()
			{
				initialize_size();
			}
	};
}

int main(int argc, char** argv)
{
	return (prog_kinect::NodeGetHeight(argc, argv)).run();
}
