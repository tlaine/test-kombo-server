#!/usr/bin/env python
import rospy

from kombos_msgs.msg import mesureKinect
from db.abstract_object import AbstractDBObject
from db.storer import InfluxStorer
from threading import RLock


class CollectorNode(object):
    """Class to wrap the classifier. Include functions to train the model and predict incomming postures as well as csv files."""

    def __init__(self):

        self.last_numii = None
        self.last_opti = None
        self.write_data = False
        self.key_tag = 'ai_key'
        self.lock = RLock()

        self.numii_listener = rospy.Subscriber("/PointCloudClassified", mesureKinect, self.handle_skeleton_numii_msgs)
        self.opt_listener = rospy.Subscriber("/Optitrack", mesureKinect, self.handle_skeleton_optitrack_msgs)

    def handle_skeleton_numii_msgs(self, msg):
        skeleton = {}
        with self.lock:
            for item in msg.messures:
                skeleton[item.name] = {'x': item.position.x,
                                       'y': item.position.y,
                                       'z': item.position.z}

            self.last_numii = skeleton
            self.write_data = True

    def handle_skeleton_optitrack_msgs(self, msg):
        skeleton = {}
        with self.lock:
            for item in msg.messures:
                skeleton[item.name] = {'x': item.position.x,
                                       'y': item.position.y,
                                       'z': item.position.z}
            self.last_opti = skeleton
            self.write_data = True

    def write_data(self):
        dt = InfluxStorer.get_timestamp()
        tag = {'ai': self.key_tag}
        with self.lock:
            self.last_opti['time'] = dt

            for item in self.last_numii:
                current_data = {'measurement': 'skeleton_ai'}

                current_data['time'] = dt
                current_data['tags'] = tag.copy()
                current_data['tags']['name'] = item

                current_data['fields'] = {}
                current_data['fields']['name'] = item
                current_data['fields']['xo'] = self.last_opti[item]['x']
                current_data['fields']['yo'] = self.last_opti[item]['y']
                current_data['fields']['zo'] = self.last_opti[item]['z']

                current_data['fields']['xn'] = self.last_numii[item]['x']
                current_data['fields']['yn'] = self.last_numii[item]['y']
                current_data['fields']['zn'] = self.last_numii[item]['z']

                AbstractDBObject.bufferize(current_data)
            self.write_data = False

    def run(self):
        rate = rospy.Rate(30)
        while not rospy.is_shutdown():
            if self.write_data:
                self.write_data()
            rate.sleep()


if __name__ == '__main__':
    rospy.init_node("collector_node")
    node = CollectorNode()
    node.run()
