#include <Eigen/Geometry>
#include <jansson.h>
#include <kombos_msgs/control_server_cmd.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Quaternion.h>
#include <kombos_msgs/mesureKinect.h>
#include <kombos_msgs/position_msg.h>
#include <kombos_msgs/quaternion_msg.h>
#include <kombos_msgs/segment_nb.h>
#include <kombos_msgs/sound.h>
#include "../include/Network.hpp"
#include "../include/Server.hpp"
#include "../include/JsonReader.hpp"
#include "../include/Node.hpp"
#include <unistd.h>


using namespace std;
using namespace std::chrono;
using namespace boost;
using namespace ros;
using namespace Eigen;
using namespace geometry_msgs;
using namespace kombos_msgs;

namespace prog_kinect
{
	class NodeJson2Opti : public Node
	{
		private:
			Server server_optitrack;
			bool is_viewing = true;
			time_point<system_clock> clock_timeout = system_clock::now();

			void server_read(string server_data)
			{
				if(server_data.size() > 0)
				{
					json_error_t error;
					json_t* json = json_loads(server_data.c_str(), 0, &error);

					if(!json)
					{
						json = NULL;
						ROS_ERROR_ONCE(JSON_MESSAGE_INVALID, server_data, error.line, error.position, error.text);
					}
					else if(!json_is_object(json))
					{
						json_decref(json);
						json = NULL;
						ROS_ERROR_ONCE(JSON_MESSAGE_NOTOBJECT, server_data);
					}
					else
					{
						//usleep(15000); // slow down frequency of optitrack data -> reduce lag
						server_parse(json);
						json_decref(json);
						is_viewing = true;
					}
				}
				else
				{
					if(is_viewing)
					{
						is_viewing = false;
						clock_timeout = system_clock::now();
					}

					if (duration_cast<seconds>(system_clock::now() - clock_timeout).count() > SERVER_TIMEOUT_SECONDS)
					{
						ROS_WARN_ONCE(SERVER_MESSAGE_NODATA, SERVER_TIMEOUT_SECONDS);
					}
				}
			}

			void server_parse(json_t* json_root, bool parse_sound = false)
			{
				mesureKinect message;
				position_msg message_position;
				position_msg message_speed;
				quaternion_msg message_quaternion;

				const char* key_message_type;
				json_t* value_message_content;
				json_object_foreach(json_root, key_message_type, value_message_content)
				{
					if(parse_sound && strcmp(key_message_type, JSON_KEY_SOUND) == 0)
					{
						int decibel = json_integer_value(value_message_content);
						if (decibel > JSON_VALUE_SOUND_MIN  && decibel < JSON_VALUE_SOUND_MAX)
						{
							sound message_sound;
							message_sound.indice = decibel;
							publisher_send(ROS_TOPIC_INDICE_SOUND, message_sound);
						}
					}
					else
					{
						int fiabilite_total = 0;
						int id_kinect = atoi(key_message_type);
						message.typeCapteur = string(key_message_type);

						const char* key_joint_name;
						json_t* value_joint_properties;
						
						json_object_foreach(value_message_content, key_joint_name, value_joint_properties)
						{
							const char* key_property_name;
							json_t* value_property_name;
							json_object_foreach(value_joint_properties, key_property_name, value_property_name)
							{
								if(strcmp(key_property_name, JSON_KEY_POSITION) == 0)
								{
									Point point;
									point.x = json_real_value(json_object_get(value_property_name, "x"));
									point.y = json_real_value(json_object_get(value_property_name, "y"));
									point.z = json_real_value(json_object_get(value_property_name, "z"));
									message_position.position = point;
									message_position.name = key_joint_name;
									message.mesures.push_back(message_position);
								}
								if(strcmp(key_property_name, JSON_KEY_ORIENTATION) == 0)
								{
									geometry_msgs::Quaternion quaternion;
									quaternion.w = json_real_value(json_object_get(value_property_name, "w"));
									quaternion.x = json_real_value(json_object_get(value_property_name, "x"));
									quaternion.y = json_real_value(json_object_get(value_property_name, "y"));
									quaternion.z = json_real_value(json_object_get(value_property_name, "z"));
									message_quaternion.quaternion = quaternion;
									message_quaternion.name = key_joint_name;

									message.orientations.push_back(message_quaternion);
								}
								else if(strcmp(key_property_name, JSON_KEY_SPEED) == 0)
								{
									Point point;
									point.x = json_real_value(json_object_get(value_property_name, "x"));
									point.y = json_real_value(json_object_get(value_property_name, "y"));
									point.z = json_real_value(json_object_get(value_property_name, "z"));
									message_speed.position = point;
									message_speed.name = key_joint_name;

									message.vitesses.push_back(message_speed);
								}
								else if(strcmp(key_property_name, JSON_KEY_TRACKINGSTATE) == 0)
								{
									int fiabilite = json_integer_value(value_property_name);
									message_position.fiabilite = fiabilite;
									message_speed.fiabilite = fiabilite;
									fiabilite_total += fiabilite;
								}
								else if(strcmp(key_property_name, JSON_KEY_BODYTRACKINGID) == 0)
								{
									message.idCapteur = json_integer_value(value_property_name);
								}
							}
						}
						
						if(fiabilite_total >= 5) /*(JSON_VALUE_TRACKINGSTATE_NUMBER * JSON_VALUE_TRACKINGSTATE_TOLERANCE))*/
						{
							string topic = string(ROS_TOPIC_O);
							publisher_send(topic.c_str(), message);
						}
					}
				}
			}

		public:
			NodeJson2Opti(int argc, char** argv):
			Node(ROS_NODE_JSON2CLOUD_NAME, ROS_NODE_JSON2CLOUD_RATE, argc, argv)
			{
				//subscriber_add<control_server_cmd>(ROS_TOPIC_RECORD, &NodeJson2Opti::server_toggle);

				publisher_add<mesureKinect>(ROS_TOPIC_O);
			}

			void on_start()
			{
				server_optitrack.init(SERVER_PORT_OPTITRACK);
				ROS_INFO(SERVER_MESSAGE_START, SERVER_NAME_OPTITRACK, SERVER_PORT_OPTITRACK);

			}

			void on_update()
			{
				string server_data = server_optitrack.retrieve();

				if(server_data.size() > 0 /*&& server_is_recording*/)
				{
					server_read(server_data);
				}
			}

			void on_end()
			{

			}
	};
}

int main(int argc, char** argv)
{
	return (prog_kinect::NodeJson2Opti(argc, argv)).run();
}
