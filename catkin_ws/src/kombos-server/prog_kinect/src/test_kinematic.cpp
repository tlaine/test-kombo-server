#include "ros/ros.h"
#include "std_msgs/String.h"
#include <Eigen/Geometry>
#include <Eigen/Core>
#include <vector>
#include <numeric>

#include "../include/ekf.h"
#include "../include/jacobianKinect.h"
#include <kombos_msgs/orientation_msg.h>
#include <kombos_msgs/mlongueur.h>
#include <kombos_msgs/kalmanState.h>
#include <kombos_msgs/mesureKinect.h>

static int dim_x = 49, dim_y = 57;

static std::map< std::string, Eigen::Vector3d > jointsPos;
static std::map< std::string, float > jointsO;
static std::map< std::string, float > jointsL;
static std::map< std::string, int > joint_idx;
static bool bo = false, bl = false, init = false;
static jacobianKinect j = jacobianKinect();

EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

void callbackPC(const kombos_msgs::mesureKinect::ConstPtr & msg)
{
 	// recup msg
	std::map< std::string, bool > fiabilites;
	unsigned int n = msg->mesures.size();
	for ( int i = 0; i<n; i++ )
	{
		fiabilites[msg->mesures[i].name] = msg->mesures[i].fiabilite;
		jointsPos[msg->mesures[i].name] = Eigen::Vector3d( msg->mesures[i].position.x,
		            msg->mesures[i].position.y,
		            msg->mesures[i].position.z);
	}

	// init kalman
	if(bo && bl)
	{
		// create etat init
		Eigen::VectorXd x(dim_x);
		Eigen::VectorXd h;
		Eigen::VectorXd y(dim_y);

		x << jointsPos["SpineBase"][0],jointsPos["SpineBase"][1],jointsPos["SpineBase"][2],jointsO["abdomenX"],jointsO["abdomenZ"],jointsO["thoraxY"],jointsO["teteX"],jointsO["teteZ"],jointsO["rhumerusY0"],jointsO["rhumerusZ"],jointsO["rhumerusY1"],jointsO["rradiusX"],jointsO["rmainX"],jointsO["rmainZ"],jointsO["lhumerusY0"],jointsO["lhumerusZ"],jointsO["lhumerusY1"],jointsO["lradiusX"],jointsO["lmainX"],jointsO["lmainZ"],jointsO["rfemurX"],jointsO["rfemurZ"],jointsO["rfemurY"],jointsO["rtibiaX"],jointsO["lfemurX"],jointsO["lfemurZ"],jointsO["lfemurY"],jointsO["ltibiaX"],jointsL["abdomen"],jointsL["thorax"], jointsL["bebecou"], jointsL["cou"],jointsL["clavicule"],jointsL["humerus"],jointsL["radius"], jointsL["main"],jointsL["pelvis"],jointsL["femur"],jointsL["tibia"];

		for(std::map<std::string,int>::iterator iterator = joint_idx.begin(); iterator != joint_idx.end(); iterator++)
		{
			for(int j=0; j<3; j++)
				y((iterator->second*4)+j) = jointsPos[iterator->first][j];
			y((iterator->second*4)+3) = 1;
		}

		h = j.eqFunction(x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7], x[8], x[9], x[10], x[11], x[12], x[13], x[14], x[15], x[16], x[17], x[18], x[19], x[20], x[21], x[22], x[23], x[24], x[25], x[26], x[27], x[28], x[29], x[30], x[31], x[32], x[33], x[34], x[35], x[36], x[37], x[38], x[39], x[40], x[41], x[42], x[43], x[44], x[45], x[46], x[47], x[48]);

		Eigen::VectorXd diff = y - h;
		Eigen::VectorXd delta = Eigen::VectorXd::Zero(dim_y/4);

		for(int i=0; i < dim_y/4 ; i++)
		{
			delta[i] = sqrt(pow(diff[(i*4)],2) + pow( diff[(i*4)+1],2) + pow( diff[(i*4)+2],2));
		}

		//if(delta[7] >= 0.6)	ROS_WARN_STREAM("delta vector : " << delta[7]);

		bo = bl = false;
	}

}

void callbackO(const kombos_msgs::orientation_msg::ConstPtr & msg)
{
	unsigned int n = msg->name.size();
  for ( int i = 0; i<n; i++ )
  {
		jointsO[msg->name[i]] = msg->orientation[i];
  }
  bo = true;
}

void callbackL(const kombos_msgs::mlongueur::ConstPtr & msg)
{
	unsigned int n = msg->name.size();
	for ( int i = 0; i<n; i++ )
	{
		jointsL[msg->name[i]] = msg->valeur[i];
	}
	bl = true;
}


int main(int argc, char **argv)
{
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  ros::init(argc, argv, "test_kinematic");
  ros::NodeHandle nh;

  // sub
  ros::Subscriber sub1 = nh.subscribe("/Orientation1", 1, callbackO);
  ros::Subscriber sub2 = nh.subscribe("/Longueur1", 1, callbackL);
  ros::Subscriber sub3 = nh.subscribe("/PointCloud1", 1, callbackPC);

	// Initiate MEASURE msg
	int i = 0;
	joint_idx["SpineBase"] = i; i++;
	joint_idx["SpineMid"] = i; i++;
	joint_idx["SpineShoulder"] = i; i++;
	joint_idx["Neck"] = i; i++;
	joint_idx["Head"] = i; i++;
	joint_idx["ShoulderRight"] = i; i++;
	joint_idx["ElbowRight"] = i; i++;
	joint_idx["WristRight"] = i; i++;
	joint_idx["HandRight"] = i; i++;
	joint_idx["ShoulderLeft"] = i; i++;
	joint_idx["ElbowLeft"] = i; i++;
	joint_idx["WristLeft"] = i; i++;
	joint_idx["HandLeft"] = i; i++;
	joint_idx["HipRight"] = i; i++;
	joint_idx["KneeRight"] = i; i++;
	joint_idx["AnkleRight"] = i; i++;
	joint_idx["HipLeft"] = i; i++;
	joint_idx["KneeLeft"] = i; i++;
	joint_idx["AnkleLeft"] = i;

  ros::Rate loop_rate(50) ;

  while ( ros::ok() )
  {

		ros::spinOnce();
		loop_rate.sleep() ;
	}

  return 0;
}
