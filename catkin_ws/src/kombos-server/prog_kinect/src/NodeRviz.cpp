#include <Eigen/Core>
#include <Eigen/Geometry>
#include <kombos_msgs/mesureKinect.h>
#include <kombos_msgs/mesureKalman.h>
#include <kombos_msgs/rulaArray.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include "../include/Network.hpp"
#include "../include/Utils.hpp"
#include "../include/Node.hpp"

using namespace std;
using namespace ros;
using namespace Eigen;
using namespace kombos_msgs;
using namespace visualization_msgs;

namespace prog_kinect
{
	class NodeRviz : public Node
	{
		private:
			map<string, int> rula_indices;


			/* -------------------------------------------------- */
			/*                     TOOLS                          */
			/* -------------------------------------------------- */

			float vectorDotDepth(const Marker arrow)
			{
				Vector3d v(arrow.points[1].x-arrow.points[0].x,arrow.points[1].y-arrow.points[0].y,arrow.points[1].z-arrow.points[0].z);
				Vector3d w(0,0,1);
				return (v.normalized().dot(w))*(v.normalized().dot(w));
			}

			/* -------------------------------------------------- */
			/*                CREATE MARKER                       */
			/* -------------------------------------------------- */

			MarkerArray get_markers(const mesureKinect& message, double r, double g, double b, double a, bool has_axes = false, bool has_text = false, bool has_normal = false)
			{
				rulaArray indices_msg;
				MarkerArray markers;
				map<string, Marker> joints;
				map<string, position_msg> joints_pos;
				map<string, double> states;

				int size = message.mesures.size();
				int count_fiab = 0;
				for(int i = 0; i < size; ++i)
				{
					Marker marker = get_marker_joint(i, message.mesures[i], r, g, b, a);
					joints[marker.text] = marker;
					joints_pos[marker.text] = message.mesures[i];
					markers.markers.push_back(marker);
					count_fiab += message.mesures[i].fiabilite;
				}

				if(has_axes)
				{
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["Head"], joints["Neck"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["Neck"], joints["SpineShoulder"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["SpineShoulder"], joints["SpineMid"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["SpineShoulder"], joints["ShoulderRight"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["ShoulderRight"], joints["ElbowRight"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["ElbowRight"], joints["WristRight"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["WristRight"], joints["HandRight"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["HandRight"], joints["HandTipRight"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["HandRight"], joints["ThumbRight"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["SpineShoulder"], joints["ShoulderLeft"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["ShoulderLeft"], joints["ElbowLeft"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["ElbowLeft"], joints["WristLeft"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["WristLeft"], joints["HandLeft"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["HandLeft"], joints["HandTipLeft"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["HandLeft"], joints["ThumbLeft"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["SpineMid"], joints["SpineBase"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["SpineBase"], joints["HipRight"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["HipRight"], joints["KneeRight"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["KneeRight"], joints["AnkleRight"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["AnkleRight"], joints["FootRight"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["SpineBase"], joints["HipLeft"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["HipLeft"], joints["KneeLeft"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["KneeLeft"], joints["AnkleLeft"]), r, g, b, a));
					markers.markers.push_back(get_marker_axe(++size, make_pair(joints["AnkleLeft"], joints["FootLeft"]), r, g, b, a));
				}

				if(has_text)
				{
					for(int i = 0; i < message.kalman_state.name.size(); ++i)
					{
						states[message.kalman_state.name[i]] = message.kalman_state.value[i];
					}

					//markers.markers.push_back(get_marker_text(++size, to_string(Utils::rad2deg(states["repauleE"])), joints["ShoulderRight"], r, g, b, a));
					//markers.markers.push_back(get_marker_text(++size, to_string(Utils::rad2deg(states["rcoudeFE"])), joints["ElbowRight"], r, g, b, a));
					//markers.markers.push_back(get_marker_text(++size, to_string(Utils::rad2deg(states["rpoignetFE"])), joints["WristRight"], r, g, b, a));
					//markers.markers.push_back(get_marker_text(++size, to_string(Utils::rad2deg(states["lcoudeFE"])), joints["ElbowLeft"], r, g, b, a));
					//markers.markers.push_back(get_marker_text(++size, to_string(Utils::rad2deg(states["lepauleE"])), joints["ShoulderLeft"], r, g, b, a));
					//markers.markers.push_back(get_marker_text(++size, to_string(Utils::rad2deg(states["lpoignetFE"])), joints["WristLeft"], r, g, b, a));

					markers.markers.push_back(get_marker_text(++size, to_string(Utils::rad2deg(states["repaulePE"])), joints["ShoulderRight"], 60.0, r, g, b, a));
					markers.markers.push_back(get_marker_text(++size, to_string(Utils::rad2deg(states["lepaulePE"])), joints["ShoulderLeft"], 60.0, r, g, b, a));

					markers.markers.push_back(get_marker_text(++size, to_string(rula_indices["Head"]), joints["Neck"], 60.0, 0.0, 1.0, 1.0, a));
					markers.markers.push_back(get_marker_text(++size, to_string(rula_indices["Spine"]), joints["SpineMid"], 60.0, 0.0, 1.0, 1.0, a));
					markers.markers.push_back(get_marker_text(++size, to_string(rula_indices["ShoulderRight"]), joints["ShoulderRight"], 60.0, 0.0, 1.0, 1.0, a));
					markers.markers.push_back(get_marker_text(++size, to_string(rula_indices["ElbowRight"]), joints["ElbowRight"], 60.0, 0.0, 1.0, 1.0, a));
					markers.markers.push_back(get_marker_text(++size, to_string(rula_indices["WristRight"]), joints["WristRight"], 60.0, 0.0, 1.0, 1.0, a));
					markers.markers.push_back(get_marker_text(++size, to_string(rula_indices["ShoulderLeft"]), joints["ShoulderLeft"], 60.0, 0.0, 1.0, 1.0, a));
					markers.markers.push_back(get_marker_text(++size, to_string(rula_indices["ElbowLeft"]), joints["ElbowLeft"], 60.0, 0.0, 1.0, 1.0, a));
					markers.markers.push_back(get_marker_text(++size, to_string(rula_indices["WristLeft"]), joints["WristLeft"], 60.0, 0.0, 1.0, 1.0, a));
					markers.markers.push_back(get_marker_text(++size, "R=" + to_string(rula_indices["RulaRight"]), joints["HipRight"], 60.0, 0.0, 1.0, 1.0, a));
					markers.markers.push_back(get_marker_text(++size, "L=" + to_string(rula_indices["RulaLeft"]), joints["HipLeft"], 60.0, 0.0, 1.0, 1.0, a));

					/*if(message.posture.size() == 0)
					{
						markers.markers.push_back(get_marker_text(++size, "Undefined ("+to_string(0)+")" , joints["Head"], 80.0, 0.0, 1.0, 1.0, a));
					}
					else
					{
						stringstream stream;
						stream << fixed << setprecision(1) << (message.posture[0].probability * 100);
						markers.markers.push_back(get_marker_text(++size, message.posture[0].label + " (" + stream.str() + "%)", joints["Head"], 80.0, 0.0, 1.0, 1.0, a));
					}*/
				}

				float indice, indice_tot=0.0;
				if(has_normal)
				{
					// 0 et 1
					Marker marker = get_marker_arrow_form_plane(++size, joints_pos["ShoulderRight"],joints_pos["ShoulderLeft"], joints_pos["SpineMid"], "UpperSpine", 1, 0, 0, 1);
					indices_msg.name.push_back("fiabilite_UpperSpine");
					indices_msg.indice.push_back((joints_pos["ShoulderRight"].fiabilite+joints_pos["ShoulderLeft"].fiabilite+joints_pos["SpineMid"].fiabilite)/3);
					indice = vectorDotDepth(marker);
					indices_msg.name.push_back("dot_UpperSpine");
					indices_msg.indice.push_back(indice);
					markers.markers.push_back(marker);
					indice_tot += 0.3*indice;

					// 2 et 3
					marker = get_marker_arrow_form_plane(++size, joints_pos["ShoulderRight"],joints_pos["ElbowRight"], joints_pos["WristRight"], "RightArm", 1, 0, 0, 1);
					indices_msg.name.push_back("fiabilite_RightArm");
					indices_msg.indice.push_back((joints_pos["ShoulderRight"].fiabilite+joints_pos["ElbowRight"].fiabilite+joints_pos["WristRight"].fiabilite)/3);
					indice = vectorDotDepth(marker);
					indices_msg.name.push_back("dot_RightArm");
					indices_msg.indice.push_back(indice);
					markers.markers.push_back(marker);
					indice_tot += 0.2*indice;

					// 4 et 5
					marker = get_marker_arrow_form_plane(++size, joints_pos["ShoulderLeft"],joints_pos["ElbowLeft"], joints_pos["WristLeft"], "LeftArm", 1, 0, 0, 1);
					indices_msg.name.push_back("fiabilite_LeftArm");
					indices_msg.indice.push_back((joints_pos["ElbowLeft"].fiabilite+joints_pos["ShoulderLeft"].fiabilite+joints_pos["WristLeft"].fiabilite)/3);
					indice = vectorDotDepth(marker);
					indices_msg.name.push_back("dot_LeftArm");
					indices_msg.indice.push_back(indice);
					markers.markers.push_back(marker);
					indice_tot += 0.2*indice;

					// 6 et 7
					marker = get_marker_arrow_form_plane(++size, joints_pos["SpineShoulder"],joints_pos["SpineBase"], joints_pos["SpineMid"], "Trunk", 1, 0, 0, 1);
					indices_msg.name.push_back("fiabilite_Trunk");
					indices_msg.indice.push_back((joints_pos["SpineShoulder"].fiabilite+joints_pos["SpineBase"].fiabilite+joints_pos["SpineMid"].fiabilite)/3);
					indice = vectorDotDepth(marker);
					indices_msg.name.push_back("dot_Trunk");
					indices_msg.indice.push_back(indice);
					markers.markers.push_back(marker);
					indice_tot += 0.1*indice;

					// 8 et 9
					marker = get_marker_arrow_form_plane(++size, joints_pos["SpineMid"],joints_pos["HipLeft"], joints_pos["HipRight"], "LowerSpine", 1, 0, 0, 1);
					indices_msg.name.push_back("fiabilite_LowerSpine");
					indices_msg.indice.push_back((joints_pos["HipLeft"].fiabilite+joints_pos["HipRight"].fiabilite+joints_pos["SpineMid"].fiabilite)/3);
					indice = vectorDotDepth(marker);
					indices_msg.name.push_back("dot_LowerSpine");
					indices_msg.indice.push_back(indice);
					markers.markers.push_back(marker);
					indice_tot += 0.2*indice;


					// 10 et 11
					indices_msg.name.push_back("fiabilite_total");
					indices_msg.indice.push_back(count_fiab/(float)(message.mesures.size()-6));
					indices_msg.name.push_back("dot_total");
					indices_msg.indice.push_back(indice_tot);

					publisher_send<rulaArray>("/PointCloud/indice_vector", indices_msg);
				}

				return markers;
			}

			Marker get_marker_joint(const int& id, const position_msg& position, double r, double g, double b, double a)
			{
				Marker marker;
				marker.id = id;
				marker.ns = "joint_marker";
				marker.type = Marker::SPHERE;
				marker.action = Marker::ADD;
				marker.text = position.name;
				marker.header.stamp = Time::now();
				marker.header.frame_id = "human_base";
				marker.color.r = r;
				marker.color.g = g;
				marker.color.b = b;
				marker.color.a = a;
				marker.pose.position.x = position.position.x;
				marker.pose.position.y = position.position.y;
				marker.pose.position.z = position.position.z;

				if(position.fiabilite)
				{
					marker.scale.x = 0.06;
					marker.scale.y = 0.06;
					marker.scale.z = 0.06;
				}
				else
				{
					marker.scale.x = 0.02;
					marker.scale.y = 0.02;
					marker.scale.z = 0.02;
				}

				return marker;
			}

			Marker get_marker_arrow_form_plane(const int& id, const position_msg& position1, const position_msg& position2, const position_msg& position3, string name, double r, double g, double b, double a)
			{
				Marker marker;
				marker.id = id;
				marker.ns = "joint_marker";
				marker.type = Marker::ARROW;
				marker.action = Marker::ADD;
				marker.text = name;
				marker.header.stamp = Time::now();
				marker.header.frame_id = "human_base";
				marker.color.r = r;
				marker.color.g = g;
				marker.color.b = b;
				marker.color.a = a;
				geometry_msgs::Point m, n, p, p1, p2, p3;
				p1 = position1.position; p2 = position2.position; p3 = position3.position;
				m.x = (p1.x+p2.x+p3.x)/3;
				m.y = (p1.y+p2.y+p3.y)/3;
				m.z = (p1.z+p2.z+p3.z)/3;
				marker.points.push_back(m);
				n.x = 6*(((p2.y-p1.y)*(p3.z-p1.z))-((p3.y-p1.y)*(p2.z-p1.z)));
				n.y = 6*(((p2.z-p1.z)*(p3.x-p1.x))-((p3.z-p1.z)*(p2.x-p1.x)));
				n.z = 6*(((p2.x-p1.x)*(p3.y-p1.y))-((p3.x-p1.x)*(p2.y-p1.y)));
				p.x = m.x+n.x;
				p.y = m.y+n.y;
				p.z = m.z+n.z;
				marker.points.push_back(p);

				marker.scale.x = 0.05;
				marker.scale.y = 0.05;
				marker.scale.z = 0;

				return marker;
			}

			Marker get_marker_axe(const int& id, const pair<Marker, Marker>& markers, double r, double g, double b, double a)
			{
				Marker marker;
				marker.id = id;
				marker.ns = "joint_marker";
				marker.type = Marker::ARROW;
				marker.action = Marker::ADD;
				marker.header.stamp = Time::now();
				marker.header.frame_id = "human_base";
				marker.color.r = r;
				marker.color.g = g;
				marker.color.b = b;
				marker.color.a = a;
				marker.points.push_back(markers.first.pose.position);
				marker.points.push_back(markers.second.pose.position);
				marker.scale.x = 0.01;
				marker.scale.y = 0.01;
				marker.scale.z = 0.01;

				return marker;
			}

			Marker get_marker_text(const int& id, const string& text, const Marker& marker_base, double size, double r, double g, double b, double a)
			{
				Marker marker;
				marker.id = id;
				marker.ns = "joint_marker";
				marker.type = Marker::TEXT_VIEW_FACING;
				marker.action = Marker::ADD;
				marker.header.stamp = Time::now();
				marker.header.frame_id = "human_base";
				marker.color.r = r;
				marker.color.g = g;
				marker.color.b = b;
				marker.color.a = a;
				marker.text = text;
				marker.pose.position.x = marker_base.pose.position.x + 0.02;
				marker.pose.position.y = marker_base.pose.position.y;
				marker.pose.position.z = marker_base.pose.position.z;
				marker.scale.x = size / 1000;
				marker.scale.y = size / 1000;
				marker.scale.z = size / 1000;

				return marker;
			}

			/* -------------------------------------------------- */
			/*                  SUBSCRIBER                        */
			/* -------------------------------------------------- */

			void update_rula(const rulaArray::ConstPtr& message)
			{
				rula_indices.clear();
				for(int i = 0; i < message->name.size(); ++i)
				{
					rula_indices[message->name[i]] = message->indice[i];
				}
			}

			void display_pointcloud(const mesureKinect::ConstPtr& message)
			{
				int source = atoi(message->typeCapteur.c_str());
				double r = (source == 3) ? 0.0 : 1.0;
				double g = (source == 2) ? 1.0 : 0.0;
				double b = (source == 2) ? 1.0 : 0.0;
				string channel = (source == 1) ? ROS_TOPIC_PC_1_MARKERS : (source == 2) ? ROS_TOPIC_PC_2_MARKERS : ROS_TOPIC_PC_3_MARKERS;

				publisher_send<MarkerArray>(channel, get_markers(*message, r, g, b, 1.0, true, false, true));
			}

			void display_pointcloud_tf(const mesureKalman::ConstPtr& message)
			{
				int source = atoi(message->data.typeCapteur.c_str());
				double r = (source == 3) ? 0.0 : 1.0;
				double g = (source == 2) ? 1.0 : 0.0;
				double b = (source == 2) ? 1.0 : 0.0;
				string channel = (source == 1) ? ROS_TOPIC_PC_TF_1_MARKERS : (source == 2) ? ROS_TOPIC_PC_TF_2_MARKERS : ROS_TOPIC_PC_TF_3_MARKERS;

				publisher_send<MarkerArray>(channel, get_markers(message->data, r, g, b, 1.0, true));
			}

			void display_fusion(const mesureKalman::ConstPtr& message)
			{
				publisher_send<MarkerArray>(ROS_TOPIC_PC_FUSION_TF_MARKERS, get_markers(message->data, 0.0, 1.0, 1.0, 1.0, true));
			}

			void display_corrected(const mesureKinect::ConstPtr& message)
			{
				//ROS_INFO("HEREuuuuuuuuuuuuuuu");
				publisher_send<MarkerArray>(ROS_TOPIC_PC_CORRECTED_MARKERS, get_markers(*message, 1.0, 0.5, 0.0, 1.0, true, true));
			}

			void display_classified(const mesureKinect::ConstPtr& message)
			{
				publisher_send<MarkerArray>(ROS_TOPIC_PC_CLASSIFIED_MARKERS, get_markers(*message, 1.0, 0.0, 1.0, 1.0, true, true));
			}

			void display_opti(const mesureKalman::ConstPtr& message)
			{
				publisher_send<MarkerArray>(ROS_TOPIC_PC_OPTI_TF_MARKERS, get_markers(message->data, 0.5, 0.8, 0.3, 0.8, true));
			}

			void display_corrected_opti(const mesureKalman::ConstPtr& message)
			{
				publisher_send<MarkerArray>(ROS_TOPIC_PC_TF_OPTI_CORRECTED_MARKERS, get_markers(message->data, 0.2, 0.5, 0.9, 1.0, true));
			}

		public:
			EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
			NodeRviz(int argc, char** argv):
			Node(ROS_NODE_RVIZ_NAME, ROS_NODE_RVIZ_RATE, argc, argv)
			{
				subscriber_add<mesureKinect>(ROS_TOPIC_PC, &NodeRviz::display_pointcloud);
				subscriber_add<mesureKalman>(ROS_TOPIC_PC_TF, &NodeRviz::display_pointcloud_tf);
				subscriber_add<mesureKalman>(ROS_TOPIC_PC_FUSION_TF, &NodeRviz::display_fusion);
				subscriber_add<mesureKinect>(ROS_TOPIC_PC_CORRECTED, &NodeRviz::display_corrected);
				//subscriber_add<mesureKinect>(ROS_TOPIC_PC_CLASSIFIED, &NodeRviz::display_classified);
				subscriber_add<rulaArray>(ROS_TOPIC_INDICE_RULA, &NodeRviz::update_rula);
				subscriber_add<mesureKalman>(ROS_TOPIC_OC_TF, &NodeRviz::display_opti);
				//subscriber_add<mesureKalman>(ROS_TOPIC_OC_THETA_TF, &NodeRviz::display_corrected_opti);
				//subscriber_add<mesureKinect>(ROS_TOPIC_OC, &NodeRviz::display_classified);
				subscriber_add<mesureKalman>(ROS_TOPIC_PC_CORRECTED_TF, &NodeRviz::display_corrected_opti);
				subscriber_add<mesureKinect>(ROS_TOPIC_PC_CORRECTED, &NodeRviz::display_classified);
				//subscriber_add<mesureKalman>("/PointCloudFusionTF_corrected", &NodeRviz::display_corrected_opti); //position
				//subscriber_add<mesureKalman>(ROS_TOPIC_PC_TF_OPTI_FORWARD_CORRECTED, &NodeRviz::display_corrected_opti); //angle

				publisher_add<MarkerArray>(ROS_TOPIC_PC_TF_1_MARKERS, 0);
				publisher_add<MarkerArray>(ROS_TOPIC_PC_TF_2_MARKERS, 0);
				publisher_add<MarkerArray>(ROS_TOPIC_PC_TF_3_MARKERS, 0);
				publisher_add<MarkerArray>(ROS_TOPIC_PC_1_MARKERS, 0);
				publisher_add<MarkerArray>(ROS_TOPIC_PC_2_MARKERS, 0);
				publisher_add<MarkerArray>(ROS_TOPIC_PC_3_MARKERS, 0);
				publisher_add<MarkerArray>(ROS_TOPIC_PC_FUSION_TF_MARKERS, 0);
				publisher_add<MarkerArray>(ROS_TOPIC_PC_CORRECTED_MARKERS, 0);
				publisher_add<MarkerArray>(ROS_TOPIC_PC_CLASSIFIED_MARKERS, 0);
				publisher_add<MarkerArray>(ROS_TOPIC_PC_OPTI_TF_MARKERS, 0);
				publisher_add<MarkerArray>(ROS_TOPIC_PC_TF_OPTI_CORRECTED_MARKERS, 0);
				publisher_add<rulaArray>("/PointCloud/indice_vector", 0);
			}

			void on_start()
			{

			}

			void on_update()
			{

			}

			void on_end()
			{

			}
	};
}

int main(int argc, char** argv)
{
	return (prog_kinect::NodeRviz(argc, argv)).run();
}
