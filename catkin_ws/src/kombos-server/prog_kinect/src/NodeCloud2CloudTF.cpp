#include <Eigen/Geometry>
#include <kombos_msgs/mesureKinect.h>
#include <kombos_msgs/mesureKalman.h>
#include "../include/Network.hpp"
#include "../include/Utils.hpp"
#include "../include/Node.hpp"
#include "../include/JsonReader.hpp"

using namespace std;
using namespace geometry_msgs;
using namespace kombos_msgs;
using namespace Eigen;

namespace prog_kinect
{
	class NodeCloud2CloudTF : public Node
	{
		private:
			const bool IS_AGGREGATION = false;
			map<string, int> my_joints_indices;
			mesureKalman message_tfk;
			map<string, int> my_joints_indices_opti;
			mesureKalman message_tfk_opti;

			void transform_joints(const mesureKinect::ConstPtr& message)
			{
				my_joints_indices.clear();
				for(int i = 0; i < message->mesures.size(); ++i)
				{
					my_joints_indices[message->mesures[i].name] = i;
					if ( std::isnan(message->mesures[i].position.x) || std::isnan(message->mesures[i].position.y) || std::isnan(message->mesures[i].position.z) )
					{
						ROS_WARN("=================Bug================= > C2CTF > Tf joints");
						return;
					}
				}

				Point base = message->mesures[my_joints_indices[JOINT_NAME_SPINEBASE]].position;
				Point mid = message->mesures[my_joints_indices[JOINT_NAME_SPINEMID]].position;
				Point hip_right = message->mesures[my_joints_indices[JOINT_NAME_HIP_RIGHT]].position;
				Point hip_left = message->mesures[my_joints_indices[JOINT_NAME_HIP_LEFT]].position;
				MatrixXf T = Utils::transform_base_inv(base.x, base.y, base.z, hip_right.x, hip_right.y, hip_right.z, hip_left.x, hip_left.y, hip_left.z, mid.x, mid.y, mid.z);

                //std::cout<<T<<std::endl<<std::endl;
                
				message_tfk.matrices.clear();
				message_tfk.data = get_message_data(*message, my_joints_indices.size(), T);
				message_tfk.matrices.push_back(Utils::matrix2msg(string(MATRIX_NAME_TF) + message->typeCapteur, T));
				publisher_send(ROS_TOPIC_PC_TF, message_tfk);
			}

			void transform_joints_opti(const mesureKinect::ConstPtr& message)
			{
				my_joints_indices_opti.clear();
				for(int i = 0; i < message->mesures.size(); ++i)
				{
					my_joints_indices_opti[message->mesures[i].name] = i;
					if ( std::isnan(message->mesures[i].position.x) || std::isnan(message->mesures[i].position.y) || std::isnan(message->mesures[i].position.z) )
					{
						ROS_WARN("=================Bug================= > C2CTF > Opti");
						return;
					}
				}

				Point base = message->mesures[my_joints_indices_opti[JOINT_NAME_SPINEBASE]].position;
				Point mid = message->mesures[my_joints_indices_opti[JOINT_NAME_SPINEMID]].position;
				Point hip_right = message->mesures[my_joints_indices_opti[JOINT_NAME_HIP_RIGHT]].position;
				Point hip_left = message->mesures[my_joints_indices_opti[JOINT_NAME_HIP_LEFT]].position;
				MatrixXf T = Utils::transform_base_inv(base.x, base.y, base.z, hip_right.x, hip_right.y, hip_right.z, hip_left.x, hip_left.y, hip_left.z, mid.x, mid.y, mid.z);

				message_tfk_opti.matrices.clear();
				message_tfk_opti.data = get_message_data(*message, my_joints_indices_opti.size(), T);
				message_tfk_opti.matrices.push_back(Utils::matrix2msg(string(MATRIX_NAME_TF) + message->typeCapteur, T));

				publisher_send(ROS_TOPIC_OC_TF, message_tfk_opti);
			}

			void transform_joints_inverse(const mesureKalman::ConstPtr& message)
			{
				map<string, int>  my_joints_indices_inv;
				for(int i = 0; i < message->data.mesures.size(); ++i)
				{
					if ( std::isnan(message->data.mesures[i].position.x) || std::isnan(message->data.mesures[i].position.y) || std::isnan(message->data.mesures[i].position.z) )
					{
						ROS_WARN("=================Bug================= > C2CTF > Tf joints inverse");
						return;
					}
					my_joints_indices_inv[message->data.mesures[i].name] = i;
				}

				Matrix4f T = Matrix4f::Identity();
				for(auto& matrix : message->matrices)
				{
					if(matrix.name == string(MATRIX_NAME_TF1))
					{
						T = Utils::passageInverse(Utils::msg2matrix(matrix));
					}
				}

				// IF sensor 1 not present
				if(T.isApprox(Matrix4f::Identity()))
				{
					for(auto& matrix : message->matrices)
					{
						if(matrix.name != string(MATRIX_NAME_TF1) && extractIdCalibration(matrix.name, 1) == "tf")
						{
							bool agreg = false, read = false;
							float tx = 0.0, tz = 0.0, ry = 0.0;
							/*JsonReader::read(_redox.get("tf_data"), [&](JsonReader* root)
    						{
    							agreg = root->at("agregation")->get_bool();
    							tx = (float)root->at("translaX")->get_double();
    							tz = (float)root->at("translaZ")->get_double();
    							ry = (float)root->at("rotaY")->get_double();
    							read = true;
    						});*/

    						//ROS_INFO_STREAM("INFO JSON : " << agreg << " : " << tx << ", " << tz << ", " << ry);
							Matrix4f invM = Matrix4f::Identity();
							invM = Utils::passageInverse(Utils::msg2matrix(matrix));

							Matrix4f W = Matrix4f::Identity();
							//if((read && !agreg) ||!IS_AGGREGATION) // FUSION MODE
							if(!IS_AGGREGATION)
							{
								string id = extractIdCalibration(matrix.name,0);
								stringstream ss;
								ss << "/calibration_" << 1 << "W" << id;
								string name_param = ss.str();

								// rotation matrix
								vector<float> w;
								nodeHandle->getParam(name_param, w);
								W = (w.size() == 0) ? Matrix4f::Identity() : Utils::vector2matrix(w, 4); // (W : N vers 1)
								W = Utils::passageInverse(W);

								// extration rotation
								float *dataT;
								dataT = extractTranslateRot(W);
								if(isnan(dataT[0]) || isnan(dataT[1]) || isnan(dataT[2]) || dataT[0] > 10 || dataT[1] > 10) // data is absurd
								{
									W = Matrix4f::Identity();
								}
								/*else
								{
									sendTf(dataT[0], dataT[1], Utils::rad2deg(dataT[2]), "fusion", id);
								}*/
							}
							/*else if (!read && IS_AGGREGATION) // AGGREGATION MODE
							{
								//ROS_INFO_STREAM("__________________________");
								float x = -0.23, z = -0.125, roty = -63;
								string id = "2";
								W = Utils::passageMatrixTrans(x, 0, z) * Utils::passageMatrixRotY(Utils::deg2rad(roty)); // (Wtmp : 2 vers 1)
								sendTf(x, z, roty, "agreg", id);
							}
							else
							{
								//ROS_WARN_STREAM("Agreg after reading");
								string id = "2";
								W = Utils::passageMatrixTrans(tx, 0, tz) * Utils::passageMatrixRotY(Utils::deg2rad(ry)); // (Wtmp : 2 vers 1)
								sendTf(tx, tz, ry, "agreg", id);
							}*/
							T = W * invM; // (T : cam1_cam2 cam2_bonhomme)
						}
					}
				}

				//TODO add other camera
				publisher_send(ROS_TOPIC_PC_CORRECTED, get_message_data(message->data, my_joints_indices_inv.size(), T));
			}

			/*void sendTf(float x, float z, float roty, string type, string id)
			{
				stringstream ss;
				ss << "{\"x\":" << x << ",\"z\":" << z << ",\"roty\":" << roty << ",\"type\":\"" << type << "\",\"id\":" << id << "}";
				string str = ss.str();
				_redox.set("TF", str);
			}*/

			float* extractTranslateRot(Matrix4f T)
			{
				static float arr[3];
				arr[0] = (abs(T(0,3)) < 1E-5) ? 0.0 : T(0,3);
				arr[1] = (abs(T(2,3)) < 1E-5) ? 0.0 : T(2,3);
				Vector3f x, nx, nz;
				x << 1, 0, 0;
				nx = T.block(0,0,3,1);
				nz = T.block(0,2,3,1);
				float alpha = acos(x.dot(nz) / (x.norm() * nz.norm()));
				float rot = acos(x.dot(nx) / (x.norm() * nx.norm()));
				rot = (alpha > M_PI/2) ? -rot : rot;
				arr[2] = (abs(rot) < 1E-3) ? 0.0 : rot;
				return arr;
			}

			string extractIdCalibration(string name, bool before)
			{
				size_t pos = 0;
				string myId;
				pos = name.find("_");
				if(before) myId = name.substr(0, pos);
				else myId = name.substr(pos + 1, name.length());
				return myId;
			}

			mesureKinect get_message_data(const mesureKinect& message, unsigned int joints_size, const MatrixXf& T)
			{
				mesureKinect message_tf = message;
				for(int i = 0; i < joints_size; i++)
				{
					Point raw = message_tf.mesures[i].position;
					Vector4f transformed = T * Vector4f(raw.x, raw.y, raw.z, 1);
					message_tf.mesures[i].position.x = transformed[0];
					message_tf.mesures[i].position.y = transformed[1];
					message_tf.mesures[i].position.z = transformed[2];
				}
				return message_tf;
			}

		public:
			EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
			NodeCloud2CloudTF(int argc, char** argv):
			Node(ROS_NODE_CLOUD2CLOUDTF_NAME, ROS_NODE_CLOUD2CLOUDTF_RATE, argc, argv)
			{
				subscriber_add<mesureKinect>(ROS_TOPIC_PC, &NodeCloud2CloudTF::transform_joints);
				subscriber_add<mesureKinect>(ROS_TOPIC_OC, &NodeCloud2CloudTF::transform_joints_opti);
				//subscriber_add<mesureKalman>(ROS_TOPIC_PC_CORRECTED_TF, &NodeCloud2CloudTF::transform_joints_inverse);

				publisher_add<mesureKalman>(ROS_TOPIC_PC_TF);
				publisher_add<mesureKinect>(ROS_TOPIC_PC_CORRECTED);
				publisher_add<mesureKalman>(ROS_TOPIC_OC_TF);
			}

			void on_start()
			{
				//redox_init(REDOX_HOST, REDOX_PORT);
			}

			void on_update()
			{

			}

			void on_end()
			{
				//_redox.disconnect();
			}
	};
}

int main(int argc, char** argv)
{
	return (prog_kinect::NodeCloud2CloudTF(argc, argv)).run();
}
