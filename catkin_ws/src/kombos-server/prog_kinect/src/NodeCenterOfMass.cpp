#include <Eigen/Core>
//#include <redox.hpp>
#include <Eigen/Geometry>
#include <kombos_msgs/mesureKinect.h>
#include "../include/Network.hpp"
#include "../include/Utils.hpp"
#include "../include/Node.hpp"
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <geometry_msgs/Point.h>
#include <kombos_msgs/kalmanState.h>
#include <kombos_msgs/anthropo.h>

using namespace std;
using namespace ros;
using namespace Eigen;
using namespace kombos_msgs;
using namespace visualization_msgs;
using namespace geometry_msgs;

namespace prog_kinect
{
	class NodeCenterOfMass : public Node
	{
		private:
			map<string, Vector3f> joints_positions;
			map<string, Vector3f> joints_positions_com;
			Vector3f average_com;
			int cpt_average = 0;

			map<string, float> average_limb_radius;
			map<string, pair<float, int>> individual_limb_radius;
			float individual_size;
			float individual_weight;

			map<float, vector<int>> frenquecy_pos;

			void message_handle(const mesureKinect::ConstPtr& msg)
			{
    		MarkerArray markers;
    		Marker marker;
    		int fiabilite_total = 0;
    		// get position in message
				for (int i = 0; i < msg->mesures.size(); ++i)
				{
					position_msg position = msg->mesures[i];
					joints_positions[position.name] = Vector3f(position.position.x, position.position.y, position.position.z);
					fiabilite_total += position.fiabilite;
				}

				// CoM for every limb
				int j = 0;
				for(map<string, pair<string,string>>::iterator it = limb_extremity.begin(); it != limb_extremity.end(); it++)
				{
    			joints_positions_com[it->first] = com_of_limb(joints_positions[it->second.first], joints_positions[it->second.second], limb_coef_proximal[it->first]);

    			marker = get_marker_joint(j, it->first, joints_positions_com[it->first], 1, 0.5, 0.5, 0.9);
    			markers.markers.push_back(marker);
    			if(individual_limb_radius.count(it->first) == 0)
    			{
    				marker = get_marker_axe(j+50, it->second, average_limb_radius[it->first] * (2*M_PI), 0.6, 0.6, 1, 0.1);
    			}
    			else{
    				marker = get_marker_axe(j+50, it->second, individual_limb_radius[it->first].first * (2*M_PI), 1, 0.6, 0.6, 0.1);
    			}
    			markers.markers.push_back(marker);
    			j++;
    		}

				// Total CoM
				Vector3f pos_com = Vector3f::Zero();
				float mass_total = 0.0, tmp_mass;
				for(map<string, pair<string,string>>::iterator it = limb_extremity.begin(); it != limb_extremity.end(); it++)
				{
					//if(it->first != "Pelvis"){
						if(individual_limb_radius.count(it->first) == 0){
	  					//tmp_mass = mass_of_limb_from_cylinder(it->second.first, it->second.second, limb_circumference[it->first], limb_density[it->first]);
	    				tmp_mass = mass_of_limb_from_radius(it->second.first, it->second.second, average_limb_radius[it->first], limb_density[it->first]);
	    			}
	    			else{
	    				tmp_mass = mass_of_limb_from_radius(it->second.first, it->second.second, individual_limb_radius[it->first].first, limb_density[it->first]);
	    			}
	    			mass_total += tmp_mass;
	    			for(int i = 0; i < 3; i++)
	    			{
	    				//pos_com[i] += joints_positions_com[it->first][i]*tmp_mass;
	    				pos_com[i] += limb_percent_weight[it->first]*joints_positions_com[it->first][i];
	    			}
	    		//}
    		}
    		//pos_com = pos_com / mass_total;
    		if(mass_total > MAX_HUMAN_WEIGHT || mass_total < MIN_HUMAN_WEIGHT) ROS_ERROR_STREAM("Mass improbable : " << mass_total);
    		else if(fiabilite_total > 20) individual_weight = mass_total;
    		//else ROS_INFO_STREAM("not enough " << fiabilite_total << " with " << individual_weight);
    		//ROS_WARN_STREAM(individual_size << " " << mass_total << " " << individual_limb_radius.size());

    		marker = get_marker_joint(28, "CentreDeMasse", pos_com, 1, 0, 0, 1);
    		markers.markers.push_back(marker);

    		// Update average, frequency and percentile
    		update_frequency(pos_com);
    		update_average(pos_com);

    		// Publish
    		publisher_send<mesureKinect>(ROS_TOPIC_PC_CLASSIFIED_COM, get_message_data(*msg, pos_com));

    		// display
    		vector<float> median = percentile(1/2.0);
    		marker = get_marker_area(30, average_com, median[0], median[1], median[2], 0, 1, 0, 0.9);
    		markers.markers.push_back(marker);

				vector<float> percentil95 = percentile(95/100.0);
    		marker = get_marker_area(31, average_com, percentil95[0], percentil95[1], percentil95[2], 1, 1, 0, 0.4);
    		markers.markers.push_back(marker);

				publisher_send<MarkerArray>(ROS_TOPIC_COM_MARKERS, markers);
			}

			void update_average(Vector3f pos_com)
			{
				average_com = ((average_com * cpt_average) + pos_com) / (cpt_average + 1);
				cpt_average++;
			}

			vector<float> percentile(float percentile_coef)
			{
				map<float, vector<int>>::reverse_iterator it  = frenquecy_pos.rbegin();
				vector<float> max{ (float)frenquecy_pos[it->first][0], (float)frenquecy_pos[it->first][1], (float)frenquecy_pos[it->first][2]};
				vector<float> inter_median(3, 0);
				vector<bool> find(3, false);

				for(it; it != frenquecy_pos.rend(); it++)
				{
					if(!find[0] && it->second[0] < max[0] * percentile_coef)
					{
						inter_median[0] = it->first;
						find[0] = true;
					}
					if(!find[1] && it->second[1] < max[1] * percentile_coef)
					{
						inter_median[1] = it->first;
						find[1] = true;
					}
					if(!find[2] && it->second[2] < max[2] * percentile_coef)
					{
						inter_median[2] = it->first;
						find[2] = true;
					}
					if(find[0] && find[1] && find[2]) return inter_median;
				}
			}

			void update_frequency(Vector3f pos)
			{
				bool x_find = false, y_find = false, z_find = false;
				for(map<float, vector<int>>::iterator it = frenquecy_pos.begin(); it != frenquecy_pos.end(); it++)
				{
					if((!x_find && pos[0] < it->first) || x_find)
					{
						frenquecy_pos[it->first][0]++;
						x_find = true;
					}
					if((!y_find && pos[1] < it->first) || y_find)
					{
						frenquecy_pos[it->first][1]++;
						y_find = true;
					}
					if((!z_find && pos[2] < it->first) || z_find)
					{
						frenquecy_pos[it->first][2]++;
						z_find = true;
					}
				}
				map<float, vector<int>>::reverse_iterator itend = frenquecy_pos.rbegin();
				if(!x_find) frenquecy_pos[itend->first][0]++;
				if(!y_find) frenquecy_pos[itend->first][1]++;
				if(!z_find) frenquecy_pos[itend->first][2]++;
			}

			float mass_of_limb_from_cylinder(string proximal, string distal, float circumference, float density)
			{
				float mass;
				float radius = (circumference / (2*M_PI)) * 1E2; // radius in cm
				float length = Utils::length_calculation(joints_positions, proximal, distal) * 1E2; // length in cm
				mass = density * M_PI * radius * radius * length; // mass in g

				return mass * 1E-3; // return in kg
			}

			float mass_of_limb_from_radius(string proximal, string distal, float radius, float density)
			{
				float mass;
				radius = radius * 1E2; // radius in cm
				float length = Utils::length_calculation(joints_positions, proximal, distal) * 1E2; // length in cm
				mass = density * M_PI * radius * radius * length; // mass in g

				return mass * 1E-3; // return in kg
			}

			mesureKinect get_message_data(const mesureKinect& message, Vector3f pos_com)
			{
				mesureKinect message_res = message;

				position_msg pos_add;
				pos_add.name = JOINT_NAME_COM;
				pos_add.position = get_point_from_vector(pos_com);
				pos_add.fiabilite = 1;

				message_res.mesures.push_back(pos_add);

				return message_res;
			}

			Vector3f com_of_limb(Vector3f pos_proximal, Vector3f pos_distal, float coef_proxi)
			{
				Vector3f pos_com;
				for( int i = 0; i < 3; i++)
				{
					pos_com[i] = coef_proxi*pos_proximal[i] + (1 - coef_proxi)*pos_distal[i];
				}
				return pos_com;
			}

			void process_limb_radius(const kalmanState::ConstPtr& msg)
			{
				float radius, seuil, last_average, last_cpt;
				bool interval = false;
				string name;
				for(int i = 0; i < msg->name.size(); ++i)
				{
					radius = msg->value[i];
					name = msg->name[i];
					seuil = average_limb_radius[name] * 30 / 100;

					interval = (radius > (average_limb_radius[name] - seuil)) && (radius < (average_limb_radius[name] + seuil));

					if(individual_limb_radius.count(name) == 0 && interval)
					{
						individual_limb_radius[name] = pair<float, int>(0.0, 0);
					}

					if(interval)
					{
						last_cpt = individual_limb_radius[name].second;
						last_average = individual_limb_radius[name].first;
						individual_limb_radius[name].first = ((last_average * last_cpt) + radius) / (last_cpt + 1);
						individual_limb_radius[name].second++;
					}
				}
			}

			void process_size(const anthropo::ConstPtr& msg)
			{
				individual_size = msg->size;
				update_average_limb_radius();
				anthropo new_msg;
				new_msg.size = msg->size;
				new_msg.weight = static_cast<int>(individual_weight);
				publisher_send<anthropo>(ROS_TOPIC_ANTHROPO_COMPLETED, new_msg);
			}

			void update_average_limb_radius()
			{
				float weight_estimation  = (101.51 * individual_size - 96.29); // kg
				float length, density, coef;

				for(map<string, pair<string,string>>::iterator it = limb_extremity.begin(); it != limb_extremity.end(); it++)
				{
					length = Utils::length_calculation(joints_positions, it->second.first, it->second.second) * 1E2; // cm
					density = limb_density[it->first]; // cm.g-3
					coef = limb_percent_weight[it->first];
					average_limb_radius[it->first] = sqrt((coef*weight_estimation * 1E3) / (density*M_PI*length)) * 1E-2; // m
				}
			}

			Marker get_marker_area(int id, Vector3f center, float scale_x, float scale_y, float scale_z, float r, float g, float b, float a)
			{
				Marker marker;
				marker.id = id;
				marker.ns = "joint_marker";
				marker.type = Marker::CYLINDER;
				marker.action = Marker::ADD;
				marker.header.stamp = Time::now();
				marker.header.frame_id = "human_base";
				marker.color.r = r;
				marker.color.g = g;
				marker.color.b = b;
				marker.color.a = a;
				marker.pose.position = get_point_from_vector(center);
				marker.scale.x = scale_x;
				marker.scale.y = scale_y;
				marker.scale.z = scale_z;

				return marker;
			}

			Marker get_marker_axe(const int& id, const pair<string, string>& extremities, float circumference, float r, float g, float b, float a)
			{
				Marker marker;
				marker.id = id;
				marker.ns = "joint_marker";
				marker.type = Marker::ARROW;
				marker.action = Marker::ADD;
				marker.header.stamp = Time::now();
				marker.header.frame_id = "human_base";
				marker.color.r = r;
				marker.color.g = g;
				marker.color.b = b;
				marker.color.a = a;
				marker.points.push_back(get_point_from_vector(joints_positions[extremities.first]));
				marker.points.push_back(get_point_from_vector(joints_positions[extremities.second]));
				marker.scale.x = circumference / M_PI ;
				marker.scale.y = 0.01;
				marker.scale.z = 0.01;

				return marker;
			}

			Marker get_marker_joint(const int& id, string name, Vector3f position, float r, float g, float b, float a)
			{
				Marker marker;
				marker.id = id;
				marker.ns = "com_marker";
				marker.type = Marker::SPHERE;
				marker.action = Marker::ADD;
				marker.text = name;
				marker.header.stamp = Time::now();
				marker.header.frame_id = "human_base";
				marker.color.r = r;
				marker.color.g = g;
				marker.color.b = b;
				marker.color.a = a;
				marker.pose.position.x = position[0];
				marker.pose.position.y = position[1];
				marker.pose.position.z = position[2];

				marker.scale.x = 0.04;
				marker.scale.y = 0.04;
				marker.scale.z = 0.04;

				return marker;
			}

			Point get_point_from_vector(Vector3f position)
			{
				Point point;
				point.x = position[0];
				point.y = position[1];
				point.z = position[2];
				return point;
			}

			void initialize_parameters()
			{
				cpt_average = 0; // init average_com with

				// Re-init with 1.75 guy
				individual_size = 1.75;

				json2model model = json2model();
				map<string, float> limb_circumference = model.GetLimbCircumference();
				for(map<string,float>::iterator it = limb_circumference.begin(); it != limb_circumference.end(); it++)
				{
					average_limb_radius[it->first] = (it->second / (2*M_PI));
				}

				vector<int> vec(3, 0);
				for(int i = 1; i < 20; i++)
				{
					frenquecy_pos[0.005*i] = vec;
				}

				individual_limb_radius.clear();
			}


		public:
			NodeCenterOfMass(int argc, char** argv):
			Node(ROS_NODE_JOINT_NAME_COM, ROS_NODE_COM_RATE, argc, argv)
			{
				subscriber_add<control_server_cmd>(ROS_TOPIC_RECORD, &NodeCenterOfMass::server_toggle);

				subscriber_add<mesureKinect>(ROS_TOPIC_PC_CLASSIFIED, &NodeCenterOfMass::message_handle);
				subscriber_add<kalmanState>(ROS_TOPIC_LIMBRADIUS, &NodeCenterOfMass::process_limb_radius);
				subscriber_add<anthropo>(ROS_TOPIC_ANTHROPO, &NodeCenterOfMass::process_size);

				publisher_add<mesureKinect>(ROS_TOPIC_PC_CLASSIFIED_COM);
				publisher_add<MarkerArray>(ROS_TOPIC_COM_MARKERS, 0);
				publisher_add<anthropo>(ROS_TOPIC_ANTHROPO_COMPLETED);
			}

			void on_start()
			{
				initialize_parameters();
			}

			void on_update()
			{
			}

			void on_end()
			{
			}

			void action_on_toggle()
			{
				initialize_parameters();
			}
		};
}

int main(int argc, char** argv)
{
	return (prog_kinect::NodeCenterOfMass(argc, argv)).run();
}
