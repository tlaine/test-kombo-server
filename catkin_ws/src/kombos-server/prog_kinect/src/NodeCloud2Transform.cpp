#include <Eigen/Core>
#include <Eigen/Geometry>
#include <kombos_msgs/mesureKalman.h>
#include <kombos_msgs/mesureKinect.h>
#include <kombos_msgs/khi2.h>
#include "../include/Network.hpp"
//#include "../include/Utils.hpp"
#include "../include/json2model.h"
#include "../include/Node.hpp"
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

using namespace std;
using namespace ros;
using namespace Eigen;
using namespace kombos_msgs;
using namespace geometry_msgs;
using namespace visualization_msgs;

namespace prog_kinect
{
	class NodeCloud2Transform : public Node
	{
	private:
		map<string, bool> data_enabled;
		map<string, map<string, deque<Vector3f>>> joints_positions;
		map<string, map<string, deque<bool>>> joints_fiabilites;
		map<string, deque<double>> joints_timestamps;
		map<pair<string,string>, map<string, deque<pair<Vector3f, Vector3f>>>> associativ_joints;
		map<pair<string,string>, map<string, deque<bool>>> associativ_fiabs;
		map<pair<string,string>, map<string, deque<pair<Vector3f, Vector3f>>>> best_associativ_joints;
		map<pair<string,string>, map<string, deque<bool>>> best_associativ_fiabs;

		// var transform
		map<pair<string,string>, MatrixXf> list_tf;
		map<pair<string,string>, bool> transform_calibrate;
		//map<pair<string,string>, Vector3f> centroid_cloud1;
		//map<pair<string,string>, Vector3f> centroid_cloud2;
		//map<pair<string,string>, float> centroid_weight;
		//map<pair<string,string>, Matrix3f> transposition_matrix;
		//map<pair<string,string>, float> sigma;
		map<pair<string,string>, Matrix4f> last_transform;
		map<pair<string,string>, float> last_norm;
		const int limitVec = 100;
		bool min_batch = false;

		// FOR EVERY MSG RECEIVED
		void enqueue(const mesureKinect::ConstPtr& message)
		{
			string kinect_name = message->typeCapteur;
			int cpt_fiab = 0;
			for (int i = 0; i < message->mesures.size(); ++i)
			{
				cpt_fiab += message->mesures[i].fiabilite;
				if ( std::isnan(message->mesures[i].position.x) || std::isnan(message->mesures[i].position.y) || std::isnan(message->mesures[i].position.z) )
				{
					ROS_WARN_STREAM("====== NAN ======== Transform");
					return;
				}
			}

			if(cpt_fiab > 12) // SAVE ONLY "GOOD OBSERVATION"
			{
				// Fill joints timestamp in deque - for association of timestamp
				if( joints_timestamps[kinect_name].size() == limitVec )
				{
					joints_timestamps[kinect_name].pop_front();
				}
				double tmptime = Time::now().toSec();
				joints_timestamps[kinect_name].push_back(tmptime);
				int size_vector = 0;

				// Fill joints position in deque
				for (int i = 0; i < message->mesures.size(); ++i)
				{
					position_msg position = message->mesures[i];
					size_vector = joints_fiabilites[kinect_name][position.name].size();
					if( size_vector == limitVec )
					{
						joints_positions[kinect_name][position.name].pop_front();
						joints_fiabilites[kinect_name][position.name].pop_front();
					}
					joints_positions[kinect_name][position.name].push_back(Vector3f(position.position.x, position.position.y, position.position.z));
					joints_fiabilites[kinect_name][position.name].push_back(position.fiabilite);
				}
				// ROS_WARN_STREAM("-------- in dequeue " << kinect_name << "/ ts : " << joints_timestamps[kinect_name].size() << "/ jf : " << joints_fiabilites[kinect_name]["SpineBase"].size() << "/ jp : " << joints_positions[kinect_name]["SpineBase"].size());
			}
		}

		void transform()
		{
			// FOR ALL SENSORS ENABLE
			for(map<string, bool>::iterator iterator1 = data_enabled.begin(); iterator1 != data_enabled.end(); iterator1++)
			{
				// For all sensors enable
				for(map<string, bool>::iterator iterator2 = data_enabled.begin(); iterator2 != data_enabled.end(); iterator2++)
				{
					// FOR DIFF PAIR AND SENSOR1 < SENSOR2
					if((iterator1->second && iterator2->second) && (iterator1->first < iterator2->first))
					{
						// CALCULATION OF TRANSFORMATION
						pair<string,string> tfname(iterator1->first, iterator2->first);
						if(transform_calibrate.count(tfname)==0 || !transform_calibrate[tfname])
						{
							list_tf[tfname] = transform_kinect(iterator1->first, iterator2->first);
							stringstream ss;
							ss << "calibration_" << iterator1->first << "W" << iterator2->first;
							string name_param = ss.str();
							nodeHandle->setParam(name_param, matrix_to_list(list_tf[tfname]));
							display_cameras(list_tf[tfname]);
						}
					}
				}
			}
		}

		// Display into rviz position of cameras with Kinect 1 (0,0,0)
		void display_cameras(MatrixXf Tfcam1_cam2)
		{
			MarkerArray markers;

			// Camera 1
			Marker marker;
			marker.id = 0;
			marker.ns = "camera_position";
			marker.type = Marker::ARROW;
			marker.action = Marker::ADD;
			marker.header.stamp = Time::now();
			marker.header.frame_id = "map";
			marker.color.r = 1;
			marker.color.g = 0;
			marker.color.b = 0;
			marker.color.a = 1;
			marker.scale.x = 0.01;
			marker.scale.y = 0.01;
			marker.scale.z = 0.01;
			geometry_msgs::Point p;
			p.x = 0; p.y = 0; p.z = 0;
			marker.points.push_back(p);
			p.x = 0.5; p.y = 0; p.z = 0;
			marker.points.push_back(p);
			markers.markers.push_back(marker);
			marker.id = 1;
			marker.header.stamp = Time::now();
			marker.color.r = 0;
			marker.color.g = 1;
			marker.color.b = 0;
			p.x = 0; p.y = 0; p.z = 0;
			marker.points.push_back(p);
			p.x = 0; p.y = 0.5; p.z = 0;
			marker.points.push_back(p);
			markers.markers.push_back(marker);
			marker.id = 2;
			marker.header.stamp = Time::now();
			marker.color.r = 0;
			marker.color.g = 0;
			marker.color.b = 1;
			p.x = 0; p.y = 0; p.z = 0;
			marker.points.push_back(p);
			p.x = 0; p.y = 0; p.z = 0.5;
			marker.points.push_back(p);
			markers.markers.push_back(marker);

			// Camera 2
			marker.id = 3;
			marker.header.stamp = Time::now();
			marker.color.r = 1;
			marker.color.g = 0;
			marker.color.b = 0;
			marker.color.a = 0.7;
			marker.scale.x = 0.01;
			marker.scale.y = 0.01;
			marker.scale.z = 0.01;
			p.x = Tfcam1_cam2(0,3); p.y = Tfcam1_cam2(1,3); p.z = Tfcam1_cam2(2,3);
			marker.points.push_back(p);
			p.x = Tfcam1_cam2(0,0); p.y = Tfcam1_cam2(1,0); p.z = Tfcam1_cam2(2,0);
			marker.points.push_back(p);
			markers.markers.push_back(marker);
			marker.id = 4;
			marker.header.stamp = Time::now();
			marker.color.r = 0;
			marker.color.g = 1;
			marker.color.b = 0;
			p.x = Tfcam1_cam2(0,3); p.y = Tfcam1_cam2(1,3); p.z = Tfcam1_cam2(2,3);
			marker.points.push_back(p);
			p.x = Tfcam1_cam2(0,1); p.y = Tfcam1_cam2(1,1); p.z = Tfcam1_cam2(2,1);
			marker.points.push_back(p);
			markers.markers.push_back(marker);
			marker.id = 5;
			marker.header.stamp = Time::now();
			marker.color.r = 0;
			marker.color.g = 0;
			marker.color.b = 1;
			p.x = Tfcam1_cam2(0,3); p.y = Tfcam1_cam2(1,3); p.z = Tfcam1_cam2(2,3);
			marker.points.push_back(p);
			p.x = Tfcam1_cam2(0,2); p.y = Tfcam1_cam2(1,2); p.z = Tfcam1_cam2(2,2);
			marker.points.push_back(p);
			markers.markers.push_back(marker);

			publisher_send<MarkerArray>("/Marker/camera_position", markers);

		}

		// Transform matrix to list to save it in param
		vector<float> matrix_to_list(MatrixXf M)
		{
			int rows = M.rows();
			int cols = M.cols();
			vector<float> w;

			for (int i = 0; i < rows; ++i)
			{
				for (int j = 0; j < cols; ++j)
				{
					w.push_back(M(i,j));
				}
			}
			return w;
		}

		// Calculate associativ indexes and fill associativ_joints
		void joint_timestamp_association(pair<string,string> tfname)
		{
			// fill associativ joints/fiab
			deque<pair<int,int>> associative_indexes;
			int ind2 = 0;
			int next_ind2 = 0;
			int last_ind1 = 0, last_ind2 = 0;
			for (int ind1=0; ind1 < joints_timestamps[tfname.first].size(); ++ind1)
			{
				ind2 = next_ind2;
				while (ind2 < joints_timestamps[tfname.second].size() && abs(joints_timestamps[tfname.first][ind1] - joints_timestamps[tfname.second][ind2]) > 0.02)
				{
					++ind2;
				}
				if (ind2 < joints_timestamps[tfname.second].size())
				{
					//associative_indexes.push_back(make_pair(ind1, ind2));
					int fiab_tot = 0;
					// ROS_INFO_STREAM("I add associativ ");
					for(std::map<std::string,int>::iterator iterator = joints_indices.begin(); iterator != joints_indices.end(); iterator++)
					{
						if( associativ_joints[tfname][iterator->first].size() == limitVec )
						{
							associativ_joints[tfname][iterator->first].pop_front();
							associativ_fiabs[tfname][iterator->first].pop_front();
							min_batch = true;
						}
						//ROS_INFO_STREAM("first pos " << joints_fiabilites[tfname.first][iterator->first].size() << " : " << ind1);
						//ROS_INFO_STREAM("second pos " << joints_fiabilites[tfname.second][iterator->first].size() << " : " << ind2);
						associativ_joints[tfname][iterator->first].push_back(make_pair(joints_positions[tfname.first][iterator->first][ind1],joints_positions[tfname.second][iterator->first][ind2]));
						int fiab = joints_fiabilites[tfname.first][iterator->first][ind1]*joints_fiabilites[tfname.second][iterator->first][ind2];
						associativ_fiabs[tfname][iterator->first].push_back(fiab);
						fiab_tot += fiab;
						last_ind1 = ind1;
						last_ind2 = ind2;
					}
					// ROS_INFO_STREAM("Finish add asso");
					if(fiab_tot >= 18) // best associativ joints
					{
						for(std::map<std::string,int>::iterator iterator = joints_indices.begin(); iterator != joints_indices.end(); iterator++)
						{
							best_associativ_joints[tfname][iterator->first].push_back(make_pair(joints_positions[tfname.first][iterator->first][ind1],joints_positions[tfname.second][iterator->first][ind2]));
							best_associativ_fiabs[tfname][iterator->first].push_back(joints_fiabilites[tfname.first][iterator->first][ind1]*joints_fiabilites[tfname.second][iterator->first][ind2]);
						}
					}
					next_ind2 = ind2 + 1;
				}
			}
			ROS_INFO_STREAM("finish associate");
			// clear point already associate
			for(int i = 0; i <= std::max(last_ind2, last_ind1); i++)
			{
				if(i <= last_ind1 && joints_timestamps[tfname.first].size() != 0)
				{
					//ROS_INFO_STREAM("delete : " << tfname.first << " / " <<  joints_timestamps[tfname.first].size() << " - " << last_ind1);
					joints_timestamps[tfname.first].pop_front();
				}
				if(i <= last_ind2 && joints_timestamps[tfname.second].size() != 0)
				{
					//ROS_INFO_STREAM("delete : " << tfname.second << " / " <<  joints_timestamps[tfname.second].size() << " - " << last_ind2);
					joints_timestamps[tfname.second].pop_front();
				}
				for(std::map<std::string,int>::iterator iterator = joints_indices.begin(); iterator != joints_indices.end(); iterator++)
				{
					if(i <= last_ind1 && joints_positions[tfname.first].size() != 0 && joints_fiabilites[tfname.first][iterator->first].size() != 0)
					{
						joints_positions[tfname.first][iterator->first].pop_front();
						joints_fiabilites[tfname.first][iterator->first].pop_front();
					}
					if(i <= last_ind2 && joints_positions[tfname.second].size() != 0 && joints_fiabilites[tfname.second][iterator->first].size() != 0)
					{
						joints_positions[tfname.second][iterator->first].pop_front();
						joints_fiabilites[tfname.second][iterator->first].pop_front();
					}
				}
			}
			//ROS_INFO_STREAM("finish clear old ");
		}

		// Calculation of transform matrix
		MatrixXf transform_kinect(string name1, string name2)
		{
			// Name of the pair
			pair<string,string> tfname(name1, name2);

			// Initialize centroids if not done yet
			/*if(centroid_cloud1.find(tfname) == centroid_cloud1.end())
			{
				centroid_cloud1[tfname] = Vector3f(0, 0, 0);
				centroid_cloud2[tfname] = Vector3f(0, 0, 0);
				centroid_weight[tfname] = 0;
				transposition_matrix[tfname] << 0,0,0,
												0,0,0,
												0,0,0;
			}*/

			// Associate indexes for centroids
			joint_timestamp_association(tfname);
			//ROS_WARN_STREAM(" >>>> ASSOCIATION DONE");
			// CENTROIDS
			Vector3f centroid_cloud1 = Vector3f(0, 0, 0);
			Vector3f centroid_cloud2 = Vector3f(0, 0, 0);
			float centroid_weight = 0;
			Matrix3f transposition_matrix;
			transposition_matrix << 0,0,0,
									0,0,0,
									0,0,0;

			int wtmp = 0;
			for(std::map<std::string,int>::iterator iterator = joints_indices.begin(); iterator != joints_indices.end(); iterator++)
			{
				if(best_associativ_fiabs[tfname][iterator->first].size() < limitVec)
				{
					for(int i = 0; i < associativ_fiabs[tfname][iterator->first].size(); i++)
					{
						centroid_cloud1 += associativ_fiabs[tfname][iterator->first][i]*associativ_joints[tfname][iterator->first][i].first;
						centroid_cloud2 += associativ_fiabs[tfname][iterator->first][i]*associativ_joints[tfname][iterator->first][i].second;
						wtmp += associativ_fiabs[tfname][iterator->first][i];
					}
				}
				for(int i = 0; i < best_associativ_fiabs[tfname][iterator->first].size(); i++)
				{
					centroid_cloud1 += best_associativ_fiabs[tfname][iterator->first][i]*best_associativ_joints[tfname][iterator->first][i].first;
					centroid_cloud2 += best_associativ_fiabs[tfname][iterator->first][i]*best_associativ_joints[tfname][iterator->first][i].second;
					wtmp += best_associativ_fiabs[tfname][iterator->first][i];
				}
			}
			centroid_weight += wtmp;
			// Cumulativ centroid
			Eigen::Vector3f centroidtmp1, centroidtmp2;
			centroidtmp1 = centroid_cloud1/centroid_weight;
			centroidtmp2 = centroid_cloud2/centroid_weight;
			//ROS_INFO_STREAM("CENTROID DONE");
			// MATRICE COVARIANCE
			std::map< std::string, Eigen::Vector3f > joints1_center;
			std::map< std::string, Eigen::Vector3f > joints2_center;
			for(std::map<std::string,int>::iterator iterator = joints_indices.begin(); iterator != joints_indices.end(); iterator++)
			{
				if(best_associativ_fiabs[tfname][iterator->first].size() < limitVec)
				{
					for(int i = 0; i < associativ_fiabs[tfname][iterator->first].size(); i++)
					{
						joints1_center[iterator->first] = associativ_fiabs[tfname][iterator->first][i]*(associativ_joints[tfname][iterator->first][i].first-centroidtmp1);
						joints2_center[iterator->first] = associativ_fiabs[tfname][iterator->first][i]*(associativ_joints[tfname][iterator->first][i].second-centroidtmp2);
						transposition_matrix += associativ_fiabs[tfname][iterator->first][i]*joints1_center[iterator->first]*joints2_center[iterator->first].transpose();
					}
				}
				for(int i = 0; i < best_associativ_fiabs[tfname][iterator->first].size(); i++)
				{
					joints1_center[iterator->first] = best_associativ_fiabs[tfname][iterator->first][i]*(best_associativ_joints[tfname][iterator->first][i].first-centroidtmp1);
					joints2_center[iterator->first] = best_associativ_fiabs[tfname][iterator->first][i]*(best_associativ_joints[tfname][iterator->first][i].second-centroidtmp2);
					transposition_matrix += best_associativ_fiabs[tfname][iterator->first][i]*joints1_center[iterator->first]*joints2_center[iterator->first].transpose();
				}
			}
			transposition_matrix = transposition_matrix/centroid_weight;
			//ROS_INFO_STREAM("COVARIANCE DONE");
			// SVD ET MATRICE ROTATION
			JacobiSVD<Eigen::Matrix3f> svd(transposition_matrix, ComputeFullU | ComputeFullV);
			Matrix3f R;
			R = svd.matrixV()*svd.matrixU().transpose();
			if((R - R) != (R - R)) {
				ROS_ERROR_STREAM("--------------------- NaN R --------------------");
				return MatrixXf::Identity(4,4);
			}

			// VECTEUR TRANSLATION
			Vector3f t;
			t = centroidtmp1 - (R * centroidtmp2);
			if((t - t) != (t - t)) {
				ROS_ERROR_STREAM("--------------------- NaN t --------------------");
				return MatrixXf::Identity(4,4);
			}

			MatrixXf T = MatrixXf::Identity(4,4);
			T.block(0,0,3,3) = R;
			T.block(0,3,3,1) = t;
			//ROS_INFO_STREAM("TRANSOFRMATION DONE");

			// CALCUL NORME
			float norme = 0;
			int cpt = 0;
			for(std::map<std::string,int>::iterator iterator = joints_indices.begin(); iterator != joints_indices.end(); iterator++)
			{
				if(best_associativ_fiabs[tfname][iterator->first].size() < limitVec)
				{
					for(int i = 0; i < associativ_fiabs[tfname][iterator->first].size(); i++)
					{
						float tmp_fiab = associativ_fiabs[tfname][iterator->first][i];
						float tmp_norme = (associativ_joints[tfname][iterator->first][i].first - ((R*associativ_joints[tfname][iterator->first][i].second)+t)).norm();
						norme += (tmp_fiab * tmp_norme);
						cpt += tmp_fiab;
					}
				}
				for(int i = 0; i < best_associativ_fiabs[tfname][iterator->first].size(); i++)
				{
					float tmp_fiab = best_associativ_fiabs[tfname][iterator->first][i];
					float tmp_norme = (best_associativ_joints[tfname][iterator->first][i].first - ((R*best_associativ_joints[tfname][iterator->first][i].second)+t)).norm();
					norme += (tmp_fiab * tmp_norme);
					cpt += tmp_fiab;
				}
			}
			khi2 n;
			if(cpt != 0) norme = norme / cpt;
			else norme = -1;
			n.c_khi2 = norme;
			//ROS_INFO_STREAM("NORME DONE");
			if(min_batch && !isnan(norme))
			{
				if(last_norm.count(tfname)==0 || (norme != -1 &&  last_norm[tfname]>norme))
				{
					last_norm[tfname] = norme;
					last_transform[tfname] = T;
					//khi2 n;
					n.khi2 = norme;
					//publisher_send("/normTransform", n);
  				}
				n.cumule = last_norm[tfname];
				publisher_send("/normTransform", n);

				if(last_norm[tfname] >= 1E-2 && !isnan(last_norm[tfname]))
				{
					ROS_WARN_STREAM("La norme pour " << name2 << " vers " << name1 << " est : " << last_norm[tfname]);
					transform_calibrate[tfname] = 0;
				}
				else if(!isnan(last_norm[tfname]))
				{
					transform_calibrate[tfname] = 1; // CALIBRATION DONE
				}

				return last_transform[tfname];
			}
			else {
				//ROS_WARN_STREAM("DOOOOOOOOOOOOOOOONE");
				return T;
			}
		}

		void initialize_transform()
		{
			associativ_joints.clear();
			associativ_fiabs.clear();
			best_associativ_joints.clear();
            best_associativ_fiabs.clear();
			joints_positions.clear();
			joints_fiabilites.clear();
			joints_timestamps.clear();
			list_tf.clear();
			//centroid_cloud1.clear();
			//centroid_cloud2.clear();
			//centroid_weight.clear();
			//transposition_matrix.clear();
			//sigma.clear();
			last_transform.clear();
			last_norm.clear();
			for(map<string, bool>::iterator iterator1 = data_enabled.begin(); iterator1 != data_enabled.end(); iterator1++)
			{
				for(map<string, bool>::iterator iterator2 = data_enabled.begin(); iterator2 != data_enabled.end(); iterator2++)
				{
					if(iterator1->first < iterator2->first)
					{
						pair<string,string> tfname(iterator1->first, iterator2->first);
						stringstream ss;
						ss << "calibration_" << iterator1->first << "W" << iterator2->first;
						string name_param = ss.str();
						nodeHandle->deleteParam(name_param);
						//ROS_INFO_STREAM("I DELETE PARAM : " << name_param);
					}
				}
			}
			data_enabled.clear();
			transform_calibrate.clear();
		}

	public:
		EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
		NodeCloud2Transform(int argc, char** argv):
		Node(ROS_NODE_CLOUD2TRANSFORM_NAME, ROS_NODE_CLOUD2TRANSFORM_RATE, argc, argv)
		{
			//subscriber_add<control_server_cmd>(ROS_TOPIC_RECORD, &NodeCloud2Transform::server_toggle);
			subscriber_add<mesureKinect>(ROS_TOPIC_PC, &NodeCloud2Transform::enqueue, 10);

			publisher_add<khi2>("/normTransform");
			publisher_add<MarkerArray>("/Marker/Camera_position");

			initialize_transform();
		}

		void on_start()
		{
		}

		void on_update()
		{
			bool update = false;
			int cpt = 0;
			for(map<string, deque<double>>::iterator it = joints_timestamps.begin(); it != joints_timestamps.end(); ++it)
			{
				double elapsedtime = Time::now().toSec() - it->second.back();
				if(elapsedtime < MAX_DELTAT)
				{
					data_enabled[it->first] = true;
					cpt++;
					update = true;
				}
				else
				{
					data_enabled[it->first] = false;
				}
			}

			if(update && cpt>1)
			{
				transform();
			}
		}

		void on_end()
		{

		}

		/*void action_on_toggle()
		{
			initialize_transform();
		}*/
	};
}

int main(int argc, char** argv)
{
	return (prog_kinect::NodeCloud2Transform(argc, argv)).run();
}
