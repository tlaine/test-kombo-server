#include <Eigen/Geometry>
#include <jansson.h>
#include <geometry_msgs/Point.h>
#include <kombos_msgs/mesureKinect.h>
#include <kombos_msgs/position_msg.h>
#include <kombos_msgs/segment_nb.h>
#include <kombos_msgs/sound.h>
#include <kombos_msgs/kalmanState.h>
#include "../include/Network.hpp"
#include "../include/Server.hpp"
#include "../include/ServerTCP.hpp"
#include "../include/JsonReader.hpp"
#include "../include/Node.hpp"
//#include <kombos_msgs/anthropo.h>
//#include "../lib/redox/include/redox.hpp"

using namespace std;
using namespace std::chrono;
using namespace boost;
using namespace ros;
using namespace Eigen;
//using namespace redox;
using namespace geometry_msgs;
using namespace kombos_msgs;

namespace prog_kinect
{
	class NodeJson2Cloud : public Node
	{
		private:
			Server server;
			string server_data;
			string server_speed;
			bool server_is_viewing = true;
			bool server_is_timeout = false;

			time_point<system_clock> clock_tag = system_clock::now();

			void server_init(const bool& is_mock)
			{
				string name = is_mock ? string(SERVER_NAME_MOCK) : string(SERVER_NAME_NORMAL);
				to_upper(name);

				int port = is_mock ? SERVER_PORT_MOCK : SERVER_PORT_NORMAL;
				ROS_INFO(SERVER_MESSAGE_START, name.c_str(), port);

				server.init(port);
				server.retrieve();
			}

			json_t* server_read(string data)
			{
				json_error_t error;
				json_t* root = json_loads(data.c_str(), 0, &error);

				if(!root)
				{
					root = NULL;
					ROS_ERROR_ONCE(JSON_MESSAGE_INVALID, data, error.line, error.position, error.text);
				}
				else if(!json_is_object(root))
				{
					json_decref(root);
					root = NULL;
					ROS_ERROR_ONCE(JSON_MESSAGE_NOTOBJECT, data);
				}

				return root;
			}

			void server_parse(json_t* json_root)
			{
				//mesureKinect message;
				kalmanState message_limbs;

				const char* key_message_type;
				json_t* value_message_content;
				json_object_foreach(json_root, key_message_type, value_message_content)
				{
					if (strcmp(key_message_type, JSON_KEY_SOUND) == 0)
					{
						int decibel = json_integer_value(value_message_content);
						if (decibel > JSON_VALUE_SOUND_MIN  && decibel < JSON_VALUE_SOUND_MAX)
						{
							sound message_sound;
							message_sound.indice = decibel;
							publisher_send(ROS_TOPIC_INDICE_SOUND, message_sound);
						}
					}
					else
					{
					    mesureKinect message;
						int fiabilite_total = 0;
						int id_kinect = atoi(key_message_type);
						message.typeCapteur = string(key_message_type);
						message.idCapteur = json_integer_value(json_object_get(value_message_content, "idSensor"));

						const char* key_joint_name;
						json_t* value_joint_properties;
						json_object_foreach(value_message_content, key_joint_name, value_joint_properties)
						{
							const char* key_property_name;
							json_t* value_property_name;
							if(strcmp(key_joint_name, JSON_KEY_INDIVIDUAL) == 0)
							{
								/*anthropo message_individual;
								json_object_foreach(value_joint_properties, key_property_name, value_property_name)
								{
									if(strcmp(key_property_name, JSON_KEY_GENDER) == 0)
									{
										message_individual.gender = json_string_value(value_property_name);
									}
									else if(strcmp(key_property_name, JSON_KEY_EMOTIONS) == 0)
									{
										message_individual.neutral = std::stof(json_string_value(json_object_get(value_property_name, "neutral")));
										message_individual.angry = std::stof(json_string_value(json_object_get(value_property_name, "angry")));
										message_individual.happy = std::stof(json_string_value(json_object_get(value_property_name, "happy")));
										message_individual.surprise = std::stof(json_string_value(json_object_get(value_property_name, "surprise")));
									}
									else if(strcmp(key_property_name, JSON_KEY_AGE) == 0)
									{
										message_individual.age = std::stoi(json_string_value(json_object_get(value_property_name, "years")));
									}
								}
								publisher_send(ROS_TOPIC_IND_PROPERTIES, message_individual);*/
							}
							else
							{
								position_msg message_position;
								position_msg message_speed;
								json_object_foreach(value_joint_properties, key_property_name, value_property_name)
								{
									if(strcmp(key_property_name, JSON_KEY_POSITION) == 0)
									{
										Point point;
										point.x = json_real_value(json_object_get(value_property_name, "x"));
										point.y = json_real_value(json_object_get(value_property_name, "y"));
										point.z = json_real_value(json_object_get(value_property_name, "z"));
										message_position.position = point;
										message_position.name = key_joint_name;
									}
									else if(strcmp(key_property_name, JSON_KEY_SPEED) == 0)
									{
										Point point;
										point.x = json_real_value(json_object_get(value_property_name, "x"));
										point.y = json_real_value(json_object_get(value_property_name, "y"));
										point.z = json_real_value(json_object_get(value_property_name, "z"));
										message_speed.position = point;
										message_speed.name = key_joint_name;

										message.vitesses.push_back(message_speed);
									}
									else if(strcmp(key_property_name, JSON_KEY_TRACKINGSTATE) == 0)
									{
									    int fiabilite = json_integer_value(json_object_get(value_property_name, JSON_KEY_TRACKINGSTATE)) -1;
									    message_position.fiabilite = fiabilite;
									    message_speed.fiabilite = fiabilite;
									    fiabilite_total += fiabilite;
									}
									/*else if(strcmp(key_property_name, JSON_KEY_BODYTRACKINGID) == 0)
									{
										message.idCapteur = json_integer_value(json_object_get(value_property_name, JSON_KEY_BODYTRACKINGID));
									}*/
									else if(strcmp(key_property_name, JSON_KEY_LIMBRADIUS) == 0)
									{
										float radius = json_real_value(json_object_get(value_property_name, JSON_KEY_LIMBRADIUS));
										message_limbs.name.push_back(key_joint_name);
										message_limbs.value.push_back(radius * 1E-3); // in mm, return m
									}
								}
								if(message_position.name != "")
								{
									message.mesures.push_back(message_position);
								}
							}
						}
						if(fiabilite_total >= (JSON_VALUE_TRACKINGSTATE_NUMBER * JSON_VALUE_TRACKINGSTATE_TOLERANCE))
						{
							segment_nb message_segment;
							message_segment.name = id_kinect;
							message_segment.nb = fiabilite_total;
							publisher_send(ROS_TOPIC_SEGMENTS, message_segment);
							publisher_send(ROS_TOPIC_LIMBRADIUS, message_limbs);

							string topic = string(ROS_TOPIC_PC);
							publisher_send(topic.c_str(), message);
						}
					}
				}
			}

		public:
			EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
			NodeJson2Cloud(int argc, char** argv):
			Node(ROS_NODE_JSON2CLOUD_NAME, ROS_NODE_JSON2CLOUD_RATE, argc, argv)
			{
				//subscriber_add<control_server_cmd>(ROS_TOPIC_RECORD, &NodeJson2Cloud::server_toggle);

				//publisher_add<anthropo>(ROS_TOPIC_IND_PROPERTIES);
				publisher_add<mesureKinect>(ROS_TOPIC_PC);
				publisher_add<segment_nb>(ROS_TOPIC_SEGMENTS);
				publisher_add<sound>(ROS_TOPIC_INDICE_SOUND);
				publisher_add<kalmanState>(ROS_TOPIC_LIMBRADIUS);
			}

			void on_start()
			{
				//redox_init(REDOX_HOST, REDOX_PORT);
				server_init(has_arg(ARG_MOCK));
			}

			void on_update()
			{
				server_data = server.retrieve();

				if(server_data.size() > 0)// && server_is_recording)
				{
					if (!server_is_viewing)
					{
						server_is_viewing = true;
						// _redox.set(REDOX_KEY_DATA, to_string(true));
					}

					json_t* json = server_read(server_data);
					if(json)
					{
						server_parse(json);
						json_decref(json);
					}
					//std::cout<<server_data<<std::endl;
					
				}
				else
				{
					if(server_is_viewing || server_is_timeout)
					{
						clock_tag = system_clock::now();
						server_is_viewing = false;
						server_is_timeout = false;
					}

					if (duration_cast<seconds>(system_clock::now() - clock_tag).count() > SERVER_TIMEOUT_SECONDS && !server_is_timeout)
					{
						// _redox.set(REDOX_KEY_DATA, to_string(false));
						// _redox.set(REDOX_KEY_ERRORS, to_string(++redox_errors));
						//_redox.set(REDOX_KEY_SKELETON, "{}");
						ROS_WARN_ONCE(SERVER_MESSAGE_NODATA, SERVER_TIMEOUT_SECONDS);

						server_is_timeout = true;
					}
				}
			}

			void on_end()
			{
				//_redox.disconnect();
			}
	};
}

int main(int argc, char** argv)
{
	return (prog_kinect::NodeJson2Cloud(argc, argv)).run();
}
