#include "../include/Node.hpp"

using namespace std;
using namespace ros;

namespace prog_kinect
{
	Node::Node(const string& name, const int& rate, int argc, char** argv)
	{
		init(argc, argv, name);
		for(int i = 1; i < argc; i++)
		{
			args.push_back(argv[i]);
		}

		title = string(name);
		loop_rate = rate;
		nodeHandle = new NodeHandle();

		// MODEL DATA
		json2model model = json2model(false);

		model_basducorps = model.GetBasDuCorps();  // true
		model_vitesse = model.GetVitesse();        // false

		joints_indices = model.GetJointIdx();      // map<string, int>, size : 18, cf PointCloudFusion constructor
		indices_joints = model.GetIdxJoint();      // map<int, string>, same as before, just flipped 

		states_names = model.GetStates();          // vector<string>, order is important, exactly "States" names list
                                                   // until "tibia"
                                                   
		joints_count = joints_indices.size();      // 18
		states_count = model.GetSize().x();        // size of states_names : 46
		measures_count = model.GetSize().y();      // (nbJointsHaut+nbJointsBas)*3 : 18*3 = 54

		limb_extremity = model.GetLimbExtremity();
		limb_coef_proximal = model.GetLimbCoefProximal();
		limb_percent_weight = model.GetLimbPercentWeight();
		limb_density = model.GetLimbDensity();
	}

	Node::~Node()
	{
		delete nodeHandle;
	}

	int Node::run()
	{
		int result = EXIT_SUCCESS;
		try
		{
			on_start();

			if(loop_rate > 0)
			{
				Rate loop_rate_manager(loop_rate);
				while(ok())
				{
					on_update();
					spinOnce();
					loop_rate_manager.sleep();
				}
			}
			else
			{
				spin();
			}

			on_end();
		}
		catch(...)
		{
			result = EXIT_FAILURE;
		}

		return result;
	}

	bool Node::has_arg(const char* arg)
	{
		return find(args.begin(), args.end(), string(arg)) != args.end();
	}
}
