#!/usr/bin/env python

import rospy

from utils.watcher import WATCHER
from db.storer_config import STORER_INFLUX
from db.longueurK_object import LongueurKObjectInflux
from db.rula_array_object import RulaArrayObjectInflux
from db.skeleton_object import SkeletonObjectInflux
from db.skeleton_optitrack_object import SkeletonOptiObjectInflux
from db.skeleton_tf_object import SkeletonTFObjectInflux
# from db.skeletonraw_object import SkeletonRawObjectInflux
# from db.segment_object import SegmentObjectInflux
from db.space_position_object import SpacePositionObjectInflux
from db.indice_object import IndiceObjectInflux

"""def handle_new_command(data):
    if data.cmd == 'start':
        AbstractStorer.RECORDING = True
        AbstractStorer.SESSION_ID = data.session_id
    elif data.cmd == 'stop':
        AbstractStorer.RECORDING = False
        AbstractStorer.SESSION_ID = False"""

ROS_TOPIC_STORER = "storer"
MESSAGE_START = "[STORER] STARTING STORER, TESTING FILTER ERROR"
MESSAGE_LISTENING = "[STORER] LISTENING FOR DB EVENTS..."

if __name__ == '__main__':
    try:
        rospy.init_node(ROS_TOPIC_STORER, anonymous=False, log_level=rospy.DEBUG)
        WATCHER.watch_log()

        rospy.loginfo(MESSAGE_START)

        """control_server = rospy.Subscriber(  CSConfig.START_STOP_MSG,
                                            control_server_cmd,
                                            handle_new_command)"""

        # ===== MOTION =====
        # mo_obj = MotionObjectInflux(storer=STORER_INFLUX, nbr_to_wait=5, frequency=5)
        # mo_obj.start()

        # counter = Counter()

        # ===== RULA ARRAY =====
        DB_OBJ_RULAARRAY = RulaArrayObjectInflux(storer=STORER_INFLUX, nbr_to_wait=1)
        DB_OBJ_RULAARRAY.start()

        # ===== RULA =====
        # r_obj = RulaObjectInflux(storer=STORER_INFLUX, nbr_to_wait=2, frequency=25)
        # r_obj.start()

        # ===== SKELETON =====
        DB_OBJ_SKELETON = SkeletonObjectInflux(storer=STORER_INFLUX, nbr_to_wait=1)
        DB_OBJ_SKELETON.start()

        # DB_OBJ_SKELETON_OPTI = SkeletonOptiObjectInflux(storer=STORER_INFLUX, nbr_to_wait=1)
        # DB_OBJ_SKELETON_OPTI.start()

        # DB_OBJ_SKELETON_TF = SkeletonTFObjectInflux(storer=STORER_INFLUX, nbr_to_wait=1)
        # DB_OBJ_SKELETON_TF.start()

        # ===== SKELETON_RAW =====
        # DB_OBJ_SKELETON_RAW = SkeletonRawObjectInflux(storer=STORER_INFLUX, nbr_to_wait=1)
        # DB_OBJ_SKELETON_RAW.start()

        # ===== SOUND =====
        # s_obj = SoundObjectInflux(storer=STORER_INFLUX, nbr_to_wait=2, frequency=25)
        # s_obj.start()

        # ===== SPACE_POSITION =====
        DB_OBJ_POSITION = SpacePositionObjectInflux(storer=STORER_INFLUX, nbr_to_wait=5)
        DB_OBJ_POSITION.start()

        # # ===== INDICE =====
        DB_OBJ_INDEX = IndiceObjectInflux(storer=STORER_INFLUX, nbr_to_wait=1)
        DB_OBJ_INDEX.start()

        # r_obj = RulaObjectPostgres(storer=STORER_POSTGRES, nbr_to_wait=1, frequency=25)
        # r_obj.start()

        # ===== ANGLE =====
        """ KALMAN """
        # jk_obj = KalmanObjectInflux(storer=STORER_INFLUX, nbr_to_wait=5, frequency=25)
        # jk_obj.start()

        # jo_obj = OrientationObjectInflux(storer=STORER_INFLUX, nbr_to_wait=5, frequency=25)
        # jo_obj.start()

        # j_obj = JointObjectInflux(storer=STORER_POSTGRES, nbr_to_wait=5, frequency=25)
        # j_obj.start()

        # ===== LONGUEUR =====
        # j_obj = JointObjectPostgres(storer=STORER_POSTGRES, nbr_to_wait=5, frequency=25)
        # j_obj.start()

        # l_obj = LongueurObjectInflux(storer=STORER_INFLUX, nbr_to_wait=5, frequency=25)
        # l_obj.start()

        # DB_OBJ_LENGTH = LongueurKObjectInflux(storer=STORER_INFLUX, nbr_to_wait=5)
        # DB_OBJ_LENGTH.start()

        # ===== LONGUEUR =====
        # j_obj = SemgentObjectPostgres(storer=STORER_POSTGRES, nbr_to_wait=5, frequency=25)
        # j_obj.start()

        # S_OBJ = SegmentObjectInflux(storer=STORER_INFLUX, nbr_to_wait=5)
        # S_OBJ.start()

        # l_obj = LongueurObjectPostgres(storer=STORER_POSTGRES, nbr_to_wait=5, frequency=25)
        # l_obj.start()

        # Start Subscribe

        rospy.loginfo(MESSAGE_LISTENING)
        rospy.spin()

    except rospy.ROSInterruptException:
        pass
