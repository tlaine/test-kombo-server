import argparse
import transforms3d

from bluepy import btle
from struct import pack, unpack
from time import sleep, gmtime, strftime
from threading import Thread
from os import system
from subprocess import call
from math import pi

from db.storer_config import STORER_INFLUX
from db.inertial_object import InertialObjectInfluxRaw

# pylint: skip-file

# global definitions
# frame values:
timestamp_1 = 0
timestamp_2 = 0
quaternion_1 = [0, 0, 0, 0]
quaternion_2 = [0, 0, 0, 0]
calib_1 = 0
calib_2 = 0
angles = [0, 0, 0]
csvfile = None
store_in_db = False
inertial_obj = None

# BLE data
battery_ch1 = None
uart_rx_ch1 = None
battery_ch2 = None
uart_rx_ch2 = None
BATTERY_LEVEL_SHORT_UUID = 0x2a19
UART_SERVICE_UUID = '6e400001-b5a3-f393-e0a9-e50e24dcca9e'
UART_TX_CHARACTERISTIC_UUID = '6e400002-b5a3-f393-e0a9-e50e24dcca9e'
UART_RX_CHARACTERISTIC_UUID = '6e400003-b5a3-f393-e0a9-e50e24dcca9e'
BATTERY_UUID = btle.UUID(BATTERY_LEVEL_SHORT_UUID)
UART_TX_UUID = btle.UUID(UART_TX_CHARACTERISTIC_UUID)
UART_RX_UUID = btle.UUID(UART_RX_CHARACTERISTIC_UUID)


def computeAngles():
    global angles, quaternion_1, quaternion_2, timestamp_1, timestamp_2, calib_1, calib_2, csvfile, inertial_obj
    # compute angles if synchronisation error is less than 5 ms
    print("%d %d" % (timestamp_1, timestamp_2))
    if (abs(timestamp_1 - timestamp_2) < 5):
        q_2_conjugate = transforms3d.quaternions.qconjugate(quaternion_2)
        q_angles = transforms3d.quaternions.qmult(q_2_conjugate, quaternion_1)
        angles = transforms3d.euler.quat2euler(q_angles)
        angles = [i * 180.0 / pi for i in angles]
        # (X, Y, Z)
        print("Euler angles: [%.6s %.6s %.6s]" % (angles[0], angles[1], angles[2]))
        if args.export:
            csvfile.write(
                '%d %d %d %.6s %.6s %.6s\n' %
                (timestamp_1, calib_1, calib_2, angles[0], angles[1], angles[2]))
        if args.db and inertial_obj:
            data = {}
            data['timestamp'] = timestamp_1
            data['calib_1'] = calib_1
            data['calib_2'] = calib_2
            data['angle_x'] = angles[0]
            data['angle_y'] = angles[1]
            data['angle_z'] = angles[2]
            inertial_obj.message_handler(data)


class MyDelegate(btle.DefaultDelegate):
    def __init__(self):
        btle.DefaultDelegate.__init__(self)

    def handleNotification(self, cHandle, data):
        # TODO switch case
        if cHandle == 0x15:  # battery notifications
            time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
            print("%s %s" % (time, unpack('B', data)[0]))

        if cHandle == 0x10:  # uart notifications
            if len(data) == 15:
                global timestamp_1, timestamp_2, calib_1, calib_2, quaternion_1, quaternion_2
                node_id, timestamp, calibration, q0, q1, q2, q3, line_end = unpack(
                    '>BLB4hB', data)
                system_calib = (calibration & 0xC0) >> 6

                if node_id == 1:
                    timestamp_1 = timestamp
                    quaternion_1 = [q0, q1, q2, q3]
                    calib_1 = system_calib

                if node_id == 2:
                    timestamp_2 = timestamp
                    quaternion_2 = [q0, q1, q2, q3]
                    calib_2 = system_calib
                computeAngles()


class ScanDelegate(btle.DefaultDelegate):
    def __init__(self):
        btle.DefaultDelegate.__init__(self)

    def handleDiscovery(self, dev, isNewDev, isNewData):
        if isNewDev:
            print("Discovered device", dev.addr)
        elif isNewData:
            print("Received new data from", dev.addr)


class NotificationListener(Thread):

    """Thread which listen and print notification"""

    def __init__(self, device):
        Thread.__init__(self)
        self.device = device

    def run(self):
        while True:
            self.device.waitForNotifications(0)


def synchronise(address_1, address_2):
    print("Synchronising ...")
    try:
        system("sudo hciconfig hci0 noleadv")
        node_1 = btle.Peripheral(address_1, btle.ADDR_TYPE_RANDOM)
        node_2 = btle.Peripheral(address_2, btle.ADDR_TYPE_RANDOM)
        uart_tx_ch1 = node_1.getCharacteristics(uuid=UART_TX_UUID)[0]
        uart_tx_ch2 = node_2.getCharacteristics(uuid=UART_TX_UUID)[0]

        sleep(1)
        uart_tx_ch1.write(
            pack(
                '>BBBBB',
                0x53,
                0x43,
                0x41,
                0x4E,
                0x00))  # enter node 1 in scan mode
        uart_tx_ch2.write(
            pack(
                '>BBBBB',
                0x53,
                0x43,
                0x41,
                0x4E,
                0x00))  # enter node 2 in scan mode
        sleep(2)
        system("hcitool -i hci0 cmd 0x08 0x0008 04 01 02 03 04 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00")
        print("sending sync packet")
        call(["hciconfig", "hci0", "leadv"])
        sleep(2)
        system("sudo hciconfig hci0 noleadv")
        print("Synchronised!")
        print('')

    except btle.BTLEException:
        print("Failed to connect")


def connect(address_1, address_2):
    print("Connecting to nodes...")
    try:
        node_1 = btle.Peripheral(address_1, btle.ADDR_TYPE_RANDOM)
        node_1.setDelegate(MyDelegate())
        print("Node 1 connected !")
    except btle.BTLEException:
        print("Cannot connect to Node 1")
        node_1 = None

    try:
        node_2 = btle.Peripheral(address_2, btle.ADDR_TYPE_RANDOM)
        node_2.setDelegate(MyDelegate())
        print("Node 2 connected !")
    except btle.BTLEException:
        print("Cannot connect to Node 2")
        node_2 = None
    return node_1, node_2


def notificationSubscribe(node1, node2):
    # subscribe to notification to receive data
    # 2 threads (1 for each node) are continiously waiting for notification
    # packets
    print("Subscribing to notifications...")
    global battery_ch1, battery_ch2, uart_rx_ch1, uart_rx_ch2, store_in_db

    if node1 is not None:
        node1.writeCharacteristic(
            battery_ch1.getHandle() + 1,
            pack(
                'bb',
                0x01,
                0x00))
        node1.writeCharacteristic(
            uart_rx_ch1.getHandle() + 1,
            pack(
                'bb',
                0x01,
                0x00))

        thread1 = NotificationListener(node_1)
        thread1.start()

    if node2 is not None:
        node2.writeCharacteristic(
            battery_ch2.getHandle() + 1,
            pack(
                'bb',
                0x01,
                0x00))
        node2.writeCharacteristic(
            uart_rx_ch2.getHandle() + 1,
            pack(
                'bb',
                0x01,
                0x00))

        thread2 = NotificationListener(node_2)
        thread2.start()


def arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("node_1", help="name of the 1st node")
    parser.add_argument("node_2", help="name of the 2nd node")
    parser.add_argument(
        "--sync",
        help="synchronise the nodes",
        action="store_true")
    parser.add_argument("--db", help="Store in DB", action="store_false")
    parser.add_argument(
        "--export",
        help="Export to csv file",
        action="store_false")

    return parser.parse_args()


def scan(args):
    # Scan BLE devices, looking for device names specified by the user
    print("Looking for Nodes...")
    scanner = btle.Scanner().withDelegate(ScanDelegate())
    devices = scanner.scan(5.0)
    node_1_address = None
    node_2_address = None

    for dev in devices:
        for (adtype, desc, value) in dev.getScanData():
            if desc == 'Complete Local Name':
                if value == args.node_1 + '\0':
                    print("Device %s (%s), RSSI=%d dB" % (dev.addr, dev.addrType, dev.rssi))
                    print('%s found !' % (value))
                    node_1_address = dev.addr
                if value == args.node_2 + '\0':
                    print("Device %s (%s), RSSI=%d dB" % (dev.addr, dev.addrType, dev.rssi))
                    print('%s found !' % (value))
                    node_2_address = dev.addr

    return node_1_address, node_2_address


# Initialisation  -------
if __name__ == "__main__":
    # parse command line arguments
    args = arg_parser()

    if args.db:
        store_in_db = True
        inertial_obj = InertialObjectInfluxRaw(storer=STORER_INFLUX)
    if args.export:
        csvfile = open('output.csv', 'a')

    node_1_address, node_2_address = scan(args)

    if not node_1_address:
        exit('Node 1 not found !')
    if not node_2_address:
        exit('Node 2 was not found !')

    # Synchronise the nodes timestamps
    if args.sync:
        synchronise(node_1_address, node_2_address)

    # Connect to nodes
    node_1, node_2 = connect(node_1_address, node_2_address)

    # get the battery service and uart service handlers
    if node_1 is not None:
        print('')
        battery_ch1 = node_1.getCharacteristics(uuid=BATTERY_UUID)[0]
        uart_rx_ch1 = node_1.getCharacteristics(uuid=UART_RX_UUID)[0]
        time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        print(time)
        print("Node 1 Battery level %d percent" % (unpack('B', battery_ch1.read())[0]))

    if node_2 is not None:
        print('')
        battery_ch2 = node_2.getCharacteristics(uuid=BATTERY_UUID)[0]
        uart_rx_ch2 = node_2.getCharacteristics(uuid=UART_RX_UUID)[0]
        time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        print(time)
        print("Node 2 Battery level %d percent" %
              (unpack('B', battery_ch2.read())[0]))

    # sleep(2)
    print('')
    raw_input("Press Enter to start dataflow...")
    notificationSubscribe(node_1, node_2)
