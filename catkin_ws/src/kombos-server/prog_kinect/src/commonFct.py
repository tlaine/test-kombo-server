import sympy as sy
import numpy as np

# retourne la matrice de passage correspondant a une rotation autour de l
# axe x d'angle o


def matrice_de_passage_rotationx(o):
    return sy.Matrix([[1, 0, 0, 0], [0, sy.cos(o), -sy.sin(o), 0],
                      [0, sy.sin(o), sy.cos(o), 0], [0, 0, 0, 1]])

# retourne la matrice de passage correspondant a une rotation autour de l
# axe y d'angle o


def matrice_de_passage_rotationy(o):
    return sy.Matrix([[sy.cos(o), 0, sy.sin(o), 0], [0, 1, 0, 0],
                      [-sy.sin(o), 0, sy.cos(o), 0], [0, 0, 0, 1]])

# retourne la matrice de passage correspondant a une rotation autour de l
# axe z d'angle o


def matrice_de_passage_rotationz(o):
    return sy.Matrix([[sy.cos(o), -sy.sin(o), 0, 0],
                      [sy.sin(o), sy.cos(o), 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])

# retourne la matrice de passage correspondant a une translation de x, y et z


def matrice_de_passage_translation(x, y, z):
    return sy.Matrix([[1, 0, 0, x], [0, 1, 0, y], [0, 0, 1, z], [0, 0, 0, 1]])

# retourne la matrice de passage correspondant a la base a partir de 3 points


def matrice_de_passage_base(pb, px1, px2, py):

    tmp = (px2 - px1) / norme(px2 - px1)
    vy = (py - pb) / norme(py - pb)
    vx = tmp - ((vy.dot(tmp)) * vy)
    vx = vx / norme(vx)
    vz = np.cross(vx, vy)
    return sy.Matrix([[vx[0], vy[0], vz[0], pb[0]], [vx[1], vy[1], vz[1], pb[1]], [
                     vx[2], vy[2], vz[2], pb[2]], [0, 0, 0, 1]])


def matrice_de_passage_inverse(m):
    t = sy.eye(4)
    rt = m[0:3, 0:3].transpose()
    t[0:3, 0:3] = rt
    t[0:3, 3] = -rt * m[0:3, 3]
    return t

# retourne le point P de la matrice de passage


def compute_p(m):
    return m[0:3, 3]

# retourne la matrice de rotation de la matrice de passage


def compute_r(m):
    return m[0:3, 0:3]

# retourne la norme au carre du vecteur V


def norme_carre(v):
    return v[0] * v[0] + v[1] * v[1] + v[2] * v[2]

# retourne la norme de V


def norme(v):
    return sy.sqrt(norme_carre(v))


def compute_f(m):
    omat = sy.Matrix([0, 0, 0, 1])
    f = m * omat
    return f


def print_matrixt(m):
    length = np.shape(m)
    print(length)
    for i in range(0, length[0]):
        for j in range(0, length[1]):
            s = 't' + repr(i) + '_' + repr(j) + ' = '
            print(s)
            print(m[i, j])


def print_matrix_col(m):
    length = np.shape(m)
    print(length)
    # i = 1
    for i in range(length[1]):
        # print('******* COL '+repr(i)+' ********')
        # for i in range(l[1]):
        for j in range(60, length[0]):
            # for j in range(l[0]):
            # s = 'M(' + repr(j) + ' , ' + repr(i) + ') = '
            # print s
            print(m[j, i])


def jacobian(f, q):
    j = sy.Matrix(np.arange((len(f)) * len(q)).reshape((len(f)), len(q)))
    for i in range(0, len(f)):
        for j in range(0, len(q)):
            j[i, j] = sy.diff(f[i], q[j])
    # print_matrixt(J)
    return j


def jacobian_t(T):
    J = T[0:2,3]
    return J


def jacobian_r(Tz, To):
    z = Tz[0:3,2]
    R = Tz[0:3,0:3]
    Po = To[0:3,3]
    tmp = R*Po
    return(product_cross(z,tmp))


def product_cross(a, b):
    return [a[1]*b[2] - a[2]*b[1], a[2]*b[0] - a[0]*b[2], a[0]*b[1] - a[1]*b[0]]


def svdinverse(j):
    length = np.shape(j)
    a = np.random.randn(length[0], length[1]) - np.random.randn(length[0], length[1])  # pylint: disable-all
    for i in range(length[0]):
        for j in range(length[1]):
            a[i, j] = j[i, j]
    u, s, v = np.linalg.svd(a, full_matrices=True)

    r = min(length[0], length[1])
    rang = r
    tolerance = 1e-12
    for i in reversed(range(r)):
        if s[i] < tolerance:
            rang -= 1

    for i in range(rang):
        s[i] = 1 / s[i]

    s = np.zeros(length)
    s[:np.shape(s)[0], :np.shape(s)[0]] = np.diag(s)
    # on transpose s ensuite
    jinv = np.dot(np.transpose(v), np.dot(np.transpose(s), np.transpose(u)))
    return jinv


def dot3(a, b, c):
    """ returns the matrix multiplication of a*b*c"""
    return np.dot(a, np.dot(b, c))


def dot4(a, b, c, d):
    """ returns the matrix multiplication of a*b*c*d"""
    return np.dot(a, np.dot(b, np.dot(c, d)))


def setter(value, dim_x, dim_y):
    """ Returns a copy of 'value' as an numpy.array with dtype=float. throws exception if the array is not dimensioned correctly. Value may be any type which converts to numpy.array (list, np.array, np.matrix, etc)
    """
    v = np.array(value, dtype=float)
    if v.shape != (dim_x, dim_y):
        raise Exception('must have shape ({},{})'.format(dim_x, dim_y))
    return v


def setter_1d(value, dim_x):
    """ Returns a copy of 'value' as an numpy.array with dtype=float. throws exception if the array is not dimensioned correctly. Value may be any type which converts to numpy.array (list, np.array, np.matrix, etc)
    """

    v = np.array(value, dtype=float)
    shape = v.shape
    if shape[0] != (dim_x) or v.ndim > 2 or (v.ndim == 2 and shape[1] != 1):
        raise Exception(
            'has shape {}, must have shape ({},{})'.format(
                shape, dim_x, 1))
    return v


def setter_scalar(value, dim_x):
    """ Returns a copy of 'value' as an numpy.array with dtype=float. throws exception if the array is not dimensioned correctly. Value may be any type which converts to numpy.array (list, np.array, np.matrix, etc), or a scalar, in which case we create a diagonal matrix with each diagonal element == value. dim_x is used iff value is scalar, otherwise it is determined from the shape of value
    """
    # if isscalar(value):
    #     v = np.eye(dim_x) * value
    # else:
    #     v = np.array(value, dtype=float)
    #     dim_x = v.shape[0]

    v = np.array(value, dtype=float)
    dim_x = v.shape[0]

    if v.shape != (dim_x, dim_x):
        raise Exception('must have shape ({},{})'.format(dim_x, dim_x))
    return v
