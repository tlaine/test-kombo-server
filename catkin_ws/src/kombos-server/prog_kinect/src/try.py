# pylint: disable=invalid-name, undefined-variable
import sympy as sy
from fctTmatrix import *

# ------ VARIABLES

# angles
th1, th2, th3 = sy.symbols('th1 th2 th3', real=True)  # spine
th4, th5 = sy.symbols('th4 th5', real=True)  # head
th6, th7, th8 = sy.symbols('th6 th7 th8', real=True)  # shoulderR
th9 = sy.symbols('th9', real=True)  # elbowR
th13, th14, th15 = sy.symbols('th13 th14 th15', real=True)  # shoulderL
th16 = sy.symbols('th16', real=True)  # elbowL
th20, th21, th22 = sy.symbols('th20 th21 th22', real=True)  # hipR
th23 = sy.symbols('th23', real=True)  # kneeR
th24, th25, th26 = sy.symbols('th24 th25 th26', real=True)  # hipL
th27 = sy.symbols('th27', real=True)  # kneeL

# longueurs
l1, l2, l3 = sy.symbols('l1 l2 l3', real=True)  # spine and head
l4, l5, l6 = sy.symbols('l4 l5 l6', real=True)  # arm
l7, l8, l9 = sy.symbols('l7 l8 l9', real=True)  # leg

# vecteurs x, y et z unitaire
x = sy.Matrix(3, 1, [1, 0, 0])
y = sy.Matrix(3, 1, [0, 1, 0])
z = sy.Matrix(3, 1, [0, 0, 1])

# ------- EQUATIONS

# le repere 0 est fixe au pelvis

# matrices de passage Ti du repere i-1 vers le repere i

T0_1 = matrice_de_passage_translation(0, l1, 0)  # spineMid
T1_5 = matrice_de_passage_rotationx(th1) * matrice_de_passage_rotationz(
    th2) * matrice_de_passage_rotationy(th3) * matrice_de_passage_translation(0, l2, 0)  # spineShoulder
T5_7 = matrice_de_passage_rotationx(
    th4) * matrice_de_passage_rotationz(th5) * matrice_de_passage_translation(0, l3, 0)  # head
T5_8 = matrice_de_passage_translation(-l4, 0, 0)  # shoulderR
T5_15 = matrice_de_passage_translation(l4, 0, 0)  # shoulderL
T8_10 = matrice_de_passage_rotationy(th6) * matrice_de_passage_rotationz(
    th7) * matrice_de_passage_rotationy(-th8) * matrice_de_passage_translation(0, l5, 0)  # elbowR
T15_17 = matrice_de_passage_rotationy(th13) * matrice_de_passage_rotationz(
    th14) * matrice_de_passage_rotationy(th15) * matrice_de_passage_translation(0, l5, 0)  # elbowL
T10_12 = matrice_de_passage_rotationx(
    th9) * matrice_de_passage_translation(0, l6, 0)  # wristR
T17_19 = matrice_de_passage_rotationx(
    th16) * matrice_de_passage_translation(0, l6, 0)  # wristL
T0_22 = matrice_de_passage_translation(-l7, 0, 0)  # hipR
T0_28 = matrice_de_passage_translation(l7, 0, 0)  # hipL
T22_25 = matrice_de_passage_rotationx(th20) * matrice_de_passage_rotationz(
    th21) * matrice_de_passage_rotationy(th22) * matrice_de_passage_translation(0, l8, 0)  # kneeR
T28_31 = matrice_de_passage_rotationx(th24) * matrice_de_passage_rotationz(
    th25) * matrice_de_passage_rotationy(th26) * matrice_de_passage_translation(0, l8, 0)  # kneeL
T25_27 = matrice_de_passage_rotationx(
    th23) * matrice_de_passage_translation(0, l9, 0)  # ankleR
T31_33 = matrice_de_passage_rotationx(
    th27) * matrice_de_passage_translation(0, l9, 0)  # ankleL

# matrice de passage T0_i du repere 0 vers le repere i
T0_5 = T0_1 * T1_5
T0_7 = T0_5 * T5_7
T0_8 = T0_5 * T5_8
T0_10 = T0_8 * T8_10
T0_12 = T0_10 * T10_12
T0_15 = T0_5 * T5_15
T0_17 = T0_15 * T15_17
T0_19 = T0_17 * T17_19
T0_25 = T0_22 * T22_25
T0_27 = T0_25 * T25_27
T0_31 = T0_28 * T28_31
T0_33 = T0_31 * T31_33

# print ' ------------------------------------------------- '
# print 'T0_5 : origin to spineShoulder'
# print_matrixt(T0_5)
# print ' ------------------------------------------------- '
# print 'T0_8 : origin to shoulderRight'
# print_matrixt(T0_8)
# print ' ------------------------------------------------- '
# print 'T0_10 : origin to elbowRight'
# print_matrixt(T0_10)
# print ' ------------------------------------------------- '
# print 'T0_12 : origin to wristRight'
# print_matrixt(T0_12)


# Jacobienne

print(' ------------------------------------------------- ')
print('T8_12 : origin shoulder to wristRight')
print_matrixt(T8_10 * T10_12)

# f = compute_f(T0_5, sy.Matrix([0.033,-0.464,2.075,1])) # origine spineBase
# f = compute_f(T8_10, sy.Matrix([0.17,0.016,2.08,1]))
f = compute_f(T8_10 * T10_12, sy.Matrix([0, 0, 0, 1]))
# f = [T0_5[0,3], T0_5[1,3], T0_5[2,3], 1]

# X = sy.Matrix([th1, th2, th3, l1, l2])
# X = [th1, th2, th3, th4, th5, th6, th7, th8, th9, th13, th14, th15, th16, th20, th21, th22, th23, th24, th25, th26, th27, l1, l2, l3, l4, l5, l6, l7, l8, l9]
# x = sy.Matrix([th1, th2, th3, th6, th7, th8, th9, l1, l2, l4, l5, l6])
X = sy.Matrix([th6, th7, th8, th9, l5, l6])
J = sy.Matrix(np.arange(len(f) * len(X)).reshape(len(f), len(X)))

# x = sy.Symbol('x')
# G = sy.diff(sy.sin(x), x)
# print G


for i in range(0, len(f)):
    for j in range(0, len(X)):
        # print j
        J[i, j] = sy.diff(f[i], X[j])
        # print J[i,j]

print(' ------------------------------------------------- ')
print('JACOBIENNE')
print_matrixt(J)

# print("\n ------------------ Eval J : -----------------------\n")
# evalJ = J.subs([(th1,-0.058077), (th2,-0.066824), (th3,0.079214), (th6,-1.1840), (th7,-0.0051473), (th8,-0.66944), (th9,-1.1953), (l1,0.28275), (l2,0.20740), (l4,0.17873), (l5,0.24616), (l6,0.25346)])
# evalJ = J.subs([(th1,-0.058077), (th2,-0.066824), (th3,0.079214), (l1,0.28241), (l2,0.20723)])
# print_matrixt(evalJ)

# deltaX = X.subs([(th1,-0.0001), (th2,-0.0004), (th3,-0.0005), (th6,0.0033211), (th7,-0.010775), (th8,0.0024545), (th9,0.016528), (l1,0.00005), (l2,0.00003), (l4,0.0004), (l5,0.00001), (l6,0.003)]) # delta [th6, th7, th8, th9, l5, l6]
# deltaX = X.subs([(th1,-1.3442e-04), (th2,-4.2041e-04), (th3,-5.3013e-04), (l1,4.8763e-05), (l2,3.4155e-05)])

# deltaY = evalJ*deltaX
# print("\n ------------------ Eval delta Y : -----------------------\n")
# print_matrixt(deltaY)
