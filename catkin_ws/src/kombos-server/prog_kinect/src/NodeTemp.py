#!/usr/bin/env python
# pylint: disable=ungrouped-imports

import serial
import rospy
from kombos_msgs.msg import Temperature
from utils.watcher import WATCHER

ROS_NODE_TEMP = "IndiceTemperature"
ROS_TOPIC_TEMPERATURE = "/IndiceTemperature"
MESSAGE_START = "[TEMPS] STARTING TEMPS"
#Serial_port = '/dev/ttyACM0'
baud_rate = 9600
# ser = serial.Serial(Serial_port, baud_rate)


def get_temp():
    temp = Temperature()
#    line = ser.readline()
#    line = line.decode('utf-8').strip()
    try:
        temp.temperature_value = 200 #round(float(line))
    except ValueError:
        print("Can't convert to float: ")
        temp.temperature_value = 0
    except Exception as e:
        print("Can't handle str " + str(e))
        temp.temperature_value = 0

    # clear serial buff
    # ser.readall()

    return temp


def callback(data):
    rospy.loginfo(rospy.get_caller_id() + "The temperature is %s", data.data)


def talker():
    rospy.init_node(ROS_NODE_TEMP, anonymous=False, log_level=rospy.DEBUG)
    pub = rospy.Publisher(ROS_TOPIC_TEMPERATURE, Temperature, queue_size=10)
    WATCHER.watch_log()
    rate = rospy.Rate(1)  # 1hz
    while not rospy.is_shutdown():
        temp = get_temp()

        rospy.loginfo(temp)
        pub.publish(temp)
        rate.sleep()


if __name__ == '__main__':
    try:

        talker()

    except rospy.ROSInterruptException:
        pass
