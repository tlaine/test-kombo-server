#include <kombos_msgs/rulaArray.h>
#include <kombos_msgs/motion.h>
#include <kombos_msgs/indiceSon.h>
#include <kombos_msgs/indice.h>
#include "../include/Network.hpp"
#include "../include/Utils.hpp"
#include "../include/Node.hpp"
#include "std_msgs/Float64.h"
#include "../include/JsonReader.hpp"

using namespace std;
using namespace ros;
using namespace kombos_msgs;

namespace prog_kinect
{
    class NodeGetIndice : public Node
    {
        private:
            map<string, float> indice_rula;
            map<string, float> indice_reba;
            map<string, float> indice_motion;
            deque<float> indice_balance_history;
            int indice_sound = 0;
            double start_at = 0;

            void refresh_rula(const rulaArray::ConstPtr& message)
            {
                for(int i = 0; i < message->name.size(); ++i)
                {
                    indice_rula[message->name[i]] = message->indice[i];
                }
            }

	       void refresh_reba(const rulaArray::ConstPtr& message)
            {
                for(int i = 0; i < message->name.size(); ++i)
                {
                    indice_reba[message->name[i]] = message->indice[i];
                }
            }

            void refresh_motion(const motion::ConstPtr& message)
            {
                for(int i = 0; i < message->names.size(); ++i)
                {
                    indice_motion[message->names[i]] = message->sums[i];
                }
            }

            void refresh_balance(const std_msgs::Float64::ConstPtr& message)
            {
                if ( indice_balance_history.size() == 5)
                {
                    indice_balance_history.pop_front();
                }
                indice_balance_history.push_back(message->data);
            }

            float compute_motion_score()
            {
                double session_duration = Time::now().toSec() - this->start_at;
                // we don't change score for the first 5min
                if (session_duration < (60*5))
                {
                    return 0;
                }
                float score = 0;

                float average_speed = (indice_motion[JOINT_NAME_COM] / 1000.0) / (session_duration / 3600.0);

                if (average_speed > 10 )
                {
                    score += 10;
                }
                else if (average_speed > 7 )
                {
                    score += 5;
                }
                else if (average_speed > 5 )
                {
                    score += 1;
                }
                else if (average_speed > 1 )
                {
                    score += 0;
                }
                else if (average_speed >= 0 )
                {
                    score += 2;
                }
            }

            float compute_balance_score()
            {
                if(indice_balance_history.size() == 0) return 0;
                float indice = std::accumulate(indice_balance_history.begin(), indice_balance_history.end(), 0) / indice_balance_history.size() ;
                /**
                    from 0 to -5 cm : +1 score
                    from -5 to -15 cm : +3 score
                    from -15 to -30 cm : +6 score
                    from -30 to infinity cm : +10 score
                **/
                if (indice > 25E-3)
                {
                    return 0;
                }
                else if (indice > -25E-3)
                {
                    return 1;
                }
                else if (indice > -15E-2)
                {
                    return 3;
                }
                else if (indice > -30E-2)
                {
                    return 6;
                }
                else
                {
                    return 10;
                }
            }

            void refresh_sound(const indiceSon::ConstPtr& message)
            {
                indice_sound = message->sound;
            }

            json_t* server_read(string data)
            {
                json_error_t error;
                json_t* root = json_loads(data.c_str(), 0, &error);

                if(!root)
                {
                    root = NULL;
                    ROS_ERROR_ONCE(JSON_MESSAGE_INVALID, data, error.line, error.position, error.text);
                }
                else if(!json_is_object(root))
                {
                    json_decref(root);
                    root = NULL;
                    ROS_ERROR_ONCE(JSON_MESSAGE_NOTOBJECT, data);
                }

                return root;
            }

            float get_segment()
            {
                if(this->start_at == 0) return 0;
                float score_segment = 0;
                // get total time
                double session_duration = Time::now().toSec() - this->start_at;
                json_t* json = server_read(_redox.get("segments_reba"));
                if(json)
                {
                    float cumul_time_ratio;
                    const char* key_message_type;
                    json_t* value_message_content;
                    json_object_foreach(json, key_message_type, value_message_content)
                    {
                        cumul_time_ratio = json_real_value(json_object_get(value_message_content, "cumul_time")) / session_duration;
                        if(cumul_time_ratio > 0.9)
                        {
                            score_segment += 2;
                        }
                        else if(cumul_time_ratio > 0.7)
                        {
                            score_segment += 1.5;
                        }
                        else if(cumul_time_ratio > 0.5)
                        {
                            score_segment += 1;
                        }
                        else if(cumul_time_ratio > 0.25)
                        {
                            score_segment += 0.5;
                        }
                    }
                    json_decref(json);
                }
                if (score_segment > 10) score_segment = 10;
                return score_segment;
            }

            void get_indice()
            {
                float score_rula = round((indice_rula["RulaRight"] + indice_rula["RulaLeft"]) * 2.85);
                if(score_rula > 100)
                {
                    score_rula = 100;
                }

                float score_reba = round((indice_reba["RebaRight"] + indice_reba["RebaLeft"]) * 2.85);
                if(score_reba > 100)
                {
                    score_reba = 100;
                }

                float score_segment = get_segment();
                float score_balance = compute_balance_score();
                float score_motion = compute_motion_score();

                indice message_indice;
                // ROS_INFO_STREAM("-> 100 - (reba) " << score_reba << " - (balance) " << score_balance << " - (segment) " << score_segment << " - (motion) " << score_motion);
                message_indice.score = 100 - score_reba - score_balance - score_segment - score_motion;
                message_indice.score = message_indice.score < 0 ? 0 : message_indice.score;
                // ROS_INFO_STREAM("-> 100 - (rula) " << score_rula << " - (balance) " << score_balance << " - (segment) " << score_segment << " - (motion) " << score_motion);
                message_indice.rula = 100 - score_rula - score_balance - score_segment - score_motion;
                message_indice.rula = message_indice.rula < 0 ? 0 : message_indice.rula;
                // message_indice.rula = indice_rula["Rula"];
                message_indice.motion = indice_motion["SpineBase"];
                message_indice.sound = indice_sound;
                message_indice.temperature = 0;

                publisher_send(ROS_TOPIC_INDICE, message_indice);
            }

        public:
            NodeGetIndice(int argc, char** argv):
            Node(ROS_NODE_GET_INDICE_NAME, ROS_NODE_GET_INDICE_RATE, argc, argv)
            {
                subscriber_add<rulaArray>(ROS_TOPIC_INDICE_RULA, &NodeGetIndice::refresh_rula);
                subscriber_add<rulaArray>(ROS_TOPIC_INDICE_REBA, &NodeGetIndice::refresh_reba);
                subscriber_add<motion>(ROS_TOPIC_INDICE_MOTION, &NodeGetIndice::refresh_motion);
                subscriber_add<std_msgs::Float64>(ROS_TOPIC_BALANCE, &NodeGetIndice::refresh_balance);
                subscriber_add<control_server_cmd>(ROS_TOPIC_RECORD, &NodeGetIndice::server_toggle);
                //subscriber_add<indiceSon>(ROS_TOPIC_INDICE_SOUND, &NodeGetIndice::refresh_sound);

                publisher_add<indice>(ROS_TOPIC_INDICE);
            }

            void on_start()
            {
                redox_init(REDOX_HOST, REDOX_PORT);
            }

            void on_update()
            {
                get_indice();
            }

            void on_end()
            {
                _redox.disconnect();
            }

            void action_on_toggle()
            {
                if(server_is_recording){
                    this->start_at = Time::now().toSec();
                }
            }

    };
}

int main(int argc, char** argv)
{
    return (prog_kinect::NodeGetIndice(argc, argv)).run();
}
