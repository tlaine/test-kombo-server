# pylint: skip-file

import sympy as sy

"""def timing(f):
    def wrap(*args):
        time1 = time.time()
        ret = f(*args)
        time2 = time.time()
        print '%s function took %0.3f ms' % (f.func_name, (time2-time1)*1000.0)
        return ret
    return wrap"""


class EquationResult(object):
    def __init__(self, dim_x, dim_u=0):
        """ Equation calculation constructor for F matrix

        Parameters
        ----------
        dim_x : int
                        Number of state variables for the Kalman filter. For example, if you are tracking the position and velocity of an object in two dimensions, dim_x would be 4.
        """

        self.dim_x = dim_x

        # ------ VARIABLES

        # dt
        dt = sy.symbols('dt', real=True)

        # base - origin
        Pbx, Pby, Pbz = sy.symbols('Pbx Pby Pbz', real=True)  # base coord
        Vbx, Vby, Vbz = sy.symbols('Vbx Vby Vbz', real=True)  # base coord

        # angles
        th1, th2, th3 = sy.symbols('th1 th2 th3', real=True)  # spine
        th4, th5 = sy.symbols('th4 th5', real=True)  # head
        th6, th7, th8 = sy.symbols('th6 th7 th8', real=True)  # shoulderR
        th9 = sy.symbols('th9', real=True)  # elbowR
        th10, th12 = sy.symbols('th10 th12', real=True)  # wristR
        th13, th14, th15 = sy.symbols('th13 th14 th15', real=True)  # shoulderL
        th16 = sy.symbols('th16', real=True)  # elbowL
        th17, th19 = sy.symbols('th17 th19', real=True)  # wristL
        th20, th21, th22 = sy.symbols('th20 th21 th22', real=True)  # hipR
        th23 = sy.symbols('th23', real=True)  # kneeR
        th24, th25, th26 = sy.symbols('th24 th25 th26', real=True)  # hipL
        th27 = sy.symbols('th27', real=True)  # kneeL

        # vitesses
        dotth1, dotth2, dotth3 = sy.symbols(
            'dotth1 dotth2 dotth3', real=True)  # spine
        dotth4, dotth5 = sy.symbols('dotth4 dotth5', real=True)  # head
        dotth6, dotth7, dotth8 = sy.symbols(
            'dotth6 dotth7 dotth8', real=True)  # shoulderR
        dotth9 = sy.symbols('dotth9', real=True)  # elbowR
        dotth10, dotth12 = sy.symbols('dotth10 dotth12', real=True)  # wristR
        dotth13, dotth14, dotth15 = sy.symbols(
            'dotth13 dotth14 dotth15', real=True)  # shoulderL
        dotth16 = sy.symbols('dotth16', real=True)  # elbowL
        dotth17, dotth19 = sy.symbols('dotth17 dotth19', real=True)  # wristL
        dotth20, dotth21, dotth22 = sy.symbols(
            'dotth20 dotth21 dotth22', real=True)  # hipR
        dotth23 = sy.symbols('dotth23', real=True)  # kneeR
        dotth24, dotth25, dotth26 = sy.symbols(
            'dotth24 dotth25 dotth26', real=True)  # hipL
        dotth27 = sy.symbols('dotth27', real=True)  # kneeL

        # longueurs
        l1, l2, l3, l3b = sy.symbols(
            'l1 l2 l3 l3b', real=True)  # spine and head
        l4, l5, l6, l6b = sy.symbols('l4 l5 l6 l6b', real=True)  # arm
        l7, l8, l9 = sy.symbols('l7 l8 l9', real=True)  # leg

        self._Q = sy.Matrix([Pbx,
                             Pby,
                             Pbz,
                             th1,
                             th2,
                             th3,
                             th4,
                             th5,
                             th6,
                             th7,
                             th8,
                             th9,
                             th10,
                             th12,
                             th13,
                             th14,
                             th15,
                             th16,
                             th17,
                             th19,
                             th20,
                             th21,
                             th22,
                             th23,
                             th24,
                             th25,
                             th26,
                             th27,
                             l1,
                             l2,
                             l3,
                             l3b,
                             l4,
                             l5,
                             l6,
                             l6b,
                             l7,
                             l8,
                             l9])

        self._dotQ = sy.Matrix([Vbx,
                                Vby,
                                Vbz,
                                dotth1,
                                dotth2,
                                dotth3,
                                dotth4,
                                dotth5,
                                dotth6,
                                dotth7,
                                dotth8,
                                dotth9,
                                dotth10,
                                dotth12,
                                dotth13,
                                dotth14,
                                dotth15,
                                dotth16,
                                dotth17,
                                dotth19,
                                dotth20,
                                dotth21,
                                dotth22,
                                dotth23,
                                dotth24,
                                dotth25,
                                dotth26,
                                dotth27,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0])

        self._QdotQ = sy.Matrix([Pbx,
                                 Pby,
                                 Pbz,
                                 th1,
                                 th2,
                                 th3,
                                 th4,
                                 th5,
                                 th6,
                                 th7,
                                 th8,
                                 th9,
                                 th10,
                                 th12,
                                 th13,
                                 th14,
                                 th15,
                                 th16,
                                 th17,
                                 th19,
                                 th20,
                                 th21,
                                 th22,
                                 th23,
                                 th24,
                                 th25,
                                 th26,
                                 th27,
                                 l1,
                                 l2,
                                 l3,
                                 l3b,
                                 l4,
                                 l5,
                                 l6,
                                 l6b,
                                 l7,
                                 l8,
                                 l9,
                                 Vbx,
                                 Vby,
                                 Vbz,
                                 dotth1,
                                 dotth2,
                                 dotth3,
                                 dotth4,
                                 dotth5,
                                 dotth6,
                                 dotth7,
                                 dotth8,
                                 dotth9,
                                 dotth10,
                                 dotth12,
                                 dotth13,
                                 dotth14,
                                 dotth15,
                                 dotth16,
                                 dotth17,
                                 dotth19,
                                 dotth20,
                                 dotth21,
                                 dotth22,
                                 dotth23,
                                 dotth24,
                                 dotth25,
                                 dotth26,
                                 dotth27])

        # ------- EQUATIONS

        f = sy.zeros(dim_x, 1)
        for i in range(len(self._Q)):
            f[i] = self._Q[i] + self._dotQ[i] * dt

        self._f = f
        self._eq = sy.lambdify(self._QdotQ, f, modules='numpy')

    def subEq(self, values):
        """ Performs the substitution of the equations for matrix F

        Parameters
        ----------
        values : np.array
                        estimate state variables
        """
        F = self._eq(*values)
        # common.print_matrixt(j)
        return F

    """ GETTER AND SETTER """
    @property
    def eq(self):
        """ Equation for jacobian matrix """
        return self._eq

    @property
    def f(self):
        """ Equations from T matrices """
        return self._f

    @property
    def Q(self):
        """ Vector symbolic variable """
        return self._Q

    @property
    def dotQ(self):
        """ Vector symbolic variable """
        return self._dotQ
