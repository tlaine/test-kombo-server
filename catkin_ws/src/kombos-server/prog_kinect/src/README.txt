
# REDIS

## Install REDIS SERVER
sudo apt-get install redis-server

## Start REDIS SERVER
service redis-server start

## Install redis client
pip install redis

# Deploy Database
In a python shell:

>>> from server import db
>>> db.create_all()


# Celery

## Install
pip install celery

## Start (dev & test purpose)
celery -A db.pool worker --loglevel=info

## Deployment (as deamon)

Check deamonizing : http://docs.celeryproject.org/en/latest/userguide/daemonizing.html#daemonizing


# Usage

Start Celery workers manualy:
> python task/pool.py
