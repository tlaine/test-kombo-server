#include "../include/Server.hpp"

using namespace std;

namespace prog_kinect
{
	Server::Server()
	{
		connexion = 0;
		id_numii = 1;
		is_listening = false;
	}

	Server::~Server()
	{
		if(connexion)
		{
			close(connexion);
		}
	}

	void Server::init(const int& port)
	{
		connexion = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

		socket_server.sin_family = AF_INET;
		socket_server.sin_addr.s_addr = INADDR_ANY;
		socket_server.sin_port = htons(port);
		bzero(&(socket_server.sin_zero), 8);

		if(bind(connexion, (sockaddr*)&socket_server, sizeof(socket_server)) == 0)
		{
			fcntl(connexion, F_SETFL, fcntl(connexion, F_GETFL) | O_NONBLOCK);
			is_listening = true;
			ROS_INFO(MESSAGE_LISTENING_SUCCESS, socket_server.sin_addr.s_addr, port);
		}
		else
		{
			ROS_WARN(MESSAGE_LISTENING_FAILURE, socket_server.sin_addr.s_addr, port);
		}
	}

	string Server::retrieve()
	{
		/*
		Cette fonction est appellée à chaque tour de boucle. Elle vérifie qu'un paquet UDP arrive via réseau (socket), auquel cas elle le traite.
		Si aucun paquet n'est disponible, la fonction "recvfrom", qui s'occupe de regarder le buffer du socket, n'est pas bloquante. Il est donc possible d'effectuer
		d'autres opérations dans la boucle principale du main.
		*/
		char data[DATA_SIZE];
		int data_count = recvfrom(connexion, data, DATA_SIZE, 0, NULL, NULL);
		if(data_count < DATA_SIZE)
		{
			data[data_count] = '\0';
		} else if (string(data) == "coucou") {
			sendData(to_string(id_numii));
			id_numii++;
			return to_string(id_numii);
		}
		return string(data);
	}

	bool Server::sendData(string data)
    {
        const char * dataStr = data.c_str();
        // char buffer[256] = "s Esc toclose window. ERROR: AstraProPerseeDepthProvider can't create RGB Stream (can't open ";
        // std::cout<<"Sending data..."<<strlen(dataStr)<<std::endl;
        // std::cout<<"\npls enter the mssg to be sent\n";
        // fgets(buffer,256,stdin);
        //récupération des données contenues dans le buffer du socket
        int bytes_send = sendto(connexion,
                                dataStr,
                                strlen(dataStr),
                                0,
                                (struct sockaddr *) &socket_server,
                                sizeof(socket_server));
        return bytes_send > 0;
    }
}
