#!/usr/bin/env python

import json
import rospy

from utils.red import redis_get
from utils.watcher import WATCHER
from server.control_server_config import CSConfig as config
from kombos_msgs.msg import control_server_cmd

ROS_TOPIC_STORER = "storer"
REDIS_CONFIG_KEY = "CSConfig"
RECORDING_RATE = 10
RECORDING_COMMAND_KEY = "RECORDING"
RECORDING_COMMAND_START = "start"
RECORDING_COMMAND_STOP = "stop"
MESSAGE_START = "[ROSIO] STARTING ROSIO"
MESSAGE_LISTENING = "[ROSIO] LISTENING FOR JSON EVENTS..."

if __name__ == "__main__":
    try:
        rospy.init_node(ROS_TOPIC_STORER, anonymous=False, log_level=rospy.DEBUG)
        PUBLISHER = rospy.Publisher(config.START_STOP_MSG, control_server_cmd, queue_size=1)
        RATE = rospy.Rate(RECORDING_RATE)
        WATCHER.watch_log()

        rospy.loginfo(MESSAGE_START)
        rospy.loginfo(MESSAGE_LISTENING)

        CSC_LAST = {}
        while not rospy.is_shutdown():
            CSC = json.loads(redis_get(REDIS_CONFIG_KEY, "{}"))
            if CSC_LAST.get(RECORDING_COMMAND_KEY, False) != CSC.get(RECORDING_COMMAND_KEY, False):
                CMD = control_server_cmd()
                CMD.cmd = RECORDING_COMMAND_START if CSC.get(RECORDING_COMMAND_KEY) else RECORDING_COMMAND_STOP
                PUBLISHER.publish(CMD)
                CSC_LAST = CSC
            RATE.sleep()

    except rospy.ROSInterruptException:
        pass
