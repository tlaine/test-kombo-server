#include <math.h>
#include <Eigen/Geometry>
#include <kombos_msgs/mesureKinect.h>
#include <kombos_msgs/kalmanState.h>
#include <kombos_msgs/motion.h>
#include <kombos_msgs/control_server_cmd.h>
#include <geometry_msgs/Point.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include "../include/Network.hpp"
#include "../include/Utils.hpp"
#include "../include/Node.hpp"
#include "std_msgs/Float64.h"


using namespace std;
using namespace std::chrono;
using namespace kombos_msgs;
using namespace ros;
using namespace Eigen;
using namespace visualization_msgs;
using namespace geometry_msgs;

namespace prog_kinect
{
	class NodeGetBalance : public Node
	{
		private:

			float distance_between_feet(map<string, Vector3f> &positions)
			{
				return Utils::length_calculation(positions, JOINT_NAME_FOOT_RIGHT, JOINT_NAME_FOOT_LEFT);
			}

			float distance_between_knee(map<string, Vector3f> &positions)
			{
				return Utils::length_calculation(positions, JOINT_NAME_KNEE_RIGHT, JOINT_NAME_KNEE_LEFT);
			}

			float add_foot_joint(map<string, Vector3f> &positions, map<string, bool> &fiabilites, float radius_length)
			{
				float foot_ankle_fiability = 0.0;
				// IF FEET == ANKLES : get radius size for foot and create false foot
				if(Utils::length_calculation(positions, JOINT_NAME_FOOT_RIGHT, JOINT_NAME_ANKLE_RIGHT) < 0.05 || Utils::length_calculation(positions, JOINT_NAME_FOOT_LEFT, JOINT_NAME_ANKLE_LEFT) < 0.05)
				{
					Vector3f base = positions[JOINT_NAME_SPINEBASE];
					Vector3f mid = positions[JOINT_NAME_SPINEMID];
					Vector3f hip_right = positions[JOINT_NAME_HIP_RIGHT];
					Vector3f hip_left = positions[JOINT_NAME_HIP_LEFT];
					MatrixXf T = Utils::transform_base_inv(base[0], base[1], base[2], hip_right[0], hip_right[1], hip_right[2], hip_left[0], hip_left[1], hip_left[2], mid[0], mid[1], mid[2]);
					float th = abs(M_PI - atan2(T(0, 2), Utils::distance(T(0, 0), T(0, 1), T(1, 2), T(2, 2)) / sqrt(2)) - Utils::deg2rad(180));

					positions[JOINT_NAME_FOOT_RIGHT] = set_foot_position(positions[JOINT_NAME_ANKLE_RIGHT], radius_length, th, true);
					foot_ankle_fiability += 0.5 * fiabilites[JOINT_NAME_ANKLE_LEFT];
					//ROS_INFO_STREAM(foot_ankle_fiability);
					positions[JOINT_NAME_FOOT_LEFT] = set_foot_position(positions[JOINT_NAME_ANKLE_LEFT], radius_length, th, true);
					foot_ankle_fiability += 0.5 * fiabilites[JOINT_NAME_ANKLE_RIGHT];
					//ROS_INFO_STREAM(foot_ankle_fiability);
				}
				else
				{
					foot_ankle_fiability += fiabilites[JOINT_NAME_FOOT_RIGHT];
					foot_ankle_fiability += fiabilites[JOINT_NAME_FOOT_LEFT];
				}
				foot_ankle_fiability += fiabilites[JOINT_NAME_ANKLE_RIGHT];
				foot_ankle_fiability += fiabilites[JOINT_NAME_ANKLE_LEFT];

				return foot_ankle_fiability;
			}

			void get_balance(const mesureKinect::ConstPtr& message)
			{
				MarkerArray markers;
         			Marker marker;
				// GET position COM + FEET + ANKLES
				map<string, Vector3f> positions;
				map<string, bool> fiabilites;
				for(int i = 0; i < message->mesures.size(); ++i)
				{
					position_msg position = message->mesures[i];
					positions[position.name] = Vector3f(position.position.x, position.position.y, position.position.z);
					fiabilites[position.name] = position.fiabilite;
				}

				map<string, float> states_msg;
				for(int i = 0; i < message->kalman_state.name.size(); i++)
				{
					states_msg[message->kalman_state.name[i]] = message->kalman_state.value[i];
				}

				float foot_ankle_fiability = add_foot_joint(positions, fiabilites, states_msg["radius"]);

				// DISPLAY SKELETON + COM
				int j = 0;
				for(map<string, Vector3f>::iterator it = positions.begin(); it != positions.end(); ++it)
				{
					if(it->first == JOINT_NAME_FOOT_RIGHT||it->first == JOINT_NAME_FOOT_LEFT) marker = get_marker_joint(j, it->first, positions[it->first], 0.1, 0.5, 0.5, 1);
					else if(it->first == JOINT_NAME_COM) marker = get_marker_joint(j, it->first, positions[it->first], 0.3, 0.9, 0.3, 1);
					else marker = get_marker_joint(j, it->first, positions[it->first], 0.5, 0.5, 0.5, 1);
        				markers.markers.push_back(marker);
    	        			j++;
				}

				// Surface polygone + dipslay (linestrip)

				float diff_height_feet = positions[JOINT_NAME_FOOT_RIGHT][1] - positions[JOINT_NAME_FOOT_LEFT][1];
				bool right_foot = true, left_foot = true;

				if(diff_height_feet < -0.05)
				{
					left_foot = false; // RIGHT FOOT ON FLOOR
				}
				else if(diff_height_feet > 0.05)
				{
					right_foot = false; // LEFT FOOT ON FLOOR
				}
				/*else if(abs(Utils::rad2deg(states_msg["lgenouFE"])) > 30 && abs(Utils::rad2deg(states_msg["rgenouFE"])) < 30)
				{
					left_foot = false; // RIGHT FOOT ON FLOOR
				}
				else if(abs(Utils::rad2deg(states_msg["rgenouFE"])) > 30 && abs(Utils::rad2deg(states_msg["lgenouFE"])) < 30)
				{
					right_foot = false; // LEFT FOOT ON FLOOR
				}*/

				Vector3f pos1, pos2, pos3, pos4;
				Vector3f back_of_foot, foot_width;
				back_of_foot << 0,0,0.02;
				foot_width << 0.06, 0, 0;
				if(right_foot && left_foot) // BOTH FEET ON FLOOR
				{
					pos1 = positions[JOINT_NAME_FOOT_RIGHT] + foot_width;
					pos2 = positions[JOINT_NAME_FOOT_LEFT] - foot_width ;
					pos3 = positions[JOINT_NAME_ANKLE_LEFT] - foot_width + back_of_foot;
					pos4 = positions[JOINT_NAME_ANKLE_RIGHT] + foot_width + back_of_foot;
				}
				else if(right_foot && !left_foot) // RIGHT FOOT ON FLOOR
				{
					//ROS_WARN_STREAM("PIED DROIT");
					pos1 = positions[JOINT_NAME_ANKLE_RIGHT] - foot_width + back_of_foot;
					pos2 = positions[JOINT_NAME_ANKLE_RIGHT] + foot_width + back_of_foot;
					pos3 = positions[JOINT_NAME_FOOT_RIGHT] + foot_width;
					pos4 = positions[JOINT_NAME_FOOT_RIGHT] - foot_width;
				}
				else if(left_foot && !right_foot) // LEFT FOOT ON FLOOR
				{
					//ROS_WARN_STREAM("PIED GAUCHE");
					pos1 = positions[JOINT_NAME_ANKLE_LEFT] - foot_width + back_of_foot;
					pos2 = positions[JOINT_NAME_ANKLE_LEFT] + foot_width + back_of_foot;
					pos3 = positions[JOINT_NAME_FOOT_LEFT] + foot_width;
					pos4 = positions[JOINT_NAME_FOOT_LEFT] - foot_width;
				}
				else
				{
					//ROS_ERROR_STREAM("PROBLEME 2 PIEDS FAUX");
				}

				markers.markers.push_back(get_marker_line(j, "AREA", pos1, pos2, pos3, pos4));
				j++;

				// Projection of COM + display
				Vector3f com_projected = positions[JOINT_NAME_COM];
				com_projected[1] = min_max_component(true, 1, pos1, pos2, pos3, pos4);
				marker = get_marker_joint(j, "COM_projected", com_projected, 0.4, 0.9, 0.4, 1);
				markers.markers.push_back(marker);
				publisher_send<MarkerArray>(ROS_TOPIC_BALANCE_MARKER, markers);

				// BALANCE TRUE FALSE
				bool balance = is_com_in_the_polygone(com_projected, pos1, pos2, pos3, pos4);
				//if(!balance && total_fiab < 1.5) ROS_WARN_STREAM("------------------------> UNBALANCED (" << total_fiab << ")");

        			stringstream ss;
				ss << "{\"balance\":" << balance << ",\"fiabilite\":" << foot_ankle_fiability <<"}";
				string str = ss.str();
				_redox.set(NAME_BALANCE_DATA, str);
			}

			float get_distance_ortho(Vector3f &COM_pos, Vector3f &pos1, Vector3f &pos2)
			{
				Vector2f ortho;
				// compute coef direc of the line
				// (Bz - Az) / (Bx - Ax)
				float coef_direct_line = (pos2[Z_AXIS] - pos1[Z_AXIS]) / (pos2[X_AXIS] - pos1[X_AXIS]);
				float c = (pos1[Z_AXIS] - coef_direct_line * pos1[X_AXIS]);
				float b = -1;
				float distance = abs(coef_direct_line * COM_pos[X_AXIS] + b * COM_pos[Z_AXIS] + c);
				distance = distance / sqrt(coef_direct_line * coef_direct_line + b * b);

				return distance;

			}
			float get_min(float a, float b)
			{
				return a > b ? b : a;
			}
			float get_closer_distance_from_polygone(Vector3f COM_pos, Vector3f pos1, Vector3f pos2, Vector3f pos3, Vector3f pos4)
			{
				float min_dist = get_distance_ortho(COM_pos, pos1, pos2);
				min_dist = get_min(get_distance_ortho(COM_pos, pos2, pos3), min_dist);
				min_dist = get_min(get_distance_ortho(COM_pos, pos3, pos4), min_dist);
				min_dist = get_min(get_distance_ortho(COM_pos, pos4, pos1), min_dist);

				return min_dist;
			}

			bool is_com_in_the_polygone(Vector3f COM_pos, Vector3f pos1, Vector3f pos2, Vector3f pos3, Vector3f pos4)
			{
				bool in_polygone = true;
				// O is for X (right-left)
				// 2 is for Z (depth map)
				float min_x = min_max_component(true, 0, pos1, pos2, pos3, pos4);
				float max_x = min_max_component(false, 0, pos1, pos2, pos3, pos4);
				float min_z = min_max_component(true, 2, pos1, pos2, pos3, pos4);
				float max_z = min_max_component(false, 2, pos1, pos2, pos3, pos4);

				std_msgs::Float64 distance_to_polygone;


				if(COM_pos[0] >= min_x && COM_pos[0] <= max_x && COM_pos[2] >= min_z && COM_pos[2] <= max_z)
				{
					distance_to_polygone.data = get_closer_distance_from_polygone(COM_pos, pos1, pos2, pos3, pos4);
					publisher_send<std_msgs::Float64>(ROS_TOPIC_BALANCE, distance_to_polygone);
					return true;
				}
				else
				{
					distance_to_polygone.data = -get_closer_distance_from_polygone(COM_pos, pos1, pos2, pos3, pos4);
					publisher_send<std_msgs::Float64>(ROS_TOPIC_BALANCE, distance_to_polygone);
					return false;
				}
				return in_polygone;
			}

			float min_max_component(bool is_min, int component, Vector3f pos1, Vector3f pos2, Vector3f pos3, Vector3f pos4)
			{
				std::vector<float> v;
				v.push_back(pos1[component]);
				v.push_back(pos2[component]);
				v.push_back(pos3[component]);
				v.push_back(pos4[component]);
				if(is_min)
				{
					return *std::min_element(std::begin(v), std::end(v));
				}
				else
				{
					return *std::max_element(std::begin(v), std::end(v));
				}
			}

			Vector3f set_foot_position(Vector3f ankle, float length, float th, bool right)
			{
				Vector3f res;
				res[1] = ankle[1];
				res[2] = length*(-cos(th + Utils::deg2rad(5))) + ankle[2];
				if(right) res[0] = length*(-sin(th + Utils::deg2rad(5))) + ankle[0];
				else res[0] = length*(sin(th + Utils::deg2rad(5))) + ankle[0];
				return res;
			}

			Marker get_marker_joint(const int& id, string name, Vector3f position, float r, float g, float b, float a)
			{
				Marker marker;
				marker.id = id;
				marker.ns = "com_marker";
				marker.type = Marker::SPHERE;
				marker.action = Marker::ADD;
				marker.text = name;
				marker.header.stamp = Time::now();
				marker.header.frame_id = "human_base";
				marker.color.r = r;
				marker.color.g = g;
				marker.color.b = b;
				marker.color.a = a;
				marker.pose.position.x = position[0];
				marker.pose.position.y = position[1];
				marker.pose.position.z = position[2];

				marker.scale.x = 0.04;
				marker.scale.y = 0.04;
				marker.scale.z = 0.04;

				return marker;
			}

			Marker get_marker_line(const int& id, string name, Vector3f pos1, Vector3f pos2, Vector3f pos3, Vector3f pos4)
			{
				Marker line;
				line.id = id;
				line.ns = "com_marker";
				line.type = Marker::LINE_STRIP;
				line.action = Marker::ADD;
				line.text = name;
				line.header.stamp = Time::now();
				line.header.frame_id = "human_base";
				line.color.r = 0.6;
				line.color.a = 0.9;
				line.scale.x = 0.05;
				line.points.push_back(vector2point(pos1));
				line.points.push_back(vector2point(pos2));
				line.points.push_back(vector2point(pos3));
				line.points.push_back(vector2point(pos4));
				line.points.push_back(vector2point(pos1));
				return line;
			}

			Point vector2point(Vector3f v)
			{
				Point p;
				p.x = v[0];
				p.y = v[1];
				p.z = v[2];
				return p;
			}

		public:
			EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
			NodeGetBalance(int argc, char** argv):
			Node(ROS_NODE_GET_BALANCE_NAME, ROS_NODE_GET_BALANCE_RATE, argc, argv)
			{
				subscriber_add<mesureKinect>(ROS_TOPIC_PC_CLASSIFIED_COM, &NodeGetBalance::get_balance, 0);

				publisher_add<MarkerArray>(ROS_TOPIC_BALANCE_MARKER, 0);
				publisher_add<std_msgs::Float64>(ROS_TOPIC_BALANCE, 0);
			}

			void on_start()
			{
				redox_init(REDOX_HOST, REDOX_PORT);
			}

			void on_update()
			{

			}

			void on_end()
			{
				_redox.disconnect();
			}
	};
}

int main(int argc, char** argv)
{
	return (prog_kinect::NodeGetBalance(argc, argv)).run();
}
