#include <Eigen/Geometry>
#include <kombos_msgs/mesureKinect.h>
#include <kombos_msgs/mesureKalman.h>
#include <kombos_msgs/position_msg.h>
#include "../include/Network.hpp"
#include "../include/Utils.hpp"
#include "../include/Node.hpp"
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

using namespace std;
using namespace ros;
using namespace geometry_msgs;
using namespace kombos_msgs;
using namespace Eigen;
using namespace visualization_msgs;

namespace prog_kinect
{
	class NodeOpti2Cloud : public Node
	{
		private:
			map<string, vector<double> > points_coord;
			map<string, Quaterniond> points_quat;

			void transform_joints(const mesureKinect::ConstPtr& message)
			{
				//ROS_INFO_STREAM("Size : " << message->mesures.size());
				for(int i = 0; i < message->mesures.size(); ++i)
				{
					if ( std::isnan(message->mesures[i].position.x) || std::isnan(message->mesures[i].position.y) || std::isnan(message->mesures[i].position.z) )
					{
						ROS_INFO("=================Bug=================TFC2C");
						return;
					}
					points_coord[message->mesures[i].name] = {message->mesures[i].position.x, message->mesures[i].position.y, message->mesures[i].position.z, 1.0};
					/*points_quat[message->mesures[i].name] = Quaterniond(message->orientations[i].quaternion.w, message->orientations[i].quaternion.x, message->orientations[i].quaternion.y, message->orientations[i].quaternion.z);*/
				}

				// PASSAGE DES MARQUEURS AU SQUELETTE
				mesureKinect messageSquelette;
				messageSquelette.typeCapteur = message->typeCapteur;
				messageSquelette.idCapteur = message->idCapteur;

				if(message->mesures.size() == 13)
				{
					points_coord["SpineBase"] = points_coord["Ab"];
					points_coord["SpineMid"] = {points_coord["SpineBase"][0]+points_coord["Chest"][0], points_coord["SpineBase"][1]+points_coord["Chest"][1], points_coord["SpineBase"][2]+points_coord["Chest"][2], 1.0};
					points_coord["SpineShoulder"] = {points_coord["SpineMid"][0]+middle_joints(points_coord["RShoulder"], points_coord["LShoulder"], 0), points_coord["SpineMid"][1]+middle_joints(points_coord["RShoulder"], points_coord["LShoulder"], 1), points_coord["SpineMid"][2]+middle_joints(points_coord["RShoulder"], points_coord["LShoulder"], 2), 1.0};
					//points_coord["Neck"] = {points_coord["SpineMid"][0]+points_coord["Neck"][0], points_coord["SpineMid"][1]+points_coord["Neck"][1], points_coord["SpineMid"][2]+points_coord["Neck"][2], 1.0};
					//points_coord["Head"] = {points_coord["Neck"][0]+points_coord["Head"][0], points_coord["Neck"][1]+points_coord["Head"][1], points_coord["Neck"][2]+points_coord["Head"][2], 1.0};

					points_coord["ShoulderRight"] = {points_coord["SpineMid"][0]+points_coord["RUArm"][0], points_coord["SpineMid"][1]+points_coord["RUArm"][1], points_coord["SpineMid"][2]+points_coord["RUArm"][2], 1.0};
					points_coord["ElbowRight"] = {points_coord["ShoulderRight"][0]+points_coord["RFArm"][0], points_coord["ShoulderRight"][1]+points_coord["RFArm"][1], points_coord["ShoulderRight"][2]+points_coord["RFArm"][2], 1.0};
					points_coord["WristRight"] = {points_coord["ElbowRight"][0]+points_coord["RHand"][0], points_coord["ElbowRight"][1]+points_coord["RHand"][1], points_coord["ElbowRight"][2]+points_coord["RHand"][2], 1.0};

					points_coord["ShoulderLeft"] = {points_coord["SpineMid"][0]+points_coord["LUArm"][0], points_coord["SpineMid"][1]+points_coord["LUArm"][1], points_coord["SpineMid"][2]+points_coord["LUArm"][2], 1.0};
					points_coord["ElbowLeft"] = {points_coord["ShoulderLeft"][0]+points_coord["LFArm"][0], points_coord["ShoulderLeft"][1]+points_coord["LFArm"][1], points_coord["ShoulderLeft"][2]+points_coord["LFArm"][2], 1.0};
					points_coord["WristLeft"] = {points_coord["ElbowLeft"][0]+points_coord["LHand"][0], points_coord["ElbowLeft"][1]+points_coord["LHand"][1], points_coord["ElbowLeft"][2]+points_coord["LHand"][2], 1.0};

					messageSquelette.mesures.push_back(points_to_joints("SpineBase", points_coord["SpineBase"]));//points_to_joints("SpineBase", points_coord["Hip"]);
					messageSquelette.mesures.push_back(points_to_joints("SpineMid", points_coord["SpineMid"]));
					messageSquelette.mesures.push_back(points_to_joints("SpineShoulder", points_coord["SpineShoulder"]));
					messageSquelette.mesures.push_back(points_to_joints("Neck", points_coord["Neck"]));
					messageSquelette.mesures.push_back(points_to_joints("Head", points_coord["Head"]));
					messageSquelette.mesures.push_back(points_to_joints("ShoulderRight", points_coord["ShoulderRight"]));
					messageSquelette.mesures.push_back(points_to_joints("ElbowRight", points_coord["ElbowRight"]));
					messageSquelette.mesures.push_back(points_to_joints("WristRight", points_coord["WristRight"]));
					messageSquelette.mesures.push_back(points_to_joints("HandRight", points_coord["WristRight"]));
					messageSquelette.mesures.push_back(points_to_joints("HandTipRight", points_coord["WristRight"]));
					messageSquelette.mesures.push_back(points_to_joints("ThumbRight", points_coord["WristRight"]));
					messageSquelette.mesures.push_back(points_to_joints("ShoulderLeft", points_coord["ShoulderLeft"]));
					messageSquelette.mesures.push_back(points_to_joints("ElbowLeft", points_coord["ElbowLeft"]));
					messageSquelette.mesures.push_back(points_to_joints("WristLeft", points_coord["WristLeft"]));
					messageSquelette.mesures.push_back(points_to_joints("HandLeft", points_coord["WristLeft"]));
					messageSquelette.mesures.push_back(points_to_joints("HandTipLeft", points_coord["WristLeft"]));
					messageSquelette.mesures.push_back(points_to_joints("ThumbLeft", points_coord["WristLeft"]));

					/*messageSquelette.mesures.push_back(points_to_joints("SpineBase", points_coord["Ab"]));//points_to_joints("SpineBase", points_coord["Hip"]);
					messageSquelette.mesures.push_back(points_to_joints("SpineMid", points_coord["Chest"]));
					messageSquelette.mesures.push_back(points_to_middle("SpineShoulder", points_coord["RShoulder"], points_coord["LShoulder"]));
					messageSquelette.mesures.push_back(points_to_joints("Neck", points_coord["Neck"]));
					messageSquelette.mesures.push_back(points_to_joints("Head", points_coord["Head"]));
					messageSquelette.mesures.push_back(points_to_joints("ShoulderRight", points_coord["RUArm"]));
					messageSquelette.mesures.push_back(points_to_joints("ElbowRight", points_coord["RFArm"]));
					messageSquelette.mesures.push_back(points_to_joints("WristRight", points_coord["RHand"]));
					messageSquelette.mesures.push_back(points_to_joints("HandRight", points_coord["RHand"]));
					messageSquelette.mesures.push_back(points_to_joints("HandTipRight", points_coord["RHand"]));
					messageSquelette.mesures.push_back(points_to_joints("ThumbRight", points_coord["RHand"]));
					messageSquelette.mesures.push_back(points_to_joints("ShoulderLeft", points_coord["LUArm"]));
					messageSquelette.mesures.push_back(points_to_joints("ElbowLeft", points_coord["LFArm"]));
					messageSquelette.mesures.push_back(points_to_joints("WristLeft", points_coord["LHand"]));
					messageSquelette.mesures.push_back(points_to_joints("HandLeft", points_coord["LHand"]));
					messageSquelette.mesures.push_back(points_to_joints("HandTipLeft", points_coord["LHand"]));
					messageSquelette.mesures.push_back(points_to_joints("ThumbLeft", points_coord["LHand"]));*/

				}
				else
				{
					messageSquelette.mesures.push_back(points_to_joints("SpineBase", points_coord["Ab"]));//points_to_joints("SpineBase", points_coord["Hip"]);
					messageSquelette.mesures.push_back(points_to_joints("SpineMid", points_coord["Chest"]));
					messageSquelette.mesures.push_back(points_to_middle("SpineShoulder", points_coord["RShoulder"], points_coord["LShoulder"]));
					messageSquelette.mesures.push_back(points_to_joints("Neck", points_coord["Neck"]));
					messageSquelette.mesures.push_back(points_to_joints("Head", points_coord["Head"]));
					messageSquelette.mesures.push_back(points_to_joints("ShoulderRight", points_coord["RUArm"]));
					messageSquelette.mesures.push_back(points_to_joints("ElbowRight", points_coord["RFArm"]));
					messageSquelette.mesures.push_back(points_to_joints("WristRight", points_coord["RHand"]));
					messageSquelette.mesures.push_back(points_to_joints("HandRight", points_coord["RHand"]));
					messageSquelette.mesures.push_back(points_to_joints("HandTipRight", points_coord["RHand"]));
					messageSquelette.mesures.push_back(points_to_joints("ThumbRight", points_coord["RHand"]));
					messageSquelette.mesures.push_back(points_to_joints("ShoulderLeft", points_coord["LUArm"]));
					messageSquelette.mesures.push_back(points_to_joints("ElbowLeft", points_coord["LFArm"]));
					messageSquelette.mesures.push_back(points_to_joints("WristLeft", points_coord["LHand"]));
					messageSquelette.mesures.push_back(points_to_joints("HandLeft", points_coord["LHand"]));
					messageSquelette.mesures.push_back(points_to_joints("HandTipLeft", points_coord["LHand"]));
					messageSquelette.mesures.push_back(points_to_joints("ThumbLeft", points_coord["LHand"]));
					messageSquelette.mesures.push_back(points_to_joints("HipRight", points_coord["RThigh"]));
					messageSquelette.mesures.push_back(points_to_joints("KneeRight", points_coord["RShin"]));
					messageSquelette.mesures.push_back(points_to_joints("AnkleRight", points_coord["RFoot"]));
					messageSquelette.mesures.push_back(points_to_joints("FootRight", points_coord["RToe"]));
					messageSquelette.mesures.push_back(points_to_joints("HipLeft", points_coord["LThigh"]));
					messageSquelette.mesures.push_back(points_to_joints("KneeLeft", points_coord["LShin"]));
					messageSquelette.mesures.push_back(points_to_joints("AnkleLeft", points_coord["LFoot"]));
					messageSquelette.mesures.push_back(points_to_joints("FootLeft", points_coord["LToe"]));
				}

				//ROS_INFO_STREAM("YEAH");
				MarkerArray markers;
				Marker marker;
				int size = message->mesures.size();
				for(int i = 0; i < size; ++i)
				{
					if(message->mesures[i].name=="RUArm"||message->mesures[i].name=="LUArm")
						marker = get_marker_joint(i, message->mesures[i], 1, 0.5, 0.5, 1);
					else
						marker = get_marker_joint(i, message->mesures[i], 0.06, 0.06, 0.06, 1);
					markers.markers.push_back(marker);
				}
				//marker = get_marker_joint(20, messageSquelette.mesures[0], 0.5, 1, 0.5, 1);
				//markers.markers.push_back(marker);
				//marker = get_marker_joint(21, messageSquelette.mesures[1], 0.8, 1, 0.5, 1);
				//markers.markers.push_back(marker);
				publisher_send<MarkerArray>("/Optitrack/Markers", markers);


				publisher_send(ROS_TOPIC_OC, messageSquelette);
			}

			// TO DO: MIDDLE OF RUARM ET LUARM POUR SPINE SHOULDER
			position_msg points_to_middle(string nameJoint, vector<double> vecCoord1, vector<double> vecCoord2)
			{
				position_msg pos;
				pos.name = nameJoint;

				pos.position.x = (vecCoord1[0] + vecCoord2[0]) / 2;
				pos.position.y = (vecCoord1[1] + vecCoord2[1]) / 2;
				pos.position.z = (vecCoord1[2] + vecCoord2[2]) / 2;
				pos.fiabilite = 1;

				return pos;
			}

			double middle_joints(vector<double> vecCoord1, vector<double> vecCoord2, int coord)
			{
				return (vecCoord1[coord] + vecCoord2[coord]) / 2;
			}

			position_msg points_to_hip(string nameJoint, vector<double> vecCoord, Quaterniond q, bool right)
			{
				position_msg pos;
				pos.name = nameJoint;
				Eigen::Matrix3d R = q.normalized().toRotationMatrix();
				Eigen::Matrix4d Trot = Matrix4d::Identity();
				Eigen::Vector4d vec = Eigen::Vector4d();
				vec << vecCoord[0], vecCoord[1], vecCoord[2], 0;
				Trot.block(0,0,3,3) = R;
				Trot.block(0,3,4,1) = vec;

				if(right) {
					vec << -0.03, 0, 0, 1;
				}
				else {
					vec << 0.03, 0, 0, 1;
				}

				Eigen::Vector4d finalPos = Trot*vec;
				pos.position.x = finalPos[0];
				pos.position.y = finalPos[1];
				pos.position.z = finalPos[2];
				pos.fiabilite = 1;

				return pos;
			}

			Marker get_marker_joint(const int& id, const position_msg& position, double r, double g, double b, double a)
			{
				Marker marker;
				marker.id = id;
				marker.ns = "joint_marker";
				marker.type = Marker::SPHERE;
				marker.action = Marker::ADD;
				marker.text = position.name;
				marker.header.stamp = Time::now();
				marker.header.frame_id = "human_base";
				marker.color.r = r;
				marker.color.g = g;
				marker.color.b = b;
				marker.color.a = a;
				marker.pose.position.x = position.position.x;
				marker.pose.position.y = position.position.y;
				marker.pose.position.z = position.position.z;

				if(position.fiabilite)
				{
					marker.scale.x = 0.05;
					marker.scale.y = 0.05;
					marker.scale.z = 0.05;
				}
				else
				{
					marker.scale.x = 0.02;
					marker.scale.y = 0.02;
					marker.scale.z = 0.02;
				}

				return marker;
			}

			position_msg points_to_joints(string nameJoint, vector<double> vecCoord)/*vector<vector<double>> points)*/
			{
				position_msg pos;
				pos.name = nameJoint;
				pos.position.x = vecCoord[0];
				pos.position.y = vecCoord[1];
				pos.position.z = vecCoord[2];
				pos.fiabilite = 1;

				return pos;
			}

		public:
			NodeOpti2Cloud(int argc, char** argv):
			Node(ROS_NODE_CLOUD2CLOUDTF_NAME, ROS_NODE_CLOUD2CLOUDTF_RATE, argc, argv)
			{
				subscriber_add<mesureKinect>(ROS_TOPIC_O, &NodeOpti2Cloud::transform_joints);

				publisher_add<mesureKinect>(ROS_TOPIC_OC);
				publisher_add<MarkerArray>("/Optitrack/Markers");
			}

			void on_start()
			{

			}

			void on_update()
			{

			}

			void on_end()
			{

			}
	};
}

int main(int argc, char** argv)
{
	return (prog_kinect::NodeOpti2Cloud(argc, argv)).run();
}
