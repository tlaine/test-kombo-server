#include <Eigen/Core>
#include <Eigen/Geometry>
#include <kombos_msgs/mesureKalman.h>
#include "../include/Network.hpp"
#include "../include/Utils.hpp"
#include "../include/json2model.h"
#include "../include/Node.hpp"

using namespace std;
using namespace ros;
using namespace Eigen;
using namespace kombos_msgs;

namespace prog_kinect
{
	class NodeCloudFusion : public Node
	{
		private:
			map<string, MatrixXf> kinect_transfer;
			string kinect_current;
			map<string, bool> joints_enabled;
			map<string, map<string, Vector3f>> joints_positions;
			map<string, map<string, Vector3f>> joints_speeds;
			map<string, map<string, bool>> joints_fiabilites;
			map<string, int> joints_trusts;
			map<string, double> joints_timestamps;

			void enqueue(const mesureKalman::ConstPtr& msg)
			{
				string kinect_name = msg->data.typeCapteur;

				joints_trusts[kinect_name] = 0;
				joints_timestamps[kinect_name] = Time::now().toSec();

				map<string, Vector3f> tmp_positions;
				map<string, Vector3f> tmp_speeds;
				map<string, bool> tmp_fiabilites;
				int tmp_trusts = 0;
				bool no_outlier = true;

				for(int i = 0; i < msg->data.mesures.size(); ++i)
				{
					position_msg position = msg->data.mesures[i];
					tmp_positions[position.name] = Vector3f(position.position.x, position.position.y, position.position.z);
					tmp_fiabilites[position.name] = position.fiabilite;
					tmp_trusts += position.fiabilite ? 1 : 0;

					if(msg->data.vitesses.size() > 0)
					{
						position_msg speed = msg->data.vitesses[i];
						tmp_speeds[speed.name] = Vector3f(speed.position.x, speed.position.y, speed.position.z);
					}
				}

				// Outlier research
				/*float length = 0;
				for(map<string, pair<string,string>>::iterator it = limb_extremity.begin(); it != limb_extremity.end(); it++)
				{
					length = Utils::length_calculation(tmp_positions, it->second.first, it->second.second);
					if(length > LENGTH_FOR_OUTLIER)
					{
						no_outlier = false;
					}
				}*/

				if(no_outlier)
				{
					joints_positions[kinect_name] = tmp_positions;
					joints_fiabilites[kinect_name] = tmp_fiabilites;
					joints_trusts[kinect_name] = tmp_trusts;

					if(tmp_speeds.size() > 0)
					{
						joints_speeds[kinect_name] = tmp_speeds;
					}

					kinect_transfer[kinect_name] = Utils::msg2matrix(msg->matrices[0]);
				}
			}

			void fusion()
			{
				vector<map<string, float>> weights;
				weights.push_back(get_trust_skeleton());
				weights.push_back(get_trust_position());
				weights.push_back(get_trust_orientation());
				weights.push_back(get_trust_time());
				
				/*std::string names[] = {"trust_skeleton", "trust_position", "trust_orientation", "trust_time"};
				for(int i=0; i<weights.size(); i++)
				{
				    std::cout<<names[i]<<std::endl;
				    for(auto it=weights[i].begin(); it!=weights[i].end(); it++)
				        std::cout<<"\t"<<it->first<<" "<<it->second<<std::endl;
				}
				std::cout<<std::endl;*/


				MatrixXf R = MatrixXf::Zero(measures_count, measures_count);
				map<string, MatrixXf> RJ;
				for(map<string, bool>::iterator it = joints_enabled.begin(); it != joints_enabled.end(); ++it)
				{
					RJ[it->first] = MatrixXf::Zero(measures_count, measures_count);
				}

				for(map<string, int>::iterator it = joints_indices.begin(); it != joints_indices.end(); ++it)
				{
					if(model_basducorps || (!model_basducorps && it->second < joints_count))
					{
						map<string, float> coefficients = get_coefficients(it->first, weights);
						for(int i = 0; i < 3; ++i)
						{
							float index = (it->second * 3) + i;
							for(map<string,bool>::iterator it_enabled = joints_enabled.begin(); it_enabled != joints_enabled.end(); ++it_enabled)
							{
								if(it_enabled->second)
								{
									float value = 1.0 / (1E-2 * coefficients[it_enabled->first]);
									R(index, index) += value;
									RJ[it_enabled->first](index, index) = value;
								}
							}

							R(index, index) = (R(index, index) > 1E-10) ? 1 / R(index, index) : 1.0;
						}
					}
				}

				mesureKalman message;
				message.data.typeCapteur = "kinectFusion";
				message.data.idCapteur = 1;
				for(map<string, Vector3f>::iterator it = joints_positions[kinect_current].begin(); it != joints_positions[kinect_current].end(); ++it)
				{
					Vector3f position = fusion_position(it->first, R, RJ);
					bool fiab = fusion_fiabilite(it->first);
					position_msg message_position;
					message_position.name = it->first;
					message_position.position.x = position[0];
					message_position.position.y = position[1];
					message_position.position.z = position[2];
					message_position.fiabilite = fiab;//(bool)R((joints_indices[it->first] * 3), (joints_indices[it->first] * 3));
					message.data.mesures.push_back(message_position);
					joints_positions["last"][it->first] = position;

					Vector3f speed = fusion_speed(it->first, R, RJ);
					position_msg message_speed;
					message_speed.name = it->first;
					message_speed.position.x = speed[0];
					message_speed.position.y = speed[1];
					message_speed.position.z = speed[2];
					message_speed.fiabilite = fiab;//(bool)R((joints_indices[it->first] * 3), (joints_indices[it->first] * 3));
					message.data.vitesses.push_back(message_speed);
					joints_speeds["last"][it->first] = speed;
				}

				for(map<string, bool>::iterator it = joints_enabled.begin(); it != joints_enabled.end(); ++it)
				{
					joints_positions["last"+it->first] = joints_positions[it->first];
					joints_speeds["last"+it->first] = joints_speeds[it->first];
				}

				message.matrices.push_back(Utils::matrix2msg(string(MATRIX_NAME_R) + "0", R));
				for(map<string, bool>::iterator it = joints_enabled.begin(); it != joints_enabled.end(); ++it)
				{
					if(it->second)
					{
						message.matrices.push_back(Utils::matrix2msg(MATRIX_NAME_R + it->first, RJ[it->first]));
						message.matrices.push_back(Utils::matrix2msg(MATRIX_NAME_TF + it->first, kinect_transfer[it->first]));
					}
				}
				//ROS_WARN_STREAM("I publish");
				publisher_send(ROS_TOPIC_PC_FUSION_TF, message);
			}

			bool fusion_fiabilite(const string& joint_name)
			{
				bool fiab = false;
				for(map<string, bool>::iterator it = joints_enabled.begin(); it != joints_enabled.end(); ++it)
				{
					fiab = fiab || joints_fiabilites[it->first][joint_name];
				}
				return fiab;
			}

			Vector3f fusion_position(const string& joint_name, const MatrixXf& R, const map<string, MatrixXf>& RJ)
			{
				Vector3f position;
				bool has_count = joints_indices.count(joint_name) && (model_basducorps || (!model_basducorps && joints_indices[joint_name] < joints_count));
				for(int i = 0; i < 3; ++i)
				{
					position[i] = 0.0;
					int index = (joints_indices[joint_name] * 3) + i;
					for(map<string, bool>::iterator it = joints_enabled.begin(); it != joints_enabled.end(); ++it)
					{
						float previous = joints_positions[it->first][joint_name][i];
						float factor = (has_count && it->second && abs(previous) > 1E-4) ? RJ.at(it->first)(index, index) : 0.0;
						position[i] += previous * factor;
					}

					position[i] *= R(index, index);
				}

				return position;
			}

			Vector3f fusion_speed(const string& joint_name, const MatrixXf& R, const map<string, MatrixXf>& RJ)
			{
				Vector3f speed;
				bool has_count = joints_indices.count(joint_name) && (model_basducorps || (!model_basducorps && joints_indices[joint_name] < joints_count));
				for(int i = 0; i < 3; ++i)
				{
					speed[i] = 0.0;
					int index = (joints_indices[joint_name] * 3) + i;
					for(map<string, bool>::iterator it = joints_enabled.begin(); it != joints_enabled.end(); ++it)
					{
						float previous = joints_speeds[it->first][joint_name][i];
						float factor = (has_count && it->second) ? RJ.at(it->first)(index, index) : 0.0;
						speed[i] += previous * factor;
					}
					speed[i] *= R(index, index);
				}

				return speed;
			}

			map<string, float> get_coefficients(const string& joint_name, vector<map<string, float>> trusts)
			{
				float sum = 0.0;

				map<string, float> result;
				for(map<string, bool>::iterator it = joints_enabled.begin(); it != joints_enabled.end(); ++it)
				{
					if(it->second)
					{
						result[it->first] = 1;
						for(vector<map<string, float>>::const_iterator it_trusts = trusts.begin(); it_trusts != trusts.end(); ++it_trusts)
						{
							map<string, float> weights = *it_trusts;
							
							//for(auto tmp=weights.begin(); tmp!=weights.end(); tmp++)
							//    std::cout<<joint_name<<" "<<tmp->first<<" "<<tmp->second<<std::endl;
							
							result[it->first] *= weights.at(it->first);
						}
						sum += result[it->first] * result[it->first];
					}
				}

				map<string, float> coeff;
				for(map<string, float>::iterator it = result.begin(); it != result.end(); ++it)
				{
					coeff[it->first] = it->second / sqrt(sum);
				}

				return coeff;
			}

			map<string, float> get_trust_skeleton()
			{
				map<string, float> weights;
				for(map<string, int>::iterator it = joints_trusts.begin(); it != joints_trusts.end(); ++it)
				{
					if(joints_enabled[it->first])
					{
						weights[it->first] = it->second / (float)joints_positions[it->first].size();
						//std::cout<<it->second<<" "<<joints_positions[it->first].size()<<std::endl;
					}
				}

				return weights;
			}

			map<string, float> get_trust_joint(string joint_name)
			{
				map<string, float> weights;
				for(map<string, double>::iterator it = joints_timestamps.begin(); it != joints_timestamps.end(); ++it)
				{
					if(joints_enabled[it->first])
					{
						weights[it->first] = (joints_fiabilites[it->first][joint_name]) ? 1.0 : 0.1;
					}
				}

				return weights;
			}

			map<string, float> get_trust_position()
			{
				float distMin = 1.5;
				float distMax = 3.5;
				float distLim = 4.5;
				float dz;

				map<string, float> weights;
				for(map<string, MatrixXf>::iterator it = kinect_transfer.begin(); it != kinect_transfer.end(); ++it)
				{
					if(joints_enabled[it->first])
					{
						dz = it->second(2, 3);
						if(dz < distMin)
						{
							weights[it->first] = TRUST_BAD;
						}
						else if(dz < distMax)
						{
							weights[it->first] = TRUST_GOOD;
						}
						else if(dz < distLim)
						{
							weights[it->first] = TRUST_MEDIUM;
						}
						else
						{
							weights[it->first] = TRUST_BAD;
						}
					}
				}

				return weights;
			}

			map<string, float> get_trust_orientation()
			{
				float thMin = Utils::deg2rad(20);
				float thMoy = Utils::deg2rad(30);
				float thMax = Utils::deg2rad(45);
				float a = -0.9 / Utils::deg2rad(180);

				map<string, float> weights;
				for(map<string, MatrixXf>::iterator it = kinect_transfer.begin(); it != kinect_transfer.end(); ++it)
				{
					if(joints_enabled[it->first])
					{
						float th = abs(M_PI - atan2(it->second(0, 2), Utils::distance(it->second(0, 0), it->second(0, 1), it->second(1, 2), it->second(2, 2)) / sqrt(2)) - Utils::deg2rad(180));
						weights[it->first] = a * th + 1;
					}
				}

				return weights;
			}

			map<string, float> get_trust_time()
			{
				float now = Time::now().toSec();
				map<string, float> weights;
				for(map<string,double>::iterator it = joints_timestamps.begin(); it != joints_timestamps.end(); ++it)
				{
					if(joints_enabled[it->first])
					{
						weights[it->first] = (-0.75 / MAX_DELTAT) * (now - (float)it->second) + 1.0;
					}
				}

				return weights;
			}

			map<string, float> get_trust_speed(const string& joint_name)
			{
				float min, max, Min, Max;
				min = 0.1; max = 1; Min = 1E-3; Max = 1E-1;
				float now = Time::now().toSec();
				float a = (max-min)/(Min-Max);
				float b = min-(a*Max);
				VectorXf epsilon = VectorXf::Zero(3);

				//ROS_WARN_STREAM("__ " << joint_name <<" __");

				map<string, float> weights;
				for(map<string, double>::iterator it = joints_timestamps.begin(); it != joints_timestamps.end(); ++it)
				{
					if(joints_enabled[it->first])
					{
						//float weight = 0.0; // TODO bouger dans le corps de la boucle ?
						double elapsed = Time::now().toSec() - joints_timestamps[it->first];
						if(joints_positions.count("last"+it->first) != 0)
						{
							epsilon = joints_positions[it->first][joint_name] - (joints_positions["last"+it->first][joint_name] + ((float)elapsed) * joints_speeds["last"+it->first][joint_name]);
						}

						//ROS_INFO_STREAM("Trust speed " << it->first << " : \n" << epsilon);
						float tmpsum = 0;
						for(int i = 0; i < 3 ; i++)
						{
							//weight += (a * abs(epsilon[i]) + b);
							tmpsum += abs(epsilon[i]);
						}
						//ROS_INFO_STREAM("EPS speed " << it->first << " : \t \t" << tmpsum/3);

						float weight = (a * tmpsum/3) + b;
						//ROS_INFO_STREAM("Weight speed " << it->first << " : \t" << weight);
						if(weight < 0.1)
						{
							weight = 0.1;
						}
						else if(weight > 1)
						{
							weight = 1.0;
						}
						//ROS_INFO_STREAM("Weight speed corr \t" << it->first << " : " << weight);

						weights[it->first] = weight;
					}
				}
				return weights;
			}

		public:
			EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
			NodeCloudFusion(int argc, char** argv):
			Node(ROS_NODE_CLOUDFUSION_NAME, ROS_NODE_CLOUDFUSION_RATE, argc, argv)
			{
				subscriber_add<mesureKalman>(ROS_TOPIC_PC_TF, &NodeCloudFusion::enqueue);

				publisher_add<mesureKalman>(ROS_TOPIC_PC_FUSION_TF);
			}

			void on_start()
			{
			}

			void on_update()
			{
				//ROS_WARN_STREAM("I update - in");
				bool update = false;
				for(map<string, double>::iterator it = joints_timestamps.begin(); it != joints_timestamps.end(); ++it)
				{
					if(Time::now().toSec() - it->second < MAX_DELTAT)
					{
						//ROS_WARN_STREAM("I update - enter");
						kinect_current = it->first;
						joints_enabled[it->first] = true;
						update = true;
					}
					else
					{
						//ROS_WARN_STREAM("I update - nop");
						joints_enabled[it->first] = false;
					}
				}

				if(update)
				{
					fusion();
				}
				//ROS_WARN_STREAM("I update - out");
			}

			void on_end()
			{

			}
		};
}

int main(int argc, char** argv)
{
	return (prog_kinect::NodeCloudFusion(argc, argv)).run();
}
