import sympy as sy
import numpy as np
import commonFct as common
import math
import sys
from sympy.utilities.codegen import codegen
from sympy.printing import ccode

class JacobianResult(object):
	def __init__(self, dim_x, dim_y, dim_u=0):

		self.dim_x = dim_x
		self.dim_y = dim_y

		## ------ VARIABLES
		t = sy.symbols('t')

		# base - origin
		P_bx = sy.Function('Pbx')(t)
		P_by = sy.Function('Pby')(t)
		P_bz = sy.Function('Pbz')(t)  # base coord
		P2_bx = sy.Function('P2bx')(t)
		P2_by = sy.Function('P2by')(t)
		P2_bz = sy.Function('P2bz')(t)  # base coord

		# longueurs
		l1, l2, l3, l3b = sy.symbols('l1 l2 l3 l3b', real=True) 	# spine and head
		l4, l5, l6 = sy.symbols('l4 l5 l6', real=True) 	# arm
		l7, l8, l9 = sy.symbols('l7 l8 l9', real=True)	 # leg
		l10, l11, l12 = sy.symbols('l10 l11 l12', real=True) 	# hand, thumb, handTip
		l13 = sy.symbols('l13', real=True) 	# foot

		# angles
		th_01 = sy.Function('th_01')(t)
		th_02 = sy.Function('th_02')(t)
		th_03 = sy.Function('th_03')(t) 	# base

		th2_01 = sy.Function('th2_01')(t)
		th2_02 = sy.Function('th2_02')(t)
		th2_03 = sy.Function('th2_03')(t) 	# base

		th_1 = sy.Function('th_1')(t)
		th_2 = sy.Function('th_2')(t) 	# spine
		th_3 = sy.Function('th_3')(t)
		th_4 = sy.Function('th_4')(t) 	# head

		th_5 = sy.Function('th_5')(t)
		th_6 = sy.Function('th_6')(t) 	#	 Before_shoulder right
		th_7 = sy.Function('th_7')(t)
		th_8 = sy.Function('th_8')(t)
		th_9 = sy.Function('th_9')(t)		# shoulderR
		th_10 = sy.Function('th_10')(t) 		# elbowR
		th_29 = sy.Function('th_29')(t)
		th_30 = sy.Function('th_30')(t)
		th_31 = sy.Function('th_31')(t)
		th_32 = sy.Function('th_32')(t)		# wristR X, Z (hand) X, Z (thumb)

		th_11 = sy.Function('th_11')(t)
		th_12 = sy.Function('th_12')(t) 		# Before_shoulder left
		th_13 = sy.Function('th_13')(t)
		th_14 = sy.Function('th_14')(t)
		th_15 = sy.Function('th_15')(t) 		# shoulderL
		th_16 = sy.Function('th_16')(t)		# elbowL
		th_33 = sy.Function('th_33')(t)
		th_34 = sy.Function('th_34')(t)
		th_35 = sy.Function('th_35')(t)
		th_36 = sy.Function('th_36')(t) 		# wristL X, Z (hand) X, Z (thumb)

		th_17 = sy.Function('th_17')(t) 		# before Hip R
		th_18 = sy.Function('th_18')(t)
		th_19 = sy.Function('th_19')(t)
		th_20 = sy.Function('th_20')(t) 		# hipR
		th_21 = sy.Function('th_21')(t)
		th_22 = sy.Function('th_22')(t) 		# kneeR
		th_37 = sy.Function('th_37')(t)
		th_38 = sy.Function('th_38')(t) 		# footR X; Y

		th_23 = sy.Function('th_23')(t) 		# before Hip L
		th_24 = sy.Function('th_24')(t)
		th_25 = sy.Function('th_25')(t)
		th_26 = sy.Function('th_24')(t) 		# hipL
		th_27 = sy.Function('th_27')(t)
		th_28 = sy.Function('th_28')(t) 		# kneeL
		th_39 = sy.Function('th_39')(t)
		th_40 = sy.Function('th_40')(t) 		# footL X, Y

		## ------- EQUATIONS

		# le repere 0 est fixe au pelvis

		#Tbase = common.matrice_de_passage_Base(np.array([Pbx, Pby, Pbz]), np.array([Phx, Phy, Phz]), np.array([Pmx, Pmy, Pmz])) #base
		Tbase = common.matrice_de_passage_Translation(P_bx, P_by, P_bz)*common.matrice_de_passage_RotationX(th_01)*common.matrice_de_passage_RotationY(th_02)*common.matrice_de_passage_RotationZ(th_03)  #base
		Tbase2 = common.matrice_de_passage_Translation(P2_bx, P2_by, P2_bz)*common.matrice_de_passage_RotationX(th_01)*common.matrice_de_passage_RotationY(th_02)*common.matrice_de_passage_RotationZ(th_03)  #base
		T0_1 = common.matrice_de_passage_Translation(0, l1, 0) # spineMid
		T1_5 = common.matrice_de_passage_RotationX(th_1)*common.matrice_de_passage_RotationZ(th_2)*common.matrice_de_passage_Translation(0, l2, 0) # spineShoulder
		T5_5b = common.matrice_de_passage_Translation(0, l3b, 0) # neck
		T5_7 = common.matrice_de_passage_RotationX(th_3)*common.matrice_de_passage_RotationZ(th_4)*common.matrice_de_passage_Translation(0, l3, 0) # head

		T5_8 = common.matrice_de_passage_RotationY(th_5)*common.matrice_de_passage_RotationZ(th_6)*common.matrice_de_passage_Translation(-l4, 0, 0) # shoulderR
		T8_10 = common.matrice_de_passage_RotationX(th_7)*common.matrice_de_passage_RotationZ(th_8)*common.matrice_de_passage_Translation(0, -l5, 0) # elbowR
		T10_11 = common.matrice_de_passage_RotationY(th_9)*common.matrice_de_passage_RotationX(th_10)*common.matrice_de_passage_Translation(0, -l6, 0)  #  wristR
		T11_27 = common.matrice_de_passage_RotationX(th_29)*common.matrice_de_passage_RotationZ(th_30)*common.matrice_de_passage_Translation(0, -l10, 0)  #  HandR
		T27_28 = common.matrice_de_passage_RotationY(th_31)*common.matrice_de_passage_Translation(l11, 0, 0) # th_umb
		#T27_28 = common.matrice_de_passage_RotationX(th_31)*common.matrice_de_passage_RotationZ(th_32)*common.matrice_de_passage_Translation(0, -l11, 0) #  Thumb
		#T27_29 = common.matrice_de_passage_Translation(0,-l12,0) # handTip

		T5_14 = common.matrice_de_passage_RotationY(th_11)*common.matrice_de_passage_RotationZ(th_12)*common.matrice_de_passage_Translation(l4, 0, 0)  # shoulderL
		T14_16 = common.matrice_de_passage_RotationX(th_13)*common.matrice_de_passage_RotationZ(th_14)*common.matrice_de_passage_Translation(0, -l5, 0)  # elbowL
		T16_17 = common.matrice_de_passage_RotationY(th_15)*common.matrice_de_passage_RotationX(th_16)*common.matrice_de_passage_Translation(0, -l6, 0)  #  wristL
		T17_30 = common.matrice_de_passage_RotationX(th_32)*common.matrice_de_passage_RotationZ(th_33)*common.matrice_de_passage_Translation(0, -l10, 0)  #  HandR
		T30_31 = common.matrice_de_passage_RotationY(th_34)*common.matrice_de_passage_Translation(-l11, 0, 0) # th_umb
		#T30_31 = common.matrice_de_passage_RotationX(th_35)*common.matrice_de_passage_RotationZ(th_36)*common.matrice_de_passage_Translation(0, -l11, 0) #  Thumb
		#T30_32 = common.matrice_de_passage_Translation(0,-l12,0) # handTip

		T0_18 = common.matrice_de_passage_RotationY(th_17)*common.matrice_de_passage_RotationZ(th_18)*common.matrice_de_passage_Translation(-l7, 0, 0)  # HipR
		T18_20 = common.matrice_de_passage_RotationX(th_19)*common.matrice_de_passage_RotationZ(th_20)*common.matrice_de_passage_Translation(0, -l8, 0)  # KneeR
		T20_21 = common.matrice_de_passage_RotationY(th_21)*common.matrice_de_passage_RotationX(th_22)*common.matrice_de_passage_Translation(0, -l9, 0)  # AnkleR
		#T21_33 = common.matrice_de_passage_RotationX(th_37)*common.matrice_de_passage_RotationY(th_38)*common.matrice_de_passage_Translation(0, -l13, 0)  #  Thumb

		T0_22 = common.matrice_de_passage_RotationY(th_23)*common.matrice_de_passage_RotationZ(th_24)*common.matrice_de_passage_Translation(l7, 0, 0)  # HipL
		T22_24 = common.matrice_de_passage_RotationX(th_25)*common.matrice_de_passage_RotationZ(th_26)*common.matrice_de_passage_Translation(0, -l8, 0)  # KneeL
		T24_26 = common.matrice_de_passage_RotationY(th_27)*common.matrice_de_passage_RotationX(th_28)*common.matrice_de_passage_Translation(0, -l9, 0)  # AnkleL
		#T26_34 = common.matrice_de_passage_RotationX(th_39)*common.matrice_de_passage_RotationY(th_40)*common.matrice_de_passage_Translation(0, -l13, 0)  #   Thumb

		Tb_1 = Tbase*T0_1		# Spine Mid
		Tb_5 = Tb_1*T1_5 		# Spine Shoulder
		Tb_5b = Tb_5*T5_5b 	# Neck
		Tb_7 = Tb_5b*T5_7		# Head

		Tb_8 = Tb_5*T5_8			# Shoulder Right
		Tb_10 = Tb_8*T8_10		# Elbow Right
		Tb_11 = Tb_10*T10_11	# Wrist Right
		Tb_27 = Tb_11*T11_27  # hand Right
		Tb_28 = Tb_27*T27_28  # th_umb right
		#Tb_29 = Tb_27*T27_29  # handTip Right
		#Tb_28 = Tb_27*T27_28  # th_umb Right

		Tb_14 = Tb_5*T5_14		# Shoulder Left
		Tb_16 = Tb_14*T14_16  # Elbow Left
		Tb_17 = Tb_16*T16_17	# Wrist Left
		Tb_30 = Tb_17*T17_30  # hand Left
		Tb_31 = Tb_30*T30_31  # th_umb left
		#Tb_32 = Tb_30*T30_32  # handTip Left
		#Tb_31 = Tb_30*T30_31  # th_umb Left

		Tb_18 = Tbase*T0_18	# Hip Right
		Tb_20 = Tb_18*T18_20	# Knee Right
		Tb_21 = Tb_20*T20_21	# Ankle Right
		#Tb_33 = Tb_21*T21_33 # foot Right

		Tb_22 = Tbase*T0_22	# Hip Left
		Tb_24 = Tb_22*T22_24	# Knee Left
		Tb_26 = Tb_24*T24_26	# Ankle Left
		#Tb_34 = Tb_26*T26_34 # foot Left

		""" 2 """

		Tb2_1 = Tbase2*T0_1		# Spine Mid
		Tb2_5 = Tb2_1*T1_5 		# Spine Shoulder
		Tb2_5b = Tb2_5*T5_5b 	# Neck
		Tb2_7 = Tb2_5b*T5_7		# Head

		Tb2_8 = Tb2_5*T5_8			# Shoulder Right
		Tb2_10 = Tb2_8*T8_10		# Elbow Right
		Tb2_11 = Tb2_10*T10_11	# Wrist Right
		Tb2_27 = Tb2_11*T11_27  # hand Right
		Tb2_28 = Tb2_27*T27_28  # th_umb right
		#Tb_29 = Tb_27*T27_29  # handTip Right
		#Tb_28 = Tb_27*T27_28  # th_umb Right

		Tb2_14 = Tb2_5*T5_14		# Shoulder Left
		Tb2_16 = Tb2_14*T14_16  # Elbow Left
		Tb2_17 = Tb2_16*T16_17	# Wrist Left
		Tb2_30 = Tb2_17*T17_30  # hand Left
		Tb2_31 = Tb2_30*T30_31  # th_umb left
		#Tb_32 = Tb_30*T30_32  # handTip Left
		#Tb_31 = Tb_30*T30_31  # th_umb Left

		Tb2_18 = Tbase2*T0_18	# Hip Right
		Tb2_20 = Tb2_18*T18_20	# Knee Right
		Tb2_21 = Tb2_20*T20_21	# Ankle Right
		#Tb_33 = Tb_21*T21_33 # foot Right

		Tb2_22 = Tbase2*T0_22	# Hip Left
		Tb2_24 = Tb2_22*T22_24	# Knee Left
		Tb2_26 = Tb2_24*T24_26	# Ankle Left
		#Tb_34 = Tb_26*T26_34 # foot Left
		print("done")

		self._T = [Tbase, Tb_1, Tb_5, Tb_5b, Tb_7, Tb_8, Tb_10, Tb_11, Tb_27, Tb_28, Tb_14, Tb_16, Tb_17, Tb_30, Tb_31, Tb_18, Tb_20, Tb_21, Tb_22, Tb_24, Tb_26]
		T2 = [Tbase2, Tb2_1, Tb2_5, Tb2_5b, Tb2_7, Tb2_8, Tb2_10, Tb2_11, Tb2_27, Tb2_28, Tb2_14, Tb2_16, Tb2_17, Tb2_30, Tb2_31, Tb2_18, Tb2_20, Tb2_21, Tb2_22, Tb2_24, Tb2_26]

		# Jacobienne
		dimytmp = 126
		g = sy.zeros(dimytmp/2,1) # PENSER A MULTIPLIER PAR TROIS ET PAS PAR QUATRE
		for i in range(len(g)/3):
			g[i*3:(i*3)+3,:] = common.computeP(self._T[i])

		g2 = sy.zeros(dimytmp/2,1) # PENSER A MULTIPLIER PAR TROIS ET PAS PAR QUATRE
		for i in range(len(g2)/3):
			g2[i*3:(i*3)+3,:] = common.computeP(T2[i])

		f = sy.zeros(dim_y,1)
		f[0:dimytmp/2,:] = g[0:dimytmp/2,:]
		f[dimytmp:dim_y,:] = g2[0:dimytmp/2,:]
		for i in range(len(g)/3):
			f[dimytmp/2+i*3:dimytmp/2+(i*3)+3,:] = sy.diff(common.computeP(self._T[i]),t)
		self._f = f
		print(self._f.shape)

		print(f[dimytmp/2+5,0])

		self._Q = sy.Matrix([P_bx, P_by, P_bz, th_01, th_02, th_03, th_1, th_2, th_3, th_4, th_5, th_6, th_7, th_8, th_9, th_10, th_29, th_30, th_31, th_11, th_12, th_13, th_14, th_15, th_16, th_32, th_33, th_34, l1, l2, l3, l3b, l4, l5, l6, l10, l11, th_17, th_18, th_19, th_20, th_21, th_22, th_23, th_24, th_25, th_26, th_27, th_28, l7, l8, l9, sy.Derivative(P_bx,t), sy.Derivative(P_by,t), sy.Derivative(P_bz,t), sy.Derivative(th_01,t), sy.Derivative(th_02,t), sy.Derivative(th_03,t), sy.Derivative(th_1,t), sy.Derivative(th_2,t), sy.Derivative(th_3,t), sy.Derivative(th_4,t), sy.Derivative(th_5,t), sy.Derivative(th_6,t), sy.Derivative(th_7,t), sy.Derivative(th_8,t), sy.Derivative(th_9,t), sy.Derivative(th_10,t), sy.Derivative(th_29,t), sy.Derivative(th_30,t), sy.Derivative(th_31,t), sy.Derivative(th_11,t), sy.Derivative(th_12,t), sy.Derivative(th_13,t), sy.Derivative(th_14,t), sy.Derivative(th_15,t), sy.Derivative(th_16,t), sy.Derivative(th_32,t), sy.Derivative(th_33,t), sy.Derivative(th_34,t), sy.Derivative(th_17,t), sy.Derivative(th_18,t), sy.Derivative(th_19,t), sy.Derivative(th_20,t), sy.Derivative(th_21,t), sy.Derivative(th_22,t), sy.Derivative(th_23,t), sy.Derivative(th_24,t), sy.Derivative(th_25,t), sy.Derivative(th_26,t), sy.Derivative(th_27,t), sy.Derivative(th_28,t),P2_bx, P2_by, P2_bz])
		print(self._Q.shape)

		J = common.jacobian(f,self._Q)
		self._J = J
		"""f = sy.zeros(dim_y,1)
		for i in range(len(f)/4):
			f[i*4:(i*4)+4,:] = common.computeF(self._T[i])
		self._f = f

		J = common.jacobian(f,self._Q)
		self._J = J"""

	""" GETTER AND SETTER """
	@property
	def J(self):
		""" Jacobian symbolic matrix """
		return self._J

	@property
	def eq(self):
		""" Equation for jacobian matrix """
		return self._eq

	@property
	def h(self):
		""" Equation for jacobian matrix """
		return self._h

	@property
	def f(self):
		""" Equations from T matrices """
		return self._f

	@property
	def Q(self):
		""" Vector symbolic variable """
		return self._Q

	@property
	def cpt(self):
		""" Vector symbolic variable """
		return self._cpt

	@property
	def T(self):
		""" Equation for jacobian matrix """
		return self._T

	@property
	def Tinv(self):
		""" Equation for jacobian matrix """
		return self._Tinv

	@property
	def T1(self):
		""" Equation for jacobian matrix """
		return self._T1

	@property
	def T2(self):
		""" Equation for jacobian matrix """
		return self._T2

	@property
	def T3(self):
		""" Equation for jacobian matrix """
		return self._T3

	@property
	def T4(self):
		""" Equation for jacobian matrix """
		return self._T4

	@property
	def T5(self):
		""" Equation for jacobian matrix """
		return self._T5

	@property
	def T6(self):
		""" Equation for jacobian matrix """
		return self._T6

	@property
	def T7(self):
		""" Equation for jacobian matrix """
		return self._T7

	@property
	def T8(self):
		""" Equation for jacobian matrix """
		return self._T8

	@property
	def T9(self):
		""" Equation for jacobian matrix """
		return self._T9

	@property
	def T10(self):
		""" Equation for jacobian matrix """
		return self._T10

	@property
	def T11(self):
		""" Equation for jacobian matrix """
		return self._T11

	@property
	def T12(self):
		""" Equation for jacobian matrix """
		return self._T12

	@property
	def T13(self):
		""" Equation for jacobian matrix """
		return self._T13


if __name__ == '__main__':
	print '**** Jacobienne ****'
	#H = JacobianResult(37,3*15)
	H = JacobianResult(95,126+63)
	print '**** Jacobienne initialisee ****'

	t = sy.symbols('t')

	P_bx = sy.Function('Pbx')(t)
	P_by = sy.Function('Pby')(t)
	P_bz = sy.Function('Pbz')(t) # base coord

	th_01 = sy.Function('th_01')(t)
	th_02 = sy.Function('th_02')(t)
	th_03 = sy.Function('th_03')(t) # base

	P2_bx = sy.Function('P2bx')(t)
	P2_by = sy.Function('P2by')(t)
	P2_bz = sy.Function('P2bz')(t) # base coord

	th2_01 = sy.Function('th2_01')(t)
	th2_02 = sy.Function('th2_02')(t)
	th2_03 = sy.Function('th2_03')(t) # base

	th_1 = sy.Function('th_1')(t)
	th_2 = sy.Function('th_2')(t) # spine
	th_3 = sy.Function('th_3')(t)
	th_4 = sy.Function('th_4')(t) # head

	th_5 = sy.Function('th_5')(t)
	th_6 = sy.Function('th_6')(t) # Before_shoulder right
	th_7 = sy.Function('th_7')(t)
	th_8 = sy.Function('th_8')(t)
	th_9 = sy.Function('th_9')(t)# shoulderR
	th_10 = sy.Function('th_10')(t) # elbowR
	th_29 = sy.Function('th_29')(t)
	th_30 = sy.Function('th_30')(t)
	th_31 = sy.Function('th_31')(t)
	th_32 = sy.Function('th_32')(t)# wristR X, Z (hand) X, Z (thumb)

	th_11 = sy.Function('th_11')(t)
	th_12 = sy.Function('th_12')(t) # Before_shoulder left
	th_13 = sy.Function('th_13')(t)
	th_14 = sy.Function('th_14')(t)
	th_15 = sy.Function('th_15')(t) # shoulderL
	th_16 = sy.Function('th_16')(t)# elbowL
	th_33 = sy.Function('th_33')(t)
	th_34 = sy.Function('th_34')(t)
	th_35 = sy.Function('th_35')(t)
	th_36 = sy.Function('th_36')(t) # wristL X, Z (hand) X, Z (thumb)

	th_17 = sy.Function('th_17')(t) # before Hip R
	th_18 = sy.Function('th_18')(t)
	th_19 = sy.Function('th_19')(t)
	th_20 = sy.Function('th_20')(t) # hipR
	th_21 = sy.Function('th_21')(t)
	th_22 = sy.Function('th_22')(t) # kneeR
	th_37 = sy.Function('th_37')(t)
	th_38 = sy.Function('th_38')(t) # footR X; Y

	th_23 = sy.Function('th_23')(t) # before Hip L
	th_24 = sy.Function('th_24')(t)
	th_25 = sy.Function('th_25')(t)
	th_26 = sy.Function('th_24')(t) # hipL
	th_27 = sy.Function('th_27')(t)
	th_28 = sy.Function('th_28')(t) # kneeL
	th_39 = sy.Function('th_39')(t)
	th_40 = sy.Function('th_40')(t) # footL X, Y

	Pbx, Pby, Pbz = sy.symbols('Pbx Pby Pbz', real=True) # base coord
	P2bx, P2by, P2bz = sy.symbols('P2bx P2by P2bz')

	th01, th02, th03 = sy.symbols('th01 th02 th03', real=True) # spine
	th201, th202, th203 = sy.symbols('th201 th202 th203', real=True) # spine

	th1, th2 = sy.symbols('th1 th2', real=True) # spine
	th3, th4 = sy.symbols('th3 th4', real=True) # head

	th5, th6 = sy.symbols('th5 th6', real=True) # Before_shoulder right
	th7, th8, th9 = sy.symbols('th7 th8 th9', real=True) # shoulderR
	th10 = sy.symbols('th10', real=True) # elbowR
	th29, th30, th31, th32 = sy.symbols('th29 th30 th31 th32', real=True) # wristR X, Z (hand) X, Z (thumb)

	th11, th12 = sy.symbols('th11 th12', real=True) # Before_shoulder left
	th13, th14, th15 = sy.symbols('th13 th14 th15', real=True) # shoulderL
	th16 = sy.symbols('th16', real=True) # elbowL
	th33, th34, th35, th36 = sy.symbols('th33 th34 th35 th36', real=True) # wristL X, Z (hand) X, Z (thumb)

	th17 = sy.symbols('th17', real=True) # before Hip R
	th18, th19, th20 = sy.symbols('th18 th19 th20', real=True) # hipR
	th21, th22 = sy.symbols('th21 th22', real=True) # kneeR
	th37, th38 = sy.symbols('th37 th38', real=True) # footR X; Y

	th23 = sy.symbols('th23', real=True) # before Hip L
	th24, th25, th26 = sy.symbols('th24 th25 th26', real=True) # hipL
	th27, th28 = sy.symbols('th27 th28', real=True) # kneeL
	th39, th40 = sy.symbols('th39 th40', real=True) # footL X, Y

	dot_Pbx, dot_Pby, dot_Pbz = sy.symbols('dot_Pbx dot_Pby dot_Pbz', real=True) # base coord

	dot_th01, dot_th02, dot_th03 = sy.symbols('dot_th01 dot_th02 dot_th03', real=True) # spine
	dot_th1, dot_th2 = sy.symbols('dot_th1 dot_th2', real=True) # spine
	dot_th3, dot_th4 = sy.symbols('dot_th3 dot_th4', real=True) # head

	dot_th5, dot_th6 = sy.symbols('dot_th5 dot_th6', real=True) # Before_shoulder right
	dot_th7, dot_th8, dot_th9 = sy.symbols('dot_th7 dot_th8 dot_th9', real=True) # shoulderR
	dot_th10 = sy.symbols('dot_th10', real=True) # elbowR
	dot_th29, dot_th30, dot_th31, dot_th32 = sy.symbols('dot_th29 dot_th30 dot_th31 dot_th32', real=True) # wristR X, Z (hand) X, Z (thumb)

	dot_th11, dot_th12 = sy.symbols('dot_th11 dot_th12', real=True) # Before_shoulder left
	dot_th13, dot_th14, dot_th15 = sy.symbols('dot_th13 dot_th14 dot_th15', real=True) # shoulderL
	dot_th16 = sy.symbols('dot_th16', real=True) # elbowL
	dot_th33, dot_th34, dot_th35, dot_th36 = sy.symbols('dot_th33 dot_th34 dot_th35 dot_th36', real=True) # wristL X, Z (hand) X, Z (thumb)

	dot_th17 = sy.symbols('dot_th17', real=True) # before Hip R
	dot_th18, dot_th19, dot_th20 = sy.symbols('dot_th18 dot_th19 dot_th20', real=True) # hipR
	dot_th21, dot_th22 = sy.symbols('dot_th21 dot_th22', real=True) # kneeR
	dot_th37, dot_th38 = sy.symbols('dot_th37 dot_th38', real=True) # footR X; Y

	dot_th23 = sy.symbols('dot_th23', real=True) # before Hip L
	dot_th24, dot_th25, dot_th26 = sy.symbols('dot_th24 dot_th25 dot_th26', real=True) # hipL
	dot_th27, dot_th28 = sy.symbols('dot_th27 dot_th28', real=True) # kneeL
	dot_th39, dot_th40 = sy.symbols('dot_th39 dot_th40', real=True) # footL X, Y

	subf, simpf = sy.cse(H.f.subs([(sy.Derivative(P_bx,t), dot_Pbx), (sy.Derivative(P_by,t), dot_Pby), (sy.Derivative(P_bz,t), dot_Pbz), (sy.Derivative(th_01,t), dot_th01), (sy.Derivative(th_02,t), dot_th02), (sy.Derivative(th_03,t), dot_th03), (sy.Derivative(th_1,t), dot_th1), (sy.Derivative(th_2,t), dot_th2), (sy.Derivative(th_3,t), dot_th3), (sy.Derivative(th_4,t), dot_th4), (sy.Derivative(th_5,t), dot_th5), (sy.Derivative(th_6,t), dot_th6), (sy.Derivative(th_7,t), dot_th7), (sy.Derivative(th_8,t), dot_th8), (sy.Derivative(th_9,t), dot_th9), (sy.Derivative(th_10,t), dot_th10), (sy.Derivative(th_11,t), dot_th11), (sy.Derivative(th_12,t), dot_th12), (sy.Derivative(th_13,t), dot_th13), (sy.Derivative(th_14,t), dot_th14), (sy.Derivative(th_15,t), dot_th15), (sy.Derivative(th_16,t), dot_th16), (sy.Derivative(th_17,t), dot_th17), (sy.Derivative(th_18,t), dot_th18), (sy.Derivative(th_19,t), dot_th19), (sy.Derivative(th_20,t), dot_th20), (sy.Derivative(th_21,t), dot_th21), (sy.Derivative(th_22,t), dot_th22), (sy.Derivative(th_23,t), dot_th23), (sy.Derivative(th_24,t), dot_th24), (sy.Derivative(th_25,t), dot_th25), (sy.Derivative(th_26,t), dot_th26), (sy.Derivative(th_27,t), dot_th27), (sy.Derivative(th_28,t), dot_th28), (sy.Derivative(th_29,t), dot_th29), (sy.Derivative(th_30,t), dot_th30), (sy.Derivative(th_31,t), dot_th31), (sy.Derivative(th_32,t), dot_th32), (sy.Derivative(th_33,t), dot_th33), (sy.Derivative(th_34,t), dot_th34), (P_bx, Pbx), (P_by, Pby), (P_bz, Pbz), (th_01, th01), (th_02, th02), (th_03, th03), (th_1, th1), (th_2, th2), (th_3, th3), (th_4, th4), (th_5, th5), (th_6, th6), (th_7, th7), (th_8, th8), (th_9, th9), (th_10, th10), (th_11, th11), (th_12, th12), (th_13, th13), (th_14, th14), (th_15, th15), (th_16, th16), (th_17, th17), (th_18, th18), (th_19, th19), (th_20, th20), (th_21, th21), (th_22, th22), (th_23, th23), (th_24, th24), (th_25, th25), (th_26, th26), (th_27, th27), (th_28, th28), (th_29, th29), (th_30, th30), (th_31, th31), (th_32, th32), (th_33,th33), (th_34, th34),(P2_bx, P2bx), (P2_by, P2by), (P2_bz, P2bz)]))
	print '**** f ****'
	subj, simpj = sy.cse(H.J.subs([(sy.Derivative(P_bx,t), dot_Pbx), (sy.Derivative(P_by,t), dot_Pby), (sy.Derivative(P_bz,t), dot_Pbz), (sy.Derivative(th_01,t), dot_th01), (sy.Derivative(th_02,t), dot_th02), (sy.Derivative(th_03,t), dot_th03), (sy.Derivative(th_1,t), dot_th1), (sy.Derivative(th_2,t), dot_th2), (sy.Derivative(th_3,t), dot_th3), (sy.Derivative(th_4,t), dot_th4), (sy.Derivative(th_5,t), dot_th5), (sy.Derivative(th_6,t), dot_th6), (sy.Derivative(th_7,t), dot_th7), (sy.Derivative(th_8,t), dot_th8), (sy.Derivative(th_9,t), dot_th9), (sy.Derivative(th_10,t), dot_th10), (sy.Derivative(th_11,t), dot_th11), (sy.Derivative(th_12,t), dot_th12), (sy.Derivative(th_13,t), dot_th13), (sy.Derivative(th_14,t), dot_th14), (sy.Derivative(th_15,t), dot_th15), (sy.Derivative(th_16,t), dot_th16), (sy.Derivative(th_17,t), dot_th17), (sy.Derivative(th_18,t), dot_th18), (sy.Derivative(th_19,t), dot_th19), (sy.Derivative(th_20,t), dot_th20), (sy.Derivative(th_21,t), dot_th21), (sy.Derivative(th_22,t), dot_th22), (sy.Derivative(th_23,t), dot_th23), (sy.Derivative(th_24,t), dot_th24), (sy.Derivative(th_25,t), dot_th25), (sy.Derivative(th_26,t), dot_th26), (sy.Derivative(th_27,t), dot_th27), (sy.Derivative(th_28,t), dot_th28), (sy.Derivative(th_29,t), dot_th29), (sy.Derivative(th_30,t), dot_th30), (sy.Derivative(th_31,t), dot_th31), (sy.Derivative(th_32,t), dot_th32), (sy.Derivative(th_33,t), dot_th33), (sy.Derivative(th_34,t), dot_th34), (P_bx, Pbx), (P_by, Pby), (P_bz, Pbz), (th_01, th01), (th_02, th02), (th_03, th03), (th_1, th1), (th_2, th2), (th_3, th3), (th_4, th4), (th_5, th5), (th_6, th6), (th_7, th7), (th_8, th8), (th_9, th9), (th_10, th10), (th_11, th11), (th_12, th12), (th_13, th13), (th_14, th14), (th_15, th15), (th_16, th16), (th_17, th17), (th_18, th18), (th_19, th19), (th_20, th20), (th_21, th21), (th_22, th22), (th_23, th23), (th_24, th24), (th_25, th25), (th_26, th26), (th_27, th27), (th_28, th28), (th_29, th29), (th_30, th30), (th_31, th31), (th_32, th32), (th_33,th33), (th_34, th34),(P2_bx, P2bx), (P2_by, P2by), (P2_bz, P2bz)]))
	print '**** J ****'
	"""
	sub, simp = sy.cse(H.Tinv)"""

	with open("tryVitesse.c","w") as output:
		for helper in subj:
			output.write("double ")
			output.write(ccode(helper[1], helper[0]))
			output.write("\n")
		output.write("\n")
		for res in simpj:
			output.write(ccode(res,"T"))
			output.write("\n")
		output.write("\n")
		output.write("\n")
		output.write("\n")
		output.write("\n")
		for helper in subf:
			output.write("double ")
			output.write(ccode(helper[1], helper[0]))
			output.write("\n")
		output.write("\n")
		output.write("\n")
		for res in simpf:
			output.write(ccode(res,"M"))
			output.write("\n")
		output.write("\n")

	#[(c_name, c_code), (h_name, c_header)] = codegen([("t1", H.T1),("t2", H.T2), ("t7",H.T7),("eqFunction", H.f)  ],"C", "newModelInv", header=False, empty=False)
	#[(c_name, c_code), (h_name, c_header)] = codegen([("eqJacobian", H.J), ("eqFunction", H.f)],"C", "newModelSimp", header=False, empty=False) #eqGeometrique : nom du fichier ou les equations sont a recuperer
	#print(c_name)

	#print '**** codegen ****'

	#fh = open(c_name,"w")
	#fh.write(c_code)
	#fh.close()