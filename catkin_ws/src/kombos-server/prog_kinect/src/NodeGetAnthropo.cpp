#include <Eigen/Geometry>
#include <kombos_msgs/mesureKinect.h>
#include <kombos_msgs/mesureKalman.h>
#include <kombos_msgs/anthropo.h>
#include "../include/Network.hpp"
#include "../include/Utils.hpp"
#include "../include/Node.hpp"

using namespace std;
using namespace std::chrono;
using namespace kombos_msgs;
using namespace Eigen;

namespace prog_kinect
{
	class NodeGetAnthropo : public Node
	{
		private:
			int cpt_ind_properties = 0;
			int cpt_male = 0;
			int cpt_neutral = 0;
			int cpt_angry = 0;
			int cpt_happy = 0;
			int cpt_surprise = 0;
			float average_age = 0;

			string average_gender;
			string average_emotion;

			void aggregate_individual_properties(const anthropo::ConstPtr& message)
			{
				if(message->gender == "male") cpt_male++;
				if(cpt_ind_properties != 0 && (cpt_male / cpt_ind_properties) > 50) average_gender = "male";
				else average_gender = "female";

				string the_emotion = max_emotion(message->neutral, message->angry, message->surprise, message->happy);
				if(the_emotion == "neutral") cpt_neutral++;
				else if(the_emotion == "surprise") cpt_surprise++;
				else if(the_emotion == "happy") cpt_happy++;
				else if(the_emotion == "angry") cpt_angry++;
				average_emotion = max_emotion(cpt_neutral, cpt_angry, cpt_surprise, cpt_happy);

				average_age = ((average_age * cpt_ind_properties) + message->age) / (cpt_ind_properties + 1);
				cpt_ind_properties++;

				//ROS_INFO_STREAM(cpt_ind_properties << " : " << average_gender << " of " << (int)average_age << " years and " << average_emotion);
			}

			string max_emotion(float neutral, float angry, float surprise, float happy)
			{
				if(neutral >= angry && neutral >= surprise && neutral >= happy) return "neutral";
				else if(angry >= neutral && angry >= surprise && angry >= happy) return "angry";
				else if(happy >= neutral && happy >= surprise && happy >= angry) return "happy";
				else return "surprise";

				return "lol";
			}

			void aggregate_morphology(const anthropo::ConstPtr& message)
			{
				anthropo new_msg = *message;
				stringstream ss;
				ss << "{\"size\":" << message->size << ",\"weight\":" << message->weight;
				if(cpt_ind_properties > 0)
				{
					new_msg.age = average_age;
					new_msg.gender = average_gender;
					new_msg.neutral = cpt_neutral / (float)cpt_ind_properties;
					new_msg.angry = cpt_angry / (float)cpt_ind_properties;
					new_msg.happy = cpt_happy / (float)cpt_ind_properties;
					new_msg.surprise = cpt_surprise / (float)cpt_ind_properties;

					ss << ",\"age\":" << (int)average_age << ",\"gender\":\"" << average_gender << "\",\"emotion\":\"" << average_emotion <<"\"}";
					string str = ss.str();
					_redox.set(NAME_ANTHROPO_DATA, str);
				}
				publisher_send(ROS_TOPIC_ANTHROPO_FINAL, new_msg);
			}

			void initialize_anthropo_value()
			{
				cpt_ind_properties = 0;
				cpt_male = 0;
				cpt_neutral = 0;
				cpt_angry = 0;
				cpt_happy = 0;
				cpt_surprise = 0;
			}

		public:
			EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
			NodeGetAnthropo(int argc, char** argv):
			Node(ROS_NODE_GET_ANTHROPO_NAME, ROS_NODE_GET_ANTHROPO_RATE, argc, argv)
			{
				subscriber_add<control_server_cmd>(ROS_TOPIC_RECORD, &NodeGetAnthropo::server_toggle);
				subscriber_add<anthropo>(ROS_TOPIC_IND_PROPERTIES, &NodeGetAnthropo::aggregate_individual_properties);
				subscriber_add<anthropo>(ROS_TOPIC_ANTHROPO_COMPLETED, &NodeGetAnthropo::aggregate_morphology);

				publisher_add<anthropo>(ROS_TOPIC_ANTHROPO_FINAL, 0);
			}

			void on_start()
			{
				redox_init(REDOX_HOST, REDOX_PORT);
			}

			void on_update()
			{

			}

			void on_end()
			{
				_redox.disconnect();
			}

			void action_on_toggle()
			{
				initialize_anthropo_value();
			}
	};
}

int main(int argc, char** argv)
{
	return (prog_kinect::NodeGetAnthropo(argc, argv)).run();
}
