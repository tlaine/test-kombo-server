#include "../include/JsonReader.hpp"

using namespace std;
using namespace ros;

namespace prog_kinect
{
	JsonReader::JsonReader(const string& key, json_t* value)
	{
		name = key;
		content = value;

		json_type type = json_typeof(content);
		if(type == JSON_OBJECT)
		{
			const char* child_key;
			json_t* child_value;
			json_object_foreach(content, child_key, child_value)
			{
				elements.push_back(new JsonReader(string(child_key), child_value));
			}
		}
		else if(type == JSON_ARRAY)
		{
			size_t child_index;
			json_t* child_value;
			json_array_foreach(content, child_index, child_value)
			{
				elements.push_back(new JsonReader(to_string((int)child_index), child_value));
			}
		}
	}

	JsonReader::~JsonReader()
	{
		if(content != NULL)
		{
			json_decref(content);
		}

		for(vector<JsonReader*>::iterator it = elements.begin(); it != elements.end(); ++it)
		{
			delete *it;
		}
	}

	string JsonReader::get_name()
	{
		return name;
	}

	int JsonReader::get_int()
	{
		return json_integer_value(content);
	}

	double JsonReader::get_double()
	{
		return json_real_value(content);
	}

	bool JsonReader::get_bool()
	{
		return json_boolean_value(content);
	}

	string JsonReader::get_string()
	{
		return string(json_string_value(content));
	}

	int JsonReader::at_key(const string& key)
	{
		int result = -1;

		int i = 0;
		foreach([&](const string& element_name, JsonReader* element)
		{
			if(element_name == key)
			{
				result = i;
			}

			i++;
		});

		return result;
	}

	JsonReader* JsonReader::at(const string& key)
	{
		JsonReader* result = this;

		try
		{
			int element_key = at_key(key);
			if(element_key >= 0 && element_key < elements.size())
			{
				result = elements[element_key];
			}
		}
		catch(exception& exception)
		{
			ROS_ERROR(JSON_MESSAGE_NOTCHILD, key, name);
		}

		return result;
	}

	void JsonReader::foreach(const std::function<void(const string& name, JsonReader*)>& callback)
	{
		for(vector<JsonReader*>::iterator it = elements.begin(); it != elements.end(); ++it)
		{
			JsonReader* element = *it;
			callback(element->name, element);
		}
	}

	void JsonReader::read(const string& content, const function<void(JsonReader*)>& callback)
	{
		json_error_t error;
		parse(json_loads(content.c_str(), 0, &error), error, callback);
	}

	void JsonReader::read_at(const string& path, const function<void(JsonReader*)>& callback)
	{
		json_error_t error;
		parse(json_load_file(path.c_str(), 0, &error), error, callback);
	}

	void JsonReader::parse(json_t* root, json_error_t error, const function<void(JsonReader*)>& callback)
	{
		if(!root)
		{
			ROS_ERROR(JSON_MESSAGE_INVALID, error.source, (int)error.line, (int)error.position, error.text);
			root = NULL;
		}
		else if(!json_is_object(root))
		{
			ROS_ERROR(JSON_MESSAGE_NOTOBJECT, error.source);
			json_decref(root);
			root = NULL;
		}
		else
		{
			JsonReader* reader = new JsonReader("", root);
			callback(reader);
			//CORRIGER CETTE FUITE
			//ATTENTION LE READER PETE UN CABLE SI ON LE DELETE
			//delete reader;
		}
	}
}
