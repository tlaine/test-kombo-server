/* EN GRANDE PARTIE OBSOLETE
SI ON LE REUTILISE IL FAUT PENSER A VERIFIER
LES MEMBRES DE LA CLASSE MERE AVEC JSON2CLOUD
*/

#include "ros/ros.h"
#include "std_msgs/String.h"
#include <Eigen/Geometry>
#include <Eigen/Core>
#include <vector>
#include <numeric>
#include <fstream>
#include <time.h>
#include <chrono>

#include "../include/FilterEKF.hpp"
#include "../include/json2model.h"

#include <kombos_msgs/SoftReinitKalman.h>
#include <kombos_msgs/kalmanState.h>
#include <kombos_msgs/mesureKinect.h>

using namespace prog_kinect;

static bool fus = false, kal = false, ik = false;

static std::map< std::string, int > joint_idx;
static std::vector<std::string> statesList;

static std::map< std::string, Eigen::Vector3d > jointsPosFusion;
static std::map< std::string, Eigen::Vector3d > jointsPosKalman;
static std::map< std::string, float > stateIK;
static std::map<std::string,std::vector<std::string>> joints2States;
static std::map<std::string,double> jointsLimitMin;
static std::map<std::string,double> jointsLimitMax;

static kombos_msgs::kalmanState state2Correct;
static ros::ServiceClient client;

static int dim_x, dim_y;
EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
void checkKalmanPC(){
	double delta;
	int i = 0;
	std::map<std::string, float> tmp;
	for(std::map<std::string,int>::iterator iterator = joint_idx.begin(); iterator != joint_idx.end(); iterator++){
		delta = sqrt(pow(jointsPosFusion[iterator->first][0] - jointsPosKalman[iterator->first][0],2) + pow(jointsPosFusion[iterator->first][1] - jointsPosKalman[iterator->first][1],2) + pow(jointsPosFusion[iterator->first][2] - jointsPosKalman[iterator->first][2],2));
		if(delta > 0.3)
		{
			ROS_WARN_STREAM("Articulation "<<iterator->first<<" : delta = "<<delta);
			for(std::vector<std::string>::iterator it = joints2States[iterator->first].begin(); it != joints2States[iterator->first].end(); it++){
				if(stateIK[*it] > jointsLimitMin[*it] && stateIK[*it] < jointsLimitMax[*it])
				{
					tmp[*it] = stateIK[*it];
				}
			}
			if(tmp.size() == joints2States[iterator->first].size())
			{
				ROS_WARN_STREAM("Articulation "<<iterator->first<<" ---- bonne taille");
				for(std::map<std::string, float>::iterator it = tmp.begin(); it != tmp.end(); it++){
					state2Correct.name.push_back(it->first);
					state2Correct.value.push_back(it->second);
				}
			}
			i++;
			tmp.clear();
		}
	}
	if(i > 0){
		bool answ;
		kombos_msgs::SoftReinitKalman srv;
		srv.request.state = state2Correct;
		if (client.call(srv)) answ = srv.response.initSuccess;
		else ROS_ERROR_STREAM("No srv delivered");
		state2Correct.name.clear();
		state2Correct.value.clear();
	}

}


void callbackPCfusion(const kombos_msgs::mesureKinect::ConstPtr & msg)
{
	unsigned int n = msg->mesures.size();
	int fiab = 0;
	for ( int i = 0; i<n; i++ )
	{
		jointsPosFusion[msg->mesures[i].name] = Eigen::Vector3d( msg->mesures[i].position.x,
		            msg->mesures[i].position.y,
		            msg->mesures[i].position.z);
	}
	fus = true;
}

void callbackPCKalman(const kombos_msgs::mesureKinect::ConstPtr & msg)
{
	unsigned int n = msg->mesures.size();
	int fiab = 0;
	for ( int i = 0; i<n; i++ )
	{
		jointsPosKalman[msg->mesures[i].name] = Eigen::Vector3d( msg->mesures[i].position.x,
		            msg->mesures[i].position.y,
		            msg->mesures[i].position.z);
	}
	kal = true;
}

void callback(const kombos_msgs::kalmanState::ConstPtr & msg)
{
  unsigned int n = msg->name.size();
  for ( int i = 0; i<n; i++ )
  {
  	stateIK[msg->name[i]] = msg->value[i];
  }
  ik = true;
}

int main(int argc, char **argv)
{
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  ros::init(argc, argv, "checkKalman");
  ros::NodeHandle nh;

  // sub
  ros::Subscriber sub1 = nh.subscribe("/PointCloudKalman", 1, callbackPCKalman);
  ros::Subscriber sub2 = nh.subscribe("/PointCloudFusion", 1, callbackPCfusion);
  ros::Subscriber sub3 = nh.subscribe("/kalmanInit", 1, callback);

  // pub
	client = nh.serviceClient<kombos_msgs::SoftReinitKalman>("/reInitKalman");

	//initialisation modele
	json2model modelParameters = json2model();
  Eigen::Vector2i size = modelParameters.GetSize();
  dim_x = size.x();
  dim_y = size.y();

	joint_idx = modelParameters.GetJointIdx();
	joints2States = modelParameters.GetJointsToStates();

	jointsLimitMin = modelParameters.GetAngleLimitMinMax(true);
  jointsLimitMax = modelParameters.GetAngleLimitMinMax(false);


  ros::Rate loop_rate(50) ;

  while ( ros::ok() )
  {
  	if(fus && kal && ik) checkKalmanPC();

		fus = kal = false;
		ros::spinOnce();
		loop_rate.sleep() ;
	}

  return 0;
}

/* EN GRANDE PARTIE OBSOLETE
SI ON LE REUTILISE IL FAUT PENSER A VERIFIER
LES MEMBRES DE LA CLASSE MERE AVEC JSON2CLOUD
*/
