#include <geometry_msgs/Point.h>
#include <kombos_msgs/mesureKalman.h>
#include "../include/Network.hpp"
#include "../include/FilterOE.hpp"
#include "../include/Node.hpp"
#include "../include/Utils.hpp"

using namespace std;
using namespace geometry_msgs;
using namespace kombos_msgs;
using namespace Eigen;

namespace prog_kinect
{
	class NodeKalman2KalmanOE : public Node
	{
		private:
			FilterOE filter = FilterOE(40, 2, 100, 1.0);

			void filter_apply(const mesureKinect::ConstPtr& message)
			{
				if(!outlier_researcher(message)) return;
				mesureKinect filter_message = *message;

				vector<vector<double>> points_raw;
				for(auto& point_raw : filter_message.mesures)
				{
					points_raw.push_back({point_raw.position.x, point_raw.position.y, point_raw.position.z});
				}

				vector<vector<double>> points_filtered = filter.filter(points_raw);
				for(int i = 0; i < points_filtered.size(); ++i)
				{
					filter_message.mesures[i].position.x = points_filtered[i][0];
					filter_message.mesures[i].position.y = points_filtered[i][1];
					filter_message.mesures[i].position.z = points_filtered[i][2];
				}

				publisher_send(ROS_TOPIC_PC_CORRECTED_OE, filter_message);
			}

			bool outlier_researcher(const mesureKinect::ConstPtr& msg)
			{
				map<string, Vector3f> tmp_positions;
				bool no_outlier = true;

				for(int i = 0; i < msg->mesures.size(); ++i)
				{
					position_msg position = msg->mesures[i];
					tmp_positions[position.name] = Vector3f(position.position.x, position.position.y, position.position.z);
				}

				float length = 0;
				for(map<string, pair<string,string>>::iterator it = limb_extremity.begin(); it != limb_extremity.end(); it++)
				{
					length = Utils::length_calculation(tmp_positions, it->second.first, it->second.second);
					if(length > LENGTH_FOR_OUTLIER)
					{
						no_outlier = false;
					}
				}
				return no_outlier;
			}

		public:
			NodeKalman2KalmanOE(int argc, char** argv):
			Node(ROS_NODE_KALMAN2KALMANOE_NAME, ROS_NODE_KALMAN2KALMANOE_RATE, argc, argv)
			{
				subscriber_add<mesureKinect>(ROS_TOPIC_PC_CORRECTED, &NodeKalman2KalmanOE::filter_apply);

				publisher_add<mesureKinect>(ROS_TOPIC_PC_CORRECTED_OE);
			}

			void on_start()
			{
			}

			void on_update()
			{

			}

			void on_end()
			{

			}
		};
}

int main(int argc, char** argv)
{
	return (prog_kinect::NodeKalman2KalmanOE(argc, argv)).run();
}
