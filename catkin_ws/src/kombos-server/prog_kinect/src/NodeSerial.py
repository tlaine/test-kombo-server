#!/usr/bin/env python
# pylint: disable=ungrouped-imports

import serial
import rospy
import json

from kombos_msgs.msg import Temperature
from kombos_msgs.msg import sound
from utils.watcher import WATCHER

ROS_NODE_SERIAL = "NodeSerial"

ROS_NODE_SOUND = "Sound"
ROS_TOPIC_SOUND = "/Sound"

ROS_NODE_TEMP = "Temperature"
ROS_TOPIC_TEMPERATURE = "/Temperature"

MESSAGE_START = "[TEMPS] STARTING TEMPS"
#Serial_port = '/dev/ttyACM0'
baud_rate = 9600

pubtemp = 0
pubsound = 0
# ser = serial.Serial(Serial_port, baud_rate)


def getTempSound():
    temp = Temperature()
    sound_msg = sound()
#    line = ser.readline()
#    line = line.decode('utf-8').strip()
    json_data = '{"Temperature": 33}'
    data_capt = json.loads(json_data)
    if "Temperature" in data_capt:
        temp.temperature_value = 200
        # round(float(line))
        pubtemp.publish(temp)
    elif "Sound" in data_capt:
        sound_msg.indice = 100
        pubsound.publish(sound_msg)


def callback(data):
    rospy.loginfo(rospy.get_caller_id() + "The temperature is %s", data.data)


def talker():
    rospy.init_node(ROS_NODE_SERIAL, anonymous=False, log_level=rospy.DEBUG)
    WATCHER.watch_log()
    rate = rospy.Rate(1)  # 1hz
    while not rospy.is_shutdown():
        temp_sound = getTempSound()
        rospy.loginfo(temp_sound)
        rate.sleep()


if __name__ == '__main__':
    try:
        pubtemp = rospy.Publisher(ROS_TOPIC_TEMPERATURE, Temperature, queue_size=10)
        pubsound = rospy.Publisher(ROS_TOPIC_SOUND, sound, queue_size=10) 
        talker()

    except rospy.ROSInterruptException:
        pass