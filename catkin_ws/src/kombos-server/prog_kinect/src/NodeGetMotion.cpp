#include <math.h>
#include <Eigen/Geometry>
#include <kombos_msgs/mesureKinect.h>
#include <kombos_msgs/motion.h>
#include <kombos_msgs/control_server_cmd.h>
#include "../include/Network.hpp"
#include "../include/Utils.hpp"
#include "../include/Node.hpp"

using namespace std;
using namespace std::chrono;
using namespace kombos_msgs;
using namespace Eigen;

namespace prog_kinect
{
	class NodeGetMotion : public Node
	{
		private:
			bool is_init = false;
			vector<string> pointcloud_segments;
			deque<mesureKinect> pointcloud_previous;
			time_point<system_clock> clock_tag = system_clock::now();
			map<string, double> sum_segments;
			double average_rate = 0.0;
			int cpt_average = 0;

			void resetDistances()
			{
				for(std::map<std::string, double>::iterator it = sum_segments.begin(); it != sum_segments.end(); ++it)
				{
					sum_segments[it->first]   = 0;
				}
			}

			void get_indice(const mesureKinect::ConstPtr& message)
			{
				Vector4d pos_com = Vector4d::Zero();
				motion message_motion;
				if(is_init)
				{
					duration<double> elapsed = system_clock::now() - clock_tag;
					if(elapsed.count() < 1E-3) return;
					if(elapsed.count() > 1E-1)
					{
						clock_tag = system_clock::now();
						return;
					}
					//ROS_INFO_STREAM("elapsed " << elapsed.count() << " / average : " << average_rate);
					update_average(elapsed.count());
					for(int i = 0; i < message->mesures.size(); ++i)
					{
						position_msg position = message->mesures[i];
						position_msg position_previous = pointcloud_previous.back().mesures[i];
						if(find(pointcloud_segments.begin(), pointcloud_segments.end(), position.name) != pointcloud_segments.end())
						{
							double distance = Utils::distance(position.position.x - position_previous.position.x, position.position.z - position_previous.position.z);
							distance = (distance > MOTION_DISTANCE_TRESHOLD) ? distance : 0;
							if(sum_segments.count(position.name) == 0) sum_segments[position.name] = 0;
							sum_segments[position.name] += distance;
							message_motion.names.push_back(position.name);
							message_motion.values.push_back(distance);
							message_motion.sums.push_back(sum_segments[position.name]);

							message_motion.velocity.push_back(norm_derivative(i));
							if(position.name == JOINT_NAME_COM)
							{
								pos_com[0] = position.position.x;
								pos_com[1] = position.position.y;
								pos_com[2] = position.position.z;
								pos_com[3] = norm_derivative(i);
							}
						}

					}
					clock_tag = system_clock::now();

	    		stringstream ss;
					ss << "{\"x\":" << pos_com[0] << ",\"y\":" << pos_com[1] << ",\"z\":" << pos_com[2] << ",\"speed\":" << pos_com[3] <<"}";
					string str = ss.str();
					_redox.set(JOINT_NAME_COM, str);

					publisher_send(ROS_TOPIC_INDICE_MOTION, message_motion);
				}
				else
				{
					is_init = true;
					clock_tag = system_clock::now();
				}

				if(pointcloud_previous.size() == 6) pointcloud_previous.pop_front();
				pointcloud_previous.push_back(*message);
			}

			void update_average(double elapsed_time)
			{
				average_rate = ((average_rate * cpt_average) + (1/elapsed_time)) / (cpt_average + 1);
				cpt_average++;
			}

			vector<double> pos_to_vector(int index_segment, int index_axe)
			{
				vector<double> res;
				for(deque<mesureKinect>::iterator it = pointcloud_previous.begin(); it != pointcloud_previous.end(); it++)
				{
					switch(index_axe){
						case 0 :
							res.push_back(it->mesures[index_segment].position.x);
							break;
						case 1 :
							res.push_back(it->mesures[index_segment].position.y);
							break;
						case 2 :
							res.push_back(it->mesures[index_segment].position.z);
							break;
					}
				}
				return res;
			}

			double derivative_vector(vector<double> vectors)
			{
				double res;
				int s = vectors.size();

				if(s <= 1)
				{
					res = 0;
				}
				else if(s == 2)
				{
					res = ((1*vectors[s-1])+((-1)*vectors[s-2]))*average_rate;
				}
				else if(s == 3)
				{
					res = (((3.0f/2.0f) * vectors[s-1]) + ((-2.0f) * vectors[s-2]) + ((1.0f/2.0f) * vectors[s-3])) * average_rate;
				}
				else if(s == 4)
				{
					res = (((11.0f / 6.0f) * vectors[s-1]) + ((-3.0f) * vectors[s-2]) + ((3.0f / 2.0f) * vectors[s-3]) + ((-1.0f/3.0f) * vectors[s-4])) * average_rate;
				}
				else if(s == 5)
				{
					res = (((25.0f / 12.0f) * vectors[s-1]) + ((-4.0f) * vectors[s-2]) + (3.0f * vectors[s-3]) + ((-4.0f / 3.0f) * vectors[s-4]) + ((1.0f / 4.0f) * vectors[s-5])) * average_rate;
				}
				else if(s == 6)
				{
					res = (((137.0f / 60.0f) * vectors[s-1]) + ((-5.0f) * vectors[s-2]) + (5.0f * vectors[s-3]) + ((-10.0f / 3.0f) * vectors[s-4]) + ((5.0f / 4.0f) * vectors[s-5]) + ((-1.0f / 5.0f) * vectors[s-6])) * average_rate;
				}
				else if(s >= 7)
				{
					res = (((49.0f / 20.0f) * vectors[s-1]) + ((-6.0f) * vectors[s-2]) + ((15.0f / 2.0f) * vectors[s-3]) + ((-20.0f / 3.0f) * vectors[s-4]) + ((15.0f / 4.0f) * vectors[s-5]) + ((-6.0f / 5.0f) * vectors[s-6]) + ((1.0f / 6.0f) * vectors[s-7])) * average_rate;
				}

				return res;
			}

			double norm_derivative(int index_segment)
			{
				double velocity_x = derivative_vector(pos_to_vector(index_segment, 0));
				double velocity_y = derivative_vector(pos_to_vector(index_segment, 1));
				double velocity_z = derivative_vector(pos_to_vector(index_segment, 2));
				double res;
				res = velocity_x*velocity_x + velocity_y*velocity_y + velocity_z*velocity_z;
				return sqrt(res);
			}

		public:
			EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
			NodeGetMotion(int argc, char** argv):
			Node(ROS_NODE_GET_MOTION_NAME, ROS_NODE_GET_MOTION_RATE, argc, argv)
			{
				subscriber_add<control_server_cmd>(ROS_TOPIC_RECORD, &NodeGetMotion::server_toggle, 0);
				subscriber_add<mesureKinect>(ROS_TOPIC_PC_CLASSIFIED_COM, &NodeGetMotion::get_indice, 0);

				publisher_add<motion>(ROS_TOPIC_INDICE_MOTION, 0);
			}

			void on_start()
			{
				redox_init(REDOX_HOST, REDOX_PORT);

			  pointcloud_segments =
				{
					JOINT_NAME_KNEE_RIGHT,
					JOINT_NAME_KNEE_LEFT,
					JOINT_NAME_HEAD,
					JOINT_NAME_SPINEBASE,
					JOINT_NAME_COM
				};
			}

			void on_update()
			{

			}

			void on_end()
			{
				_redox.disconnect();
			}

			void action_on_toggle()
			{
				resetDistances();
			}
	};
}

int main(int argc, char** argv)
{
	return (prog_kinect::NodeGetMotion(argc, argv)).run();
}
