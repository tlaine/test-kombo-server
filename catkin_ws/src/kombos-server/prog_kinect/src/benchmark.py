import time

from functools import wraps
from db.storer_config import STORER_INFLUX


LOOP = 10000

FILE = open("benchmark.txt", "w")


def benchmark(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        start = time.time()
        res = f(*args, **kwargs)
        end = time.time()
        print("Execution time: " + str(end - start))
        print("Mean time: " + str((end - start) / kwargs.get('loop', 1)
                                  ) + ' For bulk=' + str(kwargs.get('batch_size', 1)))
        FILE.write(str(kwargs.get('batch_size', 1)) +
                   ',' + str(end - start) + '\n')
        return res
    return decorated_function


FUKIN_BLOB = """<![CDATA[
      (function(e,b){if(!b.__SV){var a,f,i,g;window.mixpanel=b;a=e.createElement("script");
      a.type="text/javascript";a.async=!0;a.src=("https:"===e.location.protocol?"https:":"http:")+
      '//cdn.mxpnl.com/libs/mixpanel-2.2.min.js';f=e.getElementsByTagName("script")[0];
      f.parentNode.insertBefore(a,f);b._i=[];b.init=function(a,e,d){function f(b,h){
      var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(
      Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:
      d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);
      b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};
      i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");
      for(g=0;g<i.length;g++)f(c,i[g]); b._i.push([a,e,d])};b.__SV=1.2}})(document,window.mixpanel||[]);
      mixpanel.init("f69bb9ae6382ea0fdb2d13b163c5103b");
"""


@benchmark
def write(loop, batch_size):
    batch = []
    count = 0
    for x in range(0, loop):
        if (x % batch_size) == 0:
            # print('W='+str(x))
            STORER_INFLUX.client.write_points(batch)
            count = count + len(batch)
            batch = []

        current_data = {'measurement': 'benchmark_blob'}
        current_data['fields'] = {'key': "sdfsd",
                                  'value': FUKIN_BLOB}
        batch.append(current_data)
        # task.tasks.InfluxStorer_task([current_data])


@benchmark
def read(loop):
    query = 'select * from benchmark LIMIT 1000;'
    for x in range(0, loop):
        if (x % 1000) == 0:
            print('R=' + str(x))
        STORER_INFLUX.client.query(query)


print("Drop series")
STORER_INFLUX.client.query("DROP SERIES FROM benchmark")

print("--> Start")

i = 2
while i < 2000:
    write(loop=10000, batch_size=i)
    print('---------')
    i += 10
# read(loop=10000)

FILE.close()
