#include "../include/json2model.h"

using namespace std;
using namespace ros;
using namespace Eigen;

namespace prog_kinect
{
  json2model::json2model(bool is_default)
  {
    nbJointsHaut = 0;
    nbJointsBas = 0;
    nbStatesHaut = 0;
    nbStatesBas = 0;
    nbStatesAngle = 0;
    nbStatesLength = 0;
    nbStatesBase = 0;
    nbStatesAngleVel = 0;
    nbStatesBaseVel = 0;

    if(is_default)
    {
      basDuCorps = true;
      vitesse = false;
      kinect2 = true;
    }

    string path = string(package::getPath(PATH_JSON_PACKAGE)) + string(PATH_JSON_FILENAME);
    JsonReader::read_at(path, [&](JsonReader* root)
    {
      if(!is_default)
      {
        basDuCorps = root->at("Configuration")->at("basDuCorps")->at("activate")->get_bool();
        vitesse = root->at("Configuration")->at("vitesse")->at("activate")->get_bool();
        kinect2 = root->at("Configuration")->at("kinect2")->at("activate")->get_bool();
      }

      root->at("Joints")->foreach([&](const string& joint_name, JsonReader* joint)
      {
        jointsOrdered.push_back(joint_name);

        jointInvert[joint_name] = joint->at("inverse")->get_string();

        jointConf[joint_name] = joint->at("conf")->get_string();
        jointConf[joint_name] == "haut" ? nbJointsHaut++ : nbJointsBas++;

        jointsIdx[joint_name] = joint->at("index")->get_int();
        idxJoints[jointsIdx[joint_name]] = joint_name;
      });

      root->at("States")->foreach([&](const string& state_name, JsonReader* state)
      {
        statesOrdered.push_back(state_name);

        string type = state->at("type")->get_string();
        if(type == "position" || type == "orientation")
        {
          nbStatesBase++;
        }
        else if(type == "positionVel" || type == "orientationVel")
        {
          nbStatesBaseVel++;
        }
        else if (type == "angle")
        {
          nbStatesAngle++;
        }
        else if (type == "angleVel")
        {
          nbStatesAngleVel++;
        }
        else if (type == "length")
        {
          nbStatesLength++;
        }
        statesType[state_name] = type;

        statesConf[state_name] = state->at("conf")->get_string();
        statesConf[state_name] == "haut" ? nbStatesHaut++ : nbStatesBas++;

        statesLimitMin[state_name] = (float)state->at("limitMin")->get_double();
        statesLimitMax[state_name] = (float)state->at("limitMax")->get_double();
      });

      root->at("Joints2States")->foreach([&](const string& joint_name, JsonReader* joint)
      {
        joint->at("list")->foreach([&](const string& index, JsonReader* joint_state)
        {
          jointsToStates[joint_name].push_back(joint_state->get_string());
        });
      });

      root->at("CentreMassLimb")->foreach([&](const string& limb_name, JsonReader* state)
      {
        limbOrdered.push_back(limb_name);

        limbLength[limb_name] = state->at("length")->get_string();
        limbProximal[limb_name] = state->at("pos_proximal")->get_string();
        limbDistal[limb_name] = state->at("pos_distal")->get_string();
        limbCoefProximal[limb_name] = (float)state->at("coef_proximal")->get_double();
        limbPercentWeight[limb_name] = (float)state->at("percent_weight")->get_double();
        limbDensity[limb_name] = (float)state->at("density")->get_double(); // in g.cm-3
        limbCircumference[limb_name] = (float)state->at("circumference")->get_double() * 1E-3; // in mm, return m
      });

    });
  }

  bool json2model::GetBasDuCorps() const{
    return basDuCorps;
  }

  bool json2model::GetVitesse() const{
    return vitesse;
  }

  bool json2model::GetKinect2() const{
    return kinect2;
  }

  std::vector<std::string> json2model::GetJoints() const{
    return jointsOrdered;
  }

  std::map<std::string,int> json2model::GetJointIdx() const{
    return jointsIdx;
  }

  map<int, string> json2model::GetIdxJoint() const{
    return idxJoints;
  }

  std::map<std::string, std::string> json2model::GetInvertJoints() const{
    return jointInvert;
  }

  std::vector<std::string> json2model::GetAllStates() const{
    return statesOrdered;
  }

  vector<std::string> json2model::GetStates() const
  {
    vector<string> result;

    int i = 0;
    for(const auto& it : statesOrdered)
    {
      string state = it;
      string state_type = statesType.at(state);
      string state_conf = statesConf.at(state);
      bool is_vel = !vitesse && (state_type == "positionVel" || state_type == "orientationVel" || state_type == "angleVel");
      bool is_bottom = !basDuCorps && state_conf == "bas";
      if(!is_vel && !is_bottom)
      {
        result.push_back(state);
        i++;
      }
    }

    return result;
  }

  std::map<std::string,std::string> json2model::GetStatesType() const{
    return statesType;
  }

  std::vector<std::string> json2model::GetLength() const{
    std::vector<std::string> length;

	for(const auto& it : statesOrdered)
    {
      if(!basDuCorps && statesConf.at(it)=="bas"){    }
      else if(statesType.at(it)=="length"){
        length.push_back(it);
      }
    }

    return length;
  }

vector<string> json2model::GetAngle() const
{
	vector<string> angle;
    for(const auto& it : statesOrdered)
	{
		string state = it;
		if(statesType.at(state) == "angle" && (basDuCorps || statesConf.at(state) != "bas"))
		{
			angle.push_back(state);
		}
	}
	return angle;
}


  Vector2i json2model::GetSize() const
  {
    int width = GetStates().size();
    int height = basDuCorps ? (nbJointsHaut+nbJointsBas)*3 : nbJointsHaut*3;
    if(vitesse)
    {
      height = height * 2;
    }

    return Vector2i(width, height);
  }

  int json2model::GetNbLength() const
  {
    return GetLength().size();
  }

  int json2model::GetNbAngle() const
  {
    std::vector<std::string> angle = GetAngle();
    return angle.size();
  }

  std::map<std::string, std::vector<std::string>> json2model::GetJointsToStates() const{
    return jointsToStates;
  }

  std::map<std::string,float> json2model::GetLengthLimitMinMax(bool min) const {
    std::map<std::string,float> res;
    std::vector<std::string> lengthOrdered = GetLength();

    std::map<std::string,float> limit;
    if(min){
      limit = statesLimitMin;
    }
    else{
      limit = statesLimitMax;
    }

    for(const auto& it : lengthOrdered)
    {
      res[it] = limit[it];
    }

    return res;
  }

  std::map<std::string,float> json2model::GetAngleLimitMinMax(bool min) const
  {
    float toRad = M_PI/180.0;

    std::map<std::string,float> res;
    std::vector<std::string> angleOrdered = GetAngle();

    std::map<std::string,float> limit;
    if(min){
      limit = statesLimitMin;
    }
    else{
      limit = statesLimitMax;
    }

    for(const auto& it : angleOrdered)
    {
      res[it] = limit[it] * toRad;
    }

    return res;
  }

  std::vector<std::string> json2model::GetLimb() const
  {
    return limbOrdered;
  }

  std::map<std::string,std::string> json2model::GetLimbLength() const
  {
    return limbLength;
  }

  std::map<std::string,std::string> json2model::GetLimbProximal() const
  {
    return limbProximal;
  }

  std::map<std::string,std::string> json2model::GetLimbDistal() const
  {
    return limbDistal;
  }

  std::map<std::string, float> json2model::GetLimbCoefProximal() const
  {
    return limbCoefProximal;
  }

  std::map<std::string, float> json2model::GetLimbPercentWeight() const
  {
    return limbPercentWeight;
  }

  std::map<std::string, float> json2model::GetLimbDensity() const
  {
    return limbDensity;
  }

  std::map<std::string, float> json2model::GetLimbCircumference() const
  {
    return limbCircumference;
  }

  std::map<string, pair<string,string>> json2model::GetLimbExtremity() const
  {
    map<string, pair<string,string>> limbextrem;

    for(const auto& it : limbOrdered)
    {
      string limb_name = it;
      limbextrem[limb_name] = {limbProximal.at(limb_name), limbDistal.at(limb_name)};
    }

    return limbextrem;
  }

}
