#include "../include/FilterEKF.hpp"
#include <chrono>

using namespace std;
using namespace Eigen;
using namespace std::chrono;

namespace prog_kinect
{
	FilterEKF::FilterEKF(const json2model& model)
	{
		has_bottom = model.GetBasDuCorps();
		has_speeds = model.GetVitesse();

		Vector2i size = model.GetSize();
		states_names = model.GetStates();
		states_count = size.x();

		measures_count = size.y();

		angles = model.GetAngle();
		angles_count = 2 * model.GetNbAngle();

		joints_limits = MatrixXf::Zero(angles_count, states_count);
		states_limits = VectorXf::Zero(angles_count);
		states_length_index = VectorXf::Zero(model.GetNbLength());

		int states_found = 0;
		int state_indices =0;
		int id_length = 0;
		map<string, string> states_type = model.GetStatesType();
		map<string,float> states_limit_min = model.GetAngleLimitMinMax(true);
		map<string,float> states_limit_max = model.GetAngleLimitMinMax(false);
		for(int i = 0; i < states_names.size(); ++i)
		{
			string state_name = states_names.at(i);
			if(states_type[state_name] == "angle")
			{
				int state_index = states_found * 2;
				joints_limits(state_index , i) = JOINT_LIMIT_MAX;
				joints_limits(state_index + 1, i) = JOINT_LIMIT_MIN;
				states_limits(state_index) = states_limit_max[state_name];
				states_limits(state_index + 1) = -states_limit_min[state_name];

				states_found++;
			}
			else if(states_type[state_name] == "length")
			{
				states_length_index(id_length) = state_indices;
				id_length++;
			}
			state_indices++;
		}
		reset(VectorXf::Zero(states_count), MatrixXf::Identity(measures_count, measures_count).sparseView(), MatrixXf::Zero(states_count, states_count).sparseView(), MatrixXf::Identity(states_count, states_count));

		//ROS_INFO(MESSAGE_START, states_count, measures_count, model.GetNbLength(), angles_count);
	}

	FilterEKF::~FilterEKF()
	{

	}

	VectorXf FilterEKF::get_states()
	{
		return states;
	}

	VectorXf FilterEKF::get_measures()
	{
		return measures;
	}

	void FilterEKF::set_r(const MatrixXf& r)
	{
		R = r.sparseView();
	}

	void FilterEKF::config_f(const MatrixXf& f)
	{
		F = f.sparseView();;
		Ft = f.transpose().sparseView();
	}

	void FilterEKF::config_h(map<string, MatrixXf> _RJ)
	{
		//33 = PAS DE VITESSE + PAS DE BAS DU CORPS
		//46 = PAS DE VITESSE + BAS DU CORPS
		//59 = VITESSE + PAS DE BAS DU CORPS
		//80 = VITESSE + BAS DU CORPS
		VectorXf vector_default = Utils::vector_sub(states, 0, STATE_COUNT_NOBOTTOM_NOSPEEDS);
		VectorXf vector_has_bottom = has_bottom ? Utils::vector_sub(states, STATE_COUNT_NOBOTTOM_NOSPEEDS, STATE_COUNT_BOTTOM_NOSPEEDS) : VectorXf::Zero(STATE_COUNT_BOTTOM_NOSPEEDS - STATE_COUNT_NOBOTTOM_NOSPEEDS);
		VectorXf vector_has_speeds = has_speeds ? Utils::vector_sub(states, STATE_COUNT_BOTTOM_NOSPEEDS, STATE_COUNT_NOBOTTOM_SPEEDS) : VectorXf::Zero(STATE_COUNT_NOBOTTOM_SPEEDS - STATE_COUNT_BOTTOM_NOSPEEDS);
		VectorXf vector_has_both = (has_bottom && has_speeds) ? Utils::vector_sub(states, STATE_COUNT_NOBOTTOM_SPEEDS, STATE_COUNT_BOTTOM_SPEEDS) : VectorXf::Zero(STATE_COUNT_BOTTOM_SPEEDS - STATE_COUNT_NOBOTTOM_SPEEDS);
		VectorXf vector = Utils::vector_join(Utils::vector_join(vector_default, vector_has_bottom), Utils::vector_join(vector_has_speeds, vector_has_both));

		MatrixXf h = Utils::jacobian(states_count, measures_count, !has_bottom && has_speeds, vector);
		if(!_RJ.empty())
		{
			/*MatrixXf H_RJ = MatrixXf::Zero(measures_count, states_count);
			for(map<string, MatrixXf>::iterator it = _RJ.begin(); it != _RJ.end(); ++it)
			{
				H_RJ += it->second * H;
			}
			H = (R * H_RJ).sparseView();
			Ht = H.transpose();
			*/
			MatrixXf h_RJ = MatrixXf::Zero(measures_count, states_count);
			for(map<string, MatrixXf>::iterator it = _RJ.begin(); it != _RJ.end(); ++it)
			{
				h_RJ += it->second*h;
			}
			h = R * h_RJ;
		}

		H = h.sparseView();
		Ht = h.transpose().sparseView();
		measures = Utils::jacobian_function(measures_count, !has_bottom && has_speeds, vector);
	}

	void FilterEKF::reset(const VectorXf& value, const MatrixXf& r, const MatrixXf& q, const MatrixXf& f)
	{
		states = value;
		measures = VectorXf::Zero(measures_count);

		P = MatrixXf::Identity(states_count, states_count).sparseView();
		Q = q.sparseView();
		R = r.sparseView();

		config_f(f);
		config_h();
	}

	void FilterEKF::update(const VectorXf& y, map<string, MatrixXf> _RJ)
	{
		config_h(_RJ);

		//ROS_WARN_STREAM("K : " << K);
		/*ROS_WARN_STREAM("P : " << P.nonZeros() );
		ROS_WARN_STREAM("Q : " << Q.nonZeros() );
		ROS_WARN_STREAM("R : " << R.nonZeros() );
		ROS_WARN_STREAM("H : " << H.nonZeros() );*/

		SparseMatrix<float> K = P * Ht * (SparseMatrix<float>)inverse(H * P * Ht + R).sparseView();
		//ROS_WARN_STREAM("K : " << K.nonZeros() );
		states += K * (y - measures);

		SparseMatrix<float> I = MatrixXf::Identity(states_count, states_count).sparseView();
		P = ((I - K * H) * P);

		for(int i = 0; i < states_length_index.size() ; i++)
		{
			states[states_length_index(i)] = abs(states[states_length_index(i)]);
		}
	}

	void FilterEKF::predict()
	{
		states = F * states;
		P = (F * P * Ft + Q);
	}

	void FilterEKF::constraints()
	{
		states_constraints.clear();

		for(int i = 0; i < angles_count; ++i)
		{
			float dx = joints_limits.row(i) * states;
			if(dx > states_limits(i))
			{
				VectorXf d_row_transposed = joints_limits.row(i).transpose();
				states -= d_row_transposed * (1.0 / (joints_limits.row(i) * d_row_transposed)) * (dx - states_limits(i));
				states_constraints.push_back(angles[floor(i / 2.0)]);
			}
		}
	}

	MatrixXf FilterEKF::inverse(const MatrixXf& m)
	{
		//ROS_WARN_STREAM("size of inverse matrix : " << m.size());
		//time_point<system_clock> t1 = system_clock::now();
		int p = floor(measures_count / 2.0);
		int q = ceil(measures_count / 2.0);
		MatrixXf M1(measures_count, measures_count);
		MatrixXf M2(measures_count, measures_count);
		MatrixXf M3(measures_count, measures_count);
		MatrixXf Ip = MatrixXf::Identity(p, p);
		MatrixXf Iq = MatrixXf::Identity(q, q);
		MatrixXf Zqp = MatrixXf::Zero(q, p);
		MatrixXf Zpq = MatrixXf::Zero(p, q);
		MatrixXf A = m.block(0, 0, p, p);
		MatrixXf B = m.block(0, p, p, q);
		MatrixXf C = m.block(p, 0, q, p);
		MatrixXf D = m.block(p, p, q, q);

		MatrixXf bTmp1, bTmp2;
		FullPivLU<MatrixXf> flu(D);
		if(flu.isInvertible())
		{
			PartialPivLU<MatrixXf> lu(D);
			bTmp1 = lu.inverse();
		}
		else
		{
		 	bTmp1 = flu.inverse();
		}

		FullPivLU<MatrixXf> flu2(A - B * bTmp1 * C);
		if(flu2.isInvertible())
		{
			PartialPivLU<MatrixXf> lu2(A - B * bTmp1 * C);
			bTmp2 = lu2.inverse();
		}
		else
		{
		 	bTmp2 = flu2.inverse();
		}

		M1 << Ip, Zpq, -bTmp1 * C, Iq;
		M2 << bTmp2, Zpq, Zqp, bTmp1;
		M3 << Ip, -B * bTmp1, Zqp, Iq;

		//time_point<system_clock> t2 = system_clock::now();
		//ROS_ERROR_STREAM("Time elapsed : " << std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count());

		return  M1 * M2 * M3;
	}
}
