#!/usr/bin/env python
# pylint: disable=ungrouped-imports

import serial
import rospy
import random
from math import *
from kombos_msgs.msg import sound
from utils.watcher import WATCHER

ROS_NODE_SOUND = "sound"
ROS_TOPIC_SOUND = "/sound"
MESSAGE_START = "[TEMPS] STARTING TEMPS"
Serial_port = '/dev/ttyACM0'
baud_rate = 9600
# ser = serial.Serial(Serial_port, baud_rate)


def get_sound():
    Soun = Sound()
    test_sound = random.radint(0, 1023)
    # line = ser.readline()
    # line = line.decode('utf-8').strip()
    # try:
    Soun.indice = round(20 * log10(test_sound + 1))
    print("---------------++ sound")
    print(Soun.indice)
    print("---------------++ sound")
    # # except ValueError:
    #     print("Can't convert to float: " + line)
    #     temp.temperature_value = 0
    # # except Exception as e:
    #     print("Can't handle str " + str(e))
    #     temp.temperature_value = 0
    return Soun.indice


def callback2(data):
    rospy.loginfo(rospy.get_caller_id() + "The Sound in dB is %s", data.data)


def talker_sound():
    rospy.init_node(ROS_NODE_SOUND, anonymous=False, log_level=rospy.DEBUG)
    pub = rospy.Publisher(ROS_TOPIC_SOUND, Sound, queue_size=10)
    WATCHER.watch_log()
    rate = rospy.Rate(1)  # 1hz
    while not rospy.is_shutdown():
        temp_sound = get_sound()

        rospy.loginfo(temp_sound)
        pub.publish(temp_sound)
        rate.sleep()


if __name__ == '__main__':
    try:

        talker_sound()

    except rospy.ROSInterruptException:
        pass
