#!/usr/bin/env python
# coding: utf8
# pylint: disable=ungrouped-imports

import sys
import datetime

from server.extensions import celery
from celery.schedules import crontab
from server.extensions import db
from server.app import create_app
from utils import session_stop

app = celery  # pylint: disable=invalid-name
app.init_app(create_app())


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(
        crontab(),
        close_unended_session.s(),
    )


@app.task
def close_unended_session():
    from common.db.models import Session
    from server.getter import get_indice_mean, get_distance, get_sound_mean

    sessions = Session.query.filter_by(end_at=None).all()
    for s in sessions:
        if s.start_at < datetime.datetime.utcnow() - datetime.timedelta(seconds=20):
            indice = get_indice_mean(s.uniqid)
            noise = get_sound_mean(s.uniqid)
            distance = get_distance(s.uniqid)
            session_stop(s, indice, distance, noise)
            db.session.commit()


def main(argv):
    app.control.purge()
    print('=== POOL START ===')

    app.start(argv=['celery', 'worker', '-B', '--loglevel=info'])

    print('=== POOL STOP ===')


if __name__ == '__main__':
    main(sys.argv[1:])
