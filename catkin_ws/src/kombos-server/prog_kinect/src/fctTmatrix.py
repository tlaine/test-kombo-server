# pylint: disable-all
import sympy as sy
import numpy as np
import math

# retourne la matrice de passage correspondant a une rotation autour de l
# axe x d'angle o


def matrice_de_passage_rotationx(o):
    return sy.Matrix([[1, 0, 0, 0], [0, sy.cos(o), -sy.sin(o), 0],
                      [0, sy.sin(o), sy.cos(o), 0], [0, 0, 0, 1]])

# retourne la matrice de passage correspondant a une rotation autour de l
# axe y d'angle o


def matrice_de_passage_rotationy(o):
    return sy.Matrix([[sy.cos(o), 0, sy.sin(o), 0], [0, 1, 0, 0],
                      [-sy.sin(o), 0, sy.cos(o), 0], [0, 0, 0, 1]])

# retourne la matrice de passage correspondant a une rotation autour de l
# axe z d'angle o


def matrice_de_passage_rotationz(o):
    return sy.Matrix([[sy.cos(o), -sy.sin(o), 0, 0],
                      [sy.sin(o), sy.cos(o), 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])

# retourne la matrice de passage correspondant a une translation de x, y et z


def matrice_de_passage_translation(x, y, z):
    return sy.Matrix([[1, 0, 0, x], [0, 1, 0, y], [0, 0, 1, z], [0, 0, 0, 1]])

# retourne le point P de la matrice de passage


def compute_p(m):
    return m[0:3, 4]

# retourne la matrice de rotation de la matrice de passage


def compute_r(m):
    return m[0:3, 0:3]

# retourne la norme au carre du vecteur V


def norme_carre(v):
    return v[0] * v[0] + v[1] * v[1] + v[2] * v[2]

# retourne la norme de v


def norme(v):
    return math.sqrt(norme_carre(v))


def compute_f(m, o):
    f = m * o
    return f

# imprime les equations donnees dans un Latex


def printLatex(exp):
    txt = r"\documentclass{article}\n\usepackage[utf8]{inputenc}\n\usepackage{amsmath}\n\usepackage[margin=2cm,landscape,a3paper]{geometry}\n\usepackage{graphicx}\n\\begin{document}\n\\begin{equation}\n" + sy.latex(exp, mul_symbol='dot') + "\n\end{equation}\n\end{document}"
    with open("./printLatex.tex", "w") as text_file:
        text_file.write(txt)


def print_matrixt(M):
    length = np.shape(M)
    for i in range(0, length[0]):
        for j in range(0, length[1]):
            print
            s = 't' + repr(i) + '_' + repr(j) + ' = '
            print(s)
            print(M[i, j])
            print('')
