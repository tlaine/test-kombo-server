"""ControlServer Script

Usage:
  control_server.py [port <port>]

Options:
  -h --help
"""
from server import app
from server.control_server_config import set_server_state

set_server_state()

app = app.create_app()  # pylint: disable-all

if __name__ == '__main__':
    pass
# arguments = docopt(__doc__, version='0.1')
# port = 9001
# if arguments.get('port'):
#     port = arguments.get('<port>')
# app.run(threaded=True, host='0.0.0.0', port=int(port))
