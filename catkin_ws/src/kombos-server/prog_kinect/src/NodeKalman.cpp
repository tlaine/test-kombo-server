#include <Eigen/Core>
#include <Eigen/Geometry>
#include <kombos_msgs/matrix.h>
#include <kombos_msgs/mesureKinect.h>
#include <kombos_msgs/mesureKalman.h>
#include <kombos_msgs/kalmanState.h>
#include <kombos_msgs/control_server_cmd.h>
#include "../include/Network.hpp"
#include "../include/Utils.hpp"
#include "../include/json2model.h"
#include "../include/FilterEKF.hpp"
#include "../include/Node.hpp"

using namespace std;
using namespace std::chrono;
using namespace Eigen;
using namespace kombos_msgs;

namespace prog_kinect
{
	class NodeKalman : public Node
	{
		private:
			bool is_init = false;
			map<string, Vector3f> joints_positions;
			map<string, Vector3f> joints_speeds;
			map<string, bool> joints_fiabilites;
			kalmanState state;
			time_point<system_clock> time_last;
			FilterEKF* filter;

			mesureKalman get_pointcloud(const VectorXf& h, const map<string, Vector3f>& joints, const vector<matrix>& matrices)
			{
				mesureKalman message_kalman;
				message_kalman.matrices = matrices;
				message_kalman.data.typeCapteur = "Kalman";
				message_kalman.data.idCapteur = 1;
				message_kalman.data.kalman_state = state;

				for(map<string, int>::iterator it = joints_indices.begin(); it != joints_indices.end(); ++it)
				{
					int index = it->second * 3;
					float x = h[index + 0];
					float y = h[index + 1];
					float z = h[index + 2];
					float dotx = joints_speeds[it->first][0];
					float doty = joints_speeds[it->first][1];
					float dotz = joints_speeds[it->first][2];


					if(model_basducorps || (!model_basducorps && it->second < DEFAULT_NUMBER_POINTS))
					{
						message_kalman.data.mesures.push_back(Utils::build_message_position(it->first, x, y, z, joints_fiabilites[it->first]));
						message_kalman.data.vitesses.push_back(Utils::build_message_position(it->first, dotx, doty, dotz, joints_fiabilites[it->first]));
					}

					if(it->first == JOINT_NAME_SPINESHOULDER)
					{
						message_kalman.data.mesures.push_back(get_pointcloud_point(joints, JOINT_NAME_NECK, JOINT_NAME_SPINESHOULDER, x, y, z));
						message_kalman.data.vitesses.push_back(Utils::build_message_position(JOINT_NAME_NECK, dotx, doty, dotz, joints_fiabilites[JOINT_NAME_NECK]));
					}
					else if(it->first == JOINT_NAME_HAND_RIGHT)
					{
						message_kalman.data.mesures.push_back(get_pointcloud_point(joints, JOINT_NAME_HANDTIP_RIGHT, JOINT_NAME_HAND_RIGHT, x, y, z));
						message_kalman.data.mesures.push_back(get_pointcloud_point(joints, JOINT_NAME_THUMB_RIGHT, JOINT_NAME_HAND_RIGHT, x, y, z));
						message_kalman.data.vitesses.push_back(Utils::build_message_position(JOINT_NAME_HANDTIP_RIGHT, dotx, doty, dotz, joints_fiabilites[JOINT_NAME_HANDTIP_RIGHT]));
						message_kalman.data.vitesses.push_back(Utils::build_message_position(JOINT_NAME_THUMB_RIGHT, dotx, doty, dotz, joints_fiabilites[JOINT_NAME_THUMB_RIGHT]));
					}
					else if(it->first == JOINT_NAME_HAND_LEFT)
					{
						message_kalman.data.mesures.push_back(get_pointcloud_point(joints, JOINT_NAME_HANDTIP_LEFT, JOINT_NAME_HAND_LEFT, x, y, z));
						message_kalman.data.mesures.push_back(get_pointcloud_point(joints, JOINT_NAME_THUMB_LEFT, JOINT_NAME_HAND_LEFT, x, y, z));
						message_kalman.data.vitesses.push_back(Utils::build_message_position(JOINT_NAME_HANDTIP_LEFT, dotx, doty, dotz, joints_fiabilites[JOINT_NAME_HANDTIP_LEFT]));
						message_kalman.data.vitesses.push_back(Utils::build_message_position(JOINT_NAME_THUMB_LEFT, dotx, doty, dotz, joints_fiabilites[JOINT_NAME_THUMB_LEFT]));
					}

					if(model_basducorps)
					{
						if(it->first == JOINT_NAME_ANKLE_RIGHT)
						{
							message_kalman.data.mesures.push_back(get_pointcloud_point(joints, JOINT_NAME_FOOT_RIGHT, JOINT_NAME_ANKLE_RIGHT, x, y, z));
						message_kalman.data.vitesses.push_back(Utils::build_message_position(JOINT_NAME_FOOT_RIGHT, dotx, doty, dotz, joints_fiabilites[JOINT_NAME_FOOT_RIGHT]));
						}
						else if(it->first == JOINT_NAME_ANKLE_LEFT)
						{
							message_kalman.data.mesures.push_back(get_pointcloud_point(joints, JOINT_NAME_FOOT_LEFT, JOINT_NAME_ANKLE_LEFT, x, y, z));
						message_kalman.data.vitesses.push_back(Utils::build_message_position(JOINT_NAME_FOOT_LEFT, dotx, doty, dotz, joints_fiabilites[JOINT_NAME_FOOT_LEFT]));
						}
					}
					else if(it->first == JOINT_NAME_SPINEBASE)
					{
						message_kalman.data.mesures.push_back(get_pointcloud_point(joints, JOINT_NAME_HIP_RIGHT, JOINT_NAME_SPINEBASE, x, y, z));
						message_kalman.data.mesures.push_back(get_pointcloud_point(joints, JOINT_NAME_KNEE_RIGHT, JOINT_NAME_SPINEBASE, x, y, z));
						message_kalman.data.mesures.push_back(get_pointcloud_point(joints, JOINT_NAME_ANKLE_RIGHT, JOINT_NAME_SPINEBASE, x, y, z));
						message_kalman.data.mesures.push_back(get_pointcloud_point(joints, JOINT_NAME_FOOT_RIGHT, JOINT_NAME_SPINEBASE, x, y, z));
						message_kalman.data.mesures.push_back(get_pointcloud_point(joints, JOINT_NAME_HIP_LEFT, JOINT_NAME_SPINEBASE, x, y, z));
						message_kalman.data.mesures.push_back(get_pointcloud_point(joints, JOINT_NAME_KNEE_LEFT, JOINT_NAME_SPINEBASE, x, y, z));
						message_kalman.data.mesures.push_back(get_pointcloud_point(joints, JOINT_NAME_ANKLE_LEFT, JOINT_NAME_SPINEBASE, x, y, z));
						message_kalman.data.mesures.push_back(get_pointcloud_point(joints, JOINT_NAME_FOOT_LEFT, JOINT_NAME_SPINEBASE, x, y, z));
						message_kalman.data.vitesses.push_back(Utils::build_message_position(JOINT_NAME_HIP_RIGHT, dotx, doty, dotz, joints_fiabilites[JOINT_NAME_HIP_RIGHT]));
						message_kalman.data.vitesses.push_back(Utils::build_message_position(JOINT_NAME_KNEE_RIGHT, dotx, doty, dotz, joints_fiabilites[JOINT_NAME_KNEE_RIGHT]));
						message_kalman.data.vitesses.push_back(Utils::build_message_position(JOINT_NAME_ANKLE_RIGHT, dotx, doty, dotz, joints_fiabilites[JOINT_NAME_ANKLE_RIGHT]));
						message_kalman.data.vitesses.push_back(Utils::build_message_position(JOINT_NAME_FOOT_RIGHT, dotx, doty, dotz, joints_fiabilites[JOINT_NAME_FOOT_RIGHT]));
						message_kalman.data.vitesses.push_back(Utils::build_message_position(JOINT_NAME_HIP_LEFT, dotx, doty, dotz, joints_fiabilites[JOINT_NAME_HIP_LEFT]));
						message_kalman.data.vitesses.push_back(Utils::build_message_position(JOINT_NAME_KNEE_LEFT, dotx, doty, dotz, joints_fiabilites[JOINT_NAME_KNEE_LEFT]));
						message_kalman.data.vitesses.push_back(Utils::build_message_position(JOINT_NAME_ANKLE_LEFT, dotx, doty, dotz, joints_fiabilites[JOINT_NAME_ANKLE_LEFT]));
						message_kalman.data.vitesses.push_back(Utils::build_message_position(JOINT_NAME_FOOT_LEFT, dotx, doty, dotz, joints_fiabilites[JOINT_NAME_FOOT_LEFT]));
					}
				}

				return message_kalman;
			}

			position_msg get_pointcloud_point(const map<string, Vector3f>& joints, const string& joint_name, const string& base_name, const float& x, const float& y, const float& z)
			{
				Vector3f diff = joints.at(joint_name) - joints.at(base_name);
				return Utils::build_message_position(joint_name, x + diff(0), y + diff(1), z + diff(2), joints_fiabilites[joint_name]);
			}

			MatrixXf get_matrix_r(float r, float rv)
			{
				MatrixXf R = MatrixXf::Identity(measures_count, measures_count);
				// Not testing yet :
				// float rx, ry, rz; rx = 0.0015; ry = 0.0031; rz = 0.0008;
				for(int i = 0; i < measures_count; i++)
				{
					if(model_basducorps || (!model_basducorps && i < 36))
					{
						R(i, i) = r;
						/* FOLLOWING NOT TESTING YET */
						/*R(i, i) = rx;
						if(fmod(i+1,2)) R(i, i) = ry;
						if(fmod(i+1,3)) R(i, i) = rz;*/
						/*      NOT TESTING YET      */
					}

					if(model_vitesse && ((model_basducorps && i >= 54) || (!model_basducorps && i >= 36)))
					{
						R(i, i) = rv;
					}
				}
				return R;
			}

			MatrixXf get_matrix_q(float r, float rv, float ql, float qo, float qov)
			{
				MatrixXf Q = MatrixXf::Zero(states_count, states_count);
				for(int i = 0; i < states_count; i++)
				{
					if((i >= 3 && i < 26) || (model_basducorps && i > 32 && i < 41))
					{
						Q(i, i) = qo;
					}
					else if(i < 3)
					{
						Q(i, i) = r;
					}
					else if(model_vitesse && ((!model_basducorps && i > 32) || (model_basducorps && i > 45)))
					{
						Q(i, i) = ((model_basducorps && i < 35)||(!model_basducorps && i<48)) ? rv : qov;
					}
					else
					{
						Q(i, i) = ql;
					}
				}

				return Q;
			}

			MatrixXf get_matrix_f(float dt)
			{
				MatrixXf F = MatrixXf::Identity(states_count, states_count);
				if(model_vitesse)
				{
					int states_count_active = model_basducorps ? 41 : 26;//46 : 33;
					int states_to_begin = 46;
					int col = 0;
					for(int raw = 0; raw < states_count_active; ++raw)
					{
						if(raw < 26 || (model_basducorps && raw > 32 && raw < 41))
						{
							F(raw, states_to_begin + col) = dt;
							col++;
						}
					}
				}

				return F;
			}

			/*void kalman_toggle(const control_server_cmd::ConstPtr& message)
			{
				is_init = (message->cmd != SERVER_COMMAND_START);
				server_is_recording = !is_init;
			}*/

			void kalman_init(const kalmanState::ConstPtr& message)
			{
				//ROS_WARN("------------------ MESSAGE RECEIVE for INIT ---------------------");
				if(!is_init /*&& server_is_recording*/)
				{
					map<string, float> states_init;
					for(int i = 0; i < message->name.size(); ++i)
					{
						states_init[message->name[i]] = message->value[i];
					}

					VectorXf states_q(states_count);
					for(int i = 0; i < states_names.size(); ++i)
					{
						states_q[i] = states_init[states_names.at(i)];
					}

					float r = DEFAULT_VALUE_R;
					float rv = DEFAULT_VALUE_RV;
					float ql = DEFAULT_VALUE_QL;
					float qo = DEFAULT_VALUE_QO;
					float qov = DEFAULT_VALUE_QOV;
					float dt = DEFAULT_VALUE_DT;

					filter->reset(states_q, get_matrix_r(r, rv), get_matrix_q(r, rv, ql, qo, qov), get_matrix_f(dt));
					is_init = true;
					time_last = system_clock::now();

					ROS_INFO(MODEL_MESSAGE_KALMAN_INIT, r, rv, ql, qo, qov, dt);
				}
			}
			void handle_fusion_corrected(const kombos_msgs::mesureKalman::ConstPtr& message)
			{
				//kalman_loop(message);
			}

			void handle_fusion(const kombos_msgs::mesureKalman::ConstPtr& message)
			{
				//ROS_WARN("------------------ MESSAGE RECEIVE ---------------------");
				kalman_loop(message);
			}


			void kalman_loop(const kombos_msgs::mesureKalman::ConstPtr& message)
			{
				if(is_init /*&& server_is_recording*/)
				{
					ROS_INFO_ONCE(MODEL_MESSAGE_KALMAN_LOOP, message->data.mesures.size());

					bool is_valid = true;
					for (int i = 0; i < message->data.mesures.size(); ++i)
					{
						position_msg position = message->data.mesures[i];
						if(is_valid && !isnan(position.position.x) && !isnan(position.position.y) && !isnan(position.position.z))
						{
							joints_positions[position.name] = Vector3f(position.position.x, position.position.y, position.position.z);
							joints_fiabilites[position.name] = position.fiabilite;
							if(message->data.vitesses.size() > 0 && !isnan(message->data.vitesses[i].position.x) && !isnan(message->data.vitesses[i].position.y) && !isnan(message->data.vitesses[i].position.z))
							{
								position_msg speed = message->data.vitesses[i];
								joints_speeds[speed.name] = Vector3f(speed.position.x, speed.position.y, speed.position.z);
								//ROS_WARN_STREAM("pos : x=" << position.position.x << " y=" << position.position.y << " z=" << position.position.z << " / vit : x=" << speed.position.x << " y=" << speed.position.y << " z=" << speed.position.z);
							}
						}
						else
						{
							is_valid = false;
						}
					}

					if(is_valid)
					{
						MatrixXf R = Utils::msg2matrix(message->matrices[0]);
						map<string, MatrixXf> RJ;
						for(int i = 1; i < message->matrices.size(); ++i)
						{
							string name_matrix = message->matrices[i].name;
							if(name_matrix.at(0) == 'r') RJ[to_string(i)] = Utils::msg2matrix(message->matrices[i]);
						}

						time_point<system_clock> now = system_clock::now();
						filter->config_f(get_matrix_f(std::chrono::duration_cast<std::chrono::duration<double>>(time_last - now).count()));//(now - time_last).count()));
						time_last = now;

						filter->set_r(R);

						VectorXf measure(measures_count);

						mesureKinect message_pc;
						message_pc.typeCapteur = "test";
						message_pc.idCapteur = 1;
						for(map<string, int>::iterator it = joints_indices.begin(); it != joints_indices.end(); ++it)
						{
							if(model_basducorps || (!model_basducorps && it->second < DEFAULT_NUMBER_POINTS))
							{
								int index = it->second * 3;
								for(int i = 0; i < 3; ++i)
								{
									measure(index + i) = joints_positions[it->first][i];
									if(model_vitesse)
									{
										measure((joints_indices.size() * 3) + index + i) = joints_speeds[it->first][i];
									}
								}
								message_pc.mesures.push_back(Utils::build_message_position(it->first, measure(index + 0), measure(index + 1), measure(index + 2), (bool)R(index, index)));
							}
						}

						filter->predict();
						filter->update(measure, RJ);
						// filter->constraints();

						VectorXf x = filter->get_states();
						for(int i = 0; i < states_count; ++i)
						{
							state.value[i] = x[i];
						}
						publisher_send<mesureKalman>(ROS_TOPIC_PC_KALMAN_TF, get_pointcloud(filter->get_measures(), joints_positions, message->matrices));
					}
				}
			}

		public:
			EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
			NodeKalman(int argc, char** argv):
			Node(ROS_NODE_KALMAN_NAME, ROS_NODE_KALMAN_RATE, argc, argv)
			{
				//subscriber_add<control_server_cmd>(ROS_TOPIC_RECORD, &NodeKalman::server_toggle);
				subscriber_add<kalmanState>(ROS_TOPIC_KALMAN_INIT, &NodeKalman::kalman_init);
				//subscriber_add<mesureKalman>(ROS_TOPIC_PC_FUSION_TF_CORRECTED, &NodeKalman::handle_fusion_corrected);
				subscriber_add<mesureKalman>(ROS_TOPIC_PC_FUSION_TF, &NodeKalman::handle_fusion);

				publisher_add<mesureKalman>(ROS_TOPIC_PC_KALMAN_TF);
			}

			void on_start()
			{
				json2model model = json2model(false);
				state.value.resize(states_count);
				for(vector<string>::iterator it = states_names.begin(); it != states_names.end(); ++it)
				{
					state.name.push_back(*it);
				}

				filter = new FilterEKF(model);

				ROS_INFO(MODEL_MESSAGE_KALMAN_START, states_count, measures_count, model_basducorps ? MESSAGE_TRUE : MESSAGE_FALSE, model_vitesse ? MESSAGE_TRUE : MESSAGE_FALSE);
			}

			void on_update()
			{

			}

			void on_end()
			{
				delete filter;
			}

			/*void action_on_toggle()
			{
				is_init = false;
			}*/
	};
}

int main(int argc, char** argv)
{
	return (prog_kinect::NodeKalman(argc, argv)).run();
}
