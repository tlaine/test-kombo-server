#include "../include/FilterLowpass.hpp"

namespace prog_kinect
{
	FilterLowpass::FilterLowpass(double x, double a)
	{
	    value_raw = x;
	    value_filtered = x;
	    is_initialized = false;
	    set_alpha(a);
	}

	double FilterLowpass::filter(double x)
	{
	    double result = 0.0;

	    if (is_initialized)
	    {
	        result = alpha * x + (1.0 - alpha) * value_filtered;
	    }
	    else
	    {
	        result = x;
	        is_initialized = true;
	    }

	    value_raw = x;
	    value_filtered = result;

	    return result;
	}

	double FilterLowpass::filter_alpha(double x, double a)
	{
	    set_alpha(a);
	    return filter(x);
	}

	bool FilterLowpass::get_is_initialized()
	{
	    return is_initialized;
	}

	double FilterLowpass::get_last()
	{
	    return value_raw;
	}

	void FilterLowpass::set_alpha(double a)
	{
	    if (a <= 0.0 || a >= 1.0)
	    {
	        alpha = DEFAULT_ALPHA;
	        except(MESSAGE_INVALID_ALPHA, a, alpha);
	    }
	    else
	    {
	        alpha = a;
	    }
	}
}
