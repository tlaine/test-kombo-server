#include "ros/ros.h"
#include "std_msgs/String.h"
#include <Eigen/Geometry>
#include <Eigen/Core>
#include <vector>
#include <numeric>
#include <fstream>

#include "../include/ekf.h"
#include "../include/eqGeometrique.h"

#include <kombos_msgs/orientation_msg.h>
#include <kombos_msgs/mlongueur.h>
#include <kombos_msgs/kalmanState.h>
#include <kombos_msgs/mesureKinect.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

static int dim_x = 44, dim_y = 68; //dim_x = 39, dim_y = 76;


static ekf filtre(dim_x,dim_y);
static std::map< std::string, float > joints;
static std::map< std::string, int > joint_idx;

static ros::Subscriber sub1;

static ros::Publisher* joint_markers_pub;
static visualization_msgs::Marker marker;
static visualization_msgs::Marker markerFrame;

static int k = 0;
const double toRad = M_PI/180.0;
const double toDeg = 180.0/M_PI;

EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

void createMarkerFrame(int i, visualization_msgs::MarkerArray* marker_array, Eigen::MatrixXd T)
{
	geometry_msgs::Point start;
	geometry_msgs::Point end;
	start.x = T(0,3);
	start.y = T(1,3);
	start.z = T(2,3);
	double lx, ly, lz;
	lx = sqrt(pow(T(0,0) - T(0,3),2)+pow(T(1,0) - T(1,3),2)+pow(T(2,0) - T(2,3),2));
	ly = sqrt(pow(T(0,1) - T(0,3),2)+pow(T(1,1) - T(1,3),2)+pow(T(2,1) - T(2,3),2));
	lz = sqrt(pow(T(0,2) - T(0,3),2)+pow(T(1,2) - T(1,3),2)+pow(T(2,2) - T(2,3),2));

	//ROS_INFO_STREAM(" P : \n" << P);
	// x
	markerFrame.id = 650 + (3*i + 1);
	markerFrame.header.stamp = ros::Time();
	markerFrame.color.r = 1.0;
	markerFrame.color.g = 1.0;
	markerFrame.color.b = 0.0;
	end.x = T(0,0)/3 + T(0,3);
	end.y = T(1,0)/3 + T(1,3);
	end.z = T(2,0)/3 + T(2,3);
	markerFrame.points.clear();
	markerFrame.points.push_back(start);
	markerFrame.points.push_back(end);
	marker_array->markers.push_back(markerFrame);

	// y
	markerFrame.id = 650 + (3*i + 2);
	markerFrame.header.stamp = ros::Time();
	markerFrame.color.r = 0.0;
	markerFrame.color.g = 1.0;
	markerFrame.color.b = 1.0;
	end.x = T(0,1)/3 + T(0,3);
	end.y = T(1,1)/3 + T(1,3);
	end.z = T(2,1)/3 + T(2,3);
	markerFrame.points.clear();
	markerFrame.points.push_back(start);
	markerFrame.points.push_back(end);
	marker_array->markers.push_back(markerFrame);

	// z
	markerFrame.id = 650 + (3*i + 3);
	markerFrame.header.stamp = ros::Time();
	markerFrame.color.r = 1.0;
	markerFrame.color.g = 0.0;
	markerFrame.color.b = 1.0;
	end.x = T(0,2)/3 + T(0,3);
	end.y = T(1,2)/3 + T(1,3);
	end.z = T(2,2)/3 + T(2,3);
	markerFrame.points.clear();
	markerFrame.points.push_back(start);
	markerFrame.points.push_back(end);
	marker_array->markers.push_back(markerFrame);
}


void callback(const kombos_msgs::kalmanState::ConstPtr & msg)
{
  unsigned int n = msg->name.size();
  for ( int i = 0; i<n; i++ )
  {
  	joints[msg->name[i]] = msg->value[i];
  }

  Eigen::VectorXd x(dim_x); Eigen::VectorXd th(dim_x-16); Eigen::VectorXd l(10);
	x << joints["SpineBaseX"],joints["SpineBaseY"],joints["SpineBaseZ"],joints["baseX"],joints["baseY"],joints["baseZ"],joints["abdomenX"],joints["abdomenZ"],joints["teteX"],joints["teteZ"],joints["rscapulaY"],joints["rscapulaZ"],joints["rhumerusX"],joints["rhumerusZ"],joints["rhumerusY"],joints["rradiusX"],joints["lscapulaY"],joints["lscapulaZ"],joints["lhumerusX"],joints["lhumerusZ"],joints["lhumerusY"],joints["lradiusX"],joints["rhipY"],joints["rhipZ"],joints["rfemurX"],joints["rfemurZ"],joints["rfemurY"],joints["rtibiaX"],joints["lhipY"],joints["lhipZ"],joints["lfemurX"],joints["lfemurZ"],joints["lfemurY"],joints["ltibiaX"],joints["abdomen"],joints["thorax"], joints["cou"], joints["tete"],joints["clavicule"],joints["humerus"],joints["radius"],joints["pelvis"],joints["femur"],joints["tibia"];
	th << joints["abdomenX"],joints["abdomenZ"],joints["teteX"],joints["teteZ"],joints["rscapulaY"],joints["rscapulaZ"],joints["rhumerusX"],joints["rhumerusZ"],joints["rhumerusY"],joints["rradiusX"],joints["lscapulaY"],joints["lscapulaZ"],joints["lhumerusX"],joints["lhumerusZ"],joints["lhumerusY"],joints["lradiusX"],joints["rhipY"],joints["rhipZ"],joints["rfemurX"],joints["rfemurZ"],joints["rfemurY"],joints["rtibiaX"],joints["lhipY"],joints["lhipZ"],joints["lfemurX"],joints["lfemurZ"],joints["lfemurY"],joints["ltibiaX"];
	l << joints["abdomen"],joints["thorax"], joints["cou"], joints["tete"],joints["clavicule"],joints["humerus"],joints["radius"],joints["pelvis"],joints["femur"],joints["tibia"];
	eqGeometrique eq = eqGeometrique(16);

	if(k==0)
	{
		// create Q & R
		Eigen::MatrixXd Q = Eigen::MatrixXd::Zero(dim_x, dim_x);       // process uncertainty
		Eigen::MatrixXd R = Eigen::MatrixXd::Identity(dim_y, dim_y);

		double valueQl = 1E-2;
		double valueQo = 1.0;
		double valueR = 1E-2;
		Eigen::Matrix4d babyR;
		babyR << valueR, 0, 0, 0,
		 0, valueR, 0, 0,
		 0, 0, valueR, 0,
		 0, 0, 0, 1;

		for(int i = 0; i < dim_x-10; i++)
		{
			if(i < dim_x-11) Q(i,i) = valueQo;
			else Q(i,i) = valueQl;
		}
		for(int i = 0; i < dim_y/4; i++)
		{
			R.block(i*4,i*4,4,4) << babyR;
		}

		// init objet ekf
		filtre.initialise(x, R, Q);
		k++;
	}

	Eigen::VectorXd h;
	h = filtre.Seth(x);
	visualization_msgs::MarkerArray marker_array;
	Eigen::MatrixXd T;

	int i = 0;
	for(std::map< std::string, int >::iterator iterator = joint_idx.begin(); iterator != joint_idx.end(); iterator++)
	{
		marker.text = iterator->first;
		marker.header.stamp = ros::Time::now();
		marker.id = 500+i;

		marker.color.r = 1.0;
		marker.color.g = 0.5;
		marker.color.b = 0;
		marker.color.a = 1;
		marker.header.frame_id = "kalman";

		marker.type= visualization_msgs::Marker::SPHERE;
		marker.scale.x = 0.05;
		marker.scale.y = 0.05;
		marker.scale.z = 0.05;
		marker.pose.position.x = h[(iterator->second*4)+0];
		marker.pose.position.y = h[(iterator->second*4)+1];
		marker.pose.position.z = h[(iterator->second*4)+2];

		marker_array.markers.push_back(marker);

		if(i<8)
		{
			T = eq.eqTransformee(i, joints["SpineBaseX"],joints["SpineBaseY"],joints["SpineBaseZ"],joints["baseX"],joints["baseY"],joints["baseZ"], th, l);
			createMarkerFrame(i, &marker_array, T);
		}
		i++;
	}
	joint_markers_pub->publish(marker_array);

}



int main(int argc, char **argv)
{
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  ros::init(argc, argv, "test_kalman");
  ros::NodeHandle nh;

  // sub
  sub1 = nh.subscribe("/kalmanState", 1, callback);

  // pub
  ros::Publisher joint_markers_pub_tmp = nh.advertise<visualization_msgs::MarkerArray>("/kalmanIN/Markers",0);
  joint_markers_pub = &joint_markers_pub_tmp;

  // Initiate MEASURE msg
	int i = 0;
	joint_idx["SpineBase"] = i; i++;
	joint_idx["SpineMid"] = i; i++;
	joint_idx["SpineShoulder"] = i; i++;
	joint_idx["Neck"] = i; i++;
	joint_idx["Head"] = i; i++;
	joint_idx["ShoulderRight"] = i; i++;
	joint_idx["ElbowRight"] = i; i++;
	joint_idx["WristRight"] = i; i++;
	joint_idx["ShoulderLeft"] = i; i++;
	joint_idx["ElbowLeft"] = i; i++;
	joint_idx["WristLeft"] = i; i++;
	joint_idx["HipRight"] = i; i++;
	joint_idx["KneeRight"] = i; i++;
	joint_idx["AnkleRight"] = i; i++;
	joint_idx["HipLeft"] = i; i++;
	joint_idx["KneeLeft"] = i; i++;
	joint_idx["AnkleLeft"] = i;

	//declaration des messages
  marker.ns = "joint_marker";
  marker.action = visualization_msgs::Marker::ADD;

  markerFrame.header.frame_id = "kalman";
  markerFrame.type = visualization_msgs::Marker::ARROW;
  markerFrame.pose.position.x = 0;
  markerFrame.pose.position.y = 0;
  markerFrame.pose.position.z = 0;
  markerFrame.pose.orientation.x = 0.0;
  markerFrame.pose.orientation.y = 0.0;
  markerFrame.pose.orientation.z = 0.0;
  markerFrame.pose.orientation.w = 1.0;
  markerFrame.scale.x = 0.05;
  markerFrame.scale.y = 0.05;
  markerFrame.scale.z = 0;
  markerFrame.color.a = 1.0;

  ros::Rate loop_rate(50) ;

  while ( ros::ok() )
  {

		ros::spinOnce();
		loop_rate.sleep() ;
	}

  return 0;
}
