#include <Eigen/Geometry>
#include <kombos_msgs/mesureKinect.h>
#include <kombos_msgs/mesureKalman.h>
#include <kombos_msgs/kalmanState.h>
#include "../include/Network.hpp"
#include "../include/Utils.hpp"
#include "../include/Node.hpp"

using namespace std;
using namespace kombos_msgs;
using namespace Eigen;

namespace prog_kinect
{
	class NodeInverseKinematic : public Node
	{
		private:
			bool is_init = true;
			map<int, int> joints_conf;
			map<int, int> states_to_th;
			map<int, int> states_to_l;

			void kinematic_init(const mesureKalman::ConstPtr& message)
			{
				publisher_send(ROS_TOPIC_KALMAN_INIT, kinematic_invert(message->data, true).kalman_state);
			}

			void kinematic_opti(const mesureKalman::ConstPtr& message)
			{
				/*double th01 = fmod((double)message->data.kalman_state.value[3],M_PI);
				double th02 = fmod((double)message->data.kalman_state.value[4],M_PI);
				double th03 = fmod((double)message->data.kalman_state.value[5],M_PI);*/

				mesureKalman message_corrected = *message;
				message_corrected.data = kinematic_invert(message->data, false);
				/*
				message_corrected.data.kalman_state.value[3] = th01;
				message_corrected.data.kalman_state.value[5] = th03;
				message_corrected.data.kalman_state.value[4] = th02;*/
				publisher_send(ROS_TOPIC_OC_THETA_TF, message_corrected);
			}

			void kinematic_kalman(const mesureKalman::ConstPtr& message)
			{
				float th01 = fmod((float)message->data.kalman_state.value[3],M_PI);
				float th02 = fmod((float)message->data.kalman_state.value[4],M_PI);
				float th03 = fmod((float)message->data.kalman_state.value[5],M_PI);

				mesureKalman message_corrected = *message;
				message_corrected.data = kinematic_invert(message->data, false);
				message_corrected.data.kalman_state.value[3] = th01;
				message_corrected.data.kalman_state.value[5] = th03;
				message_corrected.data.kalman_state.value[4] = th02;
				publisher_send(ROS_TOPIC_PC_CORRECTED_TF, message_corrected);
			}

			int sign(const float& x)
			{
				return (x < 0) ? -1 : 1;
			}

			vector<float> rot_to_vect(const MatrixXf& R)
			{
				vector<float> v;
				float c = 0.5*(R(0,0)+R(1,1)+R(2,2)-1);
        		float s = 0.5*sqrt(((R(2,1)-R(1,2)) * (R(2,1)-R(1,2)) +
        			                 (R(0,2)-R(2,0)) * (R(0,2)-R(2,0)) +
        			                 (R(1,0)-R(0,1)) * (R(1,0)-R(0,1))));
        		if( c == 1 || c == -1)
        		{
                	v = {0, 0, 0};
        		}
        		else
        		{
	                float ang = atan2(s, c);
	                float ux = sign(R(2,1)-R(1,2))*sqrt((R(0,0)-c)/(1-c));
	                float uy = sign(R(0,2)-R(2,0))*sqrt((R(1,1)-c)/(1-c));
	                float uz = sign(R(1,0)-R(0,1))*sqrt((R(2,2)-c)/(1-c));

	                vector<float> x = {abs(ux), abs(uy), abs(uz)};
	                int k = distance(x.begin(), max_element(x.begin(), x.end()));
	                if (k == 0)
	                {
	                	uy = (R(1,2)+R(2,1))/(2*ux*(1-c));
	                    uz = (R(3,1)+R(1,3))/(2*ux*(1-c));
	                }
	                else if (k == 1)
	                {
						ux = (R(1,2)+R(2,1))/(2*uy*(1-c));
	                    uz = (R(3,2)+R(2,3))/(2*uy*(1-c));
	                }

	                else
	                {
	                	ux = (R(3,1)+R(1,3))/(2*uz*(1-c));
	                	uy = (R(3,2)+R(2,3))/(2*uz*(1-c));
	                }
	                float norm_v = sqrt(ux*ux + uy*uy + uz*uz);
	                v = {ang*ux / norm_v, ang*uy / norm_v, ang*uz / norm_v};
	            }
	            return v;
			}

			mesureKinect kinematic_invert(const mesureKinect& message, const bool& is_init)
			{
				int message_mesures_count = message.mesures.size();

				mesureKinect mesure;
				// copy the non-modified elements
				mesure.mesures = message.mesures;
				mesure.vitesses = message.vitesses;

				kalmanState state;
				state.name = states_names;
				state.value.resize(is_init ? states_count + (message_mesures_count * 3) : states_count);

				map<string, Vector4f> joints;
				map<string, bool> fiabilites;
				for(int i = 0; i < message_mesures_count; i++)
				{
					position_msg joint = message.mesures[i];
					joints[joint.name] = Vector4f(joint.position.x, joint.position.y, joint.position.z, 1);
					fiabilites[joint.name] = joint.fiabilite;

					if(is_init)
					{
						state.name.push_back(joint.name + "X");
						state.name.push_back(joint.name + "Y");
						state.name.push_back(joint.name + "Z");

						int model_state_index = states_count + (i * 3);
						state.value[model_state_index + 0] = joint.position.x;
						state.value[model_state_index + 1] = joint.position.y;
						state.value[model_state_index + 2] = joint.position.z;
					}
				}

				VectorXf l = VectorXf::Zero((joints_count == 18) ? 21 : joints_count);
				VectorXf th = VectorXf::Zero((joints_count - 1) * 2);
				float th01 = 0;
				float th02 = 0;
				float th03 = 0;
				int theta = 0;
				for(int i = 0; i < joints_count; i++)
				{
					MatrixXf T;
					VectorXf P;

					Vector4f base = joints[JOINT_NAME_SPINEBASE];
					Vector4f mid = joints[JOINT_NAME_SPINEMID];
					Vector4f hip_right = joints[JOINT_NAME_HIP_RIGHT];
					Vector4f hip_left = joints[JOINT_NAME_HIP_LEFT];
					if(i == JOINT_ID_SPINEBASE)
					{
						state.value[0] = joints[JOINT_NAME_SPINEBASE][0];
						state.value[1] = joints[JOINT_NAME_SPINEBASE][1];
						state.value[2] = joints[JOINT_NAME_SPINEBASE][2];

						T = Utils::transform_base(base[0], base[1], base[2], hip_right[0], hip_right[1], hip_right[2], hip_left[0], hip_left[1], hip_left[2], mid[0], mid[1], mid[2], joints_count);
						vector<float> rotation = rot_to_vect(T);
						for (float r: rotation)
						{
							mesure.rotation_vectors.push_back(r);
						}
						// P = Utils::transform_origin(T);

						state.value[3] = th01;
						state.value[4] = th02;
						state.value[5] = th03;
					}
					else
					{
						VectorXf solution = Utils::transform_inv(i, base[0], base[1], base[2], th01, th02, th03, th, l) * joints[indices_joints[i]];

						bool is_hip = (i == JOINT_ID_HIPRIGHT || i == JOINT_ID_HIPLEFT);
						bool is_spinemid = (i == JOINT_ID_SPINEMID);
						bool is_shoulder = (i == JOINT_ID_SHOULDERRIGHT) || (i == JOINT_ID_SHOULDERLEFT);
						bool is_bottom = (i == JOINT_ID_KNEERIGHT || i == JOINT_ID_ANKLERIGHT || i == JOINT_ID_KNEELEFT || i == JOINT_ID_ANKLELEFT) || is_hip;
						bool is_mid = (i == JOINT_ID_SPINEMID || i == JOINT_ID_SPINESHOULDER || i == JOINT_ID_HEAD);
						bool is_right = (i == JOINT_ID_SHOULDERRIGHT || i == JOINT_ID_ELBOWRIGHT || i == JOINT_ID_WRISTRIGHT || i == JOINT_ID_HANDRIGHT || i == JOINT_ID_KNEERIGHT || i == JOINT_ID_ANKLERIGHT || i == JOINT_ID_HIPRIGHT);
						bool is_left = (i == JOINT_ID_SHOULDERLEFT || i == JOINT_ID_ELBOWLEFT || i == JOINT_ID_WRISTLEFT || i == JOINT_ID_HANDLEFT || i == JOINT_ID_KNEELEFT || i == JOINT_ID_ANKLELEFT || i == JOINT_ID_HIPLEFT);

						int offset = (is_bottom) ? ((is_right) ? 1 : 3) : -1;
						int pos = (is_bottom) ? 2 : 1;
						if(!is_hip)
						{
							if(is_mid || is_right || is_left)
							{
								l[i + offset] = Utils::distance(solution[0], solution[1], solution[2]);
							}

							if(!is_spinemid)
							{
								int i_th1 = theta++;
								int i_th2 = theta++;
								Utils::theta_l(joints_conf[i], i + offset, i_th1, i_th2, solution, l, th);
							}
						}
						else
						{
							if(is_right || is_left)
							{
								l[i + offset] = abs(solution[0]);
								l[i + offset - 1] = abs(solution[1]);
								l[i + offset - 2] = abs(solution[2]);
							}
						}

						T = Utils::transform(i, base[0], base[1], base[2], th01, th02, th03, th, l);
						vector<float> rotation = rot_to_vect(T);
						for (float r: rotation)
						{
							mesure.rotation_vectors.push_back(r);
						}
						// P = Utils::transform_origin(T);
					}
				}
				for(int i = 6; i < states_count; ++i)
				{
					if(states_to_th.count(i) > 0 && !isnan(th[states_to_th[i]]))
					{
						state.value[i] = th[states_to_th[i]];
						//ROS_WARN("STATE CORRESPONDANCE TH (%d, %d) => %s (%d) => %f (%d)", state.value.size(), th.size(), state.name[i].c_str(), i, Utils::rad2deg(th[states_to_th[i]]), states_to_th[i]);
					}
					else if(states_to_l.count(i) > 0 && !isnan(l[states_to_l[i]]))
					{
						state.value[i] = l[states_to_l[i]];
						//ROS_WARN("STATE CORRESPONDANCE L (%d, %d) => %s (%d) => %f (%d)", state.value.size(), th.size(), state.name[i].c_str(), i, l[states_to_l[i]], states_to_l[i]);
					}
				}
				mesure.kalman_state = state;
				return mesure;
			}

		public:
			NodeInverseKinematic(int argc, char** argv):
			Node(ROS_NODE_IK_NAME, ROS_NODE_IK_RATE, argc, argv)
			{
				subscriber_add<mesureKalman>(ROS_TOPIC_PC_FUSION_TF, &NodeInverseKinematic::kinematic_init);
				subscriber_add<mesureKalman>(ROS_TOPIC_PC_KALMAN_TF, &NodeInverseKinematic::kinematic_kalman);
				subscriber_add<mesureKalman>(ROS_TOPIC_OC_TF, &NodeInverseKinematic::kinematic_opti);

				publisher_add<kalmanState>(ROS_TOPIC_KALMAN_INIT);
				publisher_add<mesureKalman>(ROS_TOPIC_PC_CORRECTED_TF);
				publisher_add<mesureKalman>(ROS_TOPIC_OC_THETA_TF);
			}

			void on_start()
			{

				joints_conf =
				{
					{ JOINT_ID_SPINESHOULDER, 1},
					{ JOINT_ID_HEAD, 1},
					{ JOINT_ID_SHOULDERRIGHT, 6},
					{ JOINT_ID_ELBOWRIGHT, 4},
					{ JOINT_ID_WRISTRIGHT, 3},
					{ JOINT_ID_HANDRIGHT, 2},
					{ JOINT_ID_SHOULDERLEFT, 5},
					{ JOINT_ID_ELBOWLEFT, 4},
					{ JOINT_ID_WRISTLEFT, 3},
					{ JOINT_ID_HANDLEFT, 2},
					{ JOINT_ID_KNEERIGHT, 2},
					{ JOINT_ID_ANKLERIGHT, 3},
					{ JOINT_ID_KNEELEFT, 2},
					{ JOINT_ID_ANKLELEFT, 3}
				};

				states_to_th =
				{
					{6, 0},
					{7, 1},
					{8, 2},
					{9, 3},
					{10, 4},
					{11, 5},
					{12, 6},
					{13, 7},
					{14, 8},
					{15, 9},
					{16, 10},
					{17, 11},
					{18, 12},
					{19, 13},
					{20, 14},
					{21, 15},
					{22, 16},
					{23, 17},
					{24, 18},
					{25, 19},
					{33, 20},
					{34, 21},
					{35, 22},
					{36, 23},
					{37, 24},
					{38, 25},
					{39, 26},
					{40, 27}
				};
				states_to_l =
				{
					{26, 0},
					{27, 1},
					{28, 2},
					{29, 3},
					{30, 4},
					{31, 5},
					{32, 6},
					{41, 11},
					{42, 12},
					{43, 13},
					{44, 14},
					{45, 15}
				};
			}

			void on_update()
			{

			}

			void on_end()
			{

			}
		};
}

int main(int argc, char** argv)
{
	return (prog_kinect::NodeInverseKinematic(argc, argv)).run();
}
